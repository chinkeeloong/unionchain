USE [UnionChain]
GO
/****** Object:  StoredProcedure [dbo].[SP_Trading_Get_Match_PendingQueue]    Script Date: 11/9/2018 5:50:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_Trading_Get_Match_PendingQueue]
(
	@BUYER			NVARCHAR(50),
	@SELLER			NVARCHAR(50),
	@STATUS			NVARCHAR(50),
	@RATE			NVARCHAR(50),
	@STARTDATE		NVARCHAR(50),
	@ENDDATE		NVARCHAR(50),
	@viewPage		INT,
	@pages			INT OUTPUT,
	@ok				INT OUTPUT,
	@msg			VARCHAR(50) OUTPUT
)

AS

	SET NOCOUNT ON

	SET @ok = 0

	SET @pages = 0

	

	DECLARE @tableRows INT = 0
	SELECT @tableRows = CPARA_FLOATVALUE
	FROM CVD_PARAMETER
	WHERE CPARA_NAME = 'TableRows'

	

	--select the rows

	IF @BUYER != ''
	BEGIN
			SELECT @pages = COUNT(CTMP_ID)
			FROM CVD_TOKEN_MATCH_POINT TBQ
			WHERE CTMP_BUYER LIKE '%' + @BUYER + '%'
			AND TBQ.CTMP_STATUS NOT IN ('Auto Cancel','Cancel','Completed')
	END
	ELSE IF @SELLER != ''
	BEGIN
			SELECT @pages = COUNT(CTMP_ID)
			FROM CVD_TOKEN_MATCH_POINT TBQ
			WHERE CTMP_SELLER LIKE '%' + @SELLER + '%'
			AND TBQ.CTMP_STATUS NOT IN ('Auto Cancel','Cancel','Completed')
	END
	ELSE IF @STATUS != ''
	BEGIN  
			SELECT @pages = COUNT(CTMP_ID)
			FROM CVD_TOKEN_MATCH_POINT TBQ
			WHERE CTMP_STATUS = @STATUS
			AND TBQ.CTMP_STATUS NOT IN ('Auto Cancel','Cancel','Completed')
		
	END
	ELSE IF @STARTDATE != ''
	BEGIN
		IF @ENDDATE = ''
		BEGIN
			SET @ENDDATE = GETDATE()
		END

			SELECT @pages = COUNT(CTMP_ID)
			FROM CVD_TOKEN_MATCH_POINT TBQ
			WHERE CTMP_CREATEDON BETWEEN @STARTDATE AND @ENDDATE
			AND TBQ.CTMP_STATUS NOT IN ('Auto Cancel','Cancel','Completed')
	END
	ELSE
	BEGIN
			SELECT @pages = COUNT(CTMP_ID)
			FROM CVD_TOKEN_MATCH_POINT TBQ
			WHERE TBQ.CTMP_STATUS NOT IN ('Auto Cancel','Cancel','Completed')
	END




	

	--check if modulus, if it is 0, if not add another page
	DECLARE @mod INT = 0
	SET @mod = @pages % @tableRows
	SET @pages = @pages / @tableRows
	

	IF @mod <> 0
	BEGIN
		SET @pages = @pages + 1
	END
	
	IF @BUYER != ''
	BEGIN
		SELECT * FROM 
		(
			SELECT ROW_NUMBER() OVER (ORDER BY TBQ.CTMP_CREATEDON DESC) AS rownumber,  * 
			FROM CVD_TOKEN_MATCH_POINT AS TBQ
			WHERE TBQ.CTMP_BUYER LIKE '%' + @BUYER + '%'
			AND TBQ.CTMP_STATUS NOT IN ('Auto Cancel','Cancel','Completed')
		) AS FOO
		WHERE rownumber > ((@viewPage - 1) * @tableRows) AND rownumber < (((@viewPage - 1) * @tableRows) + @tableRows) + 1
	END
	ELSE IF @SELLER != ''
	BEGIN
		SELECT * FROM 
		(
			SELECT ROW_NUMBER() OVER (ORDER BY TBQ.CTMP_CREATEDON DESC) AS rownumber,  * 
			FROM CVD_TOKEN_MATCH_POINT AS TBQ
			WHERE TBQ.CTMP_SELLER LIKE '%' + @BUYER + '%'
			AND TBQ.CTMP_STATUS NOT IN ('Auto Cancel','Cancel','Completed')
		) AS FOO
		WHERE rownumber > ((@viewPage - 1) * @tableRows) AND rownumber < (((@viewPage - 1) * @tableRows) + @tableRows) + 1
	END
	ELSE IF @STATUS != ''
	BEGIN  
		SELECT * FROM 
		(
			SELECT ROW_NUMBER() OVER (ORDER BY TBQ.CTMP_CREATEDON DESC) AS rownumber,  * 
			FROM CVD_TOKEN_MATCH_POINT AS TBQ
			WHERE TBQ.CTMP_STATUS = @STATUS
			AND TBQ.CTMP_STATUS NOT IN ('Auto Cancel','Cancel','Completed')
		) AS FOO
		WHERE rownumber > ((@viewPage - 1) * @tableRows) AND rownumber < (((@viewPage - 1) * @tableRows) + @tableRows) + 1
	END
	ELSE IF @STARTDATE != ''
	BEGIN
		IF @ENDDATE = ''
		BEGIN
			SET @ENDDATE = GETDATE()
		END

		SELECT * FROM 
		(
			SELECT ROW_NUMBER() OVER (ORDER BY TBQ.CTMP_CREATEDON DESC) AS rownumber,  * 
			FROM CVD_TOKEN_MATCH_POINT AS TBQ
			WHERE TBQ.CTMP_CREATEDON BETWEEN @STARTDATE AND @ENDDATE
			AND TBQ.CTMP_STATUS NOT IN ('Auto Cancel','Cancel','Completed')
		) AS FOO
		WHERE rownumber > ((@viewPage - 1) * @tableRows) AND rownumber < (((@viewPage - 1) * @tableRows) + @tableRows) + 1
	END
	ELSE
	BEGIN
		SELECT * FROM 
		(
			SELECT ROW_NUMBER() OVER (ORDER BY TBQ.CTMP_CREATEDON DESC) AS rownumber,  * 
			FROM CVD_TOKEN_MATCH_POINT AS TBQ
			WHERE TBQ.CTMP_STATUS NOT IN ('Auto Cancel','Cancel','Completed')
		) AS FOO
		WHERE rownumber > ((@viewPage - 1) * @tableRows) AND rownumber < (((@viewPage - 1) * @tableRows) + @tableRows) + 1
	END

	

	SET @ok = 1

	SET @msg = 'Success'

	

	RETURN
