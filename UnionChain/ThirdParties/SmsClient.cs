﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace ECFBase.ThirdParties
{
    public class SmsClient : ISms
    {
        #region public

        public void SendMessage(string receiverPhoneNo, string message, string countryCode)
        {
            url = string.Format(url, this.vendor, this.key, receiverPhoneNo, message, countryCode);
            using (var client = new WebClient())
            {
                client.Encoding = System.Text.Encoding.UTF8;
                client.UploadString(url, string.Empty);
            }
        }

        #endregion

        #region private

        private readonly string vendor = "eti2u";
        private readonly string key = "4sObRRURYKarNGUKK8wDAcaO";

        // e.g: http://milkysms.net/websmsapi/ISendSMS.aspx?username=xxxx&password=xxxxx&message=xxxxxx&mobile=9999999999&Sender=xxxxxxx&type=1
        private string url = "http://milkysms.com/apis/sms/{0}/{1}/{2}/{3}/{4}/0";
        #endregion
    }
}
