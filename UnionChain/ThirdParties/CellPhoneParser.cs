﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECFBase.ThirdParties
{
    public static class CellPhoneParser
    {
        // temporary handle for malaysia phone number first
        public static string Parse(string countryCode, string phoneNumber)
        {
            if (countryCode == "my")
            {
                if (phoneNumber.StartsWith("01"))
                {
                    phoneNumber = "6" + phoneNumber;
                }
                else if (phoneNumber.StartsWith("1"))
                {
                    phoneNumber = "60" + phoneNumber;
                }
            }
            else if (countryCode == "id")
            {
                    phoneNumber = "62" + phoneNumber;
                
            }
            else if (countryCode == "cn")
            {
                            
                    phoneNumber = "85" + phoneNumber;
                
            }
            else if (countryCode == "th")
            {
              
                    phoneNumber = "66" + phoneNumber;
                
            }
            else if (countryCode == "ph")
            {
                
                    phoneNumber = "63" + phoneNumber;
                
            }
            else if (countryCode == "vn")
            {
               
                    phoneNumber = "84" + phoneNumber;
                
            }
            else if (countryCode == "sg")
            {
               
              
                    phoneNumber = "65" + phoneNumber;
                
            }
            else if (countryCode == "bn")
            {


                phoneNumber = "673" + phoneNumber;

            }
            else if (countryCode == "hk")
            {


                phoneNumber = "852" + phoneNumber;

            }
            else if (countryCode == "us")
            {


                phoneNumber = "1" + phoneNumber;

            }
            else if (countryCode == "jp")
            {


                phoneNumber = "81" + phoneNumber;

            }

            return phoneNumber;
        }
    }
}