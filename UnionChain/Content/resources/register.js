$(document).ready(function () {
    $("#basicForm").validate({
        onsubmit: true, //在提交时验证
        onfocusout: false, //在得到焦点时是否验证
        onkeyup: false, //在键盘弹起时验证
        rules: {
            username: {
                required: true,
                minlength: 4,
                maxlength: 20,
                checkUserName: true,
                remote: {
                    type: "POST",
                    url: "/user/register/ajax_succ/username", //请求地址  
                    data: {
                        username: function () {
                            return $("#username").val();
                        }
                    }
                }
            },
            truename: {
                required: true,
                maxlength: 10
            },
            referee: {
                required: true,
                remote: {
                    type: "POST",
                    url: "/user/register/ajax_referee/referee", //请求地址  
                    data: {
                        referee: function () {
                            return $("#referee").val();
                        }
                    }
                }
            },

            _referee: {
                required: true,
                remote: {
                    type: "POST",
                    url: "/user/register/ajax_refer/_referee", //请求地址  
                    data: {
                        _referee: function () {
                            return $("#_referee").val();
                        }
                    }
                }
            },

            password: {
                required: true,
                minlength: 6,
                maxlength: 32
            },
            repass: {
                required: true,
                minlength: 6,
                maxlength: 32
            },
            userphone: {
                required: true,
                checkUserPhone: true,
            },
            msalt: {
                required: true,
            },
            captcha: {
                required: true,
                minlength: 4,
                maxlength: 4
            },
            email: {
                required: true,
                checkEmail: true
            }
        },
        messages: {
            username: {
                required: getLang("请输入用户名"),
                minlength: getLang("用户名长度4-20个字符"),
                maxlength: getLang("用户名长度4-20个字符"),
                remote: getLang("用户名已注册"),
            },
            
            userphone: {
                required: getLang("请输入您的手机号码"),
            },
            truename: {
                required: getLang("请填写会员姓名"),
                maxlength: getLang("会员姓名长度为10个字符")
            },

            referee: {
                required: getLang("请输入推荐人账号"),
                remote: getLang("推荐人不存在")
            },
            _referee: {
                required: getLang("请输入接点人账号"),
                remote: getLang("接点人不存在")
            },

            repass: {
                required: getLang("请输入安全密码"),
                minlength: getLang("至少6位的数字、字母组合！"),
                maxlength: getLang("密码长度不能大于 32个字符"),
            },

            password: {
                required: getLang("请输入密码"),
                minlength: getLang("至少6位的数字、字母组合！"),
                maxlength: getLang("密码长度不能大于 32个字符"),
            },
            msalt: {
                required: getLang("请输入手机验证码"),
            },
            captcha: {
                required: getLang("请输入图形验证码"),
                minlength: getLang("图形验证码长度不能小于 4个字符"),
                maxlength: getLang("图形验证码长度不能大于 4个字符"),
            },
            email: {
                required: getLang("请输入邮箱")
            }
        },
        errorPlacement: function (error, element) {
            if ($(element).next("div").hasClass("tooltip")) {
                if (element.attr("name") == "captcha") {
                    $(element).attr("data-original-title", $(error).text()).tooltip({placement: "bottom"});
                }
                $(element).attr("data-original-title", $(error).text()).tooltip("show");
            } else {
                if (element.attr("name") == "captcha") {
                    $(element).attr("title", $(error).text()).tooltip({placement: "bottom"});
                }
                $(element).attr("title", $(error).text()).tooltip("show");
            }
        },
        submitHandler: function (form) { //验证成功时调用
            form.submit();
        },
    })
    $.validator.addMethod("checkUserName", function (value, element, params) {
        var checkUserName = /^[a-zA-Z]\w{4,20}$/;
        return this.optional(element) || (checkUserName.test(value));
    }, getLang("仅可使用字母、数字、下划线,且不能以数字开头！"));
    $.validator.addMethod("checkUserPhone", function (value, element, params) {
        // alert("+"+$('#user_type').val()+value);
        if($('#user_type').val()==86){
            var checkUserPhone = /(?:\(?[0\+]?\d{1,3}\)?)[\s-]?(?:0|\d{1,4})[\s-]?(?:(?:13\d{9})|(?:\d{7,8}))/;
        }else{
            var checkUserPhone = /^[0-9]{1,}$/;
        }
        
        return this.optional(element) || (checkUserPhone.test(value));
    }, getLang("手机号码格式错误！"));
    $.validator.addMethod("checkEmail", function (value, element, params) {
        var checkUserName = /\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/;
        return this.optional(element) || (checkUserName.test(value));
    }, getLang("请输入正确的邮箱地址！"));
});


var time = 60;
function send_sms(obj, send_type) {
    if (sendvalidate()) {
        sendSms($('input[name=captcha]').val(), $('input[name=userphone]').val(), $('#user_type').val(), $('#email').val(), send_type);
    }

}


function sendvalidate() {
    var _boolsendvalidate = true;
    //获取表单数据
    var username = $('input[name=username]').val();
    var truename = $('input[name=truename]').val();
    var referee = $('input[name=referee]').val();
    var _referee = $('input[name=_referee]').val();
    var password = $('input[name=password]').val();
    var repass = $('input[name=repass]').val();
    var captcha = $('input[name=captcha]').val();
    var userphone = $('input[name=userphone]').val();
    var user_type = $('#user_type').val();
    var email = $('#email').val();

    if (username.length <= 0 || truename.length <= 0 || referee.length <= 0 || _referee.length <= 0 || password.length <= 0 || repass.length <= 0 || email.length <= 0 || userphone.length <= 0)
    {
        var _strShowMessage = getLang("请先填写基本信息");
        if ($("#getmsalt").next("div").hasClass("tooltip")) {
            $("#getmsalt").attr("data-original-title", _strShowMessage).tooltip("show");
        } else {
            $("#getmsalt").attr("title", _strShowMessage).tooltip("show");
        }
        return false;
    }
    if (captcha.length <= 0)
    {
        var _strShowMessage = getLang("请先填写图片验证码");
        if ($("#getmsalt").next("div").hasClass("tooltip")) {
            $("#getmsalt").attr("data-original-title", _strShowMessage).tooltip("show");
        } else {
            $("#getmsalt").attr("title", _strShowMessage).tooltip("show");
        }
        return false;
    }
    if (userphone == '') {
        var _strShowMessage = getLang("请输入您的手机号码");
        if ($("#getmsalt").next("div").hasClass("tooltip")) {
            $("#getmsalt").attr("data-original-title", _strShowMessage).tooltip("show");
        } else {
            $("#getmsalt").attr("title", _strShowMessage).tooltip("show");
        }
        return false;
    }

    return true;
}


function countdown(){
    time--;
    if(time >0){
        $("#getmsalt").attr('disabled',true);
        $('#getmsalt').text(getLang('重新获取')+time+")");

    }else{
        clearInterval(clock);
        $("#getmsalt").text(getLang('获取验证码'));
        $("#getmsalt").attr('disabled',false);
        time = 60;
    }
}

function sendSms(__captcha, __userphone, __user_type, email, send_type) {
    //组装发送数据data
    if (__user_type == '86') {
        var data = {'captcha': __captcha, 'userphone': __userphone, 'user_type': 86, 'email':email, 'send_type': send_type};
    } else {
        var data = {'captcha': __captcha, 'userphone': __user_type + __userphone, 'user_type': __user_type, 'email':email, 'send_type': send_type};
    }

    //发送短信验证码
    $.ajax({
        url: '/system/find/smsSendForReg',
        type: 'post',
        data: data,
        datatype: 'json',
        success: function (data) {
            var daTas = JSON.parse(data);
            var _strShowMessage = "";
            if (daTas['errorCode'] == -1) {
                _strShowMessage = getLang('图片验证码错误!');
                $(".captcha-img").click();
            } else if (daTas['errorCode'] == -2) {
                _strShowMessage = getLang('发送失败!');
                $(".captcha-img").click();
            } else {
                _strShowMessage = getLang('发送成功!');
                clock = setInterval(countdown, 1000);
            }

            if ($("#getmsalt").next("div").hasClass("tooltip")) {
                $("#getmsalt").attr("data-original-title", _strShowMessage).tooltip("show");
            } else {
                $("#getmsalt").attr("title", _strShowMessage).tooltip("show");
            }
        }
    });
}
//选择发送验证码
function appeal(obj,op) {
    bootbox.dialog({
        title: getLang("请选择验证码发送类型"),
        message:'<div class=""> ' +
        '<div class="col-md-12"> ' +
        '<form class=""> ' +
        '<div class="" style="margin-top: 15px;margin-left: 50px;width: auto display: -webkit-box;display: -ms-flexbox;display: -webkit-flex; display: flex; justify-content: center;align-items: center;"> ' +
        '<div style="flex:1;">'+
        '<b><font color="#6495ed">'+getLang('手机发送')+'：</font></b> ' +
        '<input id="id1" name="type" type="radio"   value="1" onclick="checkSend(1)" style="vertical-align: top">' +
        '</div> ' +
        '<div style="flex:1;">'+
        '<b><font color="#ff7f50">'+getLang('邮件发送')+'：</font></b> ' +
        '<input id="id1" name="type" type="radio"   value="2" onclick="checkSend(2)" style="vertical-align: top">' +
        '</div> ' +
        '</div> ' +
        '<div class="form-group"> '+
        '<div class="col-md-8"> ' +
        '                                       <div class="form-block"> </div>' +
        '</div> ' +
        '                       </div>' +
        '</form> ' +
        '           </div> ' +
        '       </div>',
    });
}

/*
 * 1是手机
 * 2是邮件
 */
function checkSend(type){
    //手机发送短信
    if(type==1){
        send_sms('','1');
    }else{
        send_sms('','2');
    }

    //执行完毕关闭窗口
    bootbox.hideAll();
}