function clickject(obj) {//无限级AJAX
    var _this = $(obj);
    var _floor = parseInt(_this.attr('floor'));
    var username = _this.attr('username');
    var classname = _this.attr('class');
    if (classname.indexOf('have_display') != -1) {
        _this.parent().next().hide();
        _this.removeClass(classname).addClass(classname.replace("have_display", "have"));
    } else {
        if (_this.attr('data') == '1') {
            _this.parent().next().show();
        } else {
            host = window.location.host;
            $.getJSON("http://" + host + "/user/referee/childrentree/username/" + username, function (res) {
                var div = _this.attr("yesend") == '1' ? '<div class="sub_0">' : '<div class="sub" style="padding-left: 20px;">';
                for (var i = 0; i < res.length; i++) {
                    var yesend = res.length - 1 > i ? "2" : "1";
                    div += '<div class="node"><div class="title">';
                    if (res[i]._referee != '') {
                        div += '<div class="click have_' + yesend + '" yesend="' + yesend + '" onClick="clickject(this);" floor="' + (_floor + 1) + '" username="' + res[i].username + '"></div>';
                    } else {
                        div += '<div class="no_' + yesend + '" yesend="' + yesend + '" floor="' + (_floor + 1) + '" username="' + res[i].username + '"></div>';
                    }
                    div += '<span>[' + (_floor + 1) + '][' + res[i].username + ']['+getLang('普通用户')+']';
                    div += '</span>';
                    div += '</div></div>';
                }
                div += "</div>";
                _this.parent().parent().append(div);
                _this.attr('data', '1');
            });
        }
        _this.removeClass(classname).addClass(classname.replace("have", "have_display"));
    }
}