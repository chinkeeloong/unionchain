$(document).ready(function () {
    $("#buy_form").validate({
        onsubmit: true, //在提交时验证
        onfocusout: false, //在得到焦点时是否验证
        onkeyup: false, //在键盘弹起时验证
        rules: {
            buy_num: {
                required: true,
                checkBuy_num: true

            },
            buy_pay_pass: {
                required: true,
                remote: {
                    type: "POST",
                    url: "/user/trade/ajax_buy_pay_pass/buy_pay_pass", //请求地址
                    data: {
                        buy_pay_pass: function () {
                            return $("#buy_pay_pass").val();
                        }
                    }
                }
            },
            buy_code: {
                required: true,
                remote: {
                    type: "POST",
                    url: "/user/trade/ajax_buy_code/buy_code", //验证手机验证码
                    data: {
                        buy_code: function () {
                            return $("#buy_code").val();
                        }
                    }
                }
            }

        },
        messages: {
            buy_num: {
                required: getLang("请输入交易数量"),
            },
            buy_pay_pass: {
                required: getLang("请输入交易密码"),
                remote: getLang("交易密码不正确")
            },
            buy_code: {
                required: getLang("请输入手机验证码密码"),
                remote: getLang("手机验证码不正确")
            }
        },
        errorPlacement: function (error, element) {
            if ($(element).next("div").hasClass("tooltip")) {
                if (element.attr("name") == "captcha") {
                    $(element).attr("data-original-title", $(error).text()).tooltip({placement: "bottom"});
                }
                $(element).attr("data-original-title", $(error).text()).tooltip("show");
            } else {
                if (element.attr("name") == "captcha") {
                    $(element).attr("title", $(error).text()).tooltip({placement: "bottom"});
                }
                $(element).attr("title", $(error).text()).tooltip("show");
            }
        },
        submitHandler: function (form) { //验证成功时调用
            form.submit();
        }

    });
    $.validator.addMethod("checkBuy_num", function (value, element, params) {
        var checkBuy_num = /^(?:0|\d*[50]+0)$/;
        return this.optional(element) || (checkBuy_num.test(value));
    }, getLang("买入数量最大额不能超过30000且是50的倍数"));


    $("#sell_form").validate({
        onsubmit: true, //在提交时验证
        onfocusout: false, //在得到焦点时是否验证
        onkeyup: false, //在键盘弹起时验证
        rules: {
            sell_num: {
                required: true,
                checkSell_num: true

            },
            sell_pay_pass: {
                required: true,
                remote: {
                    type: "POST",
                    url: "/user/trade/ajax_sell_pay_pass/sell_pay_pass", //请求地址
                    data: {
                        sell_pay_pass: function () {
                            return $("#sell_pay_pass").val();
                        }
                    }
                }
            },
            sell_code: {
                required: true,
                remote: {
                    type: "POST",
                    url: "/user/trade/ajax_sell_code/buy_code", //验证手机验证码
                    data: {
                        sell_code: function () {
                            return $("#sell_code").val();
                        }
                    }
                }
            }

        },
        messages: {
            sell_num: {
                required: getLang("请输入交易数量")
            },
            sell_pay_pass: {
                required: getLang("请输入交易密码"),
                remote: getLang("交易密码不正确")
            },
            sell_code: {
                required: getLang("请输入手机验证码密码"),
                remote: getLang("手机验证码不正确")
            }
        },
        errorPlacement: function (error, element) {
            if ($(element).next("div").hasClass("tooltip")) {
                if (element.attr("name") == "captcha") {
                    $(element).attr("data-original-title", $(error).text()).tooltip({placement: "bottom"});
                }
                $(element).attr("data-original-title", $(error).text()).tooltip("show");
            } else {
                if (element.attr("name") == "captcha") {
                    $(element).attr("title", $(error).text()).tooltip({placement: "bottom"});
                }
                $(element).attr("title", $(error).text()).tooltip("show");
            }
        },
        submitHandler: function (form) { //验证成功时调用
            form.submit();
        }

    });
    $.validator.addMethod("checkSell_num", function (value, element, params) {
        var checkSell_num = /^(?:0|\d*[50]+0)$/;
        return this.optional(element) || (checkSell_num.test(value));
    }, getLang("卖出数量最大额不能超过5000且是50的倍数"));
})