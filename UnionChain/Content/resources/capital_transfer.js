﻿$(document).ready(function(){
    $("#opcardbutton").click(function(){
        var howmoney = parseFloat($("#howmoney").val());
        var mymoney  = parseFloat($("#mymoney").val());
        var  type    = $("#types").val();
        if($("#baoress").val()==''||$("#baoress").val()=='U宝地址'){
            boot("对不起，请先输入U宝地址");return false;
        }
        if($("#howmoney").val()==''){
            boot("对不起，请输入转账数量");return false;
        }
        if($("#repass").val()==''){
            boot("对不起，请输入安全密码");return false;
        }
        if($("#msalt").val()=='' &&  type !=='期权'){
            boot("对不起，请输入手机验证码");return false;
        }
        if(isNaN(howmoney)){
            boot("对不起，转账数量必须是数字");return false;
        }
        if(howmoney<10){
            boot("转账数量不能小于10");return false;
        }
        if(mymoney<howmoney &&  type !=='期权'){
            boot("对不起，可转账数量不足");return false;
        }

        if($("#captcha").val()==''){
            boot("对不起，请输入验证码");return false;
        }


        // if(post){
        //     showhandle({
        //         html:'<table class="lotab"><tr><th><label>安全密码：</label></th><td><input id="repass" class="myinput repass" type="password"></td></tr></table>',
        //         width:330,
        //         height:136,
        //         id:'atmmymoney',
        //         title:'现金转账'
        //     },function(){
        //         if(_boolSubmit){
        //             _boolSubmit = false;
        //             $("#controlLoad").show();
        //             removetip("atmmymoney");
        //             $.getJSON(get_path_url("/user/profile/capitals/"+$("#mymone").val()+"&baoress="+encodeURIComponent($("#baoress").val())+"&howmoney="+howmoney+"&repass="+$("#repass").val()+"&content="+encodeURIComponent($("#content").val())),function(res){
        //                 $("#controlLoad").hide();
        //                 if(res.error=='0'){
        //                     hidebox('atmmymoney',true);
        //                     Right('U宝转账成功。',{},function(){
        //                         location.href = res.url;
        //                     });
        //                 }else{
        //                     _boolSubmit = true;
        //                     addtip("atmmymoney",res.error);
        //                 }
        //             });
        //         }
        //     });
        // }
    });

    $("#opcardbuttonreg").click(function(){
        var post = true;
        var howmoney = parseFloat($("#howmoney").val());
        var mymoney  = parseFloat($("#mymoney").val());
        if($("#username").val()==''||$("#username").val()=='用户名/电子邮箱/绑定手机'){
            post = false;
        }
        if($("#usernametip").html().indexOf('对不起') != -1){
            post = false;
        }
        if($("#howmoney").val()==''){
            post = false;
        }else if(!isNum(howmoney)){
            post = false;
        }else if(howmoney<5){
            post = false;
        }else if(mymoney<howmoney){
            post = false;
        }
        if(post){
            showhandle({
                html:'<table class="lotab"><tr><th><label>安全密码：</label></th><td><input id="repass" class="myinput repass" type="password"></td></tr></table>',
                width:320,
                height:136,
                id:'atmmymoney',
                title:'报单币转账'
            },function(){
                $("#controlLoad").show();
                removetip("atmmymoney");
                $.getJSON(get_path_url("?mod=member&act=capital&type=transfer&method=regmoney&username="+encodeURIComponent($("#username").val())+"&money="+howmoney+"&repass="+$("#repass").val()+"&content="+encodeURIComponent($("#content").val())),function(res){
                    $("#controlLoad").hide();
                    if(res.error=='0'){
                        hidebox('atmmymoney',true);
                        Right('报单币转账成功。',{},function(){
                            location.href = res.url;
                        });
                    }else{
                        addtip("atmmymoney",res.error);
                    }
                });
            });
        }
    });

    $("#opcardbutton2").click(function(){
        var post = true;
        var howmoney = parseFloat($("#howmoney").val());
        var mymoney = parseFloat($("#mymoney").val());
        if($("#username").val()==''||$("#username").val()=='用户名/电子邮箱/绑定手机'){
            addtip("username","请输入用户名/电子邮箱/绑定手机");
            post = false;
        }
        if($("#usernametip").html().indexOf('对不起') != -1){
            post = false;
        }
        if($("#howmoney").val()==''){
            addtip("howmoney","对不起，请输入转账数量");
            post = false;
        }else if(!isNum(howmoney)){
            addtip("howmoney","对不起，转账数量必须是数字");
            post = false;
        }else if(howmoney<50){
            addtip("howmoney","转账数量不能小于50");
            post = false;
        }else if(mymoney<howmoney){
            addtip("howmoney","对不起，可转账数量不足");
            post = false;
        }else{
            yestip("howmoney","");
        }
        if(post){
            showhandle({
                html:'<table class="lotab"><tr><th><label>安全密码：</label></th><td><input id="repass" class="myinput repass" type="password"></td></tr></table>',
                width:320,
                height:136,
                id:'atmmymoney',
                title:'副卡转账'
            },function(){
                $("#controlLoad").show();
                removetip("atmmymoney");
                $.getJSON(get_path_url("?mod=member&act=capital&type=transfer&method=balance&username="+encodeURIComponent($("#username").val())+"&money="+howmoney+"&repass="+$("#repass").val()+"&content="+encodeURIComponent($("#content").val())),function(res){
                    $("#controlLoad").hide();
                    if(res.error=='0'){
                        hidebox('atmmymoney',true);
                        Right('副卡转账成功。',{},function(){
                            location.href = res.url;
                        });
                    }else{
                        addtip("atmmymoney",res.error);
                    }
                });
            });
        }
    });

});

//定义提示窗口
function boot(str){
    str = getLang(str); //多语言
    bootbox.alert(str, function(){
        $.niftyNoty({
            type: 'info',
            icon : 'pli-exclamation icon-2x',
            message : str,
            container : 'floating',
            timer : 2000
        });
    });
}

//定义提示窗口
function boots_s(str){
    str = getLang(str); //多语言
    bootbox.alert(str, function(){
    });
}

function checkmoney(){
    var howmoney = parseFloat($("#howmoney").val());
    var mymoney  = parseFloat($("#mymoney").val());
    if($("#howmoney").val()==''){
        boot('对不起，请输入转账数量');return false;
    }else if(isNaN(howmoney)){
        boot('对不起，转账数量必须是数字');return false;
    }else if(howmoney<10){
        boot('转账数量不能小于10');return false;
    }else if(mymoney<howmoney){
        boot('对不起，可转账数量不足');return false;
    }
}

function checkaamoney(){
    var howmoney = parseFloat($("#howmoney").val());
    var mymoney  = parseFloat($("#mymoney").val());
    if($("#howmoney").val()==''){
        boot('对不起，请输入转账数量');return false;
    }else if(isNaN(howmoney)){
        boot('对不起，转账数量必须是数字');return false;
    }
}

function checkbaoress(){

    var show = true,baoress = $("#baoress").val();
    if(baoress=='U宝地址'){
        baoress='';
        show = false;
    }
    if(baoress == ''&& show){
        boot("对不起，请输入U宝地址!");return false;
    }
    // else if(show) {
    //     $.ajax({
    //         url: '/user/profile/capital/res/' + baoress,
    //         type: 'post',
    //         success: function (data) {
    //             var codes = JSON.parse(data);
    //             // if (codes['code'] == false) {
    //             //     alert('对不起，您输入的U宝地址不存在');return false;
    //             // }
    //         },
    //         error: function () {
    //
    //         }
    //     });
    // }
}
        
function checkrepass(){
    var show = true,repass = $("#repass").val();
    if(repass =='安全密码'){
        repass ='';
        show = false;
    }
    if(repass ==''&& show){
        boot('对不起，请输入安全密码');return false;
    }else if(show){
        $.ajax({
            url:'/user/profile/capital/repass/'+repass,
            type:'post',
            success:function(data){
                console.log(data);
                var codes =JSON.parse(data);
                if(codes['code'] == false){
                    boot('对不起，您输入的安全密码不正确');return false;
                }
            },
            error:function(){
            }
        });
    }
}


function checkmsalt(){

    var msalt = $("#msalt").val();
    if(msalt ==''){
        boot( "请输入手机验证码");
    }
}


function check()
{
   // $.getScript(appdir + "/placeholder.js");
    var post = true;
    var howmoney = parseFloat($("#howmoney").val());
    var mymoney = parseFloat($("#mymoney").val());

    if($("#baoress").val()==''||$("#baoress").val()=='U宝地址'){
        addtip("baoress","对不起，请输入U宝地址");
        return false;
    }
    if($("#baoresstip").html().indexOf('对不起') != -1){
        post = false;
    }
    if($("#howmoney").val()==''){
        addtip("howmoney","对不起，请输入转账数量");
        post = false;
        return false;
    }else if(!isNum(howmoney)){
        addtip("howmoney","对不起，转账数量必须是数字");
        post = false;
        return false;
    }else if(howmoney<10){
        addtip("howmoney","转账数量不能小于10");
        post = false;
        return false;
    }else if(mymoney<howmoney){
        addtip("howmoney","对不起，可转账数量不足");
        post = false;
        return false;
    }else if($("#repass").val()==''){
        addtip("repass","对不起，请输入安全密码");
    }else if($("#msalt").val()==''){
        addtip("msalt", "请输入手机验证码");
        return false;
    }else if($("#msalt").val()!==''){
        addtip("msalt", "");
    }else{
        yestip("howmoney","");
    }
    $('input[name=opcardbutton]').attr('disabled','disabled');
}


//function checkusername(){
//    var show = true,username = $("#username").val();
//
//    if(username=='用户名/电子邮箱/绑定手机'){
//        username='';
//        show = false;
//    }
//
//    if(username==''&&show){
//        addtip("username","请输入用户名/电子邮箱/绑定手机");
//    }else if(show){
//        $.getJSON(get_url("act=verifyusername&transfer=1&username="+encodeURIComponent(username)),function(res){
//            if(res.error=='0'){
//                yestip("username",res.truename);
//            }else{
//                addtip("username",res.error);
//            }
//        });
//    }
//}


function checktranmoney(){
    var howmoney = ($("#howmoney").val());
    var mymoney  = parseFloat($("#mymoney").val());
    if($("#howmoney").val() == ''){
        addtip("howmoney","对不起，请输入转账数量");
    }else if(!isNum(howmoney)){
        addtip("howmoney","对不起，转账数量必须是数字");
    }else if(howmoney%50 != 0){
        addtip("howmoney","交易数量必须是50整数倍");
    }else if(mymoney<howmoney){
        addtip("howmoney","对不起，可转账数量不足");
    }else if(howmoney < 0){
        addtip("howmoney","交易数量必须大于0");
    }else{
        yestip("howmoney","");
    }
}
  function checktran()
  {
     // $.getScript(appdir + "/placeholder.js");
      var post = true;
      var howmoney = $("#howmoney").val();
      var mymoney = parseFloat($("#mymoney").val());
      var msalt = $("#msalt").val();
      var repass = $("#repass").val();
      if(howmoney==''){
          addtip("howmoney","对不起，请输入U宝数量");
          post = false;
          return false;
      }else if(!isNum(howmoney)){
          addtip("howmoney","对不起，转账数量必须是数字");
          post = false;
          return false;
      }else if(howmoney<0){
          addtip("howmoney","对不起，转账数量必须大于0");
          post = false;
          return false;
      }else if(!repass){
          addtip("repass","对不起，请输入安全密码");
          post = false;
          return false;
      }else if(mymoney<howmoney){
          addtip("howmoney","对不起，可转账数量不足");
          post = false;
          return false;
      }else if(msalt==''){
          addtip("msalt", "请输入手机验证码");
          post = false;
          return false;
      }else{
          yestip("howmoney","");
      }
      $('input[name=opcardbutton]').attr('disabled','disabled');
  }
