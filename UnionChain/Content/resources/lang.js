function getCookie(name)
{
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
    if(arr=document.cookie.match(reg))
    return unescape(arr[2]);
    else
    return null;
}
var lang = getCookie('think_var');
if(lang==undefined){
    lang = 'zh-CN';
}else if(lang.indexOf('en')>-1){
    lang = 'en-US';
}
$.ajaxSettings.async = false; 
var langJson = '';
var timestamp = Date.parse(new Date());
$.getJSON("/app/lang/json/"+lang+".json?"+timestamp,function(data){ 
    console.log(lang+"多语言获取成功");
    langJson = data;
});
function getLang(str){
    if(langJson[str] == undefined){
        return str;
    }else{
        return langJson[str];
    }
}