	  var touch = false;
	  /* DETECT PLATFORM */
	  handleDetectPlatform();
	  
	  /* Handle Animate */
	  handleAnimate();
	  
	  $(".scroll-to-top").click(function (e) {    
      $('body,html').animate({scrollTop:0},'800','swing' );
	});

	/* Handle detect platform */
	function handleDetectPlatform(){
	  /* DETECT PLATFORM */
	  $.support.touch = 'ontouchend' in document;
	  
	  if ($.support.touch) {
		touch = true;
		$('body').addClass('touch');
		clickEv = 'touchstart';
	  }
	  else{
		$('body').addClass('notouch');
		if (navigator.appVersion.indexOf("Mac")!=-1){
		  if (navigator.userAgent.indexOf("Safari") > -1){
			$('body').addClass('macos');
		  }
		  else if (navigator.userAgent.indexOf("Chrome") > -1){
			$('body').addClass('macos-chrome');
		  }
			else if (navigator.userAgent.indexOf("Mozilla") > -1){
			  $('body').addClass('macos-mozilla');
			}
		}
	  }
	}
	
	function handleAnimate() {
	
	  if(touch == false){
		$('[data-animate]').each(function(){
		 
		  var $toAnimateElement = $(this);
		  
		  var toAnimateDelay = $(this).attr('data-delay');
		  
		  var toAnimateDelayTime = 0;
		  
		  if( toAnimateDelay ) { toAnimateDelayTime = Number( toAnimateDelay ); } else { toAnimateDelayTime = 150; }
		  
		  if( !$toAnimateElement.hasClass('animated') ) {
			
			$toAnimateElement.addClass('not-animated');
			
		  }
		});
	  }
	  
	}
	 
    function checkScroll() {
		
	   var $win = $(window);
	 
	   var heightHeader = $(window).height();
		 
		  var scroll = $win.scrollTop()+$(window).height() ;
		
	    $('.not-animated').each(function(){
			
		  var t = $(this).offset().top;
	
		  var $toAnimateElement = $(this);
		  
		  var toAnimateDelay = $(this).attr('data-delay');
		  
		  var toAnimateDelayTime = 0;
		  
		  //if (scroll > (t+($(this).innerHeight()/3*4))) {
		if (scroll > t) {	
			  if( toAnimateDelay ) { toAnimateDelayTime = Number( toAnimateDelay ); } else { toAnimateDelayTime = 200; }
			   if( !$toAnimateElement.hasClass('notouch') && touch == false) {$toAnimateElement.addClass('notouch'); }
			  if( !$toAnimateElement.hasClass('animated') ) {
				
				$toAnimateElement.addClass('not-animated');
				
				var elementAnimation = $toAnimateElement.attr('data-animate');

				  setTimeout(function() {
					$toAnimateElement.removeClass('not-animated').addClass( elementAnimation + ' animated');
				  }, toAnimateDelayTime);
			  };
		  }
		});
		
		if (scroll >= $(document).height()) {
			$(window).off('scroll', func);
		}
    }
	
	function func()
	{
		checkScroll();
	}
	
	var _isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
		},
		any: function() {
			return (_isMobile.Android() || _isMobile.BlackBerry() || _isMobile.iOS() || _isMobile.Opera() || _isMobile.Windows());
		}
	};
	
$(window).on('scroll', func);
$(window).on('resize', func);
func();