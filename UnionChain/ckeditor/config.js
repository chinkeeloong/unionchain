/**
 * @license Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
    config.language = 'en';
    config.defaultLanguage = 'en';
    config.filebrowserWindowWidth = 1500;
    config.filebrowserWindowHeight = 1650;
    config.filebrowserUploadUrl = "/AdminInfoDesk/UploadImage";
    
	// config.uiColor = '#AADC6E';
};
