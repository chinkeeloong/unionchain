﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ECFBase.Models
{
    public class CategoryModel
    {
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
    }

    public class UsernameSearchModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgReqUsername")]
        public string Username { get; set; }
    }

    public class PINValidationModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningPIN")]
        public string PIN { get; set; }
    }

    public class BankModel
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
    }

    public class ProductModel
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
    }

    public class ProvinceModel
    {
        public string ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
    }

    public class CityModel
    {
        public string CityCode { get; set; }
        public string CityName { get; set; }
    }

    public class DistrictModel
    {
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
    }

    public class StockistModel
    {
        public string StockistFullName { get; set; }
        public string StockistUserName { get; set; }
    }

    public class TextLanguageModel
    {
        public string LanguageCode { get; set; }
        public string LanguageName { get; set; }
        public string Text { get; set; }
        
    }

    public class CurrencyModel
    {
        public string CurrencyName { get; set; }
        public string CountryCode { get; set; }
        public string Text { get; set; }
    }

    public class GeneralSettingModel
    {
        public string ParameterName { get; set; }
        public string ParameterDescription { get; set; }
        public string ParameterUOM { get; set; }
        public string ParameterStringVal { get; set; }
        public float ParameterFloatVal { get; set; }
    }

   

}