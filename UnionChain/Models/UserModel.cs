﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Data.Objects;

namespace ECFBase.Models
{
    #region User

    public class UserHomeModel
    {
        public string userIntro { get; set; }
    }

    #endregion


    #region MemberMarketTrading


    public class ExchangeModel
    {
        public string AvailableUNS { get; set; }
        public string FrozenUNS { get; set; }
        public string TotalValue { get; set; }
        public string WalletAddress { get; set; }
        public string CurrentPrice { get; set; }
        public string LastPrice { get; set; }
        public string TotalBuyingPrice { get; set; }
        public string TotalBuyingLocalPrice { get; set; }
        public string VerficationCode { get; set; }
        public string username { get; set; }
        public string ICORPASSWORD { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }

        public string CanSell { get; set; }
        public string CanBuy { get; set; }
        public string HGMMemberFlag { get; set; }
        public string RealNameVerificationFlag { get; set; }
        public string SelectedCountry { get; set; }
        public int BuyQuantity { get; set; }
        public int SellQuantity { get; set; }
        public string SecurityPIN { get; set; }

        public string TenPercentageFees { get; set; }
        public string TotalSellPrice { get; set; }
        public string TotalSellLocalPrice { get; set; }

        public List<RateModel> RateResultList { get; set; }

        public List<RateModel> TradingResultList { get; set; }

        public ExchangeModel()
        {
            RateResultList = new List<RateModel>();

            TradingResultList = new List<RateModel>();

        }
    }

    public class RateModel
    {
        public string Rate { get; set; }
        public string Volume { get; set; }
        public string Time { get; set; }

    }

    public class BuyOrderManagement
    {
        public DateTime CreatedTime { get; set; }
        public int ID { get; set; }

        public int TableID { get; set; }
        public int BuyQuantity { get; set; }
        public int PendingQuantity { get; set; }
        public string TotalBuyingPrice { get; set; }
        public string TotalBuyingLocalPrice { get; set; }
        public string OrderStatus { get; set; }
        public List<BuyOrderManagement> BuyOrderList { get; set; }
        
        public int MatchQuantity { get; set; }
        public string PaymentSlipPhotoName { get; set; }
        public string PaymentSlipPhotoPath { get; set; }
        public HttpPostedFileBase PaymentSlipPhoto { get; set; }

        public string SellerFullname { get; set; }
        public string SellerCellPhone { get; set; }
        public string SellerBranch { get; set; }
        public string SellerBank { get; set; }
        public string SellerBankAccount { get; set; }
        public BuyOrderManagement()
        {
            BuyOrderList = new List<BuyOrderManagement>();
        }
    }

    public class SellOrderManagement
    {
        public DateTime CreatedTime { get; set; }
        public int ID { get; set; }
        public int TableID { get; set; }
        public int SellQuantity { get; set; }
        public int PendingQuantity { get; set; }
        public string TotalSellingPrice { get; set; }
        public string TotalSellingLocalPrice { get; set; }
        public string OrderStatus { get; set; }
        public List<SellOrderManagement> SellOrderList { get; set; }

        public int MatchQuantity { get; set; }
        public string PaymentSlipPhotoName { get; set; }
        public string PaymentSlipPhotoPath { get; set; }
        public HttpPostedFileBase PaymentSlipPhoto { get; set; }

        public string BuyerFullname { get; set; }
        public string BuyerContact { get; set; }
        public string BuyerEmail { get; set; }


        public SellOrderManagement()
        {
            SellOrderList = new List<SellOrderManagement>();
        }
    }
}

    #endregion

