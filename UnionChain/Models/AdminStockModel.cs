﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECFBase.Models
{
    public class MobileAgent
    {

        public string ModeID { get; set; }
        public string ModeName { get; set; }
        public string Number { get; set; }
        public string AgentID { get; set; }
        public string FullName { get; set; }
        public string JoinedDate { get; set; }
        public string Position { get; set; }
        public string Country { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string Rate { get; set; }
        public string AdminApproved { get; set; }
        public string DateApproved { get; set; }
    }


    public class PaginationMobileAgent
    {
        public string Username;
        public string Name;
        public string MobileBonus;
        public string SearchName;

        public IEnumerable<SelectListItem> Pages { get; set; }
        public List<MobileAgent> MobileList { get; set; }

        public IEnumerable<SelectListItem> Positions { get; set; }
        public string SelectedPosition { get; set; }

        public List<MobileRequestList> MobileTopUpList { get; set; }

        public string No { get; set; }
        public string Date { get; set; }
        public string PinID { get; set; }
        public float LastPayOut { get; set; }
        public string ImagePath { get; set; }
        public string AdminID { get; set; }
        public string Intro { get; set; }
        public string PINAmount { get; set; }
        public string LocalAmount { get; set; }
        public string USDAmount { get; set; }
        public string Country { get; set; }
        public string PinCode { get; set; }
        public string PaymentMethod { get; set; }

        public string AdminRemark { get; set; }
        public string ApproveBy { get; set; }
        public string ApproveOn { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
        public string GeneratedBy { get; set; }
        public string GeneratedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ReferenceOrder { get; set; }
        public string Remark { get; set; }
        public string Status { get; set; }
        public string ProcessedDate { get; set; }
        public string MemberID { get; set; }
        public string Note { get; set; }
        public float TotalLAP { get; set; }
        public float LAPTotal { get; set; }
        public string Total { get; set; }
        public string TotalAmount { get; set; }
        public string Amount { get; set; }
        public float LAPRebate { get; set; }
        public float LAPOverriding { get; set; }
        public float LAPDirected { get; set; }
        public string PoolPayout { get; set; }
        public float TotalPool { get; set; }

        public PaginationMobileAgent()
        {
            MobileList = new List<MobileAgent>();
            MobileTopUpList = new List<MobileRequestList>();
        }
    }

    public class MobileRequestList
    {
        public string No { get; set; }
        public IEnumerable<SelectListItem> paymentmode1 { get; set; }
        public string Date { get; set; }
        public string PinID { get; set; }
        public float LastPayOut { get; set; }
        public string ImagePath { get; set; }
        public string AdminID { get; set; }
        public string Intro { get; set; }
        public string PINAmount { get; set; }
        public string LocalAmount { get; set; }
        public string USDAmount { get; set; }
        public string Country { get; set; }
        public string PinCode { get; set; }
        public string PaymentMethod { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
        public string GeneratedBy { get; set; }
        public string GeneratedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ReferenceOrder { get; set; }

        public string GenerateBy { get; set; }
        public string GenerateOn { get; set; }
        public string Remark { get; set; }
        public string Status { get; set; }
        public string ProcessedDate { get; set; }
        public string MemberID { get; set; }
        public string FullName { get; set; }
        public string TopUpFor { get; set; }
        public string Currency { get; set; }
        public string AdminRemark { get; set; }
        public string VerifyBy { get; set; }
        public string VerifyOn { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedOn { get; set; }
        public string Note { get; set; }
        public string Username { get; set; }
        public float TotalLAP { get; set; }
        public float LAPTotal { get; set; }
        public string Total { get; set; }
        public string TotalAmount { get; set; }
        public string Amount { get; set; }
        public float LAPRebate { get; set; }
        public float LAPOverriding { get; set; }
        public float LAPDirected { get; set; }
        public string PoolPayout { get; set; }
        public float TotalPool { get; set; }

    }

    public class Rank
    {
        public string RankCode { get; set; }
        public string RankName { get; set; }
    }



    public class StockistProduct
    {
        public string Stockist { get; set; }
        public string SelectedProduct { get; set; }
        public IEnumerable<SelectListItem> ProductList { get; set; }
        public int ProductAmount { get; set; }

        public StockistProduct()
        {
            ProductList = new List<SelectListItem>();
        }
    }

    public class StockistAgent : MobileAgent
    {
        public string Commitment { get; set; }
        public string AdjustedCommitment { get; set; }
        public string SelectedProvince { get; set; }
        public string SelectedCity { get; set; }
        public string SelectedCountry { get; set; }
        public string SelectedDistrict { get; set; }
        public string SelectedActionType { get; set; }
        public string SelectedBank { get; set; }
        public string Remark { get; set; }
        public IEnumerable<SelectListItem> BankList { get; set; }
        public IEnumerable<SelectListItem> ActionList { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public IEnumerable<SelectListItem> ProvinceList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> DistrictList { get; set; }

        public StockistAgent()
        {
            CountryList = new List<SelectListItem>();
            ProvinceList = new List<SelectListItem>();
            CityList = new List<SelectListItem>();
            BankList = new List<SelectListItem>();
            DistrictList = new List<SelectListItem>();
            ActionList = new List<SelectListItem>();
        }
    }

    public class PaginationStockistAgent
    {
        public IEnumerable<SelectListItem> Pages { get; set; }
        public List<StockistAgent> StockistList { get; set; }

        public PaginationStockistAgent()
        {
            StockistList = new List<StockistAgent>();
        }
    }

    public class CustomModel
    {
        public string CustomName { get; set; }
        public string CustomValue { get; set; }
    }

    public class PaginationStockistCommitment
    {
        public string Username { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public List<StockistCommitment> StockistCommitmentList { get; set; }

        public PaginationStockistCommitment()
        {
            StockistCommitmentList = new List<StockistCommitment>();
        }
    }

    public class StockistCommitment
    {
        public string Username { get; set; }
        public string CommitmentDate { get; set; }
        public string Remark { get; set; }
        public string AmountIn { get; set; }
        public string AmountOut { get; set; }
        public string Amount { get; set; }
    }
}