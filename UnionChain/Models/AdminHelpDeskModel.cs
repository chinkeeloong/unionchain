﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ECFBase.Models
{
    public class PaginationInboxModel
    {
        public List<InboxModel> InboxList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public string SelectedFilteringCriteria { get; set; }
        public IEnumerable<SelectListItem> FilteringCriteria { get; set; }

        public PaginationInboxModel()
        {
            InboxList = new List<InboxModel>();
        }
    }

    public class InboxModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public string SelectedType { get; set; }
        public IEnumerable<SelectListItem> QuestionTypes { get; set; }
      
        public int? InboxID { get; set; }
        public string Username { get; set; }
        public string Title { get; set; }
        public string Reply { get; set; }
        public string SendTo { get; set; }
        public int State { get; set; }
        public string PostDate { get; set; }
        public string ReplyDate { get; set; }


        public List<TextLanguageModel> TitleLanguageList { get; set; }
        public List<TextLanguageModel> ContentLanguageList { get; set; }
        public List<TextLanguageModel> ReplyLanguageList { get; set; }

        public InboxModel()
        {
            LanguageList = new List<SelectListItem>();
            TitleLanguageList = new List<TextLanguageModel>();
            ContentLanguageList = new List<TextLanguageModel>();
            ReplyLanguageList = new List<TextLanguageModel>();
        }
    }

    public class SmsModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public string SelectedRecipient { get; set; }
        public IEnumerable<SelectListItem> RecipientList { get; set; }

        public string MemberId { get; set; }
        public string Content { get; set; }
        public int MaxChar { get; set; }

        public SmsModel()
        {
            LanguageList = new List<SelectListItem>();
            RecipientList = new List<SelectListItem>();
        }
    }
}