﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECFBase.Models
{
    public class OrderModel
    {
        public string ProductNameAndDescription { get; set; }
        public int Quantity { get; set; }
        public float SingleUnitPrice { get; set; }
        public float TotalPrice { get; set; }
        public int Number { get; set; }
    }

    public class DeliveryOrderModel
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPhone { get; set; }
        public string ShippingAddress1 { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingAddress3 { get; set; }
        public string ShippingAddress4 { get; set; }
        public string ShippingPhoneNo { get; set; }
        public string ShippingPostCode { get; set; }
        public string Country { get; set; }
        public string Remark {get; set;}
        public string CourierNumber { get; set; }
        public float TotalPrice { get; set; }
        public float TotalCharge { get; set; }
        public float GrandTotal { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceDate { get; set; }
        public string DeliveryOrderNo { get; set; }
        public string SDONo { get; set; }
        public string DeliveryOrderDate { get; set; }
        public string Username { get; set; }
        public string DeliveryAmount { get; set; }
        public string DeliveryType { get; set; }
        public string DeliveryCourier { get; set; }
        public string DeliveryCourierCode { get; set; }
        public string DeliveryStatus { get; set; }
        public string DeliveryPackageName { get; set; }
        public string DeliveryOrderApprovedOn { get; set; }
        public string DeliveryOrderApprovedBy { get; set; }
        public string IsApproved { get; set; }
        public string IsNotApproved { get; set; }
        public string SelectedDeliveryModes { get; set; }
        public string SelectedCourierCompany { get; set; }
        public string StockistName { get; set; }
        public List<OrderModel> OrderList { get; set; }
        public List<SelectListItem> DeliveryModes { get; set; }
        public List<SelectListItem> CourierCompanies { get; set; }


        public DeliveryOrderModel()
        {
            OrderList = new List<OrderModel>();
            DeliveryModes = new List<SelectListItem>();
            CourierCompanies = new List<SelectListItem>();
        }
    }

    public class PaginationDeliveryOrderModel
    {
        public string SelectedFilteringCriteria { get; set; }
        public List<DeliveryOrderModel> DeliveryOrderList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public List<SelectListItem> FilteringCriteria { get; set; }
        public List<SelectListItem> StatusCriteria { get; set; }
        public List<SelectListItem> FromDays { get; set; }
        public List<SelectListItem> FromMonths { get; set; }
        public List<SelectListItem> FromYears { get; set; }
        public List<SelectListItem> ToDays { get; set; }
        public List<SelectListItem> ToMonths { get; set; }
        public List<SelectListItem> ToYears { get; set; }
        public string SelectedFromDay { get; set; }
        public string SelectedFromMonth { get; set; }
        public string SelectedFromYear { get; set; }
        public string SelectedToDay { get; set; }
        public string SelectedToMonth { get; set; }
        public string SelectedToYear { get; set; }
        public string SelectedStatus {get;set;}

        public PaginationDeliveryOrderModel()
        {
            DeliveryOrderList = new List<DeliveryOrderModel>();
            FilteringCriteria = new List<SelectListItem>();
            StatusCriteria = new List<SelectListItem>();
            FromDays = new List<SelectListItem>();
            FromMonths = new List<SelectListItem>();
            FromYears = new List<SelectListItem>();
            ToDays = new List<SelectListItem>();
            ToMonths = new List<SelectListItem>();
            ToYears = new List<SelectListItem>();
        }
    }

    public class PaginationProductMovementModel
    {
        public List<ProductMovementModel> ProductMovementList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public string ProductCode { get; set; }

        public PaginationProductMovementModel()
        {
            ProductMovementList = new List<ProductMovementModel>();
        }
    }

    public class ProductMovementModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string PackageCode { get; set; }
        public string ProductCode { get; set; }
        public int FlowId { get; set; }
        public int Ingoing { get; set; }
        public int Outgoing { get; set; }
        public string CashName { get; set; }
        public int Balance { get; set; }
        public string CreatedDate { get; set; }
        public string AppUser { get; set; }
        public string Number { get; set; }
        public int ProductStatus { get; set; } //for color
    }

    public class PaginationRegistrationPackageLog
    {
        public List<RegistrationPackageLog> PackageLog { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationRegistrationPackageLog()
        {
            PackageLog = new List<RegistrationPackageLog>();
        }
    }

    public class RegistrationPackageLog
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Joined { get; set; }
        public string PackageCode { get; set; }
        public string Investment { get; set; }

        public RegistrationPackageLog()
        {
        }
    }
    
    public class StockAdjustmentPageModel
    {
        public IEnumerable<SelectListItem> TypeOfAdjustment { get; set; }
        public IEnumerable<SelectListItem> PackageCategory { get; set; }
        public IEnumerable<SelectListItem> RegisterPackageCode { get; set; }
        public IEnumerable<SelectListItem> MaintenancePackageCode { get; set; }
        
        public List<PackageSetupModel> RegisterPackageList { get; set; }
        public List<PackageSetupModel> MaintenancePackageList { get; set; }

        public string SelectedType { get; set; }
        public string SelectedPackageCategory { get; set; }
        public string SelectedRegisterPackageId { get; set; }
        public string SelectedMaintenancePackageId { get; set; }
        public string CurrentDate { get; set; }

        public string StockDescription { get; set; }
        public string StockBalance { get; set; }
        public int QuantityAdjustment { get; set; }
        public string NewStockBalance { get; set; }
        public string Remark { get; set; }

        public StockAdjustmentPageModel()
        {
            RegisterPackageList = new List<PackageSetupModel>();
            MaintenancePackageList = new List<PackageSetupModel>();            
        }
    }

    public class StockistStockAdjustmentPageModel
    {
        public IEnumerable<SelectListItem> TypeOfAdjustment { get; set; }
        public IEnumerable<SelectListItem> Stockist { get; set; }

        public IEnumerable<SelectListItem> ProductList { get; set; }

        public string SelectedType { get; set; }
        public string SelectedProduct { get; set; }
        public string SelectedStockist { get; set; }
        public string CurrentDate { get; set; }

        public string ProductDescription { get; set; }
        public string StockBalance { get; set; }
        public int QuantityAdjustment { get; set; }
        public string NewStockBalance { get; set; }
        public string Remark { get; set; }

        public StockistStockAdjustmentPageModel()
        {
        }
    }

    public class StockAdjustmentLogModel
    {
        public string CreatedDate { get; set; }
        public string StockNumber { get; set; }
        public string CashName { get; set; }
        public string PackageCategory { get; set; }
        public string PackageID { get; set; }
        public string QuantityIn { get; set; }
        public string QuantityOut { get; set; }
        public string QuantityBalance { get; set; }
        public string AdminUser { get; set; }
    }

    public class PaginationStockAdjustmentLogModel
    {
        public List<StockAdjustmentLogModel> Lists { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationStockAdjustmentLogModel()
        {
            Lists = new List<StockAdjustmentLogModel>();
        }
    }

    #region InvoiceListing

    public class PaginationInvoiceListingModel
    {
        public List<InvoiceModel> InvoiceLists { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public IEnumerable<SelectListItem> FromDay { get; set; }
        public IEnumerable<SelectListItem> FromMonth { get; set; }
        public IEnumerable<SelectListItem> FromYear { get; set; }
        public IEnumerable<SelectListItem> ToDay { get; set; }
        public IEnumerable<SelectListItem> ToMonth { get; set; }
        public IEnumerable<SelectListItem> ToYear { get; set; }
        public IEnumerable<SelectListItem> FilteringCriteria { get; set; }
        public IEnumerable<SelectListItem> StatusFilteringCriteria { get; set; }

        public string SelectedFromDay { get; set; }
        public string SelectedFromMonth { get; set; }
        public string SelectedFromYear { get; set; }
        public string SelectedToDay { get; set; }
        public string SelectedToMonth { get; set; }
        public string SelectedToYear { get; set; }
        public string SelectedFilteringCriteria { get; set; }
        public string SelectedStatusFilteringCriteria { get; set; }

        public PaginationInvoiceListingModel()
        {
            InvoiceLists = new List<InvoiceModel>();
        }
    }

    public class InvoiceModel
    {
        public int Id { get; set; }
        public string RowNumber { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime Date { get; set; }
        public string InvoiceCashName { get; set; }//Type
        public string Status { get; set; }

        //user info
        public string Username { get; set; }
        public string CellPhone { get; set; }
        public string Phone { get; set; }
        public string ShipAddress { get; set; }
        public string ShipAddress1 { get; set; }
        public string ShipAddress2 { get; set; }
        public string ShipAddress3 { get; set; }
        public string CountryCode { get; set; }
        //member
        public string IntroPerson { get; set; } //aka. SalesPerson
        //package
        public string PackageCode { get; set; }
        public int PackageQuantity { get; set; }
        public float Investment { get; set; }
        public string PackageDescription { get; set; }
        public float TotalAmount { get; set; }
        public float DeliveryCharge { get; set; }
        public float PackagePV { get; set; }
        public string MobileMember { get; set; }
    }
    #endregion

}