﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ECFBase.Models
{
    public class PaginationPairingBonusLogModel
    {
        public List<PairingBonusLogModel> ModelCollection { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public string username { get; set; }
        public string nickname { get; set; }
        public IEnumerable<SelectListItem> Months { get; set; }
        public IEnumerable<SelectListItem> Years { get; set; }
        public string SelectedMonth { get; set; }
        public string SelectedYears { get; set; }
        public string TotalNewGroupA { get; set; }
        public string TotalNewGroupB { get; set; }
        public string TotalGroupA { get; set; }
        public string TotalGroupB { get; set; }
        public string TotalBonus { get; set; }






        public PaginationPairingBonusLogModel()
        {
            ModelCollection = new List<PairingBonusLogModel>();
        }
    }

    public class PairingBonusLogModel
    {
        public string Number { get; set; }
        public string Username { get; set; }
        public string CurrentLeftSales { get; set; }
        public string CurrentRightSales { get; set; }
        public string TotalLeftSales { get; set; }
        public string TotalRightSales { get; set; }
        public string Pair { get; set; }
        public string PairBonus { get; set; }
        public string LogTime { get; set; }
        public string Name { get; set; }
        public string Level { get; set; }
        public string Amount { get; set; }
    }

    public class PaginationBonusLogModel
    {
        public List<BonusLogModel> ModelCollection { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public IEnumerable<SelectListItem> Months { get; set; }
        public IEnumerable<SelectListItem> Years { get; set; }
        public string SelectedMonth { get; set; }
        public string SelectedYears { get; set; }

        public PaginationBonusLogModel()
        {
            ModelCollection = new List<BonusLogModel>();
        }
    }

    public class BonusLogModel
    {
        public string SubName { get; set; }
        public string LogTime { get; set; }
        public string Sponsor { get; set; }
        public string Pairing { get; set; }
        public string Mobile { get; set; }
        public string Unilevel { get; set; }
        public string Total { get; set; }
    }

    public class PaginationSuper2BonusLogModel
    {
        public List<Super2BonusLogModel> ModelCollection { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public string pages { get; set; }
        public PaginationSuper2BonusLogModel()
        {
            ModelCollection = new List<Super2BonusLogModel>();
        }
    }

    public class Super2BonusLogModel
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Rank { get; set; }
        public string CashIn { get; set; }
        public string CreatedDate { get; set; }
        public string PackageCode { get; set; }
    }

    public class PaginationJackpotBonusLogModel
    {
        public List<JackpotBonusLogModel> ModelCollection { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        
        public string Total { get; set; }
        public string username { get; set; }
        public string starttime { get; set; }
        public string endtime { get; set; }
        public string pages { get; set; }
        public PaginationJackpotBonusLogModel()
        {
            ModelCollection = new List<JackpotBonusLogModel>();
        }
    }

    public class JackpotBonusLogModel
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Rank { get; set; }
        public string CashIn { get; set; }
        public string Remark { get; set; }
        public string NewSalesGroupA { get; set; }
        public string NewSalesGroupB { get; set; }
        public string NewSalesGroupC { get; set; }
        public string AccumulateSalesGroupA { get; set; }
        public string AccumulateSalesGroupB { get; set; }
        public string AccumulateSalesGroupC { get; set; }
        public string Status { get; set; }
        public string Cycle { get; set; }
        public string CreatedDate { get; set; }
        public string IC { get; set; }
    }


    public class DailySponserLogModel
    {
        public List<DailySponserModel> ModelCollection { get; set; }
        public bool ShowTable { get; set; }
        public string Date { get; set; }
        public float Total { get; set; }
        public DailySponserLogModel()
        {
            ModelCollection = new List<DailySponserModel>();
        }
    }

    public class DailySponserModel
    {
        public string Date { get; set; }
        public string Username { get; set; }
        public float Total { get; set; }
    }

    public class WalletAdjustmentModel
    {
        public string TypeResource { get; set; }
        public string WalletBalanceTypeResource { get; set; }
        public string Nickname { get; set; }
        
        public string Type { get; set; }
        public float CurrentWallet { get; set; }
        public bool paymentmodeshow { get; set; }

        [RegularExpression(@"^[0-9]*(?:\.[0-9]*)?$", ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgInvalidWallet")]
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgReqWallet")]
        public float TransactionWallet { get; set; }

        public string Username { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgReqRemark")]
        public string Remark { get; set; }

        public IEnumerable<SelectListItem> Transaction { get; set; }
        public string SelectedTransaction { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }
        public string SelectedCountry { get; set; }

        public IEnumerable<SelectListItem> Bank { get; set; }
        public string SelectedBank { get; set; }

        public IEnumerable<SelectListItem> paymentmode { get; set; }
        public string selectedmode { get; set; }
    }

    public class PaginationWalletLogModel
    {
        public bool show { get; set; }
        public List<WalletLogModel> WalletLogList { get; set; }
        public string TypeOfWallet { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public bool HideWalletBalance { get; set; }
        public bool HideCashOut { get; set; }
        public bool HideAppUser { get; set; }

        public IEnumerable<SelectListItem> WithdrawalDateList { get; set; }
        public string SelectedWithdrawalDate { get; set; }

        public IEnumerable<SelectListItem> WithdrawalState { get; set; }
        public string SelectedWithdrawalState { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }
        public string SelectedCountry { get; set; }

        public IEnumerable<SelectListItem> SelectedCashName { get; set; }
        public string selectedname { get; set; }

        public string startdate { get; set; }
        public string enddate { get; set; }
        public string username { get; set; }

        public IEnumerable<SelectListItem> Months { get; set; }
        public IEnumerable<SelectListItem> Years { get; set; }

        public string SelectedMonth { get; set; }
        public string SelectedYears { get; set; }

        public string TotalSponsorAmount { get; set; }
        public string TotalPairingAmount { get; set; }
        public string TotalMatchingAmount { get; set; }
        public string TotalGrandAmount { get; set; }

        public string TotalPackageAmount { get; set; }
        public string TotalAmount { get; set; }
        public PaginationWalletLogModel()
        {
            WalletLogList = new List<WalletLogModel>();
            HideWalletBalance = false;
            HideCashOut = false;
            HideAppUser = false;
        }
    }

    public class WalletLogModel
    {
        public string refferenceID { get; set; }
        public string refference { get; set; }
        public string No { get; set; }
        public int ID { get; set; }
        public string FlowID { get; set; }
        public string Username { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float CashIn { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float CashOut { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float LocalAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float AmountUSD { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float BankCharges { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float NetAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float AdminFee { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float NetWithdrawal { get; set; }
        public string ApproveOn { get; set; }
        public string CountryName { get; set; }
        public string BranchName { get; set; }
        public string AccNo { get; set; }
        public string FullName { get; set; }
        public string MobileNumber { get; set; }
        public string Rank { get; set; }
        public string CashName { get; set; }
        public string PackageAmount { get; set; }
        public string Remark { get; set; }
        public string Amount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float Wallet { get; set; }

        public string Subtotal { get; set; }
        public string TotalAmout { get; set; }
        public bool ShowSubTotal { get; set; }
        public string IC { get; set; }
        public string paymentmode { get; set; }
        public string AppUser { get; set; }
        public string AppOther { get; set; }
        public string AppNumber { get; set; }
        public string AppRate { get; set; }
        public string Bank { get; set; } //need to convert to bank name instead of bank ID during stored procedure
        public string CreatedDate { get; set; }
        public string TranState { get; set; }

        public string TopUpFor { get; set; }

        public string PaymentMethod { get; set; }

        public string Currency { get; set; }
        public bool IsChecked { get; set; }
        public string status { get; set; }

        public  string ROIPACKAGE { get; set; }
        public string Days { get; set; }

        public string SponsorAmount { get; set; }
        public string PairingAmount { get; set; }
        public string MatchingAmount { get; set; }
        public string TotalAmount { get; set; }

        

        public float XGSTMYR { get; set; }
        public float GSTMYR { get; set; }
        public float GSTUSD { get; set; }
        public float XGSTUSD { get; set; }
        
    }

    public class DailyWalletLogSummary
    {
        public string DateTime { get; set; }

        // for bonus-wallet
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float Bonus { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float Transfer { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float PVWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float Withdrawal { get; set; }

        // for pv-wallet
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float TopUp { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float BonusWallet { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float Registration { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public float Maintaince { get; set; }
    }

    public class PaginationDailyWalletLogSummary
    {
         public List<DailyWalletLogSummary> WalletLogSummaryList { get; set; }
         public IEnumerable<SelectListItem> Pages { get; set; }
         public IEnumerable<SelectListItem> Years { get; set; }
         public IEnumerable<SelectListItem> Months { get; set; }
         public string SelectedYear { get; set; }
         public string SelectedMonth { get; set; }

         public PaginationDailyWalletLogSummary()
         {
             WalletLogSummaryList = new List<DailyWalletLogSummary>();
             Years = new List<SelectListItem>();
             Months = new List<SelectListItem>();
             Pages = new List<SelectListItem>(); 
         }         
    }

    public class PaginationBonusReportModel
    {
        public DateTime SelectedDate { get; set; }
        public string AgentID { get; set; }
        public string Name { get; set; }
        public string DateJoined { get; set; }
        public string PersonalBV { get; set; }
        public string PersonalGroupBV { get; set; }
        public float IntroBonus { get; set; }
        public float DiffBonus { get; set; }
        public float DirectorBonus { get; set; }
        public float StarBonus { get; set; }
        public float CEOBonus { get; set; }
        public float MobileBonus { get; set; }
        public float StockistBonus { get; set; }
        public float GrandTotal { get; set; }
        public float GrandTotalRMB { get; set; }
        public int CurrencyRate { get; set; }
        public float GroupBV { get; set; }
        public string Rank { get; set; }
        public string TypeOfWallet { get; set; }

        public List<MonthlyBonusDetail> WalletLogList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public IEnumerable<SelectListItem> Months { get; set; }
        public IEnumerable<SelectListItem> Years { get; set; }
        public string SelectedMonth { get; set; }
        public string SelectedYears { get; set; }
        public string SearchUsername { get; set; }

        public PaginationBonusReportModel()
        {
            AgentID = "";
            Name = "";
            DateJoined = "";
            PersonalBV = "";
            PersonalGroupBV = "";
            WalletLogList = new List<MonthlyBonusDetail>();
        }
    }

    public class PaginationMonthlyBonusDetail
    {
        public List<MonthlyBonusDetail> WalletLogList { get; set; }
        public string TypeOfWallet { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public DateTime SelectedDate { get; set; }
        
        public PaginationMonthlyBonusDetail()
        {
            WalletLogList = new List<MonthlyBonusDetail>();
        }
    }

    public class MonthlyBonusDetail
    {
        public string No { get; set; }
        public string Username { get; set; }

        public string FullName { get; set; }
        public string PersonalSales { get; set; }
        public string Rank { get; set; }
        public string PersonalGroupSales { get; set; }
        public string GroupSales { get; set; }
        public string SalesBV { get; set; }
        public string Level { get; set; }
        public string Payout { get; set; }
    }
}