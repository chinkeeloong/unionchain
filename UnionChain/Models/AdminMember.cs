﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ECFBase.Models
{
    public class MemberPermissionModel : MemberBaseModel
    {
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public bool IsBlocked { get; set; }
    }

    public class MemberBaseModel
    {
        public DateTime DateJoined;
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public float BonusWallet;
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public float PVWallet;
        public string MemberPhoto;
    }

    public class ChangePasswordModel : MemberBaseModel
    {
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgReqCurrPwd")]
        public string CurrentPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgReqNewPwd")]
        public string NewPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningConfirmNewPwd")]
        public string ConfirmNewPassword { get; set; }
        public bool NoRedirectionRequired { get; set; }
    }

    public class ChangeRankingModel : MemberBaseModel
    {
        public List<SelectListItem> RankList { get; set; }
        public string UserName { get; set; }
        public string SelectedRanking { get; set; }        
    }

    public class ChangeOwnershipModel : MemberBaseModel
    {
        public string Username { get; set; }
        public string SurName { get; set; }
        public string FirstName { get; set; }
    }

    public class ChangeSponsorModel : MemberBaseModel
    {
        public string Username { get; set; }
        public string CurrIntro { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningNewIntro")]
        public string NewIntro { get; set; }

    }

    public class MemberFunctionSettingModel
    {
        public string FunctionID { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }

        public bool TradeableHash { get; set; }
        public bool TradeableHashPerfomance { get; set; }
        public bool TradeableHashManagement { get; set; }
        public bool TradeableTransfer { get; set; }
        public bool TradeableSellTrade { get; set; }
        public bool TradeableBuyTrade { get; set; }
       
        public bool UnTradeableHash { get; set; }
        public bool UnTradeableHashPerfomance { get; set; }
        public bool UnTradeableHashManagement { get; set; }
        public bool UnTradeableTransfer { get; set; }
        public bool UnTradeableSellTrade { get; set; }
        public bool UnTradeableBuyTrade { get; set; }

        public bool AccessRight { get; set; }


        public IEnumerable<SelectListItem> Pages { get; set; }
        public List<MemberFunctionSettingModel> FunctionList { get; set; }



        public MemberFunctionSettingModel ()
        {
            FunctionList = new List<MemberFunctionSettingModel>();
        }
    }

    //public class MultiAccount
    //{
    //    public string Username { get; set; }
    //    public string Nickname { get; set; }
    //    public string NewSubAccount { get; set; }
    //    public string MasterAccount { get; set; }
    //    public IEnumerable<SelectListItem> Pages { get; set; }

    //    public List<MultiAccountSub> MemberList { get; set; }

    //    public MultiAccount()
    //    {
    //        MemberList = new List<MultiAccountSub>();
    //        MasterAccount = "";
    //    }
    //}

    //public class MultiAccountSub
    //{
    //    public string MultiID { get; set; }
    //    public string No { get; set; }
    //    public string MemberID { get; set; }
    //    public string Nickname { get; set; }
    //    public string Admin { get; set; }
    //    public string Date { get; set; }
    //}

    //public class ChangeCenterModel : MemberBaseModel
    //{
    //    public string Username { get; set; }
    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningLeftMember")]
    //    public string LeftMember { get; set; }
    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningRightMember")]
    //    public string RightMember { get; set; }
    //}

    //public class ChangeMemberTreeModel : MemberBaseModel
    //{
    //    public string Username { get; set; }
    //    public string LeftMember { get; set; }
    //    public string RightMember { get; set; }
    //}

    //public class ManualAdjustMemberAccountModel
    //{
    //    public string FromMember { get; set; }
    //    public string ToMember { get; set; }
    //    public float Amount { get; set; }
    //    public string Reason { get; set; }

    //    public string SelectedWalletType { get; set; }
    //    public IEnumerable<SelectListItem> WalletType { get; set; }
       
    //    public string SelectedAdjustType { get; set; }
    //    public IEnumerable<SelectListItem> AdjustType { get; set; }
    //}

}