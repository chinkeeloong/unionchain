﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Data.Objects;

namespace ECFBase.Models
{

    public class PaginationInfoDeskModel
    {
        public List<InfoDeskModel> InfoDeskList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationInfoDeskModel()
        {
            InfoDeskList = new List<InfoDeskModel>();
        }
    }

    //public class SubAcc
    //{
    //    public string MemberID { get; set; }

    //    public SubAcc()
    //    { 
            
    //    }
    //}

    public class InfoDeskModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public int? InfoDeskID { get; set; }
        public string Number { get; set; }
        public string InfoDeskType { get; set; }
        public string InfoDeskPostDate { get; set; }
        public bool IsNewInfo { get; set; }

        public int? CINF_ID { get; set; }
        public string ProductImageName { get; set; }
        public string ProductImagePath { get; set; }
        public HttpPostedFileBase ProductImage { get; set; }
        public List<PackageModel> PackageProducts { get; set; }
        public string Image { get; set; }

        public string InfoDeskTitle { get; set; }
        public string InfoDeskContent { get; set; }
        public string Created { get; set; }
        public string A { get; set; }
        public string imageName { get; set; }
        public List<TextLanguageModel> TitleLanguageList { get; set; }

        [AllowHtml]
        public List<TextLanguageModel> ContentLanguageList { get; set; }

        public InfoDeskModel()
        {
            LanguageList = new List<SelectListItem>();
            TitleLanguageList = new List<TextLanguageModel>();
            ContentLanguageList = new List<TextLanguageModel>();
            //Created = new string();
        }
    }

    public class PaginationFileCenterModel
    {
        public List<FileCenterModel> FileCenterList { get; set; }

        public PaginationFileCenterModel()
        {
            FileCenterList = new List<FileCenterModel>();
        }
    }

    public class FileCenterModel
    {
        public string FileName { get; set; }
        public string FileTitle { get; set; }
        public string FileSize { get; set; }
        public string FilePath { get; set; }
    }

}