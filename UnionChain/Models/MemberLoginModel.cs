﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ECFBase.Components;
using System.Web.Mvc;

namespace ECFBase.Models
{
    
    #region SignUp Model
    public class SignUpModel
    {

        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        public string RetypeNewPassword { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string IntroName { get; set; }
        public string SponsorName { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string EncryptPassword { get; set; }
        [DataType(DataType.Password)]
        public string PayPassword { get; set; }
        public string EncryptPayPassword { get; set; }
        public string Captcha { get; set; }
        public List<SelectListItem> PhoneCodeList { get; set; }
        public string SelectedPhoneCode { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Phone must be numeric only")]
        public string Phone { get; set; }
        public string Email { get; set; }
        public string VerficationCode { get; set; }
        //public IEnumerable<SelectListItem> PositionList { get; set; }
        public List<SelectListItem> PositionList { get; set; }
        public string SelectedPosition { get; set; }
        public string SelectedCountry { get; set; }
        public string SelectedCountryName { get; set; }
        public string MobileCountryCode { get; set; }
        public string ICLength { get; set; }
        public string SelectedICType { get; set; }
        public string MemberType { get; set; }
        public IEnumerable<SelectListItem> Countries { get; set; }
        

        public SignUpModel()
        {
            PositionList = new List<SelectListItem>();
            PhoneCodeList = new List<SelectListItem>();
            Countries = new List<SelectListItem>();
        }
    }

    #endregion

}