﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace ECFBase.Models
{

    #region SponsorBonusModel

    //public class SponsorBonusModelList
    //{
    //    public List<SponsorBonusModel> SponsorBonuses { get; set; }

    //    public SponsorBonusModelList()
    //    {
    //        SponsorBonuses = new List<SponsorBonusModel>();
    //    }
    //}

    //public class SponsorBonusModel
    //{
    //    /* sponsor bonus param summary */
    //    public int? SponID { get; set; }

    //    public int SponLvl { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningSponBonusValue")]
    //    public float SponBonus { get; set; }

    //    public string SponUnit { get; set; }
    //}
    #endregion

    #region PairingBonusModel

    //public class PairingBonusModelList
    //{
    //    public List<PairingBonusModel> PairingBonuses { get; set; }

    //    public PairingBonusModelList()
    //    {
    //        PairingBonuses = new List<PairingBonusModel>();
    //    }
    //}

    //public class PairingBonusModel
    //{
    //    /* pairing bonus param summary */
    //    public int? PairID { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningPairBonusValue")]
    //    public float PairBonus { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningPairDailyCap")]
    //    public int PairDailyCap { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningPairCarryFwdCap")]
    //    public int PairDailyCarryFwdCap { get; set; }

    //    public string PairUnit { get; set; }
    //}
    #endregion

    #region MatchingBonusModel

    //public class MatchingBonusModelList
    //{
    //    public List<MatchingBonusModel> MatchingBonuses { get; set; }

    //    public MatchingBonusModelList()
    //    {
    //        MatchingBonuses = new List<MatchingBonusModel>();
    //    }
    //}

    //public class MatchingBonusModel
    //{
    //    /* matching bonus param summary */

    //    public int? MatchID { get; set; }

    //    public int MatchLvl { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningMatchingBonusValue")]
    //    public float MatchBonus { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningMatchingDirectSponReq")]
    //    public int MatchDirectSponReq { get; set; }

    //    public string MatchUnit { get; set; }
    //}
    #endregion

    #region WorldPoolBonusModel

    //public enum WorldPoolRankingType
    //{
    //    Supervisor = 0,
    //    Manager,
    //    Director,
    //    President,
    //    Chairman,
    //    SponsoredMember,
    //    None
    //}

    //public class WorldPoolRanking
    //{
    //    public WorldPoolRankingType Name { get; set; }

    //    public WorldPoolRanking(WorldPoolRankingType name)
    //    {
    //        Name = name;
    //    }
    //}

    //public class WorldPoolRankingList
    //{
    //    public List<WorldPoolRanking> WorldPoolRankingTypes { get; private set; }

        //static private WorldPoolRankingList _instance;
        //static public WorldPoolRankingList Instance
        //{
        //    get
        //    {
        //        if (_instance == null)
        //        {
        //            _instance = new WorldPoolRankingList();
        //        }

        //        return _instance;
        //    }
        //}

    //    public IEnumerable<SelectListItem> GetRankList(string id = "", bool isDefaultDisplay = true)
    //    {
    //        if (id == null)
    //            return new List<SelectListItem>();

    //        if (isDefaultDisplay)
    //        {
    //            WorldPoolRankingTypes = new List<WorldPoolRanking>
    //                                {
    //                                    new WorldPoolRanking(WorldPoolRankingType.Supervisor),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Manager),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Director),
    //                                    new WorldPoolRanking(WorldPoolRankingType.President),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Chairman)
    //                                };
    //        }
    //        else
    //        {
    //            WorldPoolRankingTypes = new List<WorldPoolRanking>
    //                                {
    //                                    new WorldPoolRanking(WorldPoolRankingType.Supervisor),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Manager),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Director),
    //                                    new WorldPoolRanking(WorldPoolRankingType.President),
    //                                    new WorldPoolRanking(WorldPoolRankingType.Chairman),
    //                                    new WorldPoolRanking(WorldPoolRankingType.SponsoredMember),
    //                                    new WorldPoolRanking(WorldPoolRankingType.None)
    //                                };
    //        }

    //        var ranks = WorldPoolRankingTypes.Select(m => new SelectListItem()
    //        {
    //            Text = m.Name.ToString(),
    //            Value = m.Name.ToString(),
    //            Selected = id != "" && id.ToUpper() == m.Name.ToString().ToUpper()
    //        }).ToList();

    //        return ranks;
    //    }
    //}
    //public class WorldPoolBonusModelList
    //{
    //    public IEnumerable<SelectListItem> RankList { get; set; }
    //    public string SelectedRanking { get; set; } 
    //    public List<WorldPoolBonusModel> WorldPoolBonuses { get; set; }

    //    public WorldPoolBonusModelList()
    //    {
    //        WorldPoolBonuses = new List<WorldPoolBonusModel>();
    //    }
    //}

    //public class WorldPoolBonusModel
    //{
    //    /* world pool param summary */

    //    public int? WorldPoolID { get; set; }

    //    public string WorldPoolRank { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningWPoolBonusValue")]
    //    public float WorldPoolBonus { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningWPoolEntitle")]
    //    public int WorldPoolBonusEntitlement { get; set; }

    //    //World Pool - qualification for rank
    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "WarningWPoolSales")]
    //    public int WorldPoolQlfSalesNeeded { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningWPoolRankAmt")]
    //    public int WorldPoolQlfRankAmountReq { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningRankingType")]
    //    public string WorldPoolQlfRankingTypeReq { get; set; }

    //    public string WorldPoolBonusUOM { get; set; }
    //}


    #endregion

    #region CurrentSales/Bonus Payout Summary
    public class PaginationCurrentSalesModel
    {
        public List<CurrentSalesModel> CurrentSalesModel { get; set; }
        public List<HashPerformanceBonusModel> HashPerformanceBonusModel { get; set; }
        public List<HashBonusModel> HashBonusModel { get; set; }
        public List<HashPowerModel> HashPowerModel { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }
        public string SelectedCountry { get; set; }

        public string TotalPairing { get; set; }
        public string TotalSponsor { get; set; }
        public string TotalMatching { get; set; }
        public string TotalAmount { get; set; }
        public string DateFrom  { get; set; }
        public string DateTo { get; set; }
        public string MemberID { get; set; }

        public PaginationCurrentSalesModel()
        {
            CurrentSalesModel = new List<CurrentSalesModel>();
            HashPerformanceBonusModel = new List<HashPerformanceBonusModel>();
            HashBonusModel = new List<HashBonusModel>();
            HashPowerModel = new List<HashPowerModel>();
        }
    }

    public class CurrentSalesModel
    {
        public string MemberID { get;set; }
        public string SalesDate { get; set; }
        public float MemberUpgrade { get; set; }
        public float NewRegSalesAmount { get; set; }
        public float TotalSalesInBVAmount { get; set; }
        public float MaintainanceSalesAmount { get; set; }
        public float TopupPVSalesAmount { get; set; }
        public float TopupBonusAmount { get; set; }
        public float SponsorBonusAmount { get; set; }
        public float TotalReferralAmount { get; set; }

        public float LevelBonusAmount { get; set; }

        public float TotalLevelBonusAmount { get; set; }
        public float PairingBonusAmount { get; set; }
        public float MatchingBonusAmount { get; set; }
        public float WorldPoolBonusAmount { get; set; }
        public float TotalBonusAmount { get; set; }
        public float UniLevelAmount { get; set; }
        public float RegisterRpoint { get; set; }
        public float RegisterCRpoint { get; set; }
        public float UpgradeRpoint { get; set; }
        public float UpgradeCRpoint { get; set; }
        public string Name { get; set; }
        public string ID { get; set; }
        public string Rank { get; set; }
        public string Package { get; set; }
        public string RP { get; set; }
        public string CRP { get; set; }
        public string Total { get; set; }
        public float EPBV { get; set; }
        public float SPBV { get; set; }
        public float WRPBV { get; set; }
        public float Rpoint { get; set; }
        public float CRpoint { get; set; }



        public float TotalTradeAmount { get; set; }
        public float TotalUntradeAmount { get; set; }
        public float TotalTopUP { get; set; }
        public float TotalSalesAmount { get; set; }
        public float Hash { get; set; }
        public float HashPower { get; set; }
        public float HashPerformance { get; set; }
        public float TotalBonus { get; set; }

        public CurrentSalesModel()
        {
            NewRegSalesAmount = 0;
            MaintainanceSalesAmount = 0;
            TopupPVSalesAmount = 0;
            TotalTradeAmount = 0;
            TopupBonusAmount = 0;
            SponsorBonusAmount = 0;
            PairingBonusAmount = 0;
            MatchingBonusAmount = 0;
            WorldPoolBonusAmount = 0;
            TotalSalesInBVAmount = 0;
            UniLevelAmount = 0;
        }
    }

    public class sortOnSalesDate : IComparer<CurrentSalesModel>
    {
        public int Compare(CurrentSalesModel a, CurrentSalesModel b)
        {
            DateTime dtA = DateTime.ParseExact(a.SalesDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            DateTime dtB = DateTime.ParseExact(b.SalesDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            if (dtA.Date > dtB.Date) return 1;
            else if (dtA.Date < dtB.Date) return -1;
            else return 0;
        }
    }

    public class HashPerformanceBonusModel
    {
        public string MemberID { get; set; }
        public string SalesDate { get; set; }
        public string CashName { get; set; }
        public float SalesAmount { get; set; }
        public float TotalAmount { get; set; }
        public string AppUser { get; set; }
        public string Remarks { get; set; }

    }

    public class HashBonusModel
    {
        public string MemberID { get; set; }
        public string SalesDate { get; set; }
        public string CashName { get; set; }
        public float SalesAmount { get; set; }
        public float TotalAmount { get; set; }
        public string AppUser { get; set; }
        public string Remarks { get; set; }

    }

    public class HashPowerModel
    {
        public string MemberID { get; set; }
        public string SalesDate { get; set; }
        public string CashName { get; set; }
        public float SalesAmount { get; set; }
        public float TotalAmount { get; set; }
        public string AppUser { get; set; }
        public string Remarks { get; set; }

    }

    public class DirectSponsorModel
    {
        public string MemberID { get; set; }
        public string Fullname { get; set; }
        public string TotalDirectSponsor { get; set; }
        public List<DirectSponsorModel> TotalSponsorModel { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public DirectSponsorModel()
        {
            TotalSponsorModel = new List<DirectSponsorModel>();
        }

    }

    #endregion
}