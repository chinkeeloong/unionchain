﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Data.Objects;

namespace ECFBase.Models
{
    #region AdminPassword

    public class MemberBankModel
    {
        public int? MemberBankID { get; set; }

        public IEnumerable<SelectListItem> CountryList { get; set; }
        public string SelectedCountry { get; set; }

        public IEnumerable<SelectListItem> BankList { get; set; }
        public string SelectedBank { get; set; }

        public string Username { get; set; }
        public string SelectedCountryStraing { get; set; }
        public string BranchName { get; set; }
        public string AccountNumber { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryIC { get; set; }
        public string BeneficiaryRelationship { get; set; }
        public string BeneficiaryPhone { get; set; }
        public bool EnableEdit { get; set; }
        public bool EnableEditAll { get; set; }

        public string MemberID { get; set; }

        public MemberBankModel()
        {
            CountryList = new List<SelectListItem>();
            BankList = new List<SelectListItem>();
            EnableEdit = false;
            EnableEditAll = false;
        }
    }

    public class MemberPwdPINModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgReqUsername")]
        public string Username { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgReqCurrPwd")]
        public string CurrPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningCurrPIN")]
        public string CurrPIN { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgReqNewPwd")]
        public string NewPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningConfirmNewPIN")]
        public string NewPIN { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningConfirmNewPwd")]
        public string ConfirmNewPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningConfirmNewPIN")]
        public string ConfirmNewPIN { get; set; }

    }

    #endregion

}