﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ECFBase.Models
{
    #region CenterChartModel
    public class CenterChartModel
    {
        public List<string> MemberList { get; set; }
        public List<string> LogoList { get; set; }
        public List<string> TooltipList { get; set; }
        public IEnumerable<SelectListItem> FilteringCriteria { get; set; }
        public string SelectedFilteringCriteria { get; set; }

        public string MainUser { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningLeftTotSales")]
        public float MainLeftYJ { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningRightTotSales")]
        public float MainRightYJ { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningLeftBalSales")]
        public float MainLeftBalance { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningRightBalSales")]
        public float MainRightBalance { get; set; }

        public CenterChartModel()
        {
            MemberList = new List<string>();
            LogoList = new List<string>();
            TooltipList = new List<string>();
        }
    }
    #endregion

    public class MemberSponsorChartModel
    {

        public string FirstLevelMember { get; set; }
        public string FirstLevelLevel { get; set; }
        public string FirstLevelMemberCount { get; set; }
        public string FirstLevelMemberTotalInvest { get; set; }
        public string FirstLevelJoinedDate { get; set; }
        public string FirstLevelRanking { get; set; }
        public string FirstLevelNickName { get; set; }
        public string FirstLevelPrevGroupSales { get; set; }
        public string FirstLevelThailandPBV { get; set; }
        public string FirstLevelIntro { get; set; }
        public string FirstLevelFullname { get; set; }
        public string FirstLevelRankIcon { get; set; }

        public string SecondLevelMember { get; set; }
        public string SecondLevelMemberCount { get; set; }
        public string SecondLevelMemberTotalInvest { get; set; }
        public string SecondLevelJoinedDate { get; set; }
        public string SecondLevelRanking { get; set; }
        public string SecondLevelNickName { get; set; }
        public string SecondLevelMega { get; set; }
        public string SecondLevelPrevGroupSales { get; set; }

        public string ThirdLevelMember { get; set; }
        public string ThirdLevelMemberCount { get; set; }
        public string ThirdLevelMemberTotalInvest { get; set; }
        public string ThirdLevelJoinedDate { get; set; }
        public string ThirdLevelRanking { get; set; }
        public string ThirdLevelNickName { get; set; }
        public string ThirdLevelMega { get; set; }
        public string ThirdLevelPrevGroupSales { get; set; }

        public string Level { get; set; }
        public string Member { get; set; }
        public string Ranking { get; set; }
        public string JoinedDate { get; set; }
        public string FullName { get; set; }
        public string Intro { get; set; }
        public string RankIcon { get; set; }

        public string CloseSentence { get; set; }
        public int close { get; set; }

        public List<RankModel> RankList { get; set; }
        public IEnumerable<SelectListItem> Result { get; set; }

        public List<MemberList> resultList { get; set; }

        public List<string> LogoList { get; set; }

        public List<IntroListModel> SponsorList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public string MemberCount { get; set; }
        public bool GoingToExpended { get; set; }
        public MemberSponsorChartModel()
        {
            LogoList = new List<string>();
            resultList = new List<MemberList>();
            RankList = new List<RankModel>();
            SponsorList = new List<IntroListModel>();
        }
       

    }
    public class MemberList
            {

        public string MemberCount { get; set; }
        public bool GoingToExpended { get; set; }
        public string Level { get; set; }
        public string Member { get; set; }
        public string Ranking { get; set; }
        public string JoinedDate { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public string Upline { get; set; }
        public string Intro { get; set; }
        public string RankIcon { get; set; }
        public string Rank { get; set; }
        public string CreatedDate { get; set; }
    }
 
    public class CenterSettingModel
    {
        public string SelectedSetting { get; set; }
        public IEnumerable<SelectListItem> SettingList { get; set; }
    }

    public class EditMemberModel
    {
        public int VerificationID { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string IdentityNumber { get; set; }
        public string CellPhone { get; set; }
        public string NewCellPhone { get; set; }

        public string VerficationCode { get; set; }
        public string CurrPassword { get; set; }
        public string NewPassword { get; set; }
        public string RetypeNewPassword { get; set; }
        public string CurrPayPassword { get; set; }
        public string NewPayPassword { get; set; }
        public string RetypePayPassword { get; set; }
        public string SelectedPosition { get; set; }
        public List<SelectListItem> PositionList { get; set; }
        public string MemberEmail { get; set; }
        public string NewMemberEmail { get; set; }
        public string SelectedCountry { get; set; }
        public IEnumerable<SelectListItem> Countries { get; set; }
        public IEnumerable<SelectListItem> MobileCountries { get; set; }
        public string MobileCountryCode { get; set; }

        public string ProtraitPhotoName { get; set; }
        public string ProtraitPhotoPath { get; set; }
        public HttpPostedFileBase ProtraitPhoto { get; set; }

        public string ReversePhotoName { get; set; }
        public string ReversePhotoPath { get; set; }
        public HttpPostedFileBase ReversePhoto { get; set; }

        public string HandheldPhotoName { get; set; }
        public string HandheldPhotoPath { get; set; }
        public HttpPostedFileBase HandheldPhoto { get; set; }

        public string Status { get; set; }
        public string ICLength { get; set; }
        public string SelectedICType { get; set; }

        public string Number { get; set; }

        public EditMemberModel()
        {
            Countries = new List<SelectListItem>();
            MobileCountries = new List<SelectListItem>();
            PositionList = new List<SelectListItem>();
        }
    }

    public class PaymentModel
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public string OrderNumber { get; set; }
        public float Swallet { get; set; }
        public float TotalAmountRM { get; set; }
        public float TotalAmountUSD { get; set; }
        public float AfterDeduce { get; set; }
        public string Epin { get; set; }
        public string Status { get; set; }
        public string URL { get; set; }


    }

    public class PaymentModelKeep
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public string OrderNumber { get; set; }
        public float Swallet { get; set; }
        public float TotalAmountRM { get; set; }
        public float TotalAmountUSD { get; set; }
        public float AfterDeduce { get; set; }
        public string Epin { get; set; }
        public string Status { get; set; }
        public float XGSTUSD { get; set; }
        public float XGSTMYR { get; set; }
        public IEnumerable<SelectListItem> Product { get; set; }

        public List<EmallProductModel> EPackageProducts { get; set; }

        public PaymentModelKeep()
        {
            EPackageProducts = new List<EmallProductModel>();
        }

    }

    public class EmallProductModel
    {
        public int EQuantity { get; set; }
        public float EAmount { get; set; }
        public float EAmountMYR { get; set; }
        public float ERP { get; set; }
        public string EProductName { get; set; }

        public List<EmallProductModel> PackageProductList { get; set; }

        public EmallProductModel()
        {
            PackageProductList = new List<EmallProductModel>();
        }
       
    }

    public class RegisterNewMemberModel
    {
        public string SponsorName;
        public string Sponsor;
        public MemberModel Member { get; set; }
        public UserInfo UserInfo { get; set; }
        public string prememberrank { get; set; }
        public MemberBankModel BankModel { get; set; }

        public bool Agree { get; set; }
        public string PaymentOption { get; set; }

        #region Option
        public string SelectedOption { get; set; }
       
        #endregion

        #region Package
        public List<PackageModel> Packages { get; set; }
        public string SelectedPackage { get; set; }
        public string SelectedPackageName { get; set; }
        #endregion

        public string AccNoStar { get; set; }
        public string AccNoGold { get; set; }
        public string AccNoVIP { get; set; }
        public string AccNoSVIP { get; set; }
        public string AccNo { get; set; }

        public string country { get; set; }

        public string CurrentUserCountry { get; set; }

        #region packageProduct
        public List<PackageModel> PackageProducts { get; set; }
        public float PackageProductsAmt { get; set; }
        public string SelectedPackageProducts { get; set; }
        #endregion

        #region MaintainProduct
        public List<PackageModel> MaintainProducts { get; set; }
        public float MaintainProductsAmt { get; set; }
        public string SelectedMaintainProducts { get; set; }
        #endregion

        #region OfferPackage
        public List<PackageModel> OfferPackages { get; set; }
        public float OfferPackagesAmt { get; set; }
        public string SelectedOfferPackage { get; set; }
        #endregion

        public string MegaPack { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }
        public string SelectedCountry { get; set; }

        public string CurrentPackageName { get; set; }
        public string SelectedCountryString { get; set; }

        public IEnumerable<SelectListItem> DeliveryMode { get; set; }
        public string SelectedDeliveryMode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningSecurityPin")]
        [DataType(DataType.Password)]
        public string SponsorSecurityPin { get; set; }
        public string EncryptedSponsorSecurityPin { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgUpUser")]
        public string UpUsername { get; set; }
        public string ReferralUsername { get; set; }
        public string SponsorFullName { get; set; }
        public string UsernameID { get; set; }
        public string MemberID { get; set; }
        public string UpLineFullName { get; set; }

       // [RegularExpression(@"^.{5,}$", ErrorMessage = "Minimum 5 characters required")]
        public string LoginUsername { get; set; }
        public string DisplayName { get; set;  }
        public List<SelectListItem> Locations { get; set; }
        public string SelectedLocation { get; set; }
        public string SelectedLocationString { get; set; }

        public IEnumerable<SelectListItem> RankList { get; set; }
        public string SelectedRank { get; set; }

        public IEnumerable<SelectListItem> Banks { get; set; }
        public string SelectedBank { get; set; }

        public string UplineIC { get; set; }
        public string UplineID { get; set; }

        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public string BankTransferName { get; set; }
        public string UserBankSwiftCode { get; set; }

        public string SelectedIntroPosition { get; set; }

        public string SelectedPaymentOption { get; set;  }

        public string SponsorIC { get; set; }
        public string RPAmount { get; set; }
        public string APAmount { get; set; }

        public string mail { get; set; }

        public string LanguageCode { get; set; }
        public string Remark { get; set; }

        public float AdditionalRD { get; set; }
        public string TotalRD { get; set; }

        public string TotalRP { get; set; }
        public string TotalCRP { get; set; }

        public string TOtalRPStar { get; set; }
        public string TOtalRPGold { get; set; }
        public string TOtalRPVIP { get; set; }
        public string TOtalRPSVIP { get; set; }

        public string introid { get; set; }

        public float TotalEastDeliveryCharges { get; set; }

        public bool Packagechecking { get; set; }

        #region sponsor
        public float SponsorEWallet { get; set; }
        public string SponsorCurrency { get; set; }
        #endregion

        public RegisterNewMemberModel()
        {
            Member = new MemberModel();
            UserInfo = new UserInfo();
            Packages = new List<PackageModel>();
            OfferPackages = new List<PackageModel>();
            MaintainProducts = new List<PackageModel>();
            PackageProducts = new List<PackageModel>();
            Locations = new List<SelectListItem>();
            RankList = new List<SelectListItem>();
            BankModel = new MemberBankModel();
            Agree = false;
        }

        public IEnumerable<SelectListItem> CategoryList { get; set; }
        public List<ProductModel> ProductList { get; set; }
        public string TypeOfProduct { get; set; }
        public string SelectedCategory { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }


    }

    public class PackageModel
    {
        public string Rp { get; set; }
        public string Cp { get; set; }
        public string Pp { get; set; }
        public string Ap { get; set; }
        public string ShowRp { get; set; }
        public string ShowCp { get; set; }
        public string ShowPp { get; set; }
        public int PackageID { get; set; }
        public int PPUNIT { get; set; }
        public int TOTALPPUNIT { get; set; }
        public string PackageCode { get; set; }
        public string PackageDescription { get; set; }
        public string PackageName { get; set; }
        public float Investment { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }
        public string RD { get; set; }
        public float Delivery { get; set; }
        public float RetailPrice { get; set; }
        public float MemberPrice { get; set; }
        public float Total { get; set; }
        public int Quantity { get; set; }
        public int AdditionalRD { get; set; }
        public bool IsBronzePackage { get; set; }
        public float Share { get; set; }
        public string Country { get; set; }
        public string CountryImage { get; set; }
        public float MemberPriceLocal { get; set; }
        public float RetailPriceLocal { get; set; }
        public float PV { get; set; }
        public float EastDeliveryCharges { get; set; }

        public List<ProductSetupModel> PackageProductList { get; set; }

        public PackageModel()
        {
            PackageProductList = new List<ProductSetupModel>();
        }
    }

    public class MaintenancePackages
    {
        public List<PackageModel> Packages { get; set; }
        public DateTime BonusEntitlement { get; set; }
        public string SelectedPackage { get; set; }
        public string Pin { get; set; }
        public string Currency { get; set; }
        public float PvWallet { get; set; }
        public float ProductDividen { get; set; }
        public UserInfo UserInfo { get; set; }
        public string SelectedCountry { get; set; }

        public IEnumerable<SelectListItem> DeliveryMode { get; set; }
        public string SelectedDeliveryMode { get; set; }
        public string Username { get; set; }
        public string LanguageCode { get; set; }

        public MaintenancePackages()
        {
            Packages = new List<PackageModel>();
            UserInfo = new UserInfo();
        }
    }

    public class UserInfo
    {
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]

        public IEnumerable<SelectListItem> Days { get; set; }
        public IEnumerable<SelectListItem> Months { get; set; }
        public IEnumerable<SelectListItem> Years { get; set; }

        public string SelectedMonth { get; set; }
        public string SelectedYears { get; set; }
        public string SelectedDay { get; set; }
        public DateTime DOB { get; set; }

        public int ID { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningCountry")]
        public string SelectedCountry { get; set; }

        public string SelectedCountryString { get; set; }

        public string SelectedUpgradePackageString { get; set; }

        public IEnumerable<SelectListItem> Province { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningProvince")]
        public string SelectedProvince { get; set; }

        public string SelectedProvinceString { get; set; }

        public IEnumerable<SelectListItem> City { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqCity")]
        public string SelectedCity { get; set; }

        public string SelectedCityString { get; set; }

        public IEnumerable<SelectListItem> District { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqDistrict")]
        public string SelectedDistrict { get; set; }

        public IEnumerable<SelectListItem> Stockist { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqStockist")]
        public string SelectedStockist { get; set; }

        public IEnumerable<SelectListItem> Gender { get; set; }
        public string SelectedGender { get; set; }

        public string BeneficiaryName { get; set; }

        public IEnumerable<SelectListItem> Relationship { get; set; }
        public string SelectedRelationship { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        public string Skype { get; set; }

        public string MemberID { get; set; }
        public string Username { get; set; }


        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningCellPhone")]
        public string CellPhone { get; set; }
        public string Sms { get; set; }
        public string Fax { get; set; }
        public string MobileCountryCode { get; set; }
        public string ICLength { get; set; }
        public string SelectedICType { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningIC")]
        public string IC { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningNickName")]
        public string Nickname { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningShipName")]
        public string ShipName { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningShipAddress")]
        public string ShipAddress  { get; set; }
        public string ShipAddress1 { get; set; }
        public string ShipAddress2 { get; set; }
        public string ShipAddress3 { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqShipCity")]
        public string ShipCity { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningShipPostCode")]
        public string ShipPostcode { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningShipPhoneNo")]
        public string ShipPhoneNo { get; set; }

    }

    public class MemberAutoMaintainModel
    {
        public string Number { get; set; }
        public string MemberId { get; set; }
        public string Name { get; set; }
        public string Package { get; set; }
        public string Rank { get; set; }
        public string Sponsor { get; set; }
        public DateTime JoinedDate { get; set; }
        public DateTime AutoMaintainDate { get; set; }
    }

    public class PaginationMemberAutoMaintainModel
    {
        public string SearchMember { get; set; }

        public List<MemberAutoMaintainModel> MemberMaintainList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationMemberAutoMaintainModel()
        {
            MemberMaintainList = new List<MemberAutoMaintainModel>();
        }
    }

        public class PaginationIntroListModel
    {
        public List<IntroListModel> SponsorList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public PaginationIntroListModel()
        {
            SponsorList = new List<IntroListModel>();
        }
    }

    public class IntroListModel
    {
        public string No { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Sponsor { get; set; }
        public string Upline { get; set; }
        public string Level { get; set; }
        public string Package { get; set; }
        public string MonthlyFee { get; set; }
        public string TopUpAmount { get; set; }
        public string CreatedDate { get; set; }
        public string Rank { get; set; }
        public string FullName { get; set; }
        //public float Package { get; set; }
        //[DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        //public float Wallet { get; set; }
        public string RankIcon { get; set; }

        
    }
}