﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Data.Objects;

namespace ECFBase.Models
{
    #region MasterSetup

    #region Language
    public class LanguageSetupModel
    {
        public int? LanguageId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgLanguageCodeInvalid")]
        public string LanguageCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgLanguageNameInvalid")]
        public string LanguageName { get; set; }
    }

    public class PaginationLanguageSetupModel
    {
        public List<LanguageSetupModel> LanguageList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationLanguageSetupModel()
        {
            LanguageList = new List<LanguageSetupModel>();
        }
    }
    #endregion

    #region Country

    public class CountrySetupModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public int? CountryId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningCountryCode")]
        public string CountryCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningCountryName")]
        public string CountryName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningSequenceNo")]
        public int SequenceNumber { get; set; }

        public string Status { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "ReqMobileCode")]
        public string MobileCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "ReqICLength")]
        public string ICLength { get; set; }
        public string ICType { get; set; }
        public string SelectedICType { get; set; }

        public string ICTypeCode { get; set; }

        public IEnumerable<SelectListItem> ICTypeSelection { get; set; }
        public string FlagImageName { get; set; }
        public string FlagImagePath { get; set; }
        public HttpPostedFileBase FlagImage { get; set; }

        public List<TextLanguageModel> NameLanguageList { get; set; }

        public CountrySetupModel()
        {
            LanguageList = new List<SelectListItem>();
            NameLanguageList = new List<TextLanguageModel>();
        }
    }

    public class PaginationCountrySetupModel
    {
        public List<CountrySetupModel> CountryList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationCountrySetupModel()
        {
            CountryList = new List<CountrySetupModel>();
        }
    }

    #endregion

    public class CashName
    {
        public string cashname { get; set; }
        public string cashnamedisplay { get; set; }
    }

    public class Status
    {
        public string StatusX { get; set; }
        public string StatusDisplay { get; set; }
    }


    #region Rank

    public class RankSetupModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public int? RankId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningCountryCode")]
        public string RankCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningCountryName")]
        public string RankName { get; set; }

        public List<TextLanguageModel> NameLanguageList { get; set; }

        public RankSetupModel()
        {
            LanguageList = new List<SelectListItem>();
            NameLanguageList = new List<TextLanguageModel>();
        }
    }

    public class RankModel
    {
        public int? RankID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningRankCode")]
        public string RankCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "ReqIRankName")]
        public string RankName { get; set; }

        public string Hash { get; set; }
        public string PerformanceHash { get; set; }
        public string ManagementHash { get; set; }
        public string FirstPairingBonus { get; set; }
        public string FirstPairingMaxDaily { get; set; }
        public string SecondPairingBonus { get; set; }
        public string SecondPairingMaxDaily { get; set; }
        public string MinimumDailyPairing { get; set; }
        public string MaxOfferPackage { get; set; }
        public string SponsorBonus { get; set; }
        public float Referral { get; set; }
        public float Pairing { get; set; }
        public string MaxWeek { get; set; }
        public float PairingEUnit { get; set; }

        public float PV { get; set; }


        public string MaxSplit { get; set; }
        public string MaxSell { get; set; }
        public string DaysSell { get; set; }

        public string ImageIcon { get; set; }

        public string RankIconName { get; set; }
        public string RankIconPath { get; set; }
        public float RankAmount { get; set; }

        public HttpPostedFileBase RankIcon { get; set; }

        public List<ImageFile> ImageList { get; set; }
        public RankModel()
        {
            ImageList = new List<ImageFile>();
        }
        public class ImageFile
        {
            public string FileName { get; set; }
            public string FileTitle { get; set; }
            public string FileSize { get; set; }
        }
    }

    public class PaginationRankSetupModel
    {
        public List<RankSetupModel> RankList { get; set; }
        public List<RankModel> CategoryList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public List<ImageFile> ImageList { get; set; }

        public PaginationRankSetupModel()
        {
            RankList = new List<RankSetupModel>();
            CategoryList = new List<RankModel>();
            ImageList = new List<ImageFile>();
        }
        public class ImageFile
        {
            public string FileName { get; set; }
            public string FileTitle { get; set; }
            public string FileSize { get; set; }
        }
    }

    #endregion

    #region Province

    public class ProvinceSetupModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public int? ProvinceId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqProvinceCode")]
        public string ProvinceCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqProvinceName")]
        public string ProvinceName { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }
        public string SelectedCountry { get; set; }

        public List<TextLanguageModel> NameLanguageList { get; set; }

        public ProvinceSetupModel()
        {
            LanguageList = new List<SelectListItem>();
            NameLanguageList = new List<TextLanguageModel>();
        }
    }

    public class PaginationProvinceSetupModel
    {
        public string SelectedCountry { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }

        public List<ProvinceSetupModel> ProvinceList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationProvinceSetupModel()
        {
            CountryList = new List<SelectListItem>();
            ProvinceList = new List<ProvinceSetupModel>();
        }
    }

    #endregion

    #region City

    public class CitySetupModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public string SelectedCountry { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }

        public string SelectedProvince { get; set; }
        public IEnumerable<SelectListItem> ProvinceList { get; set; }

        public int? CityID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqCityCode")]
        public string CityCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqCityName")]
        public string CityName { get; set; }

        public List<TextLanguageModel> NameLanguageList { get; set; }

        public CitySetupModel()
        {
            LanguageList = new List<SelectListItem>();
            NameLanguageList = new List<TextLanguageModel>();
        }
    }

    public class PaginationCitySetupModel
    {
        public string SelectedCountry { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }

        public string SelectedProvince { get; set; }
        public IEnumerable<SelectListItem> ProvinceList { get; set; }

        public List<CitySetupModel> CityList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationCitySetupModel()
        {
            CountryList = new List<SelectListItem>();
            ProvinceList = new List<SelectListItem>();
            CityList = new List<CitySetupModel>();
        }
    }

    #endregion

    #region District

    public class DistrictSetupModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public string SelectedCountry { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }

        public string SelectedProvince { get; set; }
        public IEnumerable<SelectListItem> ProvinceList { get; set; }

        public string SelectedCity { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }

        public int? DistrictID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqDistrictCode")]
        public string DistrictCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqDistrictName")]
        public string DistrictName { get; set; }

        public List<TextLanguageModel> NameLanguageList { get; set; }

        public DistrictSetupModel()
        {
            LanguageList = new List<SelectListItem>();
            NameLanguageList = new List<TextLanguageModel>();
        }
    }

    public class PaginationDistrictSetupModel
    {
        public string SelectedCountry { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }

        public string SelectedProvince { get; set; }
        public IEnumerable<SelectListItem> ProvinceList { get; set; }

        public string SelectedCity { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }

        public List<DistrictSetupModel> DistrictList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationDistrictSetupModel()
        {
            CountryList = new List<SelectListItem>();
            ProvinceList = new List<SelectListItem>();
            CityList = new List<SelectListItem>();
            DistrictList = new List<DistrictSetupModel>();
        }
    }

    #endregion

    #region Stockist

    public class StockistSetupModel
    {
        public string SelectedUsername { get; set; }

        public string StockistFullName { get; set; }
        public string StockistUserName { get; set; }
    }

    #endregion

    #region Bank

    public class PaginationBankSetupModel
    {
        public List<BankSetupModel> BankList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationBankSetupModel()
        {
            BankList = new List<BankSetupModel>();
        }
    }

    public class BankSetupModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public string SelectedCountryCode { get; set; }

        public int? BankID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqBankCode")]
        public string BankCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqBankName")]
        public string BankName { get; set; }

        public string CountryName { get; set; }

        public float BankCharges { get; set; }
        public float MaximumBankCharges { get; set; }

        public List<TextLanguageModel> NameLanguageList { get; set; }

        public BankSetupModel()
        {
            LanguageList = new List<SelectListItem>();
            NameLanguageList = new List<TextLanguageModel>();
            CountryList = new List<SelectListItem>();
        }
    }

    #endregion

    #region Courier Company

    //public class CourierCompanySetupModel
    //{
    //    public string SelectedLanguage { get; set; }
    //    public IEnumerable<SelectListItem> LanguageList { get; set; }

    //    public int? CourierId { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqCourierCode")]
    //    public string CourierCode { get; set; }

    //    public string CourierCompany { get; set; }

    //    [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqContactPerson")]
    //    public string ContactPerson { get; set; }

    //    public string ContactNumber { get; set; }
    //    public string ContactNumber2 { get; set; }
    //    public string Website { get; set; }

    //    public List<TextLanguageModel> NameLanguageList { get; set; }

    //    public CourierCompanySetupModel()
    //    {
    //        LanguageList = new List<SelectListItem>();
    //        NameLanguageList = new List<TextLanguageModel>();
    //    }
    //}

    //public class PaginationCourierCompanySetupModel
    //{
    //    public List<CourierCompanySetupModel> CourierCompanyList { get; set; }
    //    public IEnumerable<SelectListItem> Pages { get; set; }

    //    public PaginationCourierCompanySetupModel()
    //    {
    //        CourierCompanyList = new List<CourierCompanySetupModel>();
    //    }
    //}

    #endregion

    #region Currency

    public class PaginationCurrencySetupModel
    {
        public List<CurrencySetupModel> CurrencyList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationCurrencySetupModel()
        {
            CurrencyList = new List<CurrencySetupModel>();
        }
    }

    public class CurrencySetupModel
    {
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public string SelectedCountryCode { get; set; }

        public int? CurrencyId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqCurrencyCode")]
        public string CurrencyCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqCurrencyName")]
        public string CurrencyName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqCurrencyBuy")]
        public double CurrencyBuy { get; set; }

        public string StringCurrencyBuy { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqCurrencySell")]
        public double CurrencySell { get; set; }

        public string StringCurrencySell { get; set; }

        public string Status { get; set; }

        public string CountryName { get; set; }

        public string CountryImage { get; set; }

        public string CountryCode { get; set; }

        public CurrencySetupModel()
        {
            CountryList = new List<SelectListItem>();
        }
    }

    #endregion


    public class PaginationUtilitySetupModel
    {
        public List<UtilitySetupModel> UtilityList { get; set; }
        public MemberModel Member { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public IEnumerable<SelectListItem> Countries { get; set; }
        public string SelectedCountry { get; set; }

        public PaginationUtilitySetupModel()
        {
            UtilityList = new List<UtilitySetupModel>();
            Countries = new List<SelectListItem>();
        }
    }

    public class UtilitySetupModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public int? UtilityID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqCatCode")]
        public string UtilityCode { get; set; }
        public string ProcessingTime { get; set; }
        public string Digit { get; set; }
        public string UtilityImageName { get; set; }
        public string UtilityImagePath { get; set; }
        public HttpPostedFileBase UtilityImage { get; set; }
        public IEnumerable<SelectListItem> Countries { get; set; }

        public string MinAmount { get; set; }
        public string MaxAmount { get; set; }

        public string UtilityName { get; set; }
        public string UtilityDescription { get; set; }
        public IEnumerable<SelectListItem> UtilityList { get; set; }


        public string Status { get; set; }
        public int StatusID { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        public string SelectedStatusList { get; set; }

        public List<UtilityModel> BillList { get; set; }
        public string SelectedUtilityList { get; set; }
        public string BillAmount { get; set; }
        public string BillAccount { get; set; }
        public string MobileNumber { get; set; }
        public string TotalQP { get; set; }
        public int? BillID { get; set; }
        public string CreatedDate { get; set; }
        public string CurrentPrice { get; set; }
        public string AvailableUNS { get; set; }
        public string UNSAmount { get; set; }
        public string Fees { get; set; }
        public string Currency { get; set; }
        public bool Mobile { get; set; }
        public string RealNameVerificationFlag { get; set; }
        public int TotalDigit { get; set; }

        public IEnumerable<SelectListItem> Pages { get; set; }
        public List<TextLanguageModel> NameLanguageList { get; set; }
        public List<TextLanguageModel> DescriptionLanguageList { get; set; }
        public bool MobileBill { get; set; }
        public string SelectedCountry { get; set; }
       

        public UtilitySetupModel()
        {
            BillList = new List<UtilityModel>();
            DescriptionLanguageList = new List<TextLanguageModel>();
            NameLanguageList = new List<TextLanguageModel>();
             
        }


    }


    public class UtilityModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public int? UtilityID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "reqCatCode")]
        public string UtilityCode { get; set; }
        public string ProcessingTime { get; set; }
        public string Digit { get; set; }
        public string UtilityImageName { get; set; }
        public string UtilityImagePath { get; set; }
        public HttpPostedFileBase UtilityImage { get; set; }

        public string UtilityName { get; set; }
        public string UtilityDescription { get; set; }
        public IEnumerable<SelectListItem> UtilityList { get; set; }

        public List<UtilityModel> BillList { get; set; }
        public string SelectedUtilityList { get; set; }
        public string BillAmount { get; set; }
        public string BillAccount { get; set; }
        public string MobileNumber { get; set; }
        public string TotalQP { get; set; }
        public int? BillID { get; set; }
        public string CreatedDate { get; set; }
        public string PaymentDate { get; set; }
        public string Status { get; set; }
        public string CurrentPrice { get; set; }
        public string AvailableUNS { get; set; }
        public string UNSAmount { get; set; }
        public string Fees { get; set; }
        public string Currency { get; set; }
        public bool Mobile { get; set; }
        public string MemberID { get; set; }
        public string MemberName { get; set; }
        public int TotalDigit { get; set; }

        public IEnumerable<SelectListItem> Pages { get; set; }
        public List<TextLanguageModel> NameLanguageList { get; set; }
        public List<TextLanguageModel> DescriptionLanguageList { get; set; }
        public bool MobileBill { get; set; }

    }

    //public class WorldReward
    //{
    //    public string CurrentSubAcc { get; set; }
    //    public string NewSubAcc { get; set; }
    //    public string Rank0101 { get; set; }
    //    public string Rank01Total { get; set; }
    //    public string Rank0201 { get; set; }
    //    public string Rank0202 { get; set; }
    //    public string Rank0203 { get; set; }
    //    public string Rank0204 { get; set; }
    //    public string Rank0205 { get; set; }
    //    public string Rank02Total { get; set; }
    //    public string Rank0301 { get; set; }
    //    public string Rank0302 { get; set; }
    //    public string Rank0303 { get; set; }
    //    public string Rank0304 { get; set; }
    //    public string Rank0305 { get; set; }
    //    public string Rank0306 { get; set; }
    //    public string Rank0307 { get; set; }
    //    public string Rank0308 { get; set; }
    //    public string Rank0309 { get; set; }
    //    public string Rank0310 { get; set; }
    //    public string Rank03Total { get; set; }
    //    public string Rank0401 { get; set; }
    //    public string Rank0402 { get; set; }
    //    public string Rank0403 { get; set; }
    //    public string Rank0404 { get; set; }
    //    public string Rank0405 { get; set; }
    //    public string Rank0406 { get; set; }
    //    public string Rank0407 { get; set; }
    //    public string Rank0408 { get; set; }
    //    public string Rank0409 { get; set; }
    //    public string Rank0410 { get; set; }
    //    public string Rank0411 { get; set; }
    //    public string Rank0412 { get; set; }
    //    public string Rank0413 { get; set; }
    //    public string Rank0414 { get; set; }
    //    public string Rank0415 { get; set; }
    //    public string Rank0416 { get; set; }
    //    public string Rank0417 { get; set; }
    //    public string Rank0418 { get; set; }
    //    public string Rank0419 { get; set; }
    //    public string Rank0420 { get; set; }
    //    public string Rank0421 { get; set; }
    //    public string Rank0422 { get; set; }
    //    public string Rank0423 { get; set; }
    //    public string Rank0424 { get; set; }
    //    public string Rank0425 { get; set; }
    //    public string Rank0426 { get; set; }
    //    public string Rank0427 { get; set; }
    //    public string Rank0428 { get; set; }
    //    public string Rank0429 { get; set; }
    //    public string Rank0430 { get; set; }
    //    public string Rank0431 { get; set; }
    //    public string Rank0432 { get; set; }
    //    public string Rank0433 { get; set; }
    //    public string Rank0434 { get; set; }
    //    public string Rank0435 { get; set; }
    //    public string Rank0436 { get; set; }
    //    public string Rank0437 { get; set; }
    //    public string Rank0438 { get; set; }
    //    public string Rank0439 { get; set; }
    //    public string Rank0440 { get; set; }
    //    public string Rank0441 { get; set; }
    //    public string Rank0442 { get; set; }
    //    public string Rank0443 { get; set; }
    //    public string Rank0444 { get; set; }
    //    public string Rank0445 { get; set; }
    //    public string Rank0446 { get; set; }
    //    public string Rank0447 { get; set; }
    //    public string Rank0448 { get; set; }
    //    public string Rank0449 { get; set; }
    //    public string Rank0450 { get; set; }
    //    public string Rank04Total { get; set; }


    //}

    //public class PaginationBranchUniLVBonusSetupModel
    //{
    //    public List<BranchUniLVBonusSetupModel> CurrencyList { get; set; }
    //    public IEnumerable<SelectListItem> Pages { get; set; }

    //    public PaginationBranchUniLVBonusSetupModel()
    //    {
    //        CurrencyList = new List<BranchUniLVBonusSetupModel>();
    //    }
    //}

    //public class BranchUniLVBonusSetupModel
    //{
    //    public bool Flash { get; set; }
    //    public string ShopName { get; set; }
    //    public int Bonus { get; set; }
    //    public string ID { get; set; }
    //    public string Location { get; set; }
    //    public int UnicID { get; set; }

    //}

    //public class SalesOrderReentry
    //{
    //    public float Swallet { get; set; }
    //    public string Username { get; set; }
    //    public string FullName { get; set; }
    //    public string IC { get; set; }
    //    public string SponsorID { get; set; }
    //    public string SponsorName { get; set; }
    //    public string BillNo { get; set; }
    //    public string BillDate { get; set; }
    //    public float XGSTUSD { get; set; }
    //    public float XGSTRM { get; set; }
    //    public float GSTUSD { get; set; }
    //    public float GSTRM { get; set; }
    //    public IEnumerable<SelectListItem> Pages { get; set; }

    //    public List<SalesOder> SalesOrderList { get; set; }
    //    public SalesOrderReentry()
    //    {
    //        SalesOrderList = new List<SalesOder>();
    //    }
    //}

    //public class SalesOder
    //{
    //    public string Username { get; set; }
    //    public string FullName { get; set; }
    //    public string IC { get; set; }
    //    public string SponsorID { get; set; }
    //    public string SponsorName { get; set; }
    //    public string BillNo { get; set; }
    //    public string BillDate { get; set; }
    //    public float XGSTUSD { get; set; }
    //    public float XGSTRM { get; set; }
    //    public float GSTUSD { get; set; }
    //    public float GSTRM { get; set; }
    //    public string PaymentMode { get; set; }

    //}


    #region Product

    public class PaginationCategorySetupModel
    {
        public List<CategorySetupModel> CategoryList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationCategorySetupModel()
        {
            CategoryList = new List<CategorySetupModel>();
        }
    }

    public class CategorySetupModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public int? CategoryID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningCatCode")]
        public string CategoryCode { get; set; }
        public string CategoryImagePath { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }

        public List<TextLanguageModel> NameLanguageList { get; set; }
        public List<TextLanguageModel> DescriptionLanguageList { get; set; }

        public CategorySetupModel()
        {
            LanguageList = new List<SelectListItem>();
            NameLanguageList = new List<TextLanguageModel>();
            DescriptionLanguageList = new List<TextLanguageModel>();
        }
    }

    public class PaginationPaymentModeSetupModel
    {
        public List<PaymentModeSetupModel> CategoryList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationPaymentModeSetupModel()
        {
            CategoryList = new List<PaymentModeSetupModel>();
        }
    }

    public class PaymentModeSetupModel
    {
        public int? PayModeID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningPaymodeType")]
        public string PayModeType { get; set; }
    }

    public class PaginationProductSetupModel
    {
        public List<ProductSetupModel> ProductList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationProductSetupModel()
        {
            ProductList = new List<ProductSetupModel>();
        }
    }

    public class ProductSetupModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }

        public IEnumerable<SelectListItem> CountryList { get; set; }
        public string CountryName { get; set; }

        public int? ProductID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningProdCode")]
        public string ProductCode { get; set; }

        public string ProductName { get; set; }
        public string ProductDescription { get; set; }

        public List<TextLanguageModel> NameLanguageList { get; set; }
        public List<TextLanguageModel> DescriptionLanguageList { get; set; }

        public IEnumerable<SelectListItem> CategoryList { get; set; }
        public string SelectedCategory { get; set; }

        public IEnumerable<SelectListItem> Rank { get; set; }
        public string SelectedRank { get; set; }

        public int ProductQuantity { get; set; }
        public int IncreaseProductQuantity { get; set; }
        public string ProductURL { get; set; }


        public string RetailPrice { get; set; }
        public string CostPrice { get; set; }
        public string ProductBV { get; set; }



        public string ProductImageName { get; set; }
        public string ProductImagePath { get; set; }
        public HttpPostedFileBase ProductImage { get; set; }

        public int PPUNIT { get; set; }

        public float? ProductPV { get; set; }

        public float? OriPrice { get; set; }
        public float? SellingPrice { get; set; }

        public float? Freight { get; set; }
        public float? SubFreight { get; set; }

        public float? TotalPrice { get; set; }

        public float? AddtionalRD { get; set; }

        public float share { get; set; }

        public bool IsBronzePackage { get; set; }

        public ProductSetupModel()
        {
            LanguageList = new List<SelectListItem>();
            NameLanguageList = new List<TextLanguageModel>();
            DescriptionLanguageList = new List<TextLanguageModel>();
            CategoryList = new List<SelectListItem>();
        }
    }

    public class DividenModel
    {
        public int Month { get; set; }
        public float CashDividen { get; set; }
        public float ProductDividen { get; set; }
    }

    public class PackageSetupModel
    {
        public string SelectedLanguage { get; set; }
        public IEnumerable<SelectListItem> LanguageList { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public IEnumerable<SelectListItem> ValidityList { get; set; }

        public IEnumerable<SelectListItem> RankList { get; set; }

        public int? PackageID { get; set; }

        public string PackageType { get; set; }
        public float iwalletAmount { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningPkgCode")]
        public string PackageCode { get; set; }

        public string PackageName { get; set; }
        public string PackageDescription { get; set; }
        public float DeliveryCharge { get; set; }
        public float TempDeliveryCharge { get; set; }
        public float TempPackageInvestment { get; set; }
        
        public string ROIMaxAmount { get; set; }
        public string ROIPoint { get; set; }
        public float RefEUnit { get; set; }
        public float Eunit { get; set; }
        public float EunitB { get; set; }
        public float Referral { get; set; }
        public float Pairing { get; set; }
        public float MaxWeek { get; set; }
        public float PairingEUnit { get; set; }
        public string GoldMindPercentage { get; set; }
        public string GoldMindDays { get; set; }

        //public float CompanyPoint { get; set; }
        //public float OCTUNIT { get; set; }
        public float ShareSplit { get; set; }
        //public float OCTOrdinary { get; set; }

        public float RetailPrice { get; set; }
        public float TempRetailPrice { get; set; }
        public float MemberPrice { get; set; }
        public float TempMemberPrice { get; set; }
        public float Share { get; set; }


        public float TotalCharge { get; set; }
        public string Validity { get; set; }
        public int TotalSetOfProduct { get; set; }
        public int MaxQuantityForPromotion { get; set; }
        public int DisplaySequence { get; set; }

        public bool MegaPack { get; set; }

        public string Option1 { get; set; }
        public string Option2 { get; set; }

        [RegularExpression(@"^[0-9]*(?:\.[0-9]*)?$", ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgInvalidAmt")]
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningPkgInvest")]
        [Range(0, int.MaxValue, ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningPkgInvest")]
        public float PackageInvestment { get; set; }
        public float PackagePV { get; set; }
        public int MaxPairingPerDay { get; set; }

        public List<TextLanguageModel> NameLanguageList { get; set; }
        public List<TextLanguageModel> DescriptionLanguageList { get; set; }

        public List<CurrencyModel> CurrencyList { get; set; }

        public List<ProductSetupModel> ProductList { get; set; }

        public List<DividenModel> DividenList { get; set; }

        public IEnumerable<SelectListItem> CategoryList { get; set; }
        public string SelectedCategory { get; set; }

        public IEnumerable<SelectListItem> ProdList { get; set; }
        public string SelectedProd { get; set; }

        public IEnumerable<SelectListItem> MultipleAccountList { get; set; }
        public string SelectedAccountNo { get; set; }

        public int PackageQuantity { get; set; }
        public int ProductQuantity { get; set; }

        public int SelectedBonusEntitledMonth { get; set; }
        public IEnumerable<SelectListItem> EntitledMonthList { get; set; }

        public string Account { get; set; }
        public int MaxPairing { get; set; }
        public string ValidityStart { get; set; }
        public string ValidityEnd { get; set; }
        public string Iwallet { get; set; }
        public string CountryName { get; set; }
        public string RankName { get; set; }
        public string ProductImageName { get; set; }
        public string ProductImagePath { get; set; }
        public HttpPostedFileBase ProductImage { get; set; }
        public bool IsQualifyDividen { get; set; }

        public IEnumerable<SelectListItem> CashDirectionList { get; set; }
        public string SelectedCashDividenDirection { get; set; }

        public PackageSetupModel()
        {
            LanguageList = new List<SelectListItem>();
            NameLanguageList = new List<TextLanguageModel>();
            DescriptionLanguageList = new List<TextLanguageModel>();
            ProductList = new List<ProductSetupModel>();
            EntitledMonthList = new List<SelectListItem>();
            CountryList = new List<SelectListItem>();
            CurrencyList = new List<CurrencyModel>();
            RankList = new List<SelectListItem>();
            DividenList = new List<DividenModel>();
            CashDirectionList = new List<SelectListItem>();
            ValidityList = new List<SelectListItem>();
        }
    }

    public class PaginationPackageSetupModel
    {
        public string SelectedCountry { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public List<PackageSetupModel> PackageList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public string PacketType { get; set; }

        public PaginationPackageSetupModel()
        {
            PackageList = new List<PackageSetupModel>();
            CountryList = new List<SelectListItem>();
        }
    }

    #endregion

    #region PackageMovement
    public class PackageMovement
    {
        public string Username { get; set; }
        public string PackageCode { get; set; }
        public string FlowID { get; set; }
        public int MoveIn { get; set; }
        public int MoveOut { get; set; }
        public string Cashname { get; set; }
        public int Balance { get; set; }
        public string AppUser { get; set; }
        public string AppOther { get; set; }
        public string AppNumber { get; set; }
        public string Date { get; set; }
        public string MovementNumber { get; set; }
        public string Category { get; set; }

        public PackageMovement()
        {

        }
    }

    public class PaginationPackageMovement
    {
        public List<PackageMovement> PackageList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }
        public string PackageCode { get; set; }

        public PaginationPackageMovement()
        {
            PackageList = new List<PackageMovement>();
            Pages = new List<SelectListItem>();
        }
    }
    #endregion

    #region AdminPassword

    public class AdminPasswordModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningUsername")]
        public string Username { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "msgReqNewPwd")]
        public string NewPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningNewPIN")]
        public string NewPin { get; set; }
    }
    #endregion

    #region General Settings

    public class ParameterModel
    {
        public string ParameterType { get; set; }
        public List<GeneralSettingModel> SettingList { get; set; }

        public ParameterModel()
        {
            SettingList = new List<GeneralSettingModel>();
        }
    }

    #endregion

    #region Sales Delivery Mode
    public class PaginationSalesDeliveryModel
    {
        public List<SalesDeliveryModel> SalesDeliveryList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationSalesDeliveryModel()
        {
            SalesDeliveryList = new List<SalesDeliveryModel>();
        }
    }

    public class SalesDeliveryModel
    {
        public int SalesDeliveryID { get; set; }
        public string Number { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningDeliveryMethod")]
        public string DeliveryMethod { get; set; }
    }
    #endregion

    #region Admin - Security
    public class PaginationAdminModel
    {
        public List<AdminModel> AdminList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationAdminModel()
        {
            AdminList = new List<AdminModel>();
        }
    }

    public class AdminModel
    {
        public bool Premember { get; set; }
        public int UserID { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningUsername")]
        public string AdminUserName { get; set; }
        public bool AdminChangePassword { get; set; }
        public bool AdminChangePIN { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningPwd")]
        public string AdminPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningPIN")]
        public string AdminPIN { get; set; }
        public string AdminCurrentPassword { get; set; }
        public string AdminNewPassword { get; set; }
        public string AdminConfirmNewPassword { get; set; }
        public string AdminCurrentPIN { get; set; }
        public string AdminNewPIN { get; set; }
        public string AdminConfirmNewPIN { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Member), ErrorMessageResourceName = "warningFullname")]
        public string AdminFirstName { get; set; }
        public List<AdminAccessRightModel> AdminAccessRight { get; set; }
        public List<ImageFile> ImageList { get; set; }


        public string RankIconName { get; set; }
        public string RankIconPath { get; set; }
        public HttpPostedFileBase RankIcon { get; set; }


        public AdminModel()
        {
            ImageList = new List<ImageFile>();
        }
    }


    public class AdminAccessRightModel
    {
        public string MainModule { get; set; }
        public string Module { get; set; }
        public string FunctionCode { get; set; }
        public string FunctionName { get; set; }
        public bool Function { get; set; }
    }

    #endregion

    #region StockOrderModel
    public class StockOrderModel
    {
        public string Number { get; set; }
        public string SelectedLanguage { get; set; }
        public string OrderNumber { get; set; }
        public string StockistUsername { get; set; }

        public string StockistName { get; set; }
        public string ProdCode { get; set; }
        public string ProdName { get; set; }

        public List<ProductSetupModel> ProductList { get; set; }

        public IEnumerable<SelectListItem> CategoryList { get; set; }
        public string SelectedCategory { get; set; }

        public IEnumerable<SelectListItem> ProdList { get; set; }
        public string SelectedProd { get; set; }
        public string CreatedOn { get; set; }
        public string ApprovedOn { get; set; }
        public string State { get; set; }

        public int ProductQuantity { get; set; }

        public float? GrandTotal { get; set; }
        public string SDONumber { get; set; }
        public string Courier { get; set; }

        public StockOrderModel()
        {
            ProductList = new List<ProductSetupModel>();
        }
    }
    #endregion


    #region
    public class PaginationStockOrderModel
    {
        public List<StockOrderModel> StockRequestList { get; set; }
        public IEnumerable<SelectListItem> Pages { get; set; }

        public PaginationStockOrderModel()
        {
            StockRequestList = new List<StockOrderModel>();
        }
    }
    #endregion

    #endregion

    #region Company Setup

    public class CompanySetupModel
    {

        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public string CompanyGSTNo { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string TelNo { get; set; }
        public string FaxNo { get; set; }
        public string Email { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyAddress1 { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyAddress3 { get; set; }
        public string CompanyURL { get; set; }
        public string CompanyImageName { get; set; }
        public string CompanyImagePath { get; set; }
        public HttpPostedFileBase CompanyImage { get; set; }
    }

    #endregion
}