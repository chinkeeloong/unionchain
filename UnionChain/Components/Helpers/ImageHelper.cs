﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ECFBase.Helpers
{
    public static class ImageHelper
    {

        public static MvcHtmlString Image(this HtmlHelper helper, string id, string url, string alternateText, int height, int width, int border)
        {
            return Image(helper, id, url, alternateText, height, width, border, "", null);
        }

        public static MvcHtmlString Image(this HtmlHelper helper, string id, string url, string alternateText, int height, int width, int border, string titleText)
        {
            return Image(helper, id, url, alternateText, height, width, border, titleText, null);
        }

        public static MvcHtmlString Image(this HtmlHelper helper, string id, string url, string alternateText, int height, int width, int border,
                                            string titleText, object htmlAttributes)
        {
            //Instantiate a UrlHelper
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            //Create tag builder
            var builder = new TagBuilder("img");

            //Create valid id
            builder.GenerateId(id);

            //Add attributes
            builder.MergeAttribute("src", urlHelper.Content(url));
            builder.MergeAttribute("alt", alternateText);
            builder.MergeAttribute("title", titleText);

            if (height != 0)
            {
                builder.MergeAttribute("height", height.ToString());
            }

            if (width != 0)
            {
                builder.MergeAttribute("width", width.ToString());
            }

            builder.MergeAttribute("border", border.ToString());

            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            // Render tag
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

    }
}