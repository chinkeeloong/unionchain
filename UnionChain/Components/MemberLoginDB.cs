﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using ECFBase.Models;

namespace ECFBase.Components
{
    public class MemberLoginDB
    {
        #region Member Register

        public static DataSet VerificationCode(out string Code)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GenerateVerificationCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@CODE", SqlDbType.VarChar, 10);
            pID.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            Code = pID.Value.ToString();
            sqlConn.Close();

            return ds;
        }


        public static DataSet ValidVerificationCode(string Code)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_ValidateVerificationCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 50);
            pCode.Direction = ParameterDirection.Input;
            pCode.Value = Code;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        #region RESET PASSWORD
        public static void ResetPassword(string storedProcedure, string UserName, string NewPassword, string updatedBy,
                                                out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand(storedProcedure, sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;
            
            SqlParameter pNewPassword = sqlComm.Parameters.Add("@NewPassword", SqlDbType.NVarChar, 50);
            pNewPassword.Direction = ParameterDirection.Input;
            pNewPassword.Value = NewPassword;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        public static void RegisterMember(SignUpModel mem, out int ok, out string msg, out string invoiceNumber)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Register", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = mem.Username;

            SqlParameter pFullName = sqlComm.Parameters.Add("@FullName", SqlDbType.NVarChar, 100);
            pFullName.Direction = ParameterDirection.Input;
            pFullName.Value = mem.FullName;

            SqlParameter pIntroName = sqlComm.Parameters.Add("@IntroName", SqlDbType.NVarChar, 50);
            pIntroName.Direction = ParameterDirection.Input;
            pIntroName.Value = mem.IntroName;

            SqlParameter pSponsorName = sqlComm.Parameters.Add("@SponsorName", SqlDbType.NVarChar, 50);
            pSponsorName.Direction = ParameterDirection.Input;
            pSponsorName.Value = mem.SponsorName;

            SqlParameter pPassword = sqlComm.Parameters.Add("@Password", SqlDbType.NVarChar, 50);
            pPassword.Direction = ParameterDirection.Input;
            pPassword.Value = mem.EncryptPassword;

            SqlParameter pPin = sqlComm.Parameters.Add("@Pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = mem.EncryptPayPassword;

            SqlParameter pCountryID = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 50);
            pCountryID.Direction = ParameterDirection.Input;
            pCountryID.Value = mem.SelectedCountry;
            
            SqlParameter pCellPhone = sqlComm.Parameters.Add("@CellPhone", SqlDbType.NVarChar, 50);
            pCellPhone.Direction = ParameterDirection.Input;
            pCellPhone.Value = mem.Phone;

            SqlParameter pPhoneCode = sqlComm.Parameters.Add("@PhoneCode", SqlDbType.NVarChar, 50);
            pPhoneCode.Direction = ParameterDirection.Input;
            pPhoneCode.Value = mem.MobileCountryCode;


            SqlParameter pEmail = sqlComm.Parameters.Add("@Email", SqlDbType.NVarChar, 100);
            pEmail.Direction = ParameterDirection.Input;
            pEmail.Value = mem.Email;

            SqlParameter pPosition = sqlComm.Parameters.Add("@Position", SqlDbType.NVarChar, 50);
            pPosition.Direction = ParameterDirection.Input;
            pPosition.Value = mem.SelectedPosition;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            SqlParameter pInvoiceNumber = sqlComm.Parameters.Add("@outInvoice", SqlDbType.VarChar, 100);
            pInvoiceNumber.Direction = ParameterDirection.Output;


            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            invoiceNumber = pInvoiceNumber.Value.ToString();
            sqlConn.Close();
        }


        public static void InsertSMSLog(string URL, string Vendor , string Key , string Phone , string CountryCode)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_InsertSMSFullLog", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pURL = sqlComm.Parameters.Add("@url", SqlDbType.NVarChar, 3000);
            pURL.Direction = ParameterDirection.Input;
            pURL.Value = URL;

            SqlParameter pVendor = sqlComm.Parameters.Add("@vendor", SqlDbType.NVarChar, 100);
            pVendor.Direction = ParameterDirection.Input;
            pVendor.Value = Vendor;

            SqlParameter pKey = sqlComm.Parameters.Add("@key", SqlDbType.NVarChar, 100);
            pKey.Direction = ParameterDirection.Input;
            pKey.Value = Key;

            SqlParameter pPhone = sqlComm.Parameters.Add("@phone", SqlDbType.NVarChar, 100);
            pPhone.Direction = ParameterDirection.Input;
            pPhone.Value = Phone;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@countryCode", SqlDbType.NVarChar, 100);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = CountryCode;


            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }


        #endregion
    }
}