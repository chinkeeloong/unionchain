﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using ECFBase.Models;
using System.Data;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Configuration;

namespace ECFBase.Components
{
    public class Misc
    {
        private static string pickupLocation = @"C:\inetpub\mailroot\Pickup";
        private static string smtpServer = "localhost";
        //Test Side
        //private static string username = "testjuta_admin";
        //private static string password = "UbjZxK0Uoj69n1";
        //Live Side
        private static string username = "Administrator";
        private static string password = "hgm1368!@#7799";
        private static int port = 25;


        public static string PhoneParse(string countryCode, string phoneNumber)
        {
            if (countryCode == "my")
            {
                if (phoneNumber.StartsWith("01"))
                {
                    phoneNumber = "6" + phoneNumber;
                }
                else if (phoneNumber.StartsWith("1"))
                {
                    phoneNumber = "60" + phoneNumber;
                }
            }
            else if (countryCode == "id")
            {
                phoneNumber = "62" + phoneNumber;

            }
            else if (countryCode == "cn")
            {

                phoneNumber = "86" + phoneNumber;

            }
            else if (countryCode == "th")
            {

                phoneNumber = "66" + phoneNumber;

            }
            else if (countryCode == "ph")
            {

                phoneNumber = "63" + phoneNumber;

            }
            else if (countryCode == "vn")
            {

                phoneNumber = "84" + phoneNumber;

            }
            else if (countryCode == "sg")
            {


                phoneNumber = "65" + phoneNumber;

            }
            else if (countryCode == "bn")
            {


                phoneNumber = "673" + phoneNumber;

            }
            else if (countryCode == "hk")
            {


                phoneNumber = "852" + phoneNumber;

            }
            else if (countryCode == "us")
            {


                phoneNumber = "1" + phoneNumber;

            }
            else if (countryCode == "jp")
            {


                phoneNumber = "81" + phoneNumber;

            }

            return phoneNumber;
        }


        #region GetUtilityStatus
        public static string GetUtilityStatus(string state)
        {
            switch (state)
            {
                case "1":
                    return "Payment Made";
                case "0":
                    return "Processing";
            }

            return state.ToString();
        }

        public static List<UtilitySetupModel> GetAllUtilityList(string LanguageCode, string CountryCode, ref int ok, ref string message)
        {
            DataSet dsUtility = MemberWalletDB.GetAllUtilityList(LanguageCode, CountryCode, out ok, out message);
            List<UtilitySetupModel> utilities = new List<UtilitySetupModel>();

            UtilitySetupModel utility = new UtilitySetupModel();
            utility.UtilityName = "--Select--";
            utility.UtilityID = 0;
            utilities.Add(utility);


            foreach (DataRow dr in dsUtility.Tables[0].Rows)
            {
                utility = new UtilitySetupModel();
                utility.UtilityName = dr["UTILITYNAME"].ToString();
                utility.UtilityID = Convert.ToInt32(dr["CUTI_ID"].ToString());
                utilities.Add(utility);
            }
            return utilities;
        }

        public static List<UtilitySetupModel> GetUtilityStatus()
        {
            List<UtilitySetupModel> utilities = new List<UtilitySetupModel>();

            UtilitySetupModel utility = new UtilitySetupModel();
            utility.Status = "ALL";
            utility.StatusID = -1;
            utilities.Add(utility);

            utility = new UtilitySetupModel();
            utility.Status = "Pending";
            utility.StatusID = 0;
            utilities.Add(utility);

            utility = new UtilitySetupModel();
            utility.Status = "Approve";
            utility.StatusID = 1;
            utilities.Add(utility);

            utility = new UtilitySetupModel();
            utility.Status = "Reject";
            utility.StatusID = 2;
            utilities.Add(utility);

            utility = new UtilitySetupModel();
            utility.Status = "Cancel";
            utility.StatusID = 3;
            utilities.Add(utility);

            //utility = new UtilitySetupModel();
            //utility.Status = "Payment Made";
            //utility.StatusID = 1;
            //utilities.Add(utility);

            return utilities;
        }

        #endregion

        #region Get Listing For DropBox 
        public static List<SelectListItem> GetLanguageList(ref int ok, ref string message)
        {
            DataSet dsLanguage = GetAllLanguageList(out ok, out message);
            List<SelectListItem> languages = new List<SelectListItem>();

            foreach (DataRow dr in dsLanguage.Tables[0].Rows)
            {
                SelectListItem language = new SelectListItem();
                language.Text = dr["CLANG_NAME"].ToString();
                language.Value = dr["CLANG_CODE"].ToString();
                language.Selected = false;
                languages.Add(language);
            }
            return languages;
        }

        public static List<SelectListItem> GetMultipleAccountChoice()
        {
            var accounts = new List<SelectListItem>();

            var firstAcc = new SelectListItem();
            firstAcc.Text = "1";
            firstAcc.Value = "1";
            accounts.Add(firstAcc);

            //var secondAcc = new SelectListItem();
            //secondAcc.Text = "3";
            //secondAcc.Value = "3";
            //accounts.Add(secondAcc);

            //var thirdAcc = new SelectListItem();
            //thirdAcc.Text = "7";
            //thirdAcc.Value = "7";
            //accounts.Add(thirdAcc);

            return accounts;
        }

        //contain alphabet + number
        public static bool IsAlphaNum(string str)
        {
            return str.Any(char.IsLetter) && str.Any(char.IsNumber);
        }

        public static List<SelectListItem> GetAllPaymentMode()
        {
            int ok = 0;
            string msg = "";

            DataSet dsPaymentMode = PaymentModeSetupDB.GetAllPaymentMode(out ok, out msg);
            List<SelectListItem> Method = new List<SelectListItem>();

            foreach (DataRow dr in dsPaymentMode.Tables[0].Rows)
            {
                SelectListItem Methods = new SelectListItem();
                Methods.Value = dr["CPAYMD_TYPE"].ToString();
                Methods.Text = dr["CPAYMD_TYPE"].ToString();
                Methods.Selected = false;
                Method.Add(Methods);
            }

            return Method;
        }

        public static List<SelectListItem> GetallRankList()
        {
            var dsRank = GetallRank();
            List<SelectListItem> Rank = new List<SelectListItem>();

            foreach (var item in dsRank)
            {
                SelectListItem Rankss = new SelectListItem();
                Rankss.Text = item.RankName;
                Rankss.Value = item.RankCode;
                Rankss.Selected = false;
                Rank.Add(Rankss);
            }

            return Rank;
        }

        public static List<RankModel> GetallRank()
        {
            DataSet dsCategory = AdminGeneralDB.GetAllRankList();
            List<RankModel> Rank = new List<RankModel>();

            foreach (DataRow dr in dsCategory.Tables[0].Rows)
            {
                RankModel Ranks = new RankModel();
                Ranks.RankCode = dr["CRANKSET_CODE"].ToString();
                Ranks.RankName = dr["CRANKSET_NAME"].ToString();
                Rank.Add(Ranks);
            }
            return Rank;
        }

        public static List<CategoryModel> GetAllCategoryList(string LanguageCode, ref int ok, ref string message)
        {
            DataSet dsCategory = AdminGeneralDB.GetAllCategoryList(LanguageCode, out ok, out message);
            List<CategoryModel> categories = new List<CategoryModel>();

            foreach (DataRow dr in dsCategory.Tables[0].Rows)
            {
                CategoryModel category = new CategoryModel();
                category.CategoryCode = dr["CCAT_CODE"].ToString();
                category.CategoryName = dr["CATNAME"].ToString();
                categories.Add(category);
            }
            return categories;
        }

        public static List<SelectListItem> GetCurrencyList(ref int ok, ref string message)
        {
            var dsCurrency = GetAllCurrencyList(ref ok, ref message);
            List<SelectListItem> currencies = new List<SelectListItem>();

            foreach (var item in dsCurrency)
            {
                SelectListItem currency = new SelectListItem();
                currency.Text = item.CurrencyName;
                currency.Value = item.CountryCode;
                currency.Selected = false;
                currencies.Add(currency);
            }

            return currencies;
        }

        public static List<CurrencyModel> GetAllCurrencyList(ref int ok, ref string message)
        {
            int pages = 0;
            DataSet dsCountry = AdminGeneralDB.GetAllCurrency(1, out pages, out ok, out message);
            var currencies = new List<CurrencyModel>();

            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                CurrencyModel currency = new CurrencyModel();
                currency.CountryCode = dr["CCOUNTRY_CODE"].ToString();
                currency.CurrencyName = dr["CCURRENCY_NAME"].ToString();
                currencies.Add(currency);
            }

            return currencies;
        }

        public static List<SelectListItem> GetCountryList(string languageCode, ref int ok, ref string message)
        {
            var dsCountry = GetAllCountryList(languageCode, ref ok, ref message);
            List<SelectListItem> countries = new List<SelectListItem>();

            foreach (var item in dsCountry)
            {
                SelectListItem country = new SelectListItem();
                country.Text = item.CountryName;
                country.Value = item.CountryCode;
                country.Selected = false;
                countries.Add(country);
            }

            return countries;
        }

        public static List<CountrySetupModel> GetAllCountryList(string LanguageCode, ref int ok, ref string message)
        {
            DataSet dsCountry = AdminGeneralDB.GetAllCountryList(LanguageCode, out ok, out message);
            List<CountrySetupModel> countries = new List<CountrySetupModel>();

            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                CountrySetupModel country = new CountrySetupModel();
                country.CountryCode = dr["CCOUNTRY_CODE"].ToString();
                country.CountryName = dr["COUNTRYNAME"].ToString();
                country.MobileCode = dr["CCOUNTRY_MOBILECODE"].ToString();
                countries.Add(country);
            }
            return countries;
        }

        public static List<CountrySetupModel> GetAllCountryID(string LanguageCode)
        {
            int ok = 0;
            string message = string.Empty;
            DataSet dsCountry = AdminGeneralDB.GetAllCountryList(LanguageCode, out ok, out message);
            List<CountrySetupModel> countries = new List<CountrySetupModel>();

            CountrySetupModel country = new CountrySetupModel();
            country.CountryCode = "0";
            country.CountryName = "ALL";
            country.MobileCode = "0";
            countries.Add(country);

            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                country = new CountrySetupModel();
                country.CountryCode = dr["CCOUNTRY_ID"].ToString();
                country.CountryName = dr["COUNTRYNAME"].ToString();
                country.MobileCode = dr["CCOUNTRY_MOBILECODE"].ToString();
                countries.Add(country);
            }
            return countries;
        }

        public static List<CashName> DBGetAllCashNameList(string wallettype)
        {
            List<CashName> Name = new List<CashName>();
            if (wallettype == "TW")
            {
                CashName CashName = new CashName();
                CashName.cashname = "0";
                CashName.cashnamedisplay = Resources.Member.lblAll;
                Name.Add(CashName);

                DataSet ds = AdminGeneralDB.GetCashnameList(1);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    CashName = new CashName();
                    CashName.cashname = "";
                    CashName.cashnamedisplay = "";
                    Name.Add(CashName);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CashName = new CashName();
                    CashName.cashname = dr["CTDBWL_CASHNAME"].ToString();
                    CashName.cashnamedisplay = Misc.GetReadableCashName(dr["CTDBWL_CASHNAME"].ToString());
                    Name.Add(CashName);
                }
            }
            else if (wallettype == "UTW")
            {
                CashName CashName = new CashName();
                CashName.cashname = "0";
                CashName.cashnamedisplay = Resources.Member.lblAll;
                Name.Add(CashName);

                DataSet ds = AdminGeneralDB.GetCashnameList(2);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    CashName = new CashName();
                    CashName.cashname = "";
                    CashName.cashnamedisplay = "";
                    Name.Add(CashName);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CashName = new CashName();
                    CashName.cashname = dr["CUTWL_CASHNAME"].ToString();
                    CashName.cashnamedisplay = Misc.GetReadableCashName(dr["CUTWL_CASHNAME"].ToString());
                    Name.Add(CashName);
                }
            }
                      

            return Name;
        }

        public static List<Status> DBGetAllBuyQueueStatusList(string QueueType)
        {
            List<Status> Name = new List<Status>();
            if (QueueType == "BUY")
            {
                Status Status = new Status();
                Status.StatusX = "";
                Status.StatusDisplay = Resources.Member.lblAll;
                Name.Add(Status);

                DataSet ds = AdminGeneralDB.GetAllStatus(1);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    Status = new Status();
                    Status.StatusX = "";
                    Status.StatusDisplay = "";
                    Name.Add(Status);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Status = new Status();
                    Status.StatusX = dr["CTBQ_STATUS"].ToString();
                    Status.StatusDisplay = Misc.TradingStatus(dr["CTBQ_STATUS"].ToString());
                    Name.Add(Status);
                }
            }
            else if (QueueType == "SELL")
            {
                Status Status = new Status();
                Status.StatusX = "0";
                Status.StatusDisplay = Resources.Member.lblAll;
                Name.Add(Status);

                DataSet ds = AdminGeneralDB.GetAllStatus(2);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    Status = new Status();
                    Status.StatusX = "";
                    Status.StatusDisplay = "";
                    Name.Add(Status);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Status = new Status();
                    Status.StatusX = dr["CTSQ_STATUS"].ToString();
                    Status.StatusDisplay = Misc.TradingStatus(dr["CTSQ_STATUS"].ToString());
                    Name.Add(Status);
                }
            }
            else if (QueueType == "MATCH")
            {
                Status Status = new Status();
                Status.StatusX = "0";
                Status.StatusDisplay = Resources.Member.lblAll;
                Name.Add(Status);

                DataSet ds = AdminGeneralDB.GetAllStatus(3);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    Status = new Status();
                    Status.StatusX = "";
                    Status.StatusDisplay = "";
                    Name.Add(Status);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Status = new Status();
                    Status.StatusX = dr["CTMP_STATUS"].ToString();
                    Status.StatusDisplay = Misc.TradingStatus(dr["CTMP_STATUS"].ToString());
                    Name.Add(Status);
                }
            }


            return Name;
        }

        public static List<SelectListItem> GetRankList(int from, string languageCode, ref int ok, ref string message)
        {
            var dsRank = GetAllRankList(from, languageCode, ref ok, ref message);
            List<SelectListItem> allRank = new List<SelectListItem>();

            foreach (var item in dsRank)
            {
                SelectListItem rank = new SelectListItem();
                rank.Text = item.RankName;
                rank.Value = item.RankCode;
                rank.Selected = false;
                allRank.Add(rank);
            }

            return allRank;
        }

        public static List<RankSetupModel> GetAllRankList(int from, string LanguageCode, ref int ok, ref string message)
        {
            int page = 0;
            DataSet dsRank = new DataSet();

            if (from == 1)
            {
                dsRank = AdminGeneralDB.GetAllRankSetup(-1, out page, out ok, out message);
            }
            else
            {
                dsRank = AdminGeneralDB.GetAllRank(-1, out page, out ok, out message);
            }
            List<RankSetupModel> allRank = new List<RankSetupModel>();

            foreach (DataRow dr in dsRank.Tables[0].Rows)
            {
                RankSetupModel rank = new RankSetupModel();
                rank.RankCode = dr["CRANKSET_CODE"].ToString();
                rank.RankName = dr["CRANKSET_NAME"].ToString();
                allRank.Add(rank);
            }
            return allRank;
        }

        public static List<ProductModel> GetAllProductByCategory(string LanguageCode, string categoryCode, ref int ok, ref string message)
        {
            DataSet dsProduct = AdminGeneralDB.GetAllProductByCategory(LanguageCode, categoryCode, out ok, out message);
            List<ProductModel> products = new List<ProductModel>();

            foreach (DataRow dr in dsProduct.Tables[0].Rows)
            {
                ProductModel product = new ProductModel();
                product.ProductCode = dr["CPRD_CODE"].ToString();
                product.ProductName = dr["PRODNAME"].ToString();
                products.Add(product);
            }
            return products;
        }

        public static List<ProvinceModel> GetAllProvinceByCountry(string LanguageCode, string CountryCode, ref int ok, ref string message)
        {
            DataSet dsProvince = MemberProfileDB.GetAllProvinceByCountry(LanguageCode, CountryCode, out ok, out message);
            List<ProvinceModel> provinces = new List<ProvinceModel>();

            foreach (DataRow dr in dsProvince.Tables[0].Rows)
            {
                ProvinceModel province = new ProvinceModel();
                province.ProvinceCode = dr["CPROVINCE_CODE"].ToString();
                province.ProvinceName = dr["PROVINCENAME"].ToString();
                provinces.Add(province);
            }
            return provinces;
        }

        public static List<CityModel> GetAllCityByProvince(string LanguageCode, string ProvinceCode, ref int ok, ref string message)
        {
            DataSet dsCity = MemberProfileDB.GetAllCityByProvince(LanguageCode, ProvinceCode, out ok, out message);
            List<CityModel> cities = new List<CityModel>();

            foreach (DataRow dr in dsCity.Tables[0].Rows)
            {
                CityModel city = new CityModel();
                city.CityCode = dr["CCITY_CODE"].ToString();
                city.CityName = dr["CITYNAME"].ToString();
                cities.Add(city);
            }
            return cities;
        }

        public static List<BankModel> GetAllBankByCountry(string LanguageCode, string CountryCode, ref int ok, ref string message)
        {
            DataSet dsBank = AdminGeneralDB.GetAllBankByCountry(LanguageCode, CountryCode, out ok, out message);
            List<BankModel> banks = new List<BankModel>();

            foreach (DataRow dr in dsBank.Tables[0].Rows)
            {
                BankModel bank = new BankModel();
                bank.BankCode = dr["CBANK_CODE"].ToString();
                bank.BankName = dr["BANKNAME"].ToString();
                banks.Add(bank);
            }

            BankModel bnk = new BankModel();
            bnk.BankCode = "null";
            bnk.BankName = Resources.Member.lblSelected;
            banks.Add(bnk);

            return banks;
        }

        #endregion

        public static string GetParaByName(string Name)
        {

            string Result = string.Empty;

            var ds = MemberDB.GetParameterBasedOnName(Name);
            string result = ds.Tables[0].Rows[0]["CPARA_FLOATVALUE"].ToString();

            return result;
        }

        public static string GetCountryImageByCountryCode(string Code)
        {
            int ok = 0;
            string msg = string.Empty;
            string Result = string.Empty;

            var ds = AdminGeneralDB.GetCountryByCountryCode(Code, out ok, out msg);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Result = "";
            }
            else
            {
                Result = ds.Tables[0].Rows[0]["CCOUNTRY_IMAGEPATH"].ToString();
            }
            return Result;
        }

        public static string GetCountryNameByCountryCode(string Code)
        {
            int ok = 0;
            string msg = string.Empty;
            string Result = string.Empty;

            var ds = AdminGeneralDB.GetCountryByCountryCode(Code, out ok, out msg);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Result = "";
            }
            else
            {
                Result = ds.Tables[0].Rows[0]["CMULTILANGCOUNTRY_NAME"].ToString();
            }
            return Result;
        }

        public static string GetInboxType(string type)
        {
            if (Resources.Member.lblGeneral == type)
            {
                return InboxType.General.ToString();
            }

            // Instead of empty, better with general type
            return InboxType.General.ToString();
        }

        public static int GetMaintainProductCharge(int quantity)
        {
            int shipCharges = 0;

            if (quantity <= 4)
                shipCharges = 2;
            else if (quantity <= 14)
                shipCharges = 3;
            else if (quantity >= 15)
                shipCharges = 5;

            return shipCharges;
        }

        public static string DeliveryStatus(string status)
        {
            switch (status.ToUpper())
            {
                case "0":
                    return Resources.Member.lblStatusPending;
                case "1":
                    return Resources.Member.lblStatusDelivered;
                case "ALL":
                    return "";
                case "PENDING":
                    return "0";
                case "DELIVERED":
                    return "1";
                default:
                    return Resources.Member.lblStatusPending;
            }
        }

        public static string TradingStatus(string status)
        {
            switch (status)
            {
                case "Cancel":
                    return Resources.Member.TDSCancel;
                case "Pending Payment":
                    return Resources.Member.TDSPendingPayment;
                case "Completed":
                    return Resources.Member.TDSCompleted;
                default:
                    return status;
            }
        }

        public static string Status(string status)
        {
            switch (status.ToUpper())
            {
                case "APPROVE":
                    return Resources.Member.lblStatusApprove;
                case "PENDING":
                    return Resources.Member.lblStatusPending;
                case "REJECT":
                    return Resources.Member.lblStatusRejected;
                default:
                    return "";
            }
        }

        public static string RankNumber(string Rank)
        {
            switch (Rank.ToUpper())
            {
                case "0":
                    return "Free Account";
                case "3":
                    return "BVA3";
                case "2":
                    return "BVA2";
                case "1":
                    return "BVA1";
                default:
                    return Rank;
            }
        }

        public static string GetMemberRanking(string ranking)
        {
            int ok = 0;
            string msg = "";
            int page = 0;

            DataSet ds = AdminGeneralDB.GetAllRank(-1, out page, out ok, out msg);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["CRANKSET_CODE"].ToString().Contains(ranking))
                {
                    return dr["CRANKSET_NAME"].ToString();
                }

            }

            return Resources.Member.rankAgent;
        }

        public static string GetMemberRankingIcon(string ranking)
        {
            int ok = 0;
            string msg = "";
            int page = 0;

            DataSet ds = AdminGeneralDB.GetAllRank(-1, out page, out ok, out msg);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["CRANKSET_CODE"].ToString().Contains(ranking))
                {
                    return dr["CRANKSET_ICON"].ToString();
                }

            }

            return Resources.Member.rankAgent;
        }

        public static DataSet GetAllLanguageList(out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllLanguageList", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static string GetWithdrawalStatus(string status)
        {
            switch (status)
            {
                case "0":
                    return Resources.Member.lblStatusPending;
                case "1":
                    return Resources.Member.lblStatusApproved;
                case "-1":
                    return Resources.Member.lblStatusRefunded;
                case "-2":
                    return Resources.Member.lblStatusRejected;
            }

            return Resources.Member.lblStatusPending;
        }

        public static string GetDeliveryType(string status)
        {
            if (status == "0")
            {
                return "Send To Member";
            }
            else if (status == "1")
            {
                return "Pick Up";
            }
            else
                return "";


            //return 0;
        }

        public static void InsertWithdrawalStatus(ref List<string> list)
        {
            list.Add(Resources.Member.lblStatusPending);
            list.Add(Resources.Member.lblStatusApproved);
            list.Add(Resources.Member.lblStatusRefunded);
        }

        public static string GetReadableCashName(string cashname)
        {
            string readableString = "";
            switch (cashname)
            {
                case "Platform Transfer":
                    readableString = cashname;
                    break;
                case "Hash":
                    readableString = Resources.Member.lblHash;
                    break;
                case "Performance Hash":
                    readableString = Resources.Member.lblHashPower;
                    break;
                case "Management Hash":
                    readableString = Resources.Member.lblHashPerformance;
                    break;
                case "Exchange From":
                    readableString = Resources.Member.lblExchangeFrom;
                    break;
                case "Exchange To":
                    readableString = Resources.Member.lblExchangeFrom;
                    break;
                case "Buy Token":
                    readableString = Resources.Member.lblBuyToken;
                    break;
                case "Selling Token":
                    readableString = Resources.Member.lblSellingToken;
                    break;
                case "Cancel Sell Token":
                    readableString = Resources.Member.lblCancelSellToken;
                    break;
                case "TOPUP":
                    readableString = Resources.Member.lblTopUp;
                    break;
                case "Tradeable":
                    readableString = Resources.Member.lblTradeable;
                    break;
                case "Untradeable":
                    readableString = Resources.Member.lblUntradeable;
                    break;
                case "Utility Bill":
                    readableString = Resources.Member.lblUtilityBill;
                    break;
                default:
                    readableString = cashname;
                    break;
            }

            return readableString;
        }

        public static string GetTranslatedGeneralSetting(string paramName)
        {
            switch (paramName)
            {
                case "paraNameTableRows":
                    return Resources.Member.paraNameTableRows;
                case "MinWithdrawAmt":
                    return Resources.Member.paraNameMinWithdrawAmt;
                case "paraNameMaxWithdrawAmt":
                    return Resources.Member.paraNameMaxWithdrawAmt;
                case "WithdrawalCharge":
                    return Resources.Member.paraNameWithdrawalCharge;
                case "WithdrawalMore1000":
                    return Resources.Member.paraNameWithdrawalMore1000;
                case "Number":
                    return Resources.Member.paraNameNumber;
                case "USD":
                    return Resources.Member.paraNameUSD;
                case "Money":
                    return Resources.Member.paraNameuomMoney;
            }

            return paramName;
        }

        #region ConstructYears
        public static IEnumerable<SelectListItem> ConstructYears(int year = 1920)
        {
            var yearsList = new List<string>();
            DateTime dt = new DateTime(year, 1, 1);

            while (true)
            {
                yearsList.Add(dt.Year.ToString());
                dt = dt.AddYears(1);
                if (dt.Year > DateTime.Now.Year)
                {
                    break;
                }
            }


            yearsList.Insert(0, "Years");

            var years = from c in yearsList
                        select new SelectListItem
                        {
                            Text = c,
                            Value = c
                        };

            return years;
        }


        #endregion

        #region ConstructsMonth
        public static IEnumerable<SelectListItem> ConstructsMonth()
        {
            List<string> monthsList = new List<string>();
            for (int z = 1; z <= 12; z++)
            {
                monthsList.Add(z.ToString());
            }

            monthsList.Insert(0, "Months");

            var months = from c in monthsList
                         select new SelectListItem
                         {
                             Text = c.ToString(),
                             Value = c.ToString()
                         };

            return months;
        }
        #endregion

        #region ConstructsDay
        public static IEnumerable<SelectListItem> ConstructsDay()
        {
            List<string> daysList = new List<string>();
            for (int z = 1; z <= 31; z++)
            {
                daysList.Add(z.ToString());
            }

            daysList.Insert(0, "Days");

            var months = from c in daysList
                         select new SelectListItem
                         {

                             Text = c.ToString(),
                             Value = c.ToString()
                         };


            return months;
        }
        #endregion

        #region Constructs Status Criteria
        public static IEnumerable<SelectListItem> ConstructsStatusCriteria()
        {
            List<SelectListItem> filteringCriteriaList = new List<SelectListItem>();

            SelectListItem allSelection = new SelectListItem();
            allSelection.Text = Resources.Member.lblStatusAll;
            allSelection.Value = "ALL";
            filteringCriteriaList.Add(allSelection);

            SelectListItem pendingSelection = new SelectListItem();
            pendingSelection.Text = Resources.Member.lblStatusPending;
            pendingSelection.Value = "PENDING";
            filteringCriteriaList.Add(pendingSelection);

            SelectListItem deliveredSelection = new SelectListItem();
            deliveredSelection.Text = Resources.Member.lblStatusDelivered;
            deliveredSelection.Value = "DELIVERED";
            filteringCriteriaList.Add(deliveredSelection);

            return filteringCriteriaList;
        }

        #endregion

        #region Constructs Filtering Criteria
        public static IEnumerable<SelectListItem> ConstructsFilteringCriteria()
        {
            List<SelectListItem> filteringCriteriaList = new List<SelectListItem>();

            SelectListItem fcUserName = new SelectListItem();
            fcUserName.Text = Resources.Member.lblUsername;
            fcUserName.Value = "USERNAME";
            filteringCriteriaList.Add(fcUserName);

            SelectListItem fcFullName = new SelectListItem();
            fcFullName.Text = Resources.Member.lblFullname;
            fcFullName.Value = "FULLNAME";
            filteringCriteriaList.Add(fcFullName);

            SelectListItem fcRanking = new SelectListItem();
            fcRanking.Text = Resources.Member.lblRanking;
            fcRanking.Value = "RANKING";
            filteringCriteriaList.Add(fcRanking);

            //SelectListItem fdPackage = new SelectListItem();
            //fdPackage.Text = Resources.Member.lblPackage;
            //fdPackage.Value = "PACKAGE";
            //filteringCriteriaList.Add(fdPackage);

            SelectListItem fcSponsor = new SelectListItem();
            fcSponsor.Text = Resources.Member.lblSponsor;
            fcSponsor.Value = "SPONSOR";
            filteringCriteriaList.Add(fcSponsor);

            SelectListItem fcCountry = new SelectListItem();
            fcCountry.Text = Resources.Member.lblCountryName;
            fcCountry.Value = "COUNTRY";
            filteringCriteriaList.Add(fcCountry);

            return filteringCriteriaList;
        }

        public static IEnumerable<SelectListItem> ConstructsFilteringCriteriaWB()
        {
            List<SelectListItem> filteringCriteriaList = new List<SelectListItem>();

            SelectListItem fcALL = new SelectListItem();
            fcALL.Text = Resources.Member.lblAll;
            fcALL.Value = "0";
            filteringCriteriaList.Add(fcALL);

            SelectListItem fcUserName = new SelectListItem();
            fcUserName.Text = Resources.Member.lblUsername;
            fcUserName.Value = "USERNAME";
            filteringCriteriaList.Add(fcUserName);

            SelectListItem fcFullName = new SelectListItem();
            fcFullName.Text = Resources.Member.lblFullname;
            fcFullName.Value = "FULLNAME";
            filteringCriteriaList.Add(fcFullName);

            //SelectListItem fcRanking = new SelectListItem();
            //fcRanking.Text = Resources.Member.lblRanking;
            //fcRanking.Value = "RANKING";
            //filteringCriteriaList.Add(fcRanking);

            //SelectListItem fdPackage = new SelectListItem();
            //fdPackage.Text = Resources.Member.lblPackage;
            //fdPackage.Value = "PACKAGE";
            //filteringCriteriaList.Add(fdPackage);

            //SelectListItem fcSponsor = new SelectListItem();
            //fcSponsor.Text = Resources.Member.lblSponsor;
            //fcSponsor.Value = "SPONSOR";
            //filteringCriteriaList.Add(fcSponsor);

            //SelectListItem fcCountry = new SelectListItem();
            //fcCountry.Text = Resources.Member.lblCountryName;
            //fcCountry.Value = "COUNTRY";
            //filteringCriteriaList.Add(fcCountry);

            return filteringCriteriaList;
        }


        public static IEnumerable<SelectListItem> ConstructsNoticeFilteringCriteria()
        {
            List<SelectListItem> filteringCriteriaList = new List<SelectListItem>();

            var fcUserName = new SelectListItem();
            fcUserName.Text = Resources.Member.lblUsername;
            fcUserName.Value = "CUSR_USERNAME";
            filteringCriteriaList.Add(fcUserName);

            var fcTitle = new SelectListItem();
            fcTitle.Text = Resources.Member.lblTitle;
            fcTitle.Value = "CMULTILANGNOT_TITLE";
            filteringCriteriaList.Add(fcTitle);

            return filteringCriteriaList;
        }
        #endregion

        #region Constructs Delivery Status Filtering Criteria
        public static IEnumerable<SelectListItem> ConstructDeliveryStatusFilteringCriteria()
        {
            var filteringCriteriaList = new List<SelectListItem>();

            var fcAllStatus = new SelectListItem();
            fcAllStatus.Text = "All";
            fcAllStatus.Value = "ALL";
            filteringCriteriaList.Add(fcAllStatus);

            var fcPendingStatus = new SelectListItem();
            fcPendingStatus.Text = "Pending";
            fcPendingStatus.Value = "PENDING";
            filteringCriteriaList.Add(fcPendingStatus);

            var fcDeliveredStatus = new SelectListItem();
            fcDeliveredStatus.Text = "Delivered";
            fcDeliveredStatus.Value = "DELIVERED";
            filteringCriteriaList.Add(fcDeliveredStatus);

            return filteringCriteriaList;
        }
        #endregion

        #region ConstructGender

        public static IEnumerable<SelectListItem> ConstructGender(string currentGender = "")
        {
            var genderList = new List<SelectListItem>();

            var maleSelection = new SelectListItem
            {
                Text = Resources.Member.lblMale,
                Value = "MALE"
            };
            genderList.Add(maleSelection);

            var femaleSelection = new SelectListItem
            {
                Text = Resources.Member.lblFemale,
                Value = "FEMALE"
            };
            genderList.Add(femaleSelection);

            if (!string.IsNullOrEmpty(currentGender))
            {
                foreach (var g in genderList)
                {
                    if (System.String.Compare(g.Value, currentGender, System.StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        g.Selected = true;
                    }
                }
            }

            return genderList;
        }

        #endregion

        #region SendEmail
        public static string SendEmail(string strFrom, string strto, string strSubject, string strBody)
        {
            MailMessage message = new MailMessage();
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.IsBodyHtml = true;

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

            //Add Image
            //LinkedResource EmailImage = new LinkedResource(imagePath);
            //EmailImage.ContentId = "myImageID";

            ////Add the Image to the Alternate view
            //htmlView.LinkedResources.Add(EmailImage);

            //Add view to the Email Message
            message.AlternateViews.Add(htmlView);

            message.From = new MailAddress(strFrom, "UnionsChain");
            message.To.Add(strto);
            //message.Bcc.Add(new MailAddress("CVDiamondSupport99@hotmail.com"));
            message.Subject = strSubject;

            var client = new SmtpClient(smtpServer, port);
            client.Credentials = new System.Net.NetworkCredential(username, password);
            client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
            client.PickupDirectoryLocation = pickupLocation;

            try
            {
                client.Send(message);
                return "Successfully";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //public static string SendEmailGood(string strFrom, string strto, string strSubject, string strBody, string imagePath)
        //{
        //    MailMessage message = new MailMessage();
        //    message.BodyEncoding = System.Text.Encoding.UTF8;
        //    message.IsBodyHtml = true;

        //    AlternateView htmlView = AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

        //    //Add Image
        //    //LinkedResource EmailImage = new LinkedResource(imagePath);
        //    //EmailImage.ContentId = "myImageID";

        //    //Add the Image to the Alternate view
        //    //htmlView.LinkedResources.Add(EmailImage);

        //    //Add view to the Email Message
        //    message.AlternateViews.Add(htmlView);

        //    message.From = new MailAddress(strFrom, "NLH99");
        //    message.To.Add(strto);
        //    message.Subject = strSubject;

        //    var client = new SmtpClient(smtpServer, port);
        //    client.Credentials = new System.Net.NetworkCredential(username, password);
        //    client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
        //    client.PickupDirectoryLocation = pickupLocation;

        //    try
        //    {
        //        client.Send(message);
        //        return "Successfully";
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }
        //}
        
        public static string SendEmailGood(string strFrom, string strto, string strSubject, string strBody, string imagePath)
        {
            MailMessage message = new MailMessage();
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.IsBodyHtml = true;

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

            ////Add Image
            LinkedResource EmailImage = new LinkedResource(imagePath);
            EmailImage.ContentId = "myImageID";


            ////Add the Image to the Alternate view
            htmlView.LinkedResources.Add(EmailImage);

            //Add view to the Email Message
            message.AlternateViews.Add(htmlView);

            message.From = new MailAddress(strFrom, "UnionsChain");
            message.To.Add(strto);
            message.Subject = strSubject;

            var client = new SmtpClient(smtpServer, port);
            client.Credentials = new System.Net.NetworkCredential(username, password);
            client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
            client.PickupDirectoryLocation = pickupLocation;

            try
            {
                client.Send(message);
                return "Successfully";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }







        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
        }










        #endregion

        #region FileExtensionValidation
        public static bool IsFileExtensionValid(string file)
        {
            bool isValid = false;
            string extension = Path.GetExtension(file);

            if (extension != null)
            {
                switch (extension.ToLower())
                {
                    case ".jpeg":
                    case ".png":
                    case ".gif":
                    case ".bmp":
                    case ".jpg":
                        //Upload file as it is an image
                        isValid = true;
                        break;
                    default:
                        //Not an image - ignore
                        isValid = false;
                        break;
                }
            }
            return isValid;
        }
        #endregion

        #region ConvertToAppOtherBasedOnCashName
        public static string ConvertToAppOtherBasedOnCashName(string input, string cashname)
        {
            if (cashname == "MATCHINGBONUS")
            {
                if (input == "1")
                {
                    return ECFBase.Resources.Member.lblMatchLvl1;
                }
                else if (input == "2")
                {
                    return ECFBase.Resources.Member.lblMatchLvl2;
                }
                else if (input == "3")
                {
                    return ECFBase.Resources.Member.lblMatchLvl3;
                }
                else if (input == "4")
                {
                    return ECFBase.Resources.Member.lblMatchLvl4;
                }
                else if (input == "5")
                {
                    return ECFBase.Resources.Member.lblMatchLvl5;
                }
            }
            else if (cashname == "SPONSORBONUS")
            {
                if (input == "1")
                {
                    return ECFBase.Resources.Member.lblLevel1;
                }
                else if (input == "2")
                {
                    return ECFBase.Resources.Member.lblLevel2;
                }
                else if (input == "3")
                {
                    return ECFBase.Resources.Member.lblLevel3;
                }
            }

            return input;
        }
        #endregion

        #region GetStatusBasedOnSDONO
        public static string GetStatusBasedOnSDONO(string state)
        {
            switch (state)
            {
                case "1":
                    return Resources.Member.lblStatusDelivered;
                case "0":
                    return Resources.Member.lblStatusPending;
            }

            return state.ToString();
        }
        #endregion

    }
}