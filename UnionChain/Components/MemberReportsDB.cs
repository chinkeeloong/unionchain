﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ECFBase.Components
{
    public class MemberReportsDB
    {
        public static DataSet GetMemberCurrentSales(int selectedPage, string username, int selectedYear, int selectedMonth, int selectedDay, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetMemberCurrentSales", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pDay = sqlComm.Parameters.Add("@day", SqlDbType.Int);
            pDay.Direction = ParameterDirection.Input;
            pDay.Value = selectedDay;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

    }
}