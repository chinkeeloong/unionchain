﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using ECFBase.Models;
using ECFBase.Helpers;

namespace ECFBase.Components
{
    public class AdminGeneralDB
    {
        #region Utility
        public static DataSet GetAllUtility(string LanguageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllUtility", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }
        public static DataSet GetUtilityByID(int? UtilityID, string Language, string Country, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetUtilityByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguage = sqlComm.Parameters.Add("@Language", SqlDbType.NVarChar, 20);
            pLanguage.Direction = ParameterDirection.Input;
            pLanguage.Value = Language;

            SqlParameter pSelectedCountry = sqlComm.Parameters.Add("@Country", SqlDbType.NVarChar, 20);
            pSelectedCountry.Direction = ParameterDirection.Input;
            pSelectedCountry.Value = Country;

            SqlParameter pUtilityID = sqlComm.Parameters.Add("@UtilityID", SqlDbType.Int);
            pUtilityID.Direction = ParameterDirection.Input;
            pUtilityID.Value = UtilityID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }


        public static DataSet GetMultiLanguageUtilityByCode(string UtilityCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguageUtilityByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUtilityCode = sqlComm.Parameters.Add("@UtilityCode", SqlDbType.NVarChar, 10);
            pUtilityCode.Direction = ParameterDirection.Input;
            pUtilityCode.Value = UtilityCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }


        public static void CreateNewUtility(string UtilityCode, string SelectedCountry, string ImagePath, string UTProcess, string MinAmount, string MaxAmount, string UTDigit, bool MobileBill, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateUtility", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUtilityCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 10);
            pUtilityCode.Direction = ParameterDirection.Input;
            pUtilityCode.Value = UtilityCode;

            SqlParameter pSelectedCountry = sqlComm.Parameters.Add("@Country", SqlDbType.NVarChar, 20);
            pSelectedCountry.Direction = ParameterDirection.Input;
            pSelectedCountry.Value = SelectedCountry;

            SqlParameter pImagePath = sqlComm.Parameters.Add("@ImagePath", SqlDbType.NVarChar, 500);
            pImagePath.Direction = ParameterDirection.Input;
            pImagePath.Value = ImagePath;

            SqlParameter pUTProcess = sqlComm.Parameters.Add("@UTProcess", SqlDbType.NVarChar, 50);
            pUTProcess.Direction = ParameterDirection.Input;
            pUTProcess.Value = UTProcess;

            SqlParameter pMinAmount = sqlComm.Parameters.Add("@MinAmount", SqlDbType.Money);
            pMinAmount.Direction = ParameterDirection.Input;
            pMinAmount.Value = MinAmount;

            SqlParameter pMaxAmount = sqlComm.Parameters.Add("@MaxAmount", SqlDbType.Money);
            pMaxAmount.Direction = ParameterDirection.Input;
            pMaxAmount.Value = MaxAmount;

            SqlParameter pUTDigit = sqlComm.Parameters.Add("@UTDigit", SqlDbType.NVarChar, 10);
            pUTDigit.Direction = ParameterDirection.Input;
            pUTDigit.Value = UTDigit;

            SqlParameter pMobileBill = sqlComm.Parameters.Add("@MobileBill", SqlDbType.Bit);
            pMobileBill.Direction = ParameterDirection.Input;
            pMobileBill.Value = MobileBill;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }


        public static void CreateNewMultiLanguageUtility(string UtilityCode, string LanguageCode,
                                                            string UtilityName, string UtilityDescription, bool MobileBill,
                                                            string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguageUtility", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUtilityCode = sqlComm.Parameters.Add("@UtilityCode", SqlDbType.NVarChar, 10);
            pUtilityCode.Direction = ParameterDirection.Input;
            pUtilityCode.Value = UtilityCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pUtilityName = sqlComm.Parameters.Add("@UtilityName", SqlDbType.NVarChar, 100);
            pUtilityName.Direction = ParameterDirection.Input;
            pUtilityName.Value = Helpers.Helper.NVL(UtilityName);

            SqlParameter pUtilityDescription = sqlComm.Parameters.Add("@UtilityDescription", SqlDbType.NVarChar, 1000);
            pUtilityDescription.Direction = ParameterDirection.Input;
            pUtilityDescription.Value = Helpers.Helper.NVL(UtilityDescription);

            SqlParameter pMobileBill = sqlComm.Parameters.Add("@MobileBill", SqlDbType.Bit);
            pMobileBill.Direction = ParameterDirection.Input;
            pMobileBill.Value = MobileBill;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateUtility(int? UtilityID,string UtilityCode, string SelectedCountry, string ImagePath, string UTProcess, string MinAmount, string MaxAmount, string UTDigit, bool MobileBill, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateUtility", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUtilityID = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pUtilityID.Direction = ParameterDirection.Input;
            pUtilityID.Value = UtilityID;

            SqlParameter pSelectedCountry = sqlComm.Parameters.Add("@Country", SqlDbType.NVarChar, 20);
            pSelectedCountry.Direction = ParameterDirection.Input;
            pSelectedCountry.Value = SelectedCountry;

            SqlParameter pUtilityCode = sqlComm.Parameters.Add("@code", SqlDbType.NVarChar, 10);
            pUtilityCode.Direction = ParameterDirection.Input;
            pUtilityCode.Value = UtilityCode;

            SqlParameter pImagePath = sqlComm.Parameters.Add("@ImagePath", SqlDbType.NVarChar, 500);
            pImagePath.Direction = ParameterDirection.Input;
            pImagePath.Value = ImagePath;

            SqlParameter pUTProcess = sqlComm.Parameters.Add("@UTProcess", SqlDbType.NVarChar, 50);
            pUTProcess.Direction = ParameterDirection.Input;
            pUTProcess.Value = UTProcess;

            SqlParameter pMinAmount = sqlComm.Parameters.Add("@MinAmount", SqlDbType.Money);
            pMinAmount.Direction = ParameterDirection.Input;
            pMinAmount.Value = MinAmount;

            SqlParameter pMaxAmount = sqlComm.Parameters.Add("@MaxAmount", SqlDbType.Money);
            pMaxAmount.Direction = ParameterDirection.Input;
            pMaxAmount.Value = MaxAmount;

            SqlParameter pUTDigit = sqlComm.Parameters.Add("@UTDigit", SqlDbType.NVarChar, 10);
            pUTDigit.Direction = ParameterDirection.Input;
            pUTDigit.Value = UTDigit;

            SqlParameter pMobileBill = sqlComm.Parameters.Add("@MobileBill", SqlDbType.Bit);
            pMobileBill.Direction = ParameterDirection.Input;
            pMobileBill.Value = MobileBill;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteUtilityByID(int? UtilityID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteUtility", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUtilityID = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pUtilityID.Direction = ParameterDirection.Input;
            pUtilityID.Value = UtilityID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMultiLanguageUtilityByCode(string UtilityCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteMultiLanguageUtilityByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUtilityCode = sqlComm.Parameters.Add("@UtilityCode", SqlDbType.NVarChar, 10);
            pUtilityCode.Direction = ParameterDirection.Input;
            pUtilityCode.Value = UtilityCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion

        #region "View"

        public static DataSet GetRequestTopUpByID(int ID)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetRequestTopUpByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pType = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pType.Direction = ParameterDirection.Input;
            pType.Value = ID;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllPendingRequest()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllPendingRequest", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;


            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllRequest(int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllRequest", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetCashnameList(int type)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllCashname", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pType = sqlComm.Parameters.Add("@type", SqlDbType.Int);
            pType.Direction = ParameterDirection.Input;
            pType.Value = type;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllStatus(int type)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllStatus", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pType = sqlComm.Parameters.Add("@type", SqlDbType.Int);
            pType.Direction = ParameterDirection.Input;
            pType.Value = type;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllAdmin(int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllAdmin", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAdminByUsername(string Username, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAdminByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.VarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetModulesAccessRight(out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetModulesAccessRight", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAdminAccessRight(string Username, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Admin_GetAdminAccessRight", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.VarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetCurrencyById(int? currencyId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetCurrencyByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@CurrencyId", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = currencyId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetBankById(int? bankId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetBankByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pBankId = sqlComm.Parameters.Add("@BankID", SqlDbType.Int);
            pBankId.Direction = ParameterDirection.Input;
            pBankId.Value = bankId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetProvinceById(int? provinceId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetProvinceByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryId = sqlComm.Parameters.Add("@ProvinceID", SqlDbType.Int);
            pCountryId.Direction = ParameterDirection.Input;
            pCountryId.Value = provinceId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetProvinceByCode(string provinceCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetProvinceByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.VarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetCityByCode(string cityCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetCityByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.VarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetCityById(int? cityId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetCityByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryId = sqlComm.Parameters.Add("@CityID", SqlDbType.Int);
            pCountryId.Direction = ParameterDirection.Input;
            pCountryId.Value = cityId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetDistrictById(int? districtId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetDistrictByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pDistrictID = sqlComm.Parameters.Add("@DistrictID", SqlDbType.Int);
            pDistrictID.Direction = ParameterDirection.Input;
            pDistrictID.Value = districtId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetCourierCompanyByCode(string courierCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetCourierCompanyByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 50);
            pCode.Direction = ParameterDirection.Input;
            pCode.Value = courierCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetCountryByID(int? countryId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetCountryByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryId = sqlComm.Parameters.Add("@CountryId", SqlDbType.Int);
            pCountryId.Direction = ParameterDirection.Input;
            pCountryId.Value = countryId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetCountryByCountryCode(string CountryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetCountryByCountryCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = CountryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetLanguageByID(int languageId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetLanguageByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageId = sqlComm.Parameters.Add("@LanguageID", SqlDbType.Int);
            pLanguageId.Direction = ParameterDirection.Input;
            pLanguageId.Value = languageId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllLanguages(int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllLanguages", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllDistrictByCityAndLanguageCode(string languageCode, string cityCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllDistrictByCityAndLanguageCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCityByProvinceAndLanguageCode(string languageCode, string provinceCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllCityByProvinceAndLanguageCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllProvincesByCountryAndLanguageCode(string languageCode, string countryCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllProvincesByCountryAndLanguageCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllProvincesByCountry(string languageCode, string countryCode)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllProvincesByCountry", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCitiesByProvince(string languageCode, string provinceCode)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllCitiesByProvince", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllDistrictsByCity(string languageCode, string cityCode)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllDistrictsByCity", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllStockistByDistrict(string districtCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllStockistByDistrict", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pDistrictCode = sqlComm.Parameters.Add("@DistrictCode", SqlDbType.NVarChar, 20);
            pDistrictCode.Direction = ParameterDirection.Input;
            pDistrictCode.Value = districtCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllStockistsByCity(string cityCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllStockistByCity", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllStockistsByProvince(string provinceCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllStockistByProvince", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllStockistsByCountry(string countryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllStockistByCountry", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCourier(string languageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllCourier", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter PLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            PLanguageCode.Direction = ParameterDirection.Input;
            PLanguageCode.Value = languageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCountry(string languageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllCountry", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter PLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            PLanguageCode.Direction = ParameterDirection.Input;
            PLanguageCode.Value = languageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCountryList(string LanguageCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllCountryList", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCurrency(int viewPage, out int pages, out int ok, out string msg)
        {
            return GetAllCurrency(viewPage, string.Empty, out pages, out ok, out msg);
        }

        public static DataSet GetAllCurrency(int viewPage, string languageCode, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllCurrency", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pLang = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLang.Direction = ParameterDirection.Input;
            pLang.Value = languageCode;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllBank(string LanguageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllBank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCategory(string LanguageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllCategoryQMall", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCategoryList(string LanguageCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllCategoryList", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllRankList()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllRankList", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllProductByCategory(string LanguageCode, string categoryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllProductByCategory", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pCategoryCode = sqlComm.Parameters.Add("@CategoryCode", SqlDbType.NVarChar, 10);
            pCategoryCode.Direction = ParameterDirection.Input;
            pCategoryCode.Value = categoryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllBankByCountry(string LanguageCode, string CountryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllBankByCountry", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = CountryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllExpiredPackage(string LanguageCode, string packageType, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllExpiredPackage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pPackageType = sqlComm.Parameters.Add("@PackageType", SqlDbType.NVarChar, 1);
            pPackageType.Direction = ParameterDirection.Input;
            pPackageType.Value = packageType;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllPackage(string LanguageCode, string packageType, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllPackage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pPackageType = sqlComm.Parameters.Add("@PackageType", SqlDbType.NVarChar, 1);
            pPackageType.Direction = ParameterDirection.Input;
            pPackageType.Value = packageType;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetCategoryByID(int? categoryID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetCategoryByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryID = sqlComm.Parameters.Add("@CategoryID", SqlDbType.Int);
            pCategoryID.Direction = ParameterDirection.Input;
            pCategoryID.Value = categoryID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMultiLanguageProvinceByCountryCodeAndProvinceCode(string provinceCode, string countryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguageProvinceByCountryCodeAndProvinceCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMultiLanguageCityByProvinceAndCityCode(string cityCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguageCityByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMultiLanguageDistrictByDistrictCode(string districtCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguageDistrictByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pDistrictCode = sqlComm.Parameters.Add("@DistrictCode", SqlDbType.NVarChar, 10);
            pDistrictCode.Direction = ParameterDirection.Input;
            pDistrictCode.Value = districtCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMultiLanguageCourierCompanyByCode(string courierCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguageCourierCompanyByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 50);
            pCode.Direction = ParameterDirection.Input;
            pCode.Value = courierCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMultiLanguageCountryByCode(string countryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguageCountryByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCategoryCode.Direction = ParameterDirection.Input;
            pCategoryCode.Value = countryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMultiLanguageBankByCode(string bankCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguageBankByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pBankCode = sqlComm.Parameters.Add("@BankCode", SqlDbType.NVarChar, 10);
            pBankCode.Direction = ParameterDirection.Input;
            pBankCode.Value = bankCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMultiLanguageCategoryByCode(string CategoryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguageCategoryByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryCode = sqlComm.Parameters.Add("@CategoryCode", SqlDbType.NVarChar, 10);
            pCategoryCode.Direction = ParameterDirection.Input;
            pCategoryCode.Value = CategoryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetExpiredPackageByID(string packageType, int? packageID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetExpiredPackageByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPackageType = sqlComm.Parameters.Add("@PackageType", SqlDbType.NVarChar, 1);
            pPackageType.Direction = ParameterDirection.Input;
            pPackageType.Value = packageType;

            SqlParameter pPackageID = sqlComm.Parameters.Add("@PackageID", SqlDbType.Int);
            pPackageID.Direction = ParameterDirection.Input;
            pPackageID.Value = packageID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetPackageByID(string packageType, int? packageID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetPackageByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPackageType = sqlComm.Parameters.Add("@PackageType", SqlDbType.NVarChar, 1);
            pPackageType.Direction = ParameterDirection.Input;
            pPackageType.Value = packageType;

            SqlParameter pPackageID = sqlComm.Parameters.Add("@PackageID", SqlDbType.Int);
            pPackageID.Direction = ParameterDirection.Input;
            pPackageID.Value = packageID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetPackageByCode(string packageCode, string languageCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetPackageByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPackageCode = sqlComm.Parameters.Add("@PackageCode", SqlDbType.NVarChar, 15);
            pPackageCode.Direction = ParameterDirection.Input;
            pPackageCode.Value = packageCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMultiLanguagePackageByCode(string PackageCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguagePackageByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPackageCode = sqlComm.Parameters.Add("@PackageCode", SqlDbType.NVarChar, 15);
            pPackageCode.Direction = ParameterDirection.Input;
            pPackageCode.Value = PackageCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllSalesDeliveryMode(int viewPage, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllSalesDeliveryMode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetSalesDeliveryModeByID(int? deliveryID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSalesDeliveryModeByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryID = sqlComm.Parameters.Add("@DeliveryID", SqlDbType.Int);
            pCategoryID.Direction = ParameterDirection.Input;
            pCategoryID.Value = deliveryID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllRankSetup(int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllRankSetup", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllRank(int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllRank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetRankIcon()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetRank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetRankSettingpByID(int? rankID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetRankSettingByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryID = sqlComm.Parameters.Add("@RankID", SqlDbType.Int);
            pCategoryID.Direction = ParameterDirection.Input;
            pCategoryID.Value = rankID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        #endregion

        #region "Create"

        public static void CreateNewAdmin(string Username, string Password, string PIN, string Firstname,
                                          string createdBy, string updatedBy,
                                          out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateAdmin", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pPassword = sqlComm.Parameters.Add("@Password", SqlDbType.NVarChar, 50);
            pPassword.Direction = ParameterDirection.Input;
            pPassword.Value = Password;

            SqlParameter pPIN = sqlComm.Parameters.Add("@PIN", SqlDbType.NVarChar, 50);
            pPIN.Direction = ParameterDirection.Input;
            pPIN.Value = PIN;

            SqlParameter pSurname = sqlComm.Parameters.Add("@FullName", SqlDbType.NVarChar, 60);
            pSurname.Direction = ParameterDirection.Input;
            pSurname.Value = Firstname;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewLanguage(string languageCode, string languageName, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateLanguage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pLanguageName = sqlComm.Parameters.Add("@LanguageName", SqlDbType.NVarChar, 100);
            pLanguageName.Direction = ParameterDirection.Input;
            pLanguageName.Value = languageName;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewCourierCompany(string courierCode, string contactPerson, string contactNumber, string contactNumber1, string website, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateCourierCompany", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 50);
            pCode.Direction = ParameterDirection.Input;
            pCode.Value = courierCode;

            SqlParameter pContactPerson = sqlComm.Parameters.Add("@ContactPerson", SqlDbType.NVarChar, 100);
            pContactPerson.Direction = ParameterDirection.Input;
            pContactPerson.Value = contactPerson;

            SqlParameter pContactNumber = sqlComm.Parameters.Add("@ContactNumber1", SqlDbType.NVarChar, 50);
            pContactNumber.Direction = ParameterDirection.Input;
            pContactNumber.Value = contactNumber;

            SqlParameter pContactNumber1 = sqlComm.Parameters.Add("@ContactNumber2", SqlDbType.NVarChar, 50);
            pContactNumber1.Direction = ParameterDirection.Input;
            pContactNumber1.Value = contactNumber1;

            SqlParameter pWebSite = sqlComm.Parameters.Add("@Website", SqlDbType.NVarChar, 100);
            pWebSite.Direction = ParameterDirection.Input;
            pWebSite.Value = website;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewCountry(string countryCode, int sequenceNumber, string mobileCode, string ICLength, bool ICType, string createdBy, string updatedBy, string imagePath, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateCountry", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            var pSequenceNumber = sqlComm.Parameters.Add("@SequenceNumber", SqlDbType.Int);
            pSequenceNumber.Direction = ParameterDirection.Input;
            pSequenceNumber.Value = sequenceNumber;

            var pMobileCode = sqlComm.Parameters.Add("@MobileCode", SqlDbType.NVarChar, 10);
            pMobileCode.Direction = ParameterDirection.Input;
            pMobileCode.Value = mobileCode;

            var pICLength = sqlComm.Parameters.Add("@ICLength", SqlDbType.NVarChar, 10);
            pICLength.Direction = ParameterDirection.Input;
            pICLength.Value = ICLength;

            var pICType = sqlComm.Parameters.Add("@ICType", SqlDbType.Bit);
            pICType.Direction = ParameterDirection.Input;
            pICType.Value = ICType;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pImagePath = sqlComm.Parameters.Add("@ImagePath", SqlDbType.NVarChar, 500);
            pImagePath.Direction = ParameterDirection.Input;
            pImagePath.Value = imagePath;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewCurrency(string currencyCode, string currencyName, double currencySell, double currencyBuy, string countryCode, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateCurrency", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCurrencyCode = sqlComm.Parameters.Add("@CurrencyCode", SqlDbType.NVarChar, 10);
            pCurrencyCode.Direction = ParameterDirection.Input;
            pCurrencyCode.Value = currencyCode;

            SqlParameter pCurrencyName = sqlComm.Parameters.Add("@CurrencyName", SqlDbType.NVarChar, 100);
            pCurrencyName.Direction = ParameterDirection.Input;
            pCurrencyName.Value = currencyName;

            SqlParameter pCurrencySell = sqlComm.Parameters.Add("@CurrencySell", SqlDbType.Float);
            pCurrencySell.Direction = ParameterDirection.Input;
            pCurrencySell.Value = currencySell;

            SqlParameter pCurrencyBuy = sqlComm.Parameters.Add("@CurrencyBuy", SqlDbType.Float);
            pCurrencyBuy.Direction = ParameterDirection.Input;
            pCurrencyBuy.Value = currencyBuy;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewBank(string countryCode, string bankCode, float bankCharges, float maxBankCharges, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateBank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pBankCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 10);
            pBankCode.Direction = ParameterDirection.Input;
            pBankCode.Value = bankCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pBankCharges = sqlComm.Parameters.Add("@BankCharges", SqlDbType.Float);
            pBankCharges.Direction = ParameterDirection.Input;
            pBankCharges.Value = bankCharges;

            SqlParameter pMaxBankCharges = sqlComm.Parameters.Add("@MaxBankCharges", SqlDbType.Float);
            pMaxBankCharges.Direction = ParameterDirection.Input;
            pMaxBankCharges.Value = maxBankCharges;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewProvince(string provinceCode, string countryCode, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateProvince", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewCity(string provinceCode, string cityCode, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateCity", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewDistrict(string cityCode, string districtCode, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateDistrict", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pDistrictCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 10);
            pDistrictCode.Direction = ParameterDirection.Input;
            pDistrictCode.Value = districtCode;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewCategory(string CategoryCode, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateCategory", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 10);
            pCategoryCode.Direction = ParameterDirection.Input;
            pCategoryCode.Value = CategoryCode;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewMultiLanguageCourierCompany(string courierCode, string LanguageCode,
                                                        string courierName, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguageCourierCompany", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 50);
            pCode.Direction = ParameterDirection.Input;
            pCode.Value = courierCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pCourierName = sqlComm.Parameters.Add("@CourierName", SqlDbType.NVarChar, 100);
            pCourierName.Direction = ParameterDirection.Input;
            pCourierName.Value = courierName;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewMultiLanguageCountry(string countryCode, string LanguageCode,
                                                            string countryName, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguageCountry", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pCategoryName = sqlComm.Parameters.Add("@CountryName", SqlDbType.NVarChar, 100);
            pCategoryName.Direction = ParameterDirection.Input;
            pCategoryName.Value = countryName;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateMultiLanguageBank(string bankCode, string languageCode, string bankName,
           string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguageBank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pBankCode = sqlComm.Parameters.Add("@BankCode", SqlDbType.NVarChar, 10);
            pBankCode.Direction = ParameterDirection.Input;
            pBankCode.Value = bankCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pBankName = sqlComm.Parameters.Add("@BankName", SqlDbType.NVarChar, 20);
            pBankName.Direction = ParameterDirection.Input;
            pBankName.Value = bankName;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewMultiLanguageProvince(string countryCode, string languageCode, string provinceCode, string provinceName,
            string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguageProvince", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pProvinceName = sqlComm.Parameters.Add("@ProvinceName", SqlDbType.NVarChar, 100);
            pProvinceName.Direction = ParameterDirection.Input;
            pProvinceName.Value = provinceName;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewMultiLanguageCity(string provinceCode, string languageCode, string cityCode, string cityName,
         string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguageCity", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pCityName = sqlComm.Parameters.Add("@CityName", SqlDbType.NVarChar, 100);
            pCityName.Direction = ParameterDirection.Input;
            pCityName.Value = cityName;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewMultiLanguageDistrict(string cityCode, string languageCode, string districtCode, string districtName,
         string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguageDistrict", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pDistrictCode = sqlComm.Parameters.Add("@DistrictCode", SqlDbType.NVarChar, 10);
            pDistrictCode.Direction = ParameterDirection.Input;
            pDistrictCode.Value = districtCode;

            SqlParameter pDistrictName = sqlComm.Parameters.Add("@DistrictName", SqlDbType.NVarChar, 100);
            pDistrictName.Direction = ParameterDirection.Input;
            pDistrictName.Value = districtName;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewMultiLanguageCategory(string CategoryCode, string LanguageCode,
                                                            string CategoryName, string CategoryDescription,
                                                            string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguageCategory", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryCode = sqlComm.Parameters.Add("@CategoryCode", SqlDbType.NVarChar, 10);
            pCategoryCode.Direction = ParameterDirection.Input;
            pCategoryCode.Value = CategoryCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pCategoryName = sqlComm.Parameters.Add("@CategoryName", SqlDbType.NVarChar, 100);
            pCategoryName.Direction = ParameterDirection.Input;
            pCategoryName.Value = CategoryName;

            SqlParameter pCategoryDescription = sqlComm.Parameters.Add("@CategoryDescription", SqlDbType.NVarChar, 1000);
            pCategoryDescription.Direction = ParameterDirection.Input;
            pCategoryDescription.Value = CategoryDescription;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }


        public static void CreateNewPackage(string PackageType, PackageSetupModel psm, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreatePackage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pType = sqlComm.Parameters.Add("@Type", SqlDbType.NVarChar, 1);
            pType.Direction = ParameterDirection.Input;
            pType.Value = PackageType;

            SqlParameter pPackageCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 15);
            pPackageCode.Direction = ParameterDirection.Input;
            pPackageCode.Value = psm.PackageCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 20);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = psm.CountryName;

            SqlParameter pInvestment = sqlComm.Parameters.Add("@Investment", SqlDbType.Money);
            pInvestment.Direction = ParameterDirection.Input;
            pInvestment.Value = psm.TempPackageInvestment;

            SqlParameter pValidStart = sqlComm.Parameters.Add("@ValidStart", SqlDbType.Date);
            pValidStart.Direction = ParameterDirection.Input;
            pValidStart.Value = psm.ValidityStart;

            SqlParameter pValidEnd = sqlComm.Parameters.Add("@ValidEnd", SqlDbType.DateTime);
            pValidEnd.Direction = ParameterDirection.Input;
            pValidEnd.Value = psm.ValidityEnd;

            SqlParameter pPV = sqlComm.Parameters.Add("@PV", SqlDbType.Money);
            pPV.Direction = ParameterDirection.Input;
            pPV.Value = psm.PackagePV;

            var pValidty = sqlComm.Parameters.Add("@Validity", SqlDbType.Bit);
            pValidty.Direction = ParameterDirection.Input;
            pValidty.Value = Convert.ToInt16(psm.Validity);

            var pRank = sqlComm.Parameters.Add("@RankCode", SqlDbType.NVarChar, 20);
            pRank.Direction = ParameterDirection.Input;
            pRank.Value = psm.RankName;

            var pProductQty = sqlComm.Parameters.Add("@ProductQty", SqlDbType.Int);
            pProductQty.Direction = ParameterDirection.Input;
            pProductQty.Value = psm.TotalSetOfProduct;

            var pSequence = sqlComm.Parameters.Add("@Sequence", SqlDbType.Int);
            pSequence.Direction = ParameterDirection.Input;
            pSequence.Value = psm.DisplaySequence;

            SqlParameter pProductImage = sqlComm.Parameters.Add("@ProductImage", SqlDbType.NVarChar, 500);
            pProductImage.Direction = ParameterDirection.Input;
            pProductImage.Value = Helper.NVL(psm.ProductImagePath);

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            var pAccQty = sqlComm.Parameters.Add("@AccQty", SqlDbType.Int);
            pAccQty.Direction = ParameterDirection.Input;
            pAccQty.Value = Helper.NVLInteger(psm.SelectedAccountNo);

            SqlParameter pShare = sqlComm.Parameters.Add("@Eunit", SqlDbType.Money);
            pShare.Direction = ParameterDirection.Input;
            pShare.Value = 0;

            //SqlParameter pShareB = sqlComm.Parameters.Add("@EunitB", SqlDbType.Money);
            //pShareB.Direction = ParameterDirection.Input;
            //pShareB.Value = psm.EunitB;

            SqlParameter pREFERRAL = sqlComm.Parameters.Add("@REFERRAL", SqlDbType.Money);
            pREFERRAL.Direction = ParameterDirection.Input;
            pREFERRAL.Value = 0;

            SqlParameter pPAIRING = sqlComm.Parameters.Add("@PAIRING", SqlDbType.Money);
            pPAIRING.Direction = ParameterDirection.Input;
            pPAIRING.Value = 0;

            SqlParameter pShareSplit = sqlComm.Parameters.Add("@ROIMaxAmount", SqlDbType.Money);
            pShareSplit.Direction = ParameterDirection.Input;
            pShareSplit.Value = 0;

            SqlParameter pGoldMindPercentage = sqlComm.Parameters.Add("@ROIMaxPoint", SqlDbType.Money);
            pGoldMindPercentage.Direction = ParameterDirection.Input;
            pGoldMindPercentage.Value = 0;

            //SqlParameter pShareSplit = sqlComm.Parameters.Add("@ShareSplit", SqlDbType.Money);
            //pShareSplit.Direction = ParameterDirection.Input;
            //pShareSplit.Value = psm.ShareSplit;

            //SqlParameter pGoldMindPercentage = sqlComm.Parameters.Add("@GoldMindPercentage", SqlDbType.Money);
            //pGoldMindPercentage.Direction = ParameterDirection.Input;
            //pGoldMindPercentage.Value = psm.GoldMindPercentage;

            //SqlParameter pGoldMindDays = sqlComm.Parameters.Add("@GoldMindDays", SqlDbType.Int);
            //pGoldMindDays.Direction = ParameterDirection.Input;
            //pGoldMindDays.Value = psm.GoldMindDays;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewMultiLanguagePackage(string PackageCode, string LanguageCode,
                                                    string PackageName, string PackageDescription,
                                                    string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguagePackage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPackageCode = sqlComm.Parameters.Add("@PackageCode", SqlDbType.NVarChar, 15);
            pPackageCode.Direction = ParameterDirection.Input;
            pPackageCode.Value = PackageCode;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pPackageName = sqlComm.Parameters.Add("@PackageName", SqlDbType.NVarChar, 100);
            pPackageName.Direction = ParameterDirection.Input;
            pPackageName.Value = PackageName;

            SqlParameter pPackageDescription = sqlComm.Parameters.Add("@PackageDescription", SqlDbType.NVarChar, 1000);
            pPackageDescription.Direction = ParameterDirection.Input;
            pPackageDescription.Value = PackageDescription;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateSalesDeliveryMode(string method, string createdBy)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateSalesDeliveryMode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@Method", SqlDbType.NVarChar, 200);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = method;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        public static void UpdateSalesDeliveryMode(int? id, string method, string modifiedBy)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateSalesDeliveryMode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = id;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@Method", SqlDbType.NVarChar, 200);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = method;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = modifiedBy;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        public static void CreateNewRank(RankModel model, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateRank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pRankCode = sqlComm.Parameters.Add("@RankCode", SqlDbType.NVarChar, 10);
            pRankCode.Direction = ParameterDirection.Input;
            pRankCode.Value = model.RankCode;

            SqlParameter pRankName = sqlComm.Parameters.Add("@RankName", SqlDbType.NVarChar, 50);
            pRankName.Direction = ParameterDirection.Input;
            pRankName.Value = model.RankName;

            SqlParameter pHash = sqlComm.Parameters.Add("@Hash", SqlDbType.Money);
            pHash.Direction = ParameterDirection.Input;
            pHash.Value = model.Hash;

            SqlParameter pPerformanceHash = sqlComm.Parameters.Add("@PerformanceHash", SqlDbType.Money);
            pPerformanceHash.Direction = ParameterDirection.Input;
            pPerformanceHash.Value = model.PerformanceHash;

            SqlParameter pManagementHash = sqlComm.Parameters.Add("@ManagementHash", SqlDbType.Money);
            pManagementHash.Direction = ParameterDirection.Input;
            pManagementHash.Value = model.ManagementHash;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter RankIconPath = sqlComm.Parameters.Add("@RankIconPath", SqlDbType.NVarChar, 500);
            RankIconPath.Direction = ParameterDirection.Input;
            RankIconPath.Value = Helper.NVL(model.RankIconPath);

            SqlParameter pPV = sqlComm.Parameters.Add("@PV", SqlDbType.Money);
            pPV.Direction = ParameterDirection.Input;
            pPV.Value = model.PV;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateRank(RankModel model, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateRank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pRankCode = sqlComm.Parameters.Add("@RankCode", SqlDbType.NVarChar, 10);
            pRankCode.Direction = ParameterDirection.Input;
            pRankCode.Value = model.RankCode;

            SqlParameter pRankName = sqlComm.Parameters.Add("@RankName", SqlDbType.NVarChar, 50);
            pRankName.Direction = ParameterDirection.Input;
            pRankName.Value = model.RankName;

            SqlParameter pHash = sqlComm.Parameters.Add("@Hash", SqlDbType.Money);
            pHash.Direction = ParameterDirection.Input;
            pHash.Value = model.Hash;

            SqlParameter pPerformanceHash = sqlComm.Parameters.Add("@PerformanceHash", SqlDbType.Money);
            pPerformanceHash.Direction = ParameterDirection.Input;
            pPerformanceHash.Value = model.PerformanceHash;

            SqlParameter pManagementHash = sqlComm.Parameters.Add("@ManangementHash", SqlDbType.Money);
            pManagementHash.Direction = ParameterDirection.Input;
            pManagementHash.Value = model.ManagementHash;          


            SqlParameter pRankIcon = sqlComm.Parameters.Add("@RankIconPath", SqlDbType.NVarChar, 500);
            pRankIcon.Direction = ParameterDirection.Input;
            pRankIcon.Value = Helper.NVL(model.RankIconPath);

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        #region "Update"

        public static void UpdateAdminAccessRight(string Username, string UserAccess, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_AccessRight", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pUserAccess = sqlComm.Parameters.Add("@UserAccess", SqlDbType.NVarChar, 4000);
            pUserAccess.Direction = ParameterDirection.Input;
            pUserAccess.Value = UserAccess;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateAdmin(string Username, bool ChangePassword, string CurrentPassword, string NewPassword,
                                        bool ChangePIN, string CurrentPIN, string NewPIN, string Firstname, string updatedBy,
                                        out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateAdmin", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pChangePassword = sqlComm.Parameters.Add("@ChangePassword", SqlDbType.Bit);
            pChangePassword.Direction = ParameterDirection.Input;
            pChangePassword.Value = ChangePassword;

            SqlParameter pCurrentPassword = sqlComm.Parameters.Add("@CurrentPassword", SqlDbType.NVarChar, 50);
            pCurrentPassword.Direction = ParameterDirection.Input;
            pCurrentPassword.Value = Authentication.Encrypt(Helpers.Helper.NVL(CurrentPassword));

            SqlParameter pNewPassword = sqlComm.Parameters.Add("@NewPassword", SqlDbType.NVarChar, 50);
            pNewPassword.Direction = ParameterDirection.Input;
            pNewPassword.Value = Authentication.Encrypt(Helpers.Helper.NVL(NewPassword));

            SqlParameter pChangePIN = sqlComm.Parameters.Add("@ChangePIN", SqlDbType.Bit);
            pChangePIN.Direction = ParameterDirection.Input;
            pChangePIN.Value = ChangePIN;

            SqlParameter pCurrentPIN = sqlComm.Parameters.Add("@CurrentPIN", SqlDbType.NVarChar, 50);
            pCurrentPIN.Direction = ParameterDirection.Input;
            pCurrentPIN.Value = Authentication.Encrypt(Helpers.Helper.NVL(CurrentPIN));

            SqlParameter pNewPIN = sqlComm.Parameters.Add("@NewPIN", SqlDbType.NVarChar, 50);
            pNewPIN.Direction = ParameterDirection.Input;
            pNewPIN.Value = Authentication.Encrypt(Helpers.Helper.NVL(NewPIN));

            SqlParameter pFirstname = sqlComm.Parameters.Add("@FullName", SqlDbType.NVarChar, 60);
            pFirstname.Direction = ParameterDirection.Input;
            pFirstname.Value = Firstname;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateLanguage(int? languageID, string languageName, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateLanguage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageID = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pLanguageID.Direction = ParameterDirection.Input;
            pLanguageID.Value = languageID.Value;

            SqlParameter pLanguageName = sqlComm.Parameters.Add("@languageName", SqlDbType.NVarChar, 100);
            pLanguageName.Direction = ParameterDirection.Input;
            pLanguageName.Value = languageName;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateCourierCompany(int? courierId, string courierCode, string contactPerson, string contactNumber, string contactNumber1, string website, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateCourierCompany", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pId = sqlComm.Parameters.Add("@Id", SqlDbType.Int);
            pId.Direction = ParameterDirection.Input;
            pId.Value = courierId;

            SqlParameter pCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 50);
            pCode.Direction = ParameterDirection.Input;
            pCode.Value = courierCode;

            SqlParameter pContactPerson = sqlComm.Parameters.Add("@ContactPerson", SqlDbType.NVarChar, 100);
            pContactPerson.Direction = ParameterDirection.Input;
            pContactPerson.Value = contactPerson;

            SqlParameter pContactNumber = sqlComm.Parameters.Add("@ContactNumber1", SqlDbType.NVarChar, 50);
            pContactNumber.Direction = ParameterDirection.Input;
            pContactNumber.Value = contactNumber;

            SqlParameter pContactNumber1 = sqlComm.Parameters.Add("@ContactNumber2", SqlDbType.NVarChar, 50);
            pContactNumber1.Direction = ParameterDirection.Input;
            pContactNumber1.Value = contactNumber1;

            SqlParameter pWebSite = sqlComm.Parameters.Add("@Webpage", SqlDbType.NVarChar, 100);
            pWebSite.Direction = ParameterDirection.Input;
            pWebSite.Value = website;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateCountry(int? countryID, string countryCode, int sequenceNumber, string mobileCode, string ICLength, bool ICType, string imagePath, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateCountry", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryID = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pCategoryID.Direction = ParameterDirection.Input;
            pCategoryID.Value = countryID;

            var pSequenceNumber = sqlComm.Parameters.Add("@SequenceNumber", SqlDbType.Int);
            pSequenceNumber.Direction = ParameterDirection.Input;
            pSequenceNumber.Value = sequenceNumber;

            SqlParameter pCategoryCode = sqlComm.Parameters.Add("@code", SqlDbType.NVarChar, 10);
            pCategoryCode.Direction = ParameterDirection.Input;
            pCategoryCode.Value = countryCode;

            var pMobileCode = sqlComm.Parameters.Add("@MobileCode", SqlDbType.NVarChar, 10);
            pMobileCode.Direction = ParameterDirection.Input;
            pMobileCode.Value = mobileCode;

            var pICLength = sqlComm.Parameters.Add("@ICLength", SqlDbType.NVarChar, 10);
            pICLength.Direction = ParameterDirection.Input;
            pICLength.Value = ICLength;

            var pICType = sqlComm.Parameters.Add("@ICType", SqlDbType.Bit);
            pICType.Direction = ParameterDirection.Input;
            pICType.Value = ICType;

            SqlParameter pImagePath = sqlComm.Parameters.Add("@ImagePath", SqlDbType.NVarChar, 500);
            pImagePath.Direction = ParameterDirection.Input;
            pImagePath.Value = imagePath;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateCurrency(int? currencyId, string currencyCode, string currencyName, double currencySell, double currencyBuy, string countryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateCurrency", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@CurrencyId", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = currencyId;

            SqlParameter pCurrencyCode = sqlComm.Parameters.Add("@CurrencyCode", SqlDbType.NVarChar, 10);
            pCurrencyCode.Direction = ParameterDirection.Input;
            pCurrencyCode.Value = currencyCode;

            SqlParameter pCurrencyName = sqlComm.Parameters.Add("@CurrencyName", SqlDbType.NVarChar, 100);
            pCurrencyName.Direction = ParameterDirection.Input;
            pCurrencyName.Value = currencyName;

            SqlParameter pCurrencySell = sqlComm.Parameters.Add("@CurrencySell", SqlDbType.Float);
            pCurrencySell.Direction = ParameterDirection.Input;
            pCurrencySell.Value = currencySell;

            SqlParameter pCurrencyBuy = sqlComm.Parameters.Add("@CurrencyBuy", SqlDbType.Float);
            pCurrencyBuy.Direction = ParameterDirection.Input;
            pCurrencyBuy.Value = currencyBuy;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateBank(int? bankId, string bankCode, float bankCharges, float maxBankCharges, string countryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateBank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pId = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pId.Direction = ParameterDirection.Input;
            pId.Value = bankId;

            SqlParameter pCode = sqlComm.Parameters.Add("@code", SqlDbType.NVarChar, 20);
            pCode.Direction = ParameterDirection.Input;
            pCode.Value = bankCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pBankCharges = sqlComm.Parameters.Add("@BankCharges", SqlDbType.Float);
            pBankCharges.Direction = ParameterDirection.Input;
            pBankCharges.Value = bankCharges;

            SqlParameter pMaxBankCharges = sqlComm.Parameters.Add("@MaxBankCharges", SqlDbType.Float);
            pMaxBankCharges.Direction = ParameterDirection.Input;
            pMaxBankCharges.Value = maxBankCharges;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateProvince(int? provinceId, string provinceCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateProvince", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pProvinceId = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pProvinceId.Direction = ParameterDirection.Input;
            pProvinceId.Value = provinceId;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@code", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateCity(int? cityId, string cityCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateCity", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCityId = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pCityId.Direction = ParameterDirection.Input;
            pCityId.Value = cityId;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@code", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateDistrict(int? districtId, string districtCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateDistrict", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pDistrictId = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pDistrictId.Direction = ParameterDirection.Input;
            pDistrictId.Value = districtId;

            SqlParameter pDistrictCode = sqlComm.Parameters.Add("@code", SqlDbType.NVarChar, 10);
            pDistrictCode.Direction = ParameterDirection.Input;
            pDistrictCode.Value = districtCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateCategory(int? CategoryID, string CategoryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateCategory", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryID = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pCategoryID.Direction = ParameterDirection.Input;
            pCategoryID.Value = CategoryID;

            SqlParameter pCategoryCode = sqlComm.Parameters.Add("@code", SqlDbType.NVarChar, 10);
            pCategoryCode.Direction = ParameterDirection.Input;
            pCategoryCode.Value = CategoryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateAdminPassword(string Username, string Password, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateAdminPassword", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = Username;

            SqlParameter pPassword = sqlComm.Parameters.Add("@Password", SqlDbType.NVarChar, 50);
            pPassword.Direction = ParameterDirection.Input;
            pPassword.Value = Password;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdatePackage(int? PackageID, string PackageType, PackageSetupModel psm, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdatePackage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPackageID = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pPackageID.Direction = ParameterDirection.Input;
            pPackageID.Value = PackageID;

            SqlParameter pType = sqlComm.Parameters.Add("@Type", SqlDbType.NVarChar, 1);
            pType.Direction = ParameterDirection.Input;
            pType.Value = PackageType;

            SqlParameter pPackageCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 15);
            pPackageCode.Direction = ParameterDirection.Input;
            pPackageCode.Value = psm.PackageCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 20);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = psm.CountryName;

            SqlParameter pInvestment = sqlComm.Parameters.Add("@Investment", SqlDbType.Money);
            pInvestment.Direction = ParameterDirection.Input;
            pInvestment.Value = psm.TempPackageInvestment;

            SqlParameter pValidStart = sqlComm.Parameters.Add("@ValidStart", SqlDbType.Date);
            pValidStart.Direction = ParameterDirection.Input;
            pValidStart.Value = psm.ValidityStart;

            SqlParameter pValidEnd = sqlComm.Parameters.Add("@ValidEnd", SqlDbType.DateTime);
            pValidEnd.Direction = ParameterDirection.Input;
            pValidEnd.Value = psm.ValidityEnd;

            SqlParameter pPV = sqlComm.Parameters.Add("@PV", SqlDbType.Money);
            pPV.Direction = ParameterDirection.Input;
            pPV.Value = psm.PackagePV;

            var pValidty = sqlComm.Parameters.Add("@Validity", SqlDbType.Bit);
            pValidty.Direction = ParameterDirection.Input;
            pValidty.Value = Convert.ToInt16(psm.Validity);

            var pRank = sqlComm.Parameters.Add("@RankCode", SqlDbType.NVarChar, 20);
            pRank.Direction = ParameterDirection.Input;
            pRank.Value = psm.RankName;

            var pProductQty = sqlComm.Parameters.Add("@ProductQty", SqlDbType.Int);
            pProductQty.Direction = ParameterDirection.Input;
            pProductQty.Value = psm.TotalSetOfProduct;

            var pSequence = sqlComm.Parameters.Add("@Sequence", SqlDbType.Int);
            pSequence.Direction = ParameterDirection.Input;
            pSequence.Value = psm.DisplaySequence;

            SqlParameter pProductImage = sqlComm.Parameters.Add("@ProductImage", SqlDbType.NVarChar, 500);
            pProductImage.Direction = ParameterDirection.Input;
            pProductImage.Value = Helper.NVL(psm.ProductImagePath);

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            var pAccQty = sqlComm.Parameters.Add("@AccQty", SqlDbType.Int);
            pAccQty.Direction = ParameterDirection.Input;
            pAccQty.Value = Helper.NVLInteger(psm.SelectedAccountNo);

            SqlParameter pShare = sqlComm.Parameters.Add("@Eunit", SqlDbType.Money);
            pShare.Direction = ParameterDirection.Input;
            pShare.Value = psm.Eunit;

            //SqlParameter pShareB = sqlComm.Parameters.Add("@EunitB", SqlDbType.Money);
            //pShareB.Direction = ParameterDirection.Input;
            //pShareB.Value = psm.EunitB;

            SqlParameter pREFERRAL = sqlComm.Parameters.Add("@REFERRAL", SqlDbType.Money);
            pREFERRAL.Direction = ParameterDirection.Input;
            pREFERRAL.Value = 0;

            SqlParameter pPAIRING = sqlComm.Parameters.Add("@PAIRING", SqlDbType.Money);
            pPAIRING.Direction = ParameterDirection.Input;
            pPAIRING.Value = 0;

            SqlParameter pShareSplit = sqlComm.Parameters.Add("@ROIMaxAmount", SqlDbType.Money);
            pShareSplit.Direction = ParameterDirection.Input;
            pShareSplit.Value = 0;

            SqlParameter pGoldMindPercentage = sqlComm.Parameters.Add("@ROIMaxPoint", SqlDbType.Money);
            pGoldMindPercentage.Direction = ParameterDirection.Input;
            pGoldMindPercentage.Value = 0;

            //SqlParameter pShareSplit = sqlComm.Parameters.Add("@ShareSplit", SqlDbType.Money);
            //pShareSplit.Direction = ParameterDirection.Input;
            //pShareSplit.Value = psm.ShareSplit;

            //SqlParameter pGoldMindPercentage = sqlComm.Parameters.Add("@GoldMindPercentage", SqlDbType.Money);
            //pGoldMindPercentage.Direction = ParameterDirection.Input;
            //pGoldMindPercentage.Value = psm.GoldMindPercentage;

            //SqlParameter pGoldMindDays = sqlComm.Parameters.Add("@GoldMindDays", SqlDbType.Int);
            //pGoldMindDays.Direction = ParameterDirection.Input;
            //pGoldMindDays.Value = psm.GoldMindDays;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        #region "Delete"

        public static void DeleteAdmin(string Username, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteAdmin", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void InsertAdminOperation(string Username, string Operation, string Appoter, float AppNumber)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_InsertAdminOperation", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pOperation = sqlComm.Parameters.Add("@Operation", SqlDbType.NVarChar, 50);
            pOperation.Direction = ParameterDirection.Input;
            pOperation.Value = Operation;

            SqlParameter pAppoter = sqlComm.Parameters.Add("@AppOther", SqlDbType.NVarChar, 50);
            pAppoter.Direction = ParameterDirection.Input;
            pAppoter.Value = Appoter;

            SqlParameter pAppNumber = sqlComm.Parameters.Add("@AppNumber", SqlDbType.Float);
            pAppNumber.Direction = ParameterDirection.Input;
            pAppNumber.Value = AppNumber;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        public static void InActiveCurrencyByID(int? currencyId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_InActiveCurrencyByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@CurrencyId", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = currencyId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void ActiveCurrencyByID(int? currencyId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ActiveCurrencyByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@CurrencyId", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = currencyId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void ActiveCountryByID(int? currencyId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ActiveCountryByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@CountryID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = currencyId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void InactiveCountryByID(int? currencyId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_InActiveCountryByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@CountryID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = currencyId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteBankByCode(string bankCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteBankByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCode = sqlComm.Parameters.Add("@BankCode", SqlDbType.NVarChar, 10);
            pCode.Direction = ParameterDirection.Input;
            pCode.Value = bankCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteCityByCityCode(string cityCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteCityByCityCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteDistrictByDistrictCode(string districtCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteDistrictByDistrictCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pDistrictCode = sqlComm.Parameters.Add("@DistrictCode", SqlDbType.NVarChar, 10);
            pDistrictCode.Direction = ParameterDirection.Input;
            pDistrictCode.Value = districtCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteProvinceByProvinceCode(string provinceCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteProvinceByProvinceCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMultiLanguageBankByCode(string bankCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteMultiLanguageBankByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCode = sqlComm.Parameters.Add("@BankCode", SqlDbType.NVarChar, 10);
            pCode.Direction = ParameterDirection.Input;
            pCode.Value = bankCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMultiLanguageProvinceByProvinceCode(string provinceCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteMultiLanguageProvinceByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = provinceCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMultiLanguageCityByCityCode(string cityCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("[SP_DeleteMultiLanguageCityByCityCode]", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = cityCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMultiLanguageDistrictByDistrictCode(string districtCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("[SP_DeleteMultiLanguageDistrictByDistrictCode]", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pDistrictCode = sqlComm.Parameters.Add("@DistrictCode", SqlDbType.NVarChar, 10);
            pDistrictCode.Direction = ParameterDirection.Input;
            pDistrictCode.Value = districtCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMultiLanguageCourierCompanyByCode(string courierCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteMultiLanguageCourierCompanyByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCode = sqlComm.Parameters.Add("@Code", SqlDbType.NVarChar, 50);
            pCode.Direction = ParameterDirection.Input;
            pCode.Value = courierCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMultiLanguageCountryByCode(string countryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteMultiLanguageCountryByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCategoryCode.Direction = ParameterDirection.Input;
            pCategoryCode.Value = countryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMultiLanguageCategoryByCode(string CategoryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteMultiLanguageCategoryByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryCode = sqlComm.Parameters.Add("@CategoryCode", SqlDbType.NVarChar, 10);
            pCategoryCode.Direction = ParameterDirection.Input;
            pCategoryCode.Value = CategoryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMultiLanguagePackageByCode(string PackageCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteMultiLanguagePackageByCode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPackageCode = sqlComm.Parameters.Add("@PackageCode", SqlDbType.NVarChar, 15);
            pPackageCode.Direction = ParameterDirection.Input;
            pPackageCode.Value = PackageCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteSalesDeliveryMode(int deliveryID, string updatedBy)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteSalesDeliveryMode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPackageCode = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pPackageCode.Direction = ParameterDirection.Input;
            pPackageCode.Value = deliveryID;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@updatedBy", SqlDbType.NVarChar, 50);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = deliveryID;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        #endregion

        public static DataSet GetCompanySetup()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetCompanySetupByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;



            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        public static void CreateCompanySetup(CompanySetupModel csm, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateCompanySetup", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCompanyID = sqlComm.Parameters.Add("@CompanyID", SqlDbType.Int);
            pCompanyID.Direction = ParameterDirection.Input;
            pCompanyID.Value = 8;

            SqlParameter pCompanyGSTNo = sqlComm.Parameters.Add("@CompanyGSTNo", SqlDbType.NVarChar, 50);
            pCompanyGSTNo.Direction = ParameterDirection.Input;
            pCompanyGSTNo.Value = csm.CompanyGSTNo;

            SqlParameter pCompanyName = sqlComm.Parameters.Add("@CompanyName", SqlDbType.NVarChar, 50);
            pCompanyName.Direction = ParameterDirection.Input;
            pCompanyName.Value = csm.CompanyName;

            SqlParameter pCompanyAddress = sqlComm.Parameters.Add("@CompanyAddress", SqlDbType.NVarChar, 100);
            pCompanyAddress.Direction = ParameterDirection.Input;
            pCompanyAddress.Value = Helpers.Helper.NVL(csm.CompanyAddress);

            SqlParameter pCompanyAddress1 = sqlComm.Parameters.Add("@CompanyAddress1", SqlDbType.NVarChar, 50);
            pCompanyAddress1.Direction = ParameterDirection.Input;
            pCompanyAddress1.Value = Helpers.Helper.NVL(csm.CompanyAddress1);

            SqlParameter pCompanyAddress2 = sqlComm.Parameters.Add("@CompanyAddress2", SqlDbType.NVarChar, 50);
            pCompanyAddress2.Direction = ParameterDirection.Input;
            pCompanyAddress2.Value = Helpers.Helper.NVL(csm.CompanyAddress2);

            SqlParameter pCompanyAddress3 = sqlComm.Parameters.Add("@CompanyAddress3", SqlDbType.NVarChar, 50);
            pCompanyAddress3.Direction = ParameterDirection.Input;
            pCompanyAddress3.Value = Helpers.Helper.NVL(csm.CompanyAddress3);

            SqlParameter pPostCode = sqlComm.Parameters.Add("@CompanyPostCode", SqlDbType.NVarChar, 50);
            pPostCode.Direction = ParameterDirection.Input;
            pPostCode.Value = csm.PostCode;

            SqlParameter pCompanyState = sqlComm.Parameters.Add("@CompanyState", SqlDbType.NVarChar, 50);
            pCompanyState.Direction = ParameterDirection.Input;
            pCompanyState.Value = csm.State;

            SqlParameter pCompanyCountry = sqlComm.Parameters.Add("@CompanyCountry", SqlDbType.NVarChar, 50);
            pCompanyCountry.Direction = ParameterDirection.Input;
            pCompanyCountry.Value = csm.Country;

            SqlParameter pCompanyTel = sqlComm.Parameters.Add("@CompanyTel", SqlDbType.NVarChar, 50);
            pCompanyTel.Direction = ParameterDirection.Input;
            pCompanyTel.Value = csm.TelNo;

            SqlParameter pCompanyFax = sqlComm.Parameters.Add("@CompanyFax", SqlDbType.NVarChar, 50);
            pCompanyFax.Direction = ParameterDirection.Input;
            pCompanyFax.Value = csm.FaxNo;

            SqlParameter pCompanyEmail = sqlComm.Parameters.Add("@CompanyEmail", SqlDbType.NVarChar, 50);
            pCompanyEmail.Direction = ParameterDirection.Input;
            pCompanyEmail.Value = csm.Email;

            SqlParameter pCompanyImage = sqlComm.Parameters.Add("@CompanyImage", SqlDbType.NVarChar, 500);
            pCompanyImage.Direction = ParameterDirection.Input;
            pCompanyImage.Value = Helper.NVL(csm.CompanyImagePath);

            SqlParameter pCompanyURL = sqlComm.Parameters.Add("@CompanyURL", SqlDbType.NVarChar, 2000);
            pCompanyURL.Direction = ParameterDirection.Input;
            pCompanyURL.Value = Helper.NVL(csm.CompanyURL);

            SqlParameter pCompanyRegistrationNo = sqlComm.Parameters.Add("@CompanyRegisterNO", SqlDbType.NVarChar, 50);
            pCompanyRegistrationNo.Direction = ParameterDirection.Input;
            pCompanyRegistrationNo.Value = csm.CompanyRegistrationNo;


            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateCompanySetup(CompanySetupModel csm, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateCompanySetup", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCompanyID = sqlComm.Parameters.Add("@CompanyID", SqlDbType.Int);
            pCompanyID.Direction = ParameterDirection.Input;
            pCompanyID.Value = 8;

            SqlParameter pCompanyGSTNo = sqlComm.Parameters.Add("@CompanyGSTNo", SqlDbType.NVarChar, 50);
            pCompanyGSTNo.Direction = ParameterDirection.Input;
            pCompanyGSTNo.Value = csm.CompanyGSTNo;

            SqlParameter pCompanyName = sqlComm.Parameters.Add("@CompanyName", SqlDbType.NVarChar, 50);
            pCompanyName.Direction = ParameterDirection.Input;
            pCompanyName.Value = csm.CompanyName;

            SqlParameter pCompanyAddress = sqlComm.Parameters.Add("@CompanyAddress", SqlDbType.NVarChar, 100);
            pCompanyAddress.Direction = ParameterDirection.Input;
            pCompanyAddress.Value = Helpers.Helper.NVL(csm.CompanyAddress);

            SqlParameter pCompanyAddress1 = sqlComm.Parameters.Add("@CompanyAddress1", SqlDbType.NVarChar, 50);
            pCompanyAddress1.Direction = ParameterDirection.Input;
            pCompanyAddress1.Value = Helpers.Helper.NVL(csm.CompanyAddress1);

            SqlParameter pCompanyAddress2 = sqlComm.Parameters.Add("@CompanyAddress2", SqlDbType.NVarChar, 50);
            pCompanyAddress2.Direction = ParameterDirection.Input;
            pCompanyAddress2.Value = Helpers.Helper.NVL(csm.CompanyAddress2);

            SqlParameter pCompanyAddress3 = sqlComm.Parameters.Add("@CompanyAddress3", SqlDbType.NVarChar, 50);
            pCompanyAddress3.Direction = ParameterDirection.Input;
            pCompanyAddress3.Value = Helpers.Helper.NVL(csm.CompanyAddress3);

            SqlParameter pPostCode = sqlComm.Parameters.Add("@CompanyPostCode", SqlDbType.NVarChar, 50);
            pPostCode.Direction = ParameterDirection.Input;
            pPostCode.Value = csm.PostCode;

            SqlParameter pCompanyState = sqlComm.Parameters.Add("@CompanyState", SqlDbType.NVarChar, 50);
            pCompanyState.Direction = ParameterDirection.Input;
            pCompanyState.Value = csm.State;

            SqlParameter pCompanyCountry = sqlComm.Parameters.Add("@CompanyCountry", SqlDbType.NVarChar, 50);
            pCompanyCountry.Direction = ParameterDirection.Input;
            pCompanyCountry.Value = csm.Country;

            SqlParameter pCompanyTel = sqlComm.Parameters.Add("@CompanyTel", SqlDbType.NVarChar, 50);
            pCompanyTel.Direction = ParameterDirection.Input;
            pCompanyTel.Value = csm.TelNo;

            SqlParameter pCompanyFax = sqlComm.Parameters.Add("@CompanyFax", SqlDbType.NVarChar, 50);
            pCompanyFax.Direction = ParameterDirection.Input;
            pCompanyFax.Value = csm.FaxNo;

            SqlParameter pCompanyEmail = sqlComm.Parameters.Add("@CompanyEmail", SqlDbType.NVarChar, 50);
            pCompanyEmail.Direction = ParameterDirection.Input;
            pCompanyEmail.Value = csm.Email;

            SqlParameter pCompanyRegistrationNo = sqlComm.Parameters.Add("@CompanyRegisterNO", SqlDbType.NVarChar, 50);
            pCompanyRegistrationNo.Direction = ParameterDirection.Input;
            pCompanyRegistrationNo.Value = csm.CompanyRegistrationNo;

            SqlParameter pCompanyImage = sqlComm.Parameters.Add("@CompanyImage", SqlDbType.NVarChar, 500);
            pCompanyImage.Direction = ParameterDirection.Input;
            pCompanyImage.Value = Helper.NVL(csm.CompanyImagePath);

            SqlParameter pCompanyURL = sqlComm.Parameters.Add("@CompanyURL", SqlDbType.NVarChar, 2000);
            pCompanyURL.Direction = ParameterDirection.Input;
            pCompanyURL.Value = Helper.NVL(csm.CompanyURL);

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static DataSet GetSalesReportByMemberID(string username, string startdate, string enddate)
        {
            var sqlConn = DBOperator.GetConnection();
            var dataAdapter = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSalesReportByMemberID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pStartDate = sqlComm.Parameters.Add("@StartDate", SqlDbType.NVarChar, 20);
            pStartDate.Direction = ParameterDirection.Input;
            pStartDate.Value = startdate;

            SqlParameter pEndDate = sqlComm.Parameters.Add("@EndDate", SqlDbType.NVarChar, 20);
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = enddate;

            dataAdapter.SelectCommand = sqlComm;
            var dataset = new DataSet();

            sqlConn.Open();
            dataAdapter.Fill(dataset);

            sqlConn.Close();
            return dataset;
        }

    }
}