﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECFBase.Components
{
    public class Modules
    {
        /// <summary>
        /// Get module name
        /// </summary>
        /// <param name="moduleCode"></param>
        /// <returns></returns>
        public static string GetModuleName(string moduleCode)
        {
            switch (moduleCode)
            {
                case "ADMINUSER":
                    return Resources.Member.lblGeneral + " - " + Resources.Member.mnuAdminUser;
                case "MASTERSETUP":
                    return Resources.Member.lblGeneral + " - " + Resources.Member.mnuMasterSetup;
                case "PRODUCT":
                    return Resources.Member.lblGeneral + " - " + Resources.Member.mnuProduct;
                case "MEMBERUTILITY":
                    return Resources.Member.lblMembers + " - " + Resources.Member.mnuUtility;
                case "MEMBERREPORTS":
                    return Resources.Member.lblMembers + " - " + Resources.Member.mnuReports;
                case "BONUSUTILITY":
                    return Resources.Member.mnuBonus + " - " + Resources.Member.mnuUtility;
                case "WALLETUTILITY":
                    return Resources.Member.lblWallet + " - " + Resources.Member.mnuUtility;
                case "WALLETREPORTS":
                    return Resources.Member.lblWallet + " - " + Resources.Member.mnuReports;
                case "CORPORATENEWS":
                    return Resources.Member.mnuInfoDesk + " - " + Resources.Member.mnuCorpNews;
                case "EVENTSANDPROMOTIONS":
                    return Resources.Member.mnuInfoDesk + " - " + Resources.Member.mnuEventsPromo;
                case "UPLOADCENTER":
                    return Resources.Member.mnuInfoDesk + " - " + Resources.Member.mnuUploadCenter;
                case "SALESORDERUTILITY":
                    return Resources.Member.lblSalesOrder + " - " + Resources.Member.mnuUtility;
                case "SALESORDERREPORT":
                    return Resources.Member.lblSalesOrder + " - " + Resources.Member.mnuReports;
                case "HELPDESKINBOX":
                    return Resources.Member.mnuHelpDesk + " - " + Resources.Member.mnuInbox;
                case "HELPDESKNOTICE":
                    return Resources.Member.mnuHelpDesk + " - " + Resources.Member.mnuNotice;


                default:
                    return moduleCode;
            }
        }

        /// <summary>
        /// Get sub module name
        /// </summary>
        /// <param name="moduleCode"></param>
        /// <returns></returns>
        public static string GetSubModuleName(string moduleCode)
        {
            switch (moduleCode)
            {
                // SubModules
                case "USERSETUP":
                    return Resources.Member.mnuAdminUserSetup;
                case "PERMISSIONSETUP":
                    return Resources.Member.mnuAccessControl;
                case "RESETUSERPASSWORD":
                    return Resources.Member.mnuResetAdminPwd;
                case "GENERALSETTINGS":
                    return Resources.Member.mnuGeneralSettings;


                case "COUNTRY":
                    return Resources.Member.mnuCountry;
                case "PROVINCE":
                    return Resources.Member.mnuProvince;
                case "CITY":
                    return Resources.Member.mnuCity;
                case "DISTRICT":
                    return Resources.Member.mnuDistrict;
                case "CURRENCYEXCHANGE":
                    return Resources.Member.mnuCurrExc;
                case "PAYMENTMODE":
                    return Resources.Member.mnuPaymentmode;
                case "SALESDELIVERYMODE":
                    return Resources.Member.mnuDeliveryTo;
                case "BANK":
                    return Resources.Member.mnuBank;
                case "LANGUAGE":
                    return Resources.Member.mnuLanguage;
                case "UPGRADEPACKAGE":
                    return Resources.Member.mnuUDPackage;
                case "RANKSETUP":
                    return Resources.Member.mnuRankSetup;
                //case "MEGABONUSQUALIFIER":
                //    return Resources.Member.mnuMegaBonusQualifier;
                case "MEMBERAUTOWITHDRAWAL":
                    return Resources.Member.mnuMemberAutoWithdrawal;
                case "WALLETSBALANCE":
                    return Resources.Member.mnuWalletBalance;
                case "SEARCHSPONSOR":
                    return Resources.Member.mnuSearchSponsor;
                case "SEARCHUPLINE":
                    return Resources.Member.mnuSearchUpline;
                //case "WRPSETUP":
                //    return Resources.Member.mnuWRPSetup;
                //case "WRPSUBACCOUNTSETUP":
                //    return Resources.Member.mnuWRPSubAccountSetup;
                //case "WRPSUMMARY":
                //    return Resources.Member.mnuWRPSummary;
                //case "WRPLISTING":
                //    return Resources.Member.mnuWRPListing;
                //case "WRPDETAILSUMMARY":
                //    return Resources.Member.mnuWRPDetailSummary;
                //case "WRPLOG":
                //    return Resources.Member.mnuWRPLog;
                case "UPGRADEMEMBER":
                    return Resources.Member.mnuUpgradeMember;
                case "DAILYSALES":
                    return Resources.Member.mnuDailySales;
                case "SPONSORSALES":
                    return Resources.Member.mnuSponsorSales;
                case "SPONSORSALESCALCULATION":
                    return Resources.Member.mnuSponsorSalesCalculation;
                
                //case "RPTOPUP":
                //    return Resources.Member.mnuRwalletTopUp;
                //case "CRPTOPUP":
                //    return Resources.Member.mnuCVRwalletTopUp;
                //case "EPTOPUP":
                //    return Resources.Member.mnuEwalletTopUp;
                //case "SPTOPUP":
                //    return Resources.Member.mnuSwalletTopUp;
                //case "VPTOPUP":
                //    return Resources.Member.mnuMwalletTopUp;
                //case "VPWITHDRAWALLIST":
                //    return Resources.Member.mnuMWithdrawalList;

                //case "RPLOGREPORT":
                //    return Resources.Member.mnuRegWalletLog;
                //case "CRPLOGREPORT":
                //    return Resources.Member.mnuCVRegWalletLog;
                //case "EPLOGREPORT":
                //    return Resources.Member.mnuEwalletLog;
                //case "SPLOGREPORT":
                //    return Resources.Member.mnuSWalletLog;
                //case "VPLOGREPORT":
                //    return Resources.Member.mnuMWalletLog;
                //case "EPWITHDRAWALLOGREPORT":
                //    return Resources.Member.mnuWithdrawalLog;
                //case "VPWITHDRAWALLOGREPORT":
                //    return Resources.Member.mnuMWithdrawalLog;
                //case "RPTOPUPREPORT":
                //    return Resources.Member.mnuRwalletTopUp;
                //case "CRPTOPUPREPORT":
                //    return Resources.Member.mnuCVRwalletTopUp;
                //case "EPTOPUPREPORT":
                //    return Resources.Member.mnuEwalletTopUp;
                //case "VPTOPUPREPORT":
                //    return Resources.Member.mnuMwalletTopUp;

                case "CATEGORY":
                    return Resources.Member.mnuCategory;
                case "PRODUCT":
                    return Resources.Member.mnuProduct;
                case "REGISTRATIONPACKAGE":
                    return Resources.Member.mnuRegistrationPackage;
                case "MAINTENANCEPACKAGE":
                    return Resources.Member.mnuMaintPackage;
                case "STOCKMENTREPORT":
                    return Resources.Member.mnuStockmentReport;
                case "STOCKADJUSTMENTNOTE":
                    return Resources.Member.mnuStockAdjustment;
                case "STOCKISTSTOCKADJUSTMENTNOTE":
                    return Resources.Member.mnuStockistStockAdjustment;
                case "SALESANALYSIS":
                    return Resources.Member.mnuSalesAnalysisByCtryOrProv;
                case "MEMBERANALYSIS":
                    return Resources.Member.mnuMembershipAnalysisByCtryOrProv;


                case "FREEREGISTRATION":
                    return Resources.Member.mnuFreeReg;
                case "CHANGEPASSWORD":
                    return Resources.Member.mnuChgPassword;
                case "CHANGEPIN":
                    return Resources.Member.mnuChgPIN;
                case "MEMBERPERMISSION":
                    return Resources.Member.mnuChangePermission;
                case "CHANGENAME":
                    return Resources.Member.mnuChangeOwnership;
                case "CHANGERANKING":
                    return Resources.Member.mnuChangeRanking;
                case "CHANGESPONSOR":
                    return Resources.Member.mnuChangeSponsor;
                case "VIEWPASSWORD":
                    return Resources.Member.mnuViewPassword;
                case "COURIER":
                    return Resources.Member.mnuCourierCompany;


                case "MEMBERLISTING":
                    return Resources.Member.mnuMemberListing;
                case "SPONSORCHART":
                    return Resources.Member.mnuSponsorList;
                case "PLACEMENTCHART":
                    return Resources.Member.mnuPlacementChart;
                case "DIRECTSPONSOR":
                    return Resources.Member.mnuDirectSponsor;


                case "BONUSPARAMETERSETTING":
                    return Resources.Member.mnuBonusParamSetting;
                case "BONUSPAYOUTSUMMARY":
                    return Resources.Member.mnuBonusPayoutSummary;

                //case "EWALLETTOPUP":
                //    return Resources.Member.mnuEwalletTopUp;
                //case "PVWALLETTOPUP":
                //    return Resources.Member.mnuPVwalletTopup;
                //case "WITHDRAWALHISTORY":
                //    return Resources.Member.mnuWithdrawalLog;


                //case "EWALLETLOG":
                //    return Resources.Member.mnuEwalletLog;
                //case "PVWALLETLOG":
                //    return Resources.Member.mnuPVwalletLog;
                //case "EWALLETSUMMARY":
                //    return Resources.Member.mnuDailyEwalletSummary;
                //case "PVWALLETSUMMARY":
                //    return Resources.Member.mnuDailyPVwalletSummary;


                case "CORPORATENEWS":
                    return Resources.Member.lblNewCorpNews;
                case "EVENTSANDPROMOTIONS":
                    return Resources.Member.mnuEventsPromo;
                case "UPLOADCENTER":
                    return Resources.Member.mnuUploadCenter;


                case "INBOX":
                    return Resources.Member.mnuInbox;
                case "NOTICE":
                    return Resources.Member.mnuNotice;


                case "INVOICE":
                    return Resources.Member.mnuInvoice;
                case "DELIVERYORDER":
                    return Resources.Member.mnuDeliveryOrder;

                default:
                    return moduleCode;
            }
        }

        /// <summary>
        /// Get submodule method
        /// </summary>
        /// <param name="moduleCode"></param>
        /// <returns></returns>
        public static string GetSubModuleMethod(string moduleCode)
        {
            if (moduleCode.ToLower().Contains("view"))
            {
               return Resources.Member.mnuAllowView;
            }
            else
            {
                return Resources.Member.mnuAllowEdit;
            }

        }
    }
}