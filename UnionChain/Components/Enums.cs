﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECFBase.Components
{
    public enum Ranking
    {
        rankAgent,
        //Supervisor,
        //Manager,
        Director,
        President,
        Chairman,
        SponsoredMember,
        None
    }

    public enum MaintenanceStatus
    {
        Qualify,
        NotQualify
    }

    public enum InboxType
    {
        General
    }

}