﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Web;

namespace ECFBase.Components
{
   
    //保存单个属性的类
    public class Property
    {
        public string Key;
        public string Value;
        public Property(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}