﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ECFBase.Components
{
    public class BonusSettingDB
    {
        #region Get

        public static DataSet GetCurrentSales(int selectedPage, int selectedYear, int selectedMonth, int Country, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetCurrentSales", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetHashPerformanceBonusList(int selectedPage, int selectedYear, int selectedMonth, int Country, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashPerformanceBonusList", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetHashPerformanceBonusListExport(int selectedPage, int selectedYear, int selectedMonth, int Country, string Username, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashPerformanceBonusListExport", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.VarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetHashPerformanceBonusListByUsername(int selectedPage, int selectedYear, int selectedMonth, int Country, string Username, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashPerformanceBonusListByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.VarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetHashPowerBonusList(int selectedPage, int selectedYear, int selectedMonth, int Country, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashPowerBonusList", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetHashPowerBonusListByUsername(int selectedPage, int selectedYear, int selectedMonth, int Country, string Username, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashPowerBonusListByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.VarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }
        public static DataSet GetHashPowerBonusListExport(int selectedPage, int selectedYear, int selectedMonth, int Country, string Username, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashPowerBonusListExport", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.VarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetHashBonusList(int selectedPage, int selectedYear, int selectedMonth, int Country, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashBonusList", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetHashBonusListByUsername(int selectedPage, int selectedYear, int selectedMonth, int Country,string Username, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashBonusListByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.VarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }
        public static DataSet GetHashBonusListExport(int selectedPage, int selectedYear, int selectedMonth, int Country, string Username, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashBonusListExport", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.VarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetBonusListingByDate(int selectedPage, string DateFrom, string DateTo, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_BonusListingByDate", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pDateFrom = sqlComm.Parameters.Add("@DateFrom", SqlDbType.NVarChar, 50);
            pDateFrom.Direction = ParameterDirection.Input;
            pDateFrom.Value = DateFrom;

            var pDateTo = sqlComm.Parameters.Add("@DateTo", SqlDbType.NVarChar, 50);
            pDateTo.Direction = ParameterDirection.Input;
            pDateTo.Value = DateTo;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetCurrentDailySales(int selectedPage, int selectedYear, int selectedMonth, int Country, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetCurrentDailySales", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = selectedYear;

            var pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = selectedMonth;

            var pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.Int);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetTotalDirectSponsorList(int selectedPage, string Username, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetTotalDirectSponsor", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.VarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;
            
            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;
            
            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        #endregion

    }
}