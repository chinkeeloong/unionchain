﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ECFBase.Models;

namespace ECFBase.Components
{
    public class AdminSalesOrderDB
    {
        #region Invoice
        public static DataSet GetAllInvoices(string LanguageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllInvoices", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllInvoicesByUsername(string username, string LanguageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllInvoicesByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetInvoiceRelatedInfoById(int? InvoiceID, string LanguageCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetInvoiceRelatedInfoById", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInvoiceNo = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pInvoiceNo.Direction = ParameterDirection.Input;
            pInvoiceNo.Value = InvoiceID;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetInvoiceByID(int? InvoiceID, out int ok, out string msg, out int type)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetInvoiceByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInvoiceNo = sqlComm.Parameters.Add("@InvoiceNo", SqlDbType.Int);
            pInvoiceNo.Direction = ParameterDirection.Input;
            pInvoiceNo.Value = InvoiceID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pInvoiceType = sqlComm.Parameters.Add("@invoiceType", SqlDbType.Int);
            pInvoiceType.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            type = (int)pInvoiceType.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetShoppingInvoiceById(int? InvoiceID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetShoppingInvoiceByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInvoiceNo = sqlComm.Parameters.Add("@InvoiceNo", SqlDbType.Int);
            pInvoiceNo.Direction = ParameterDirection.Input;
            pInvoiceNo.Value = InvoiceID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetInvoiceProdByID(int? InvoiceID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetInvoiceProduct", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInvoiceNo = sqlComm.Parameters.Add("@InvoiceNo", SqlDbType.Int);
            pInvoiceNo.Direction = ParameterDirection.Input;
            pInvoiceNo.Value = InvoiceID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet UpdateInvoiceById(InvoiceModel invoice, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateInvoiceInfoById", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInvoiceNo = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pInvoiceNo.Direction = ParameterDirection.Input;
            pInvoiceNo.Value = invoice.Id;

            SqlParameter pShipAddress = sqlComm.Parameters.Add("@address", SqlDbType.NVarChar, 500);
            pShipAddress.Direction = ParameterDirection.Input;
            pShipAddress.Value = Helpers.Helper.NVL(invoice.ShipAddress) + "\r" +
                                    Helpers.Helper.NVL(invoice.ShipAddress1) + "\r" +
                                    Helpers.Helper.NVL(invoice.ShipAddress2) + "\r" +
                                    Helpers.Helper.NVL(invoice.ShipAddress3);


            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet SP_GetAllInvoicesByDate(string languageCode, DateTime fromDate, DateTime toDate, int viewPage, out int pages, out int ok, out string msg)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllInvoicesByDate", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pLanguage = sqlComm.Parameters.Add("@language", SqlDbType.NVarChar, 10);
                    pLanguage.Direction = ParameterDirection.Input;
                    pLanguage.Value = languageCode;

                    SqlParameter pFromDate = sqlComm.Parameters.Add("@fromDate", SqlDbType.DateTime);
                    pFromDate.Direction = ParameterDirection.Input;
                    pFromDate.Value = fromDate;

                    SqlParameter pToDate = sqlComm.Parameters.Add("@toDate", SqlDbType.DateTime);
                    pToDate.Direction = ParameterDirection.Input;
                    pToDate.Value = toDate;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    msg = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllInvoicesByFilteringCriteria(string languageCode, string fc, string searchFC, int viewPage, out int pages, out int ok, out string msg)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllInvoicesByFilteringCriteria", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pLanguage = sqlComm.Parameters.Add("@language", SqlDbType.NVarChar, 10);
                    pLanguage.Direction = ParameterDirection.Input;
                    pLanguage.Value = languageCode;

                    SqlParameter pFc = sqlComm.Parameters.Add("@fc", SqlDbType.NVarChar, 50);
                    pFc.Direction = ParameterDirection.Input;
                    pFc.Value = fc;

                    SqlParameter pSearchFc = sqlComm.Parameters.Add("@searchFc", SqlDbType.NVarChar, 100);
                    pSearchFc.Direction = ParameterDirection.Input;
                    pSearchFc.Value = searchFC;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    msg = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllInvoicesByStatus(string languageCode, string fc, int viewPage, out int pages, out int ok, out string msg)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllInvoicesByStatus", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pLanguage = sqlComm.Parameters.Add("@language", SqlDbType.NVarChar, 10);
                    pLanguage.Direction = ParameterDirection.Input;
                    pLanguage.Value = languageCode;

                    SqlParameter pFc = sqlComm.Parameters.Add("@fc", SqlDbType.NVarChar, 50);
                    pFc.Direction = ParameterDirection.Input;
                    pFc.Value = fc;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    msg = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        #endregion

        public static DataSet GetSalesAnalysis(string selectedMonth, string selectedYear, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSalesAnalysis", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pSelectedMonth = sqlComm.Parameters.Add("@selectedMonth", SqlDbType.NVarChar, 2);
            pSelectedMonth.Direction = ParameterDirection.Input;
            pSelectedMonth.Value = selectedMonth;

            SqlParameter pSelectedYear = sqlComm.Parameters.Add("@selectedYear", SqlDbType.NVarChar, 4);
            pSelectedYear.Direction = ParameterDirection.Input;
            pSelectedYear.Value = selectedYear;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMembershipAnalysis(string selectedMonth, string selectedYear, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMembershipAnalysis", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pSelectedMonth = sqlComm.Parameters.Add("@selectedMonth", SqlDbType.NVarChar, 2);
            pSelectedMonth.Direction = ParameterDirection.Input;
            pSelectedMonth.Value = selectedMonth;

            SqlParameter pSelectedYear = sqlComm.Parameters.Add("@selectedYear", SqlDbType.NVarChar, 4);
            pSelectedYear.Direction = ParameterDirection.Input;
            pSelectedYear.Value = selectedYear;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }



        public static DataSet GetDOByDONO(string DONO, out int ok, out string msg, out int type)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetDOByDONO", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pDONO = sqlComm.Parameters.Add("@DONO", SqlDbType.NVarChar, 10);
            pDONO.Direction = ParameterDirection.Input;
            pDONO.Value = DONO;

            var pDOType = sqlComm.Parameters.Add("@DOType", SqlDbType.Int);
            pDOType.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            type = (int)pDOType.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetDOProdByDONO(string DONO, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetInvoiceProductByDO", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pDONO = sqlComm.Parameters.Add("@DONO", SqlDbType.NVarChar, 10);
            pDONO.Direction = ParameterDirection.Input;
            pDONO.Value = DONO;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        #region GetAllDeliveryOrder

        public static void UpdateDeliveryOrder(string deliveryOrderNo, string selectedDeliveryType, string selectedDeliveryCourier, string courierNumber, string remark, string address, string date, string admin, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateDeliveryOrder", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pDO = sqlComm.Parameters.Add("@DeliveryOrder", SqlDbType.NVarChar, 100);
            pDO.Direction = ParameterDirection.Input;
            pDO.Value = deliveryOrderNo;

            SqlParameter pDeliveryType = sqlComm.Parameters.Add("@SelectedDeliveryType", SqlDbType.NVarChar, 10);
            pDeliveryType.Direction = ParameterDirection.Input;
            pDeliveryType.Value = selectedDeliveryType;

            SqlParameter pDeliveryCourier = sqlComm.Parameters.Add("@SelectedDeliveryCourier", SqlDbType.NVarChar, 10);
            pDeliveryCourier.Direction = ParameterDirection.Input;
            pDeliveryCourier.Value = selectedDeliveryCourier == null ? string.Empty : selectedDeliveryCourier;

            SqlParameter pCourierNumber = sqlComm.Parameters.Add("@CourierNumber", SqlDbType.NVarChar, 30);
            pCourierNumber.Direction = ParameterDirection.Input;
            pCourierNumber.Value = courierNumber;

            SqlParameter pRemark = sqlComm.Parameters.Add("@Remark", SqlDbType.NVarChar, 2000);
            pRemark.Direction = ParameterDirection.Input;
            pRemark.Value = remark == null ? string.Empty : remark;

            SqlParameter pAddress = sqlComm.Parameters.Add("@Address", SqlDbType.NVarChar, 2000);
            pAddress.Direction = ParameterDirection.Input;
            pAddress.Value = address == null ? string.Empty : address;

            if (date != string.Empty)
            {
                SqlParameter pDeliveryDate = sqlComm.Parameters.Add("@DeliveryDate", SqlDbType.DateTime);
                pDeliveryDate.Direction = ParameterDirection.Input;
                pDeliveryDate.Value = date;
            }

            var pApproval = sqlComm.Parameters.Add("@Approval", SqlDbType.NVarChar, 2000);
            pApproval.Direction = ParameterDirection.Input;
            pApproval.Value = admin;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateDeliveryOrderStatus(string deliveryOrder, string courierCode, string deliveryDate, string deliveryType, string remark, string address, string courierNumber, string admin, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateDeliveryOrderStatus", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pDO = sqlComm.Parameters.Add("@DeliveryOrder", SqlDbType.NVarChar, 100);
            pDO.Direction = ParameterDirection.Input;
            pDO.Value = deliveryOrder;

            SqlParameter pDeliveryDate = sqlComm.Parameters.Add("@DeliveryDate", SqlDbType.DateTime);
            pDeliveryDate.Direction = ParameterDirection.Input;
            pDeliveryDate.Value = deliveryDate;

            SqlParameter pDeliveryType = sqlComm.Parameters.Add("@SelectedDeliveryType", SqlDbType.NVarChar, 10);
            pDeliveryType.Direction = ParameterDirection.Input;
            pDeliveryType.Value = deliveryType;

            SqlParameter pDeliveryCourier = sqlComm.Parameters.Add("@SelectedDeliveryCourier", SqlDbType.NVarChar, 10);
            pDeliveryCourier.Direction = ParameterDirection.Input;
            pDeliveryCourier.Value = courierCode == null ? string.Empty : courierCode;

            SqlParameter pRemark = sqlComm.Parameters.Add("@Remark", SqlDbType.NVarChar, 2000);
            pRemark.Direction = ParameterDirection.Input;
            pRemark.Value = remark == null ? string.Empty : remark;

            SqlParameter pAddress = sqlComm.Parameters.Add("@Address", SqlDbType.NVarChar, 2000);
            pAddress.Direction = ParameterDirection.Input;
            pAddress.Value = address == null ? string.Empty : address;

            var pCourierNumber = sqlComm.Parameters.Add("@CourierNumber", SqlDbType.NVarChar, 30);
            pCourierNumber.Direction = ParameterDirection.Input;
            pCourierNumber.Value = courierNumber == null ? string.Empty : courierNumber;

            SqlParameter pAdmin = sqlComm.Parameters.Add("@Admin", SqlDbType.NVarChar, 100);
            pAdmin.Direction = ParameterDirection.Input;
            pAdmin.Value = admin;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static DataSet GetAllDeliveryOrdersByFilteringCriteria(string languageCode, string fc, string searchFC, int viewPage, out int pages, out int ok, out string msg, string username = "")
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllDeliveryOrdersByFilteringCriteria", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pLanguage = sqlComm.Parameters.Add("@language", SqlDbType.NVarChar, 10);
                    pLanguage.Direction = ParameterDirection.Input;
                    pLanguage.Value = languageCode;

                    SqlParameter pFc = sqlComm.Parameters.Add("@fc", SqlDbType.NVarChar, 50);
                    pFc.Direction = ParameterDirection.Input;
                    pFc.Value = fc;

                    SqlParameter pSearchFc = sqlComm.Parameters.Add("@searchFc", SqlDbType.NVarChar, 100);
                    pSearchFc.Direction = ParameterDirection.Input;
                    pSearchFc.Value = searchFC;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
                    pUsername.Direction = ParameterDirection.Input;
                    pUsername.Value = username;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    msg = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllDeliveryOrdersByStatus(string languageCode, string fc, int viewPage, out int pages, out int ok, out string msg, string username = "")
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllDeliveryOrdersByStatus", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pLanguage = sqlComm.Parameters.Add("@language", SqlDbType.NVarChar, 10);
                    pLanguage.Direction = ParameterDirection.Input;
                    pLanguage.Value = languageCode;

                    SqlParameter pFc = sqlComm.Parameters.Add("@fc", SqlDbType.NVarChar, 50);
                    pFc.Direction = ParameterDirection.Input;
                    pFc.Value = fc;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
                    pUsername.Direction = ParameterDirection.Input;
                    pUsername.Value = username;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    msg = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllDeliveryOrders(string languageCode, int viewPage, out int pages, out int ok, out string msg, string username = "")
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllDeliveryOrders", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pLanguage = sqlComm.Parameters.Add("@language", SqlDbType.NVarChar, 10);
                    pLanguage.Direction = ParameterDirection.Input;
                    pLanguage.Value = languageCode;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
                    pUsername.Direction = ParameterDirection.Input;
                    pUsername.Value = username;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    msg = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllDeliveryOrdersByDate(string languageCode, DateTime fromDate, DateTime toDate, int viewPage, out int pages, out int ok, out string msg, string username = "")
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllDeliveryOrdersByDate", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pLanguage = sqlComm.Parameters.Add("@language", SqlDbType.NVarChar, 10);
                    pLanguage.Direction = ParameterDirection.Input;
                    pLanguage.Value = languageCode;

                    SqlParameter pFromDate = sqlComm.Parameters.Add("@fromDate", SqlDbType.DateTime);
                    pFromDate.Direction = ParameterDirection.Input;
                    pFromDate.Value = fromDate;

                    SqlParameter pToDate = sqlComm.Parameters.Add("@toDate", SqlDbType.DateTime);
                    pToDate.Direction = ParameterDirection.Input;
                    pToDate.Value = toDate;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
                    pUsername.Direction = ParameterDirection.Input;
                    pUsername.Value = username;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    msg = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetDeliveryOrderDetailsByDONumber(string languageCode, string deliveryOrderNumber, out int ok, out string msg)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetDeliveryOrderDetailsByOrderNumber", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pLanguage = sqlComm.Parameters.Add("@language", SqlDbType.NVarChar, 10);
                    pLanguage.Direction = ParameterDirection.Input;
                    pLanguage.Value = languageCode;

                    SqlParameter pDeliveryOrderNumber = sqlComm.Parameters.Add("@deliveryOrder", SqlDbType.NVarChar, 500);
                    pDeliveryOrderNumber.Direction = ParameterDirection.Input;
                    pDeliveryOrderNumber.Value = deliveryOrderNumber;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    msg = pMessage.Value.ToString();
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        #endregion
    }
}