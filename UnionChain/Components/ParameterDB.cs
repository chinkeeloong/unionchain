﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.Mvc;
using ECFBase.Resources;
using ECFBase.Models;

namespace ECFBase.Components
{
    public class ParameterDB
    {

        #region "View"
        public static DataSet GetAllParameter(out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllParameter", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        #endregion

        #region "Update"
        public static void UpdateParameter(string ParameterName, string ParameterStringVal, float ParameterFloatVal, string updatedBy,
                                           out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateParameter", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pParaName = sqlComm.Parameters.Add("@ParaName", SqlDbType.NVarChar, 100);
            pParaName.Direction = ParameterDirection.Input;
            pParaName.Value = ParameterName;

            SqlParameter pParaStringVal = sqlComm.Parameters.Add("@ParaStringVal", SqlDbType.NVarChar, 100);
            pParaStringVal.Direction = ParameterDirection.Input;
            pParaStringVal.Value = ParameterStringVal;

            SqlParameter pParaFloatVal = sqlComm.Parameters.Add("@ParaFloatVal", SqlDbType.Float);
            pParaFloatVal.Direction = ParameterDirection.Input;
            pParaFloatVal.Value = ParameterFloatVal;
            
            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

    }
}