﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using ECFBase.Models;

namespace ECFBase.Components
{
    public class MemberMarketTradingDB
    {
      
        public static void BuyTrading(ExchangeModel model, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_InsertTokenBuyingQueue", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = model.username;

            SqlParameter pQuantity = sqlComm.Parameters.Add("@OriValue", SqlDbType.Money);
            pQuantity.Direction = ParameterDirection.Input;
            pQuantity.Value = model.BuyQuantity;

            SqlParameter pValue = sqlComm.Parameters.Add("@Value", SqlDbType.Money);
            pValue.Direction = ParameterDirection.Input;
            pValue.Value = model.BuyQuantity;

            SqlParameter pRate = sqlComm.Parameters.Add("@Rate", SqlDbType.Float);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = model.CurrentPrice;

            SqlParameter pAmount = sqlComm.Parameters.Add("@Amount", SqlDbType.Float);
            pAmount.Direction = ParameterDirection.Input;
            pAmount.Value = model.TotalBuyingPrice;

            SqlParameter pLocalPrice = sqlComm.Parameters.Add("@LocalAmount", SqlDbType.Float);
            pLocalPrice.Direction = ParameterDirection.Input;
            pLocalPrice.Value = model.TotalBuyingLocalPrice;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 50);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = model.username;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 50);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = model.username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();
            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();
        }


        public static void SellTrading(ExchangeModel model, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_InsertTokenSellingQueue", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = model.username;

            SqlParameter pQuantity = sqlComm.Parameters.Add("@OriValue", SqlDbType.Money);
            pQuantity.Direction = ParameterDirection.Input;
            pQuantity.Value = model.SellQuantity;

            SqlParameter pValue = sqlComm.Parameters.Add("@Value", SqlDbType.Money);
            pValue.Direction = ParameterDirection.Input;
            pValue.Value = model.SellQuantity;

            SqlParameter pRate = sqlComm.Parameters.Add("@Rate", SqlDbType.Float);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = model.CurrentPrice;

            SqlParameter pAmount = sqlComm.Parameters.Add("@Amount", SqlDbType.Float);
            pAmount.Direction = ParameterDirection.Input;
            pAmount.Value = model.TotalSellPrice;

            SqlParameter pLocalPrice = sqlComm.Parameters.Add("@LocalAmount", SqlDbType.Float);
            pLocalPrice.Direction = ParameterDirection.Input;
            pLocalPrice.Value = model.TotalSellLocalPrice;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 50);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = model.username;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 50);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = model.username;
            
            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();
            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static DataSet SP_GetBuyingOrderMatchQueue(string username, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetBuyingOrderMatchQueue", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet SP_GetBuyingOrderMatchDetails(string username, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetBuyingOrderMatchDetails", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static void UploadPaymentSlip(BuyOrderManagement model, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UploadBuyerPaymentSlip", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.NVarChar, 50);
            pID.Direction = ParameterDirection.Input;
            pID.Value = model.ID;

            SqlParameter pProtrait = sqlComm.Parameters.Add("@Photo", SqlDbType.NVarChar, 500);
            pProtrait.Direction = ParameterDirection.Input;
            pProtrait.Value = model.PaymentSlipPhotoPath;
          
            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static DataSet GetSellerPendingMatch(int ID)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_GetSellerPendingMatch", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.NVarChar, 20);
            pID.Direction = ParameterDirection.Input;
            pID.Value = ID;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetBuyerPendingMatch(int ID)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_GetBuyerPendingMatch", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.NVarChar, 20);
            pID.Direction = ParameterDirection.Input;
            pID.Value = ID;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        public static void CancelTradingfromSellOrderByID(int BySellerTradingID)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_Seller_Cancel", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.NVarChar, 50);
            pID.Direction = ParameterDirection.Input;
            pID.Value = BySellerTradingID;

            sqlComm.ExecuteNonQuery();
            
            sqlConn.Close();
        }

        public static void CancelTradingFromBuyOrderByID(int BuyTradingID)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_Buyer_Cancel", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.NVarChar, 50);
            pID.Direction = ParameterDirection.Input;
            pID.Value = BuyTradingID;

            
            sqlComm.ExecuteNonQuery();

            sqlConn.Close();
        }

        public static void CancelTradingFromBuyOrderDetailsByID(int BuyTradingID)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_Manual_Cancel", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.NVarChar, 50);
            pID.Direction = ParameterDirection.Input;
            pID.Value = BuyTradingID;


            sqlComm.ExecuteNonQuery();

            sqlConn.Close();
        }

        public static DataSet SP_GetSellingOrderMatchQueue(string username, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSellingOrderMatchQueue", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet SP_GetSellingOrderMatchDetails(string username, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSellingOrderMatchDetails", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet SP_GetSellingOrderMatchDetailsByTradingID(int ID, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSellingOrderMatchDetailsByTradingID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = ID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet SP_GetBuyingOrderMatchDetailsByTableID(int ID, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetBuyingOrderMatchDetailsByTableID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = ID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }


        public static DataSet SP_GetBuyingOrderMatchDetailsByID(int ID, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetBuyingOrderMatchDetailsByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = ID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static void ApproveTradingByID(int BuyTradingID)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_Seller_Receive_Money", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.NVarChar, 50);
            pID.Direction = ParameterDirection.Input;
            pID.Value = BuyTradingID;
            
            sqlComm.ExecuteNonQuery();
            
            sqlConn.Close();
        }

    }

}