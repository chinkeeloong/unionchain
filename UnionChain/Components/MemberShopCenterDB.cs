﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using ECFBase.Models;

namespace ECFBase.Components
{
    public class MemberShopCenterDB
    {

        public static DataSet GetSponsorChartVer1(string Username, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSponsorChartVer1", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pusername = sqlComm.Parameters.Add("@username", SqlDbType.VarChar, 50);
            pusername.Direction = ParameterDirection.Input;
            pusername.Value = Username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        public static DataSet GetBinaryChartLevel2(string Username, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetBinaryChartLevel2", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pusername = sqlComm.Parameters.Add("@username", SqlDbType.VarChar, 50);
            pusername.Direction = ParameterDirection.Input;
            pusername.Value = Username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        public static DataSet SP_GetMemberByIC(string IC, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberByIC", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pusername = sqlComm.Parameters.Add("@IC", SqlDbType.VarChar, 100);
            pusername.Direction = ParameterDirection.Input;
            pusername.Value = IC;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetSponsorChartVer1Fixed2Level(string Username, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSponsorChartVer1Fixed2Level", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pusername = sqlComm.Parameters.Add("@username", SqlDbType.VarChar, 50);
            pusername.Direction = ParameterDirection.Input;
            pusername.Value = Username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        #region GetAllSponsorListByUsername
        public static DataSet GetAllSponsorListByUsername(string username, int viewPage, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSponsorList ", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            pages = (int)pPages.Value;

            sqlConn.Close();

            return ds;
        }
        #endregion

        #region GetAllBinaryListByUsername
        public static DataSet GetAllBinaryListByUsername(string username, int viewPage, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetBinaryList ", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            pages = (int)pPages.Value;

            sqlConn.Close();

            return ds;
        }
        #endregion

        public static DataSet GetAllMemberList()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllMemberList", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        public static DataSet CheckPackage(string PackageCode)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_CheckPackage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPackageCode = sqlComm.Parameters.Add("@PackageCode", SqlDbType.NVarChar, 50);
            pPackageCode.Direction = ParameterDirection.Input;
            pPackageCode.Value = PackageCode;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }   
       
        #region "View GetSponsorChart"
        public static DataSet GetSponsorChart(string Username, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSponsorChart", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pusername = sqlComm.Parameters.Add("@username", SqlDbType.VarChar, 50);
            pusername.Direction = ParameterDirection.Input;
            pusername.Value = Username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetSponsorChartMemberByUsername(string username, string languageCode, out int ok, out string message)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSponsorChartMemberByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            message = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        #endregion

        #region GetAllPackage
     
        public static DataSet GetPackageAndDescription(string languageCode, string countryCode)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetPackageAndDescription", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLangCode = sqlComm.Parameters.Add("@languageCode", SqlDbType.NVarChar, 10);
            pLangCode.Direction = ParameterDirection.Input;
            pLangCode.Value = languageCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 20);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }


        public static DataSet GetPackageAndDescriptionForUpgrade(string languageCode, string countryCode,string Rank)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetPackageAndDescriptionForUpgrade", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLangCode = sqlComm.Parameters.Add("@languageCode", SqlDbType.NVarChar, 10);
            pLangCode.Direction = ParameterDirection.Input;
            pLangCode.Value = languageCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 20);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = countryCode;

            SqlParameter pRank = sqlComm.Parameters.Add("@Rank", SqlDbType.NVarChar, 20);
            pRank.Direction = ParameterDirection.Input;
            pRank.Value = Rank;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }
        #endregion

        #region registerMember
        public static void RegisterMember(RegisterNewMemberModel mem, out int ok, out string msg, out string invoiceNumber)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Register", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = mem.LoginUsername;

            SqlParameter pNickname = sqlComm.Parameters.Add("@NickName", SqlDbType.NVarChar, 50);
            pNickname.Direction = ParameterDirection.Input;
            pNickname.Value = mem.UserInfo.Nickname;

            SqlParameter pPassword = sqlComm.Parameters.Add("@Password", SqlDbType.NVarChar, 50);
            pPassword.Direction = ParameterDirection.Input;
            pPassword.Value = mem.Member.EncryptedPassword;

            SqlParameter pPin = sqlComm.Parameters.Add("@Pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = mem.Member.EncryptedPin;

            SqlParameter pCountryID = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 50);
            pCountryID.Direction = ParameterDirection.Input;
            pCountryID.Value = mem.SelectedCountry;

            SqlParameter pLangCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 50);
            pLangCode.Direction = ParameterDirection.Input;
            pLangCode.Value = mem.LanguageCode;

            SqlParameter pPackageCode = sqlComm.Parameters.Add("@PackageCode", SqlDbType.NVarChar, 50);
            pPackageCode.Direction = ParameterDirection.Input;
            pPackageCode.Value = mem.SelectedPackage;
            
            SqlParameter pRefUsername = sqlComm.Parameters.Add("@RefferalUsername", SqlDbType.NVarChar, 50);
            pRefUsername.Direction = ParameterDirection.Input;
            pRefUsername.Value = mem.ReferralUsername;

            SqlParameter pRefPin = sqlComm.Parameters.Add("@RefferalPin", SqlDbType.NVarChar, 50);
            pRefPin.Direction = ParameterDirection.Input;
            pRefPin.Value = mem.EncryptedSponsorSecurityPin;

            SqlParameter pIntroUsername = sqlComm.Parameters.Add("@Intro", SqlDbType.NVarChar, 50);
            pIntroUsername.Direction = ParameterDirection.Input;
            pIntroUsername.Value = mem.Member.Intro;

            SqlParameter pUpUsername = sqlComm.Parameters.Add("@Upline", SqlDbType.NVarChar, 20);
            pUpUsername.Direction = ParameterDirection.Input;
            pUpUsername.Value = mem.UpUsername;

            SqlParameter pLocation = sqlComm.Parameters.Add("@Position", SqlDbType.Int);
            pLocation.Direction = ParameterDirection.Input;
            pLocation.Value = int.Parse(mem.SelectedLocation);
            
            SqlParameter pFirstName = sqlComm.Parameters.Add("@FullName", SqlDbType.NVarChar, 50);
            pFirstName.Direction = ParameterDirection.Input;
            pFirstName.Value = Helpers.Helper.NVL(mem.Member.FullName);

            SqlParameter pEmail = sqlComm.Parameters.Add("@Email", SqlDbType.NVarChar, 100);
            pEmail.Direction = ParameterDirection.Input;
            pEmail.Value = mem.Member.MemberEmail;

            SqlParameter pCellPhone = sqlComm.Parameters.Add("@CellPhone", SqlDbType.NVarChar, 50);
            pCellPhone.Direction = ParameterDirection.Input;
            pCellPhone.Value = Helpers.Helper.NVL(mem.UserInfo.CellPhone);

            SqlParameter pIC = sqlComm.Parameters.Add("@IC", SqlDbType.NVarChar, 100);
            pIC.Direction = ParameterDirection.Input;
            pIC.Value = Helpers.Helper.NVL(mem.UserInfo.IC);
            

            SqlParameter pActivationWallet = sqlComm.Parameters.Add("@ActivationWallet", SqlDbType.Money);
            pActivationWallet.Direction = ParameterDirection.Input;
            pActivationWallet.Value = mem.APAmount;

            SqlParameter pRegisterWallet = sqlComm.Parameters.Add("@RegisterWallet", SqlDbType.Money);
            pRegisterWallet.Direction = ParameterDirection.Input;
            pRegisterWallet.Value = mem.RPAmount;

            SqlParameter pPaymentMethod = sqlComm.Parameters.Add("@PaymentMethod", SqlDbType.NVarChar, 50);
            pPaymentMethod.Direction = ParameterDirection.Input;
            pPaymentMethod.Value = mem.SelectedPaymentOption;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            SqlParameter pInvoiceNumber = sqlComm.Parameters.Add("@outInvoice", SqlDbType.VarChar, 100);
            pInvoiceNumber.Direction = ParameterDirection.Output;
            

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            invoiceNumber = pInvoiceNumber.Value.ToString();
            sqlConn.Close();
        }

        public static void RegisterMultipleAccount(RegisterNewMemberModel mem, out int ok, out string msg, out string invoiceNumber)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_RegisterMultipleAccount", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = mem.LoginUsername;

            SqlParameter pNickname = sqlComm.Parameters.Add("@nickname", SqlDbType.NVarChar, 50);
            pNickname.Direction = ParameterDirection.Input;
            pNickname.Value = mem.UserInfo.Nickname;

            SqlParameter pPassword = sqlComm.Parameters.Add("@password", SqlDbType.NVarChar, 50);
            pPassword.Direction = ParameterDirection.Input;
            pPassword.Value = mem.Member.EncryptedPassword;

            SqlParameter pPin = sqlComm.Parameters.Add("@pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = mem.Member.EncryptedPin;

            SqlParameter pCountryID = sqlComm.Parameters.Add("@countryCode", SqlDbType.NVarChar, 20);
            pCountryID.Direction = ParameterDirection.Input;
            pCountryID.Value = mem.SelectedCountry;

            SqlParameter pLangCode = sqlComm.Parameters.Add("@languageCode", SqlDbType.NVarChar, 20);
            pLangCode.Direction = ParameterDirection.Input;
            pLangCode.Value = mem.LanguageCode;

            SqlParameter pPackageCode = sqlComm.Parameters.Add("@packageCode", SqlDbType.NVarChar, 20);
            pPackageCode.Direction = ParameterDirection.Input;
            pPackageCode.Value = mem.SelectedPackage;

            SqlParameter pRefUsername = sqlComm.Parameters.Add("@refUsername", SqlDbType.NVarChar, 20);
            pRefUsername.Direction = ParameterDirection.Input;
            pRefUsername.Value = mem.ReferralUsername;

            SqlParameter pRefPin = sqlComm.Parameters.Add("@refPin", SqlDbType.NVarChar, 50);
            pRefPin.Direction = ParameterDirection.Input;
            pRefPin.Value = mem.EncryptedSponsorSecurityPin;

            SqlParameter pIntroUsername = sqlComm.Parameters.Add("@introUsername", SqlDbType.NVarChar, 20);
            pIntroUsername.Direction = ParameterDirection.Input;
            pIntroUsername.Value = mem.introid;

            SqlParameter pUpUsername = sqlComm.Parameters.Add("@UpUsername", SqlDbType.NVarChar, 20);
            pUpUsername.Direction = ParameterDirection.Input;
            pUpUsername.Value = mem.UpUsername;

            SqlParameter pLocation = sqlComm.Parameters.Add("@location", SqlDbType.Int);
            pLocation.Direction = ParameterDirection.Input;
            pLocation.Value = int.Parse(mem.SelectedLocation);

            SqlParameter pFirstName = sqlComm.Parameters.Add("@firstName", SqlDbType.NVarChar, 50);
            pFirstName.Direction = ParameterDirection.Input;
            pFirstName.Value = Helpers.Helper.NVL(mem.Member.FirstName);

            SqlParameter pEmail = sqlComm.Parameters.Add("@email", SqlDbType.NVarChar, 100);
            pEmail.Direction = ParameterDirection.Input;
            pEmail.Value = mem.Member.MemberEmail;

            SqlParameter pCellPhone = sqlComm.Parameters.Add("@cellPhone", SqlDbType.NVarChar, 50);
            pCellPhone.Direction = ParameterDirection.Input;
            pCellPhone.Value = Helpers.Helper.NVL(mem.UserInfo.CellPhone);

            SqlParameter pIC = sqlComm.Parameters.Add("@IC", SqlDbType.NVarChar, 100);
            pIC.Direction = ParameterDirection.Input;
            pIC.Value = Helpers.Helper.NVL(mem.UserInfo.IC);

            SqlParameter pAddRD = sqlComm.Parameters.Add("@addRD", SqlDbType.Float);
            pAddRD.Direction = ParameterDirection.Input;
            pAddRD.Value = mem.AdditionalRD;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            SqlParameter pInvoiceNumber = sqlComm.Parameters.Add("@outInvoice", SqlDbType.VarChar, 100);
            pInvoiceNumber.Direction = ParameterDirection.Output;

            SqlParameter pOption = sqlComm.Parameters.Add("@Option", SqlDbType.NVarChar, 20);
            pOption.Direction = ParameterDirection.Input;
            pOption.Value = mem.PaymentOption;

            //SqlParameter pBonus = sqlComm.Parameters.Add("@BonusPayment", SqlDbType.Int);
            //pBonus.Direction = ParameterDirection.Input;
            //pBonus.Value = mem.Member.BonusPaymentWallet;

            //SqlParameter pCompany = sqlComm.Parameters.Add("@CompanyPayment", SqlDbType.Int);
            //pCompany.Direction = ParameterDirection.Input;
            //pCompany.Value = mem.Member.CompanyPaymentWallet;


            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            invoiceNumber = pInvoiceNumber.Value.ToString();
            sqlConn.Close();
        }
     
        #endregion

        #region FreeRegister
        public static void FreeRegister(RegisterNewMemberModel mem, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_RegisterFree", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = mem.Member.Username;

            SqlParameter pPassword = sqlComm.Parameters.Add("@password", SqlDbType.NVarChar, 50);
            pPassword.Direction = ParameterDirection.Input;
            pPassword.Value = mem.Member.Password;

            SqlParameter pPin = sqlComm.Parameters.Add("@pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = mem.Member.Pin;

            SqlParameter pCountryID = sqlComm.Parameters.Add("@countryCode", SqlDbType.NVarChar, 20);
            pCountryID.Direction = ParameterDirection.Input;
            pCountryID.Value = mem.SelectedCountry;

            SqlParameter pLangCode = sqlComm.Parameters.Add("@languageCode", SqlDbType.NVarChar, 20);
            pLangCode.Direction = ParameterDirection.Input;
            pLangCode.Value = mem.LanguageCode;

            SqlParameter pPackage = sqlComm.Parameters.Add("@packageCode", SqlDbType.NVarChar, 10);
            pPackage.Direction = ParameterDirection.Input;
            pPackage.Value = mem.SelectedPackage;

            SqlParameter pRefUsername = sqlComm.Parameters.Add("@refUsername", SqlDbType.NVarChar, 20);
            pRefUsername.Direction = ParameterDirection.Input;
            pRefUsername.Value = mem.ReferralUsername;

            SqlParameter pRefPin = sqlComm.Parameters.Add("@refPin", SqlDbType.NVarChar, 50);
            pRefPin.Direction = ParameterDirection.Input;
            pRefPin.Value = mem.SponsorSecurityPin;

            SqlParameter pIntroUsername = sqlComm.Parameters.Add("@introUsername", SqlDbType.NVarChar, 20);
            pIntroUsername.Direction = ParameterDirection.Input;
            pIntroUsername.Value = mem.Member.Intro;

            SqlParameter pUpUsername = sqlComm.Parameters.Add("@UpUsername", SqlDbType.NVarChar, 20);
            pUpUsername.Direction = ParameterDirection.Input;
            pUpUsername.Value = mem.UpUsername;

            SqlParameter pLocation = sqlComm.Parameters.Add("@location", SqlDbType.Int);
            pLocation.Direction = ParameterDirection.Input;
            pLocation.Value = int.Parse(mem.SelectedLocation);

            SqlParameter pFirstName = sqlComm.Parameters.Add("@firstName", SqlDbType.NVarChar, 50);
            pFirstName.Direction = ParameterDirection.Input;
            pFirstName.Value = Helpers.Helper.NVL(mem.Member.FirstName);

            SqlParameter pNickname = sqlComm.Parameters.Add("@nickname", SqlDbType.NVarChar, 50);
            pNickname.Direction = ParameterDirection.Input;
            pNickname.Value = mem.UserInfo.Nickname;

            SqlParameter pEmail = sqlComm.Parameters.Add("@email", SqlDbType.NVarChar, 100);
            pEmail.Direction = ParameterDirection.Input;
            pEmail.Value = mem.Member.MemberEmail;

            SqlParameter pCellPhone = sqlComm.Parameters.Add("@cellPhone", SqlDbType.NVarChar, 50);
            pCellPhone.Direction = ParameterDirection.Input;
            pCellPhone.Value = Helpers.Helper.NVL(mem.UserInfo.CellPhone);

            SqlParameter pIC = sqlComm.Parameters.Add("@IC", SqlDbType.NVarChar, 100);
            pIC.Direction = ParameterDirection.Input;
            pIC.Value = Helpers.Helper.NVL(mem.UserInfo.IC);

            SqlParameter pShipName = sqlComm.Parameters.Add("@ShipName", SqlDbType.NVarChar, 100);
            pShipName.Direction = ParameterDirection.Input;
            pShipName.Value = Helpers.Helper.NVL(mem.UserInfo.ShipName);

            SqlParameter pShipAddress = sqlComm.Parameters.Add("@ShipAdd", SqlDbType.NVarChar, 300);
            pShipAddress.Direction = ParameterDirection.Input;
            pShipAddress.Value = Helpers.Helper.NVL(mem.UserInfo.ShipAddress);

            SqlParameter pShipCity = sqlComm.Parameters.Add("@ShipCity", SqlDbType.NVarChar, 100);
            pShipCity.Direction = ParameterDirection.Input;
            pShipCity.Value = Helpers.Helper.NVL(mem.UserInfo.SelectedCity);

            SqlParameter pShipState = sqlComm.Parameters.Add("@ShipState", SqlDbType.NVarChar, 100);
            pShipState.Direction = ParameterDirection.Input;
            pShipState.Value = Helpers.Helper.NVL(mem.UserInfo.SelectedProvince);

            SqlParameter pShipPostcode = sqlComm.Parameters.Add("@ShipPostcode", SqlDbType.NVarChar, 100);
            pShipPostcode.Direction = ParameterDirection.Input;
            pShipPostcode.Value = Helpers.Helper.NVL(mem.UserInfo.ShipPostcode);

            SqlParameter pShipCountry = sqlComm.Parameters.Add("@ShipCountry", SqlDbType.NVarChar, 100);
            pShipCountry.Direction = ParameterDirection.Input;
            pShipCountry.Value = Helpers.Helper.NVL(mem.UserInfo.SelectedCountry);

            SqlParameter pShipPhoneNo = sqlComm.Parameters.Add("@ShipPhoneNo", SqlDbType.NVarChar, 50);
            pShipPhoneNo.Direction = ParameterDirection.Input;
            pShipPhoneNo.Value = Helpers.Helper.NVL(mem.UserInfo.ShipPhoneNo);

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            //SqlParameter pUsernameID = sqlComm.Parameters.Add("@UsernameID", SqlDbType.NVarChar, 20);
            //pUsernameID.Direction = ParameterDirection.Input;
            //pUsernameID.Value = mem.UsernameID;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        #region CheckUsernameAvailability
        public static void CheckUsernameAvailability(string username, out int ok)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CheckUsernameAvailability", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();
            ok = (int)pOk.Value;
            sqlConn.Close();
        }
        #endregion
    
        public static DataSet SponsorUplineChecking(string USERNAME, string SPONSORNAME, out int RESULT)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_SponsorUplineChecking", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUSERNAME = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUSERNAME.Direction = ParameterDirection.Input;
            pUSERNAME.Value = USERNAME;

            SqlParameter pSPONSORNAME = sqlComm.Parameters.Add("@SPONSORNAME", SqlDbType.NVarChar, 50);
            pSPONSORNAME.Direction = ParameterDirection.Input;
            pSPONSORNAME.Value = SPONSORNAME;

            SqlParameter pRESULT = sqlComm.Parameters.Add("@RESULT", SqlDbType.Int);
            pRESULT.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            RESULT = (int)pRESULT.Value;            
            sqlConn.Close();

            return ds;
        }

        public static DataSet SearchUpline(string Username, out int ok, out int count, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_SearchUpline", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pusername = sqlComm.Parameters.Add("@username", SqlDbType.VarChar, 50);
            pusername.Direction = ParameterDirection.Input;
            pusername.Value = Username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pcount = sqlComm.Parameters.Add("@COUNT", SqlDbType.Int);
            pcount.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            count = (int)pcount.Value;
            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        
        

    }
}