﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace ECFBase.Components
{
    public class AdminHelpDeskDB
    {
        #region "View"
        public static DataSet GetAllInbox(string Type, string LanguageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllInbox", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pType = sqlComm.Parameters.Add("@Type", SqlDbType.NVarChar, 100);
            pType.Direction = ParameterDirection.Input;
            pType.Value = Type;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllNotices(string LanguageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllNotices", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllNoticesByFilteringCriteria(string LanguageCode, string fc, string searchFC, int viewPage, out int pages, out int ok, out string message)
        {
            var dataSet = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    var sqlComm = new SqlCommand("SP_GetAllNoticesByFilteringCriteria", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
                    pLanguageCode.Direction = ParameterDirection.Input;
                    pLanguageCode.Value = LanguageCode;

                    SqlParameter pFc = sqlComm.Parameters.Add("@fc", SqlDbType.NVarChar, 50);
                    pFc.Direction = ParameterDirection.Input;
                    pFc.Value = fc;

                    SqlParameter pSearchFc = sqlComm.Parameters.Add("@searchFc", SqlDbType.NVarChar, 100);
                    pSearchFc.Direction = ParameterDirection.Input;
                    pSearchFc.Value = searchFC;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(dataSet);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return dataSet;
        }

        public static DataSet GetAllNoticeByUsername(string username, string LanguageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllNoticesByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllInboxByUsername(string username, string LanguageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllInboxByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMessageByID(int? InboxID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMessageByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInboxID = sqlComm.Parameters.Add("@InboxID", SqlDbType.Int);
            pInboxID.Direction = ParameterDirection.Input;
            pInboxID.Value = InboxID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetNoticeByID(int? InboxID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetNoticeByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInboxID = sqlComm.Parameters.Add("@InboxID", SqlDbType.Int);
            pInboxID.Direction = ParameterDirection.Input;
            pInboxID.Value = InboxID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMultiLanguageMessageByID(int? InboxID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguageMessageByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInboxID = sqlComm.Parameters.Add("@InboxID", SqlDbType.Int);
            pInboxID.Direction = ParameterDirection.Input;
            pInboxID.Value = InboxID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMultiLanguageNoticeByID(int? InboxID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguageNoticeByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInboxID = sqlComm.Parameters.Add("@InboxID", SqlDbType.Int);
            pInboxID.Direction = ParameterDirection.Input;
            pInboxID.Value = InboxID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        #endregion

        #region "Create"

        public static void CreateNewMessage(string username, string Type, string createdBy, string updatedBy, out int MessageID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMessage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pType = sqlComm.Parameters.Add("@Type", SqlDbType.NVarChar, 100);
            pType.Direction = ParameterDirection.Input;
            pType.Value = Type;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pMessageID = sqlComm.Parameters.Add("@MessageID", SqlDbType.Int);
            pMessageID.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            MessageID = (int)pMessageID.Value;
            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewNotice(string username, string createdBy, string updatedBy, out int MessageID, out int ok, out string msg)
        {
            var sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            var sqlComm = new SqlCommand("SP_CreateNotice", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            var pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            var pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            var pMessageID = sqlComm.Parameters.Add("@MessageID", SqlDbType.Int);
            pMessageID.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            MessageID = (int)pMessageID.Value;
            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewMultiLanguageMessage(int? MessageID, string LanguageCode,
                                                            string MessageTitle, string MessageContent, string MessageReply,
                                                            string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguageMessage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pMessageID = sqlComm.Parameters.Add("@MessageID", SqlDbType.Int);
            pMessageID.Direction = ParameterDirection.Input;
            pMessageID.Value = MessageID;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pMessageTitle = sqlComm.Parameters.Add("@MessageTitle", SqlDbType.NVarChar, 2000);
            pMessageTitle.Direction = ParameterDirection.Input;
            pMessageTitle.Value = MessageTitle;

            SqlParameter pMessageContent = sqlComm.Parameters.Add("@MessageContent", SqlDbType.NVarChar, 4000);
            pMessageContent.Direction = ParameterDirection.Input;
            pMessageContent.Value = MessageContent;

            SqlParameter pMessageReply = sqlComm.Parameters.Add("@MessageReply", SqlDbType.NVarChar, 4000);
            pMessageReply.Direction = ParameterDirection.Input;
            pMessageReply.Value = MessageReply;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewMultiLanguageNotice(int? MessageID, string LanguageCode,
                                                           string MessageTitle, string MessageContent,
                                                           string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguageNotice", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pMessageID = sqlComm.Parameters.Add("@MessageID", SqlDbType.Int);
            pMessageID.Direction = ParameterDirection.Input;
            pMessageID.Value = MessageID;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pMessageTitle = sqlComm.Parameters.Add("@MessageTitle", SqlDbType.NVarChar, 2000);
            pMessageTitle.Direction = ParameterDirection.Input;
            pMessageTitle.Value = MessageTitle;

            SqlParameter pMessageContent = sqlComm.Parameters.Add("@MessageContent", SqlDbType.NVarChar, 4000);
            pMessageContent.Direction = ParameterDirection.Input;
            pMessageContent.Value = MessageContent;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        #region "Update"
        public static void UpdateMessage(int? InboxID, string Type, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateMessage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pMessageID = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pMessageID.Direction = ParameterDirection.Input;
            pMessageID.Value = InboxID;

            SqlParameter pType = sqlComm.Parameters.Add("@Type", SqlDbType.NVarChar, 100);
            pType.Direction = ParameterDirection.Input;
            pType.Value = Type;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@updatedBy", SqlDbType.NVarChar, 50);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion

        #region "Delete"

        public static void DeleteMultiLanguageMessageByID(int? InboxID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteMultiLanguageMessageByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInboxID = sqlComm.Parameters.Add("@InboxID", SqlDbType.Int);
            pInboxID.Direction = ParameterDirection.Input;
            pInboxID.Value = InboxID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMultiLanguageNoticeByID(int? InboxID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteMultiLanguageNoticeByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInboxID = sqlComm.Parameters.Add("@InboxID", SqlDbType.Int);
            pInboxID.Direction = ParameterDirection.Input;
            pInboxID.Value = InboxID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMessageByID(int? InboxID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteMessageByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInboxID = sqlComm.Parameters.Add("@InboxID", SqlDbType.Int);
            pInboxID.Direction = ParameterDirection.Input;
            pInboxID.Value = InboxID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteNoticeByID(int? InboxID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteNoticeByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInboxID = sqlComm.Parameters.Add("@InboxID", SqlDbType.Int);
            pInboxID.Direction = ParameterDirection.Input;
            pInboxID.Value = InboxID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion
    }
}