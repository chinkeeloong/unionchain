﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace ECFBase.Components
{
    public class AdminInfoDeskDB
    {
        #region "View"

        public static DataSet GetAllImage()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllImage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();
            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllImageSlide(int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllImageSlide", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            //SqlParameter pInfoDeskType = sqlComm.Parameters.Add("@InfoDeskType", SqlDbType.NVarChar, 1);
            //pInfoDeskType.Direction = ParameterDirection.Input;
            //pInfoDeskType.Value = InfoDeskType;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetImageSlideByID(int? ImageID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetImageSlideByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pImageID = sqlComm.Parameters.Add("@InfoDeskID", SqlDbType.Int);
            pImageID.Direction = ParameterDirection.Input;
            pImageID.Value = ImageID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllInfoDesk(string InfoDeskType, string LanguageCode, int viewPage, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllInfoDesk", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInfoDeskType = sqlComm.Parameters.Add("@InfoDeskType", SqlDbType.NVarChar, 1);
            pInfoDeskType.Direction = ParameterDirection.Input;
            pInfoDeskType.Value = InfoDeskType;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetInfoDeskByID(int? InfoDeskID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetInfoDeskByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInfoDeskID = sqlComm.Parameters.Add("@InfoDeskID", SqlDbType.Int);
            pInfoDeskID.Direction = ParameterDirection.Input;
            pInfoDeskID.Value = InfoDeskID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMultiLanguageInfoDeskByID(int? InfoDeskID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMultiLanguageInfoDeskByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInfoDeskID = sqlComm.Parameters.Add("@InfoDeskID", SqlDbType.Int);
            pInfoDeskID.Direction = ParameterDirection.Input;
            pInfoDeskID.Value = InfoDeskID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetFileByName(string FileName, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetFileByName", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pFileName = sqlComm.Parameters.Add("@fileName", SqlDbType.NVarChar, 1000);
            pFileName.Direction = ParameterDirection.Input;
            pFileName.Value = FileName;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }


        #endregion

        #region "Create"

        public static void CreateNewImageSlide(string ProductImage, string InfoDeskTitle, string createdBy, string updatedBy, out int InfoDeskID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateImageSlide", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            //SqlParameter pInfoDeskType = sqlComm.Parameters.Add("@InfoDeskType", SqlDbType.NVarChar, 10);
            //pInfoDeskType.Direction = ParameterDirection.Input;
            //pInfoDeskType.Value = InfoDeskType;

            SqlParameter pProductImage = sqlComm.Parameters.Add("@ProductImage", SqlDbType.NVarChar, 100);
            pProductImage.Direction = ParameterDirection.Input;
            pProductImage.Value = ProductImage;


            SqlParameter pInfoDeskTitle = sqlComm.Parameters.Add("@InfoDeskTitle", SqlDbType.NVarChar, 100);
            pInfoDeskTitle.Direction = ParameterDirection.Input;
            pInfoDeskTitle.Value = InfoDeskTitle;


            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pInfoDeskID = sqlComm.Parameters.Add("@InfoDeskID", SqlDbType.Int);
            pInfoDeskID.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            InfoDeskID = (int)pInfoDeskID.Value;
            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewInfoDesk(string InfoDeskType, string ProductImage, string createdBy, string updatedBy, string A, out int InfoDeskID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateInfoDesk", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInfoDeskType = sqlComm.Parameters.Add("@InfoDeskType", SqlDbType.NVarChar, 10);
            pInfoDeskType.Direction = ParameterDirection.Input;
            pInfoDeskType.Value = InfoDeskType;

            SqlParameter pProductImage = sqlComm.Parameters.Add("@ProductImage", SqlDbType.NVarChar, 100);
            pProductImage.Direction = ParameterDirection.Input;
            pProductImage.Value = ProductImage;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pA = sqlComm.Parameters.Add("@A", SqlDbType.NVarChar, 20);
            pA.Direction = ParameterDirection.Input;
            pA.Value = A;

            SqlParameter pInfoDeskID = sqlComm.Parameters.Add("@InfoDeskID", SqlDbType.Int);
            pInfoDeskID.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            InfoDeskID = (int)pInfoDeskID.Value;
            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewMultiLanguageInfoDesk(int? InfoDeskID, string ProductImage, string LanguageCode,
                                                             string InfoDeskTitle, string InfoDeskContent,
                                                             string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMultiLanguageInfoDesk", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInfoDeskID = sqlComm.Parameters.Add("@InfoDeskID", SqlDbType.Int);
            pInfoDeskID.Direction = ParameterDirection.Input;
            pInfoDeskID.Value = InfoDeskID;

            SqlParameter pProductImage = sqlComm.Parameters.Add("@ProductImage", SqlDbType.NVarChar, 100);
            pProductImage.Direction = ParameterDirection.Input;
            pProductImage.Value = ProductImage;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pInfoDeskTitle = sqlComm.Parameters.Add("@InfoDeskTitle", SqlDbType.NVarChar, 2000);
            pInfoDeskTitle.Direction = ParameterDirection.Input;
            pInfoDeskTitle.Value = InfoDeskTitle;

            SqlParameter pInfoDeskContent = sqlComm.Parameters.Add("@InfoDeskContent", SqlDbType.NVarChar, 4000);
            pInfoDeskContent.Direction = ParameterDirection.Input;
            pInfoDeskContent.Value = InfoDeskContent;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CreateNewFile(string FileName, string FileTitle, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateFile", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pFileName = sqlComm.Parameters.Add("@fileName", SqlDbType.NVarChar, 1000);
            pFileName.Direction = ParameterDirection.Input;
            pFileName.Value = FileName;

            SqlParameter pFileTitle = sqlComm.Parameters.Add("@fileTitle", SqlDbType.NVarChar, 1000);
            pFileTitle.Direction = ParameterDirection.Input;
            pFileTitle.Value = FileTitle;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion

        #region "Update"

        public static void UpdateImageSlide(int? InfoDeskID, string ProductImage, string InfoDeskTitle, string BY, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateImageSlide", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInfoDeskID = sqlComm.Parameters.Add("@InfoDeskID", SqlDbType.Int);
            pInfoDeskID.Direction = ParameterDirection.Input;
            pInfoDeskID.Value = InfoDeskID;

            SqlParameter pProductImage = sqlComm.Parameters.Add("@ProductImage", SqlDbType.NVarChar, 100);
            pProductImage.Direction = ParameterDirection.Input;
            pProductImage.Value = ProductImage;

            SqlParameter pInfoDeskTitle = sqlComm.Parameters.Add("@InfoDeskTitle", SqlDbType.NVarChar, 100);
            pInfoDeskTitle.Direction = ParameterDirection.Input;
            pInfoDeskTitle.Value = InfoDeskTitle;

            SqlParameter pBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 100);
            pBy.Direction = ParameterDirection.Input;
            pBy.Value = BY;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }


        public static void UpdateInfoDesk(int? InfoDeskID, string ProductImage, string InfoDeskType, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateInfoDesk", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInfoDeskID = sqlComm.Parameters.Add("@id", SqlDbType.Int);
            pInfoDeskID.Direction = ParameterDirection.Input;
            pInfoDeskID.Value = InfoDeskID;

            SqlParameter pProductImage = sqlComm.Parameters.Add("@ProductImage", SqlDbType.NVarChar, 100);
            pProductImage.Direction = ParameterDirection.Input;
            pProductImage.Value = ProductImage;

            SqlParameter pInfoDeskType = sqlComm.Parameters.Add("@type", SqlDbType.NVarChar, 1);
            pInfoDeskType.Direction = ParameterDirection.Input;
            pInfoDeskType.Value = InfoDeskType;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion

        #region "Delete"

        public static void DeleteSlideByID(int? InfoDeskID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteSliderByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInfoDeskID = sqlComm.Parameters.Add("@InfoDeskID", SqlDbType.Int);
            pInfoDeskID.Direction = ParameterDirection.Input;
            pInfoDeskID.Value = InfoDeskID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteMultiLanguageInfoDeskByID(int? InfoDeskID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteMultiLanguageInfoDeskByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInfoDeskID = sqlComm.Parameters.Add("@InfoDeskID", SqlDbType.Int);
            pInfoDeskID.Direction = ParameterDirection.Input;
            pInfoDeskID.Value = InfoDeskID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteInfoDeskByID(int? InfoDeskID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteInfoDeskByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pInfoDeskID = sqlComm.Parameters.Add("@InfoDeskID", SqlDbType.Int);
            pInfoDeskID.Direction = ParameterDirection.Input;
            pInfoDeskID.Value = InfoDeskID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void DeleteFile(string FileName, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeleteFile", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pFileName = sqlComm.Parameters.Add("@FileName", SqlDbType.NVarChar, 1000);
            pFileName.Direction = ParameterDirection.Input;
            pFileName.Value = FileName;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion

    }
}