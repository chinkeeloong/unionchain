﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using ECFBase.Models;

namespace ECFBase.Components
{
    public class MemberWalletDB
    {
        #region Utility
        public static DataSet GetBillByID(int? BillID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetBillByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryId = sqlComm.Parameters.Add("@BillID", SqlDbType.Int);
            pCountryId.Direction = ParameterDirection.Input;
            pCountryId.Value = BillID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetUtilityByID(int? UtilityID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetUtilityByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUtilityID = sqlComm.Parameters.Add("@UtilityID", SqlDbType.Int);
            pUtilityID.Direction = ParameterDirection.Input;
            pUtilityID.Value = UtilityID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static void CancelBill(int BillID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CancelBill", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pBillID = sqlComm.Parameters.Add("@BillID", SqlDbType.Int);
            pBillID.Direction = ParameterDirection.Input;
            pBillID.Value = BillID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;
            
            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

        }

        public static DataSet GetBillByUsername(string username, string UtilityID, string Status, int viewPage, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetBillByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pUtilityID = sqlComm.Parameters.Add("@UtilityID", SqlDbType.NVarChar, 20);
            pUtilityID.Direction = ParameterDirection.Input;
            pUtilityID.Value = UtilityID;

            SqlParameter pStatus = sqlComm.Parameters.Add("@Status", SqlDbType.NVarChar, 20);
            pStatus.Direction = ParameterDirection.Input;
            pStatus.Value = Status;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            pages = (int)pPages.Value;

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllUtilityList(string LanguageCode, string CountryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllUtilityList", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 20);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = CountryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet UtilityChecking(string UtilityID)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_UtilityChecking", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUtilityID = sqlComm.Parameters.Add("@UtilityID", SqlDbType.Int);
            pUtilityID.Direction = ParameterDirection.Input;
            pUtilityID.Value = UtilityID;


            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        public static void CreateBill(UtilitySetupModel csm, string username, string createdBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateBillSetup", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;


            SqlParameter pSelectedUtilityList = sqlComm.Parameters.Add("@UtilityCode", SqlDbType.NVarChar, 100);
            pSelectedUtilityList.Direction = ParameterDirection.Input;
            pSelectedUtilityList.Value = csm.SelectedUtilityList;

            SqlParameter pProcessingTime = sqlComm.Parameters.Add("@ProcessingTime", SqlDbType.NVarChar, 100);
            pProcessingTime.Direction = ParameterDirection.Input;
            pProcessingTime.Value = csm.ProcessingTime;

            SqlParameter pUtilityDescription = sqlComm.Parameters.Add("@UtilityDescription", SqlDbType.NVarChar, 100);
            pUtilityDescription.Direction = ParameterDirection.Input;
            pUtilityDescription.Value = Helpers.Helper.NVL(csm.UtilityDescription);

            SqlParameter pBillAccount = sqlComm.Parameters.Add("@BillAccount", SqlDbType.NVarChar, 50);
            pBillAccount.Direction = ParameterDirection.Input;
            pBillAccount.Value = Helpers.Helper.NVL(csm.BillAccount);


            SqlParameter pMobileNumber = sqlComm.Parameters.Add("@MobileNumber", SqlDbType.NVarChar, 100);
            pMobileNumber.Direction = ParameterDirection.Input;
            pMobileNumber.Value = Helpers.Helper.NVL(csm.MobileNumber);

            
            SqlParameter pBillAmount = sqlComm.Parameters.Add("@BillAmount", SqlDbType.Float);
            pBillAmount.Direction = ParameterDirection.Input;
            pBillAmount.Value = float.Parse(csm.BillAmount);

            SqlParameter pCurrentPrice = sqlComm.Parameters.Add("@CurrentPrice", SqlDbType.Float);
            pCurrentPrice.Direction = ParameterDirection.Input;
            pCurrentPrice.Value = csm.CurrentPrice;

            SqlParameter pUNSAmount = sqlComm.Parameters.Add("@UNSAmount", SqlDbType.Float);
            pUNSAmount.Direction = ParameterDirection.Input;
            pUNSAmount.Value = csm.UNSAmount;

            SqlParameter pFees = sqlComm.Parameters.Add("@FEES", SqlDbType.Float);
            pFees.Direction = ParameterDirection.Input;
            pFees.Value = csm.Fees;

            SqlParameter pTotalQP = sqlComm.Parameters.Add("@TotalAmount", SqlDbType.NVarChar, 10);
            pTotalQP.Direction = ParameterDirection.Input;
            pTotalQP.Value = csm.TotalQP;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;            

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        public static void UpdateBill(UtilitySetupModel csm, string username, string createdBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateBillSetup", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pSelectedUtilityList = sqlComm.Parameters.Add("@UtilityCode", SqlDbType.NVarChar, 100);
            pSelectedUtilityList.Direction = ParameterDirection.Input;
            pSelectedUtilityList.Value = csm.SelectedUtilityList;

            SqlParameter pProcessingTime = sqlComm.Parameters.Add("@ProcessingTime", SqlDbType.NVarChar, 100);
            pProcessingTime.Direction = ParameterDirection.Input;
            pProcessingTime.Value = csm.ProcessingTime;

            SqlParameter pUtilityDescription = sqlComm.Parameters.Add("@UtilityDescription", SqlDbType.NVarChar, 100);
            pUtilityDescription.Direction = ParameterDirection.Input;
            pUtilityDescription.Value = csm.UtilityDescription;

            SqlParameter pBillAccount = sqlComm.Parameters.Add("@BillAccount", SqlDbType.NVarChar, 50);
            pBillAccount.Direction = ParameterDirection.Input;
            pBillAccount.Value = csm.BillAccount;

            SqlParameter pMobileNumber = sqlComm.Parameters.Add("@MobileNumber", SqlDbType.NVarChar, 100);
            pMobileNumber.Direction = ParameterDirection.Input;
            pMobileNumber.Value = csm.MobileNumber;


            SqlParameter pBillAmount = sqlComm.Parameters.Add("@BillAmount", SqlDbType.Float);
            pBillAmount.Direction = ParameterDirection.Input;
            pBillAmount.Value = float.Parse(csm.BillAmount);

            SqlParameter pCurrentPrice = sqlComm.Parameters.Add("@CurrentPrice", SqlDbType.Float);
            pCurrentPrice.Direction = ParameterDirection.Input;
            pCurrentPrice.Value = csm.CurrentPrice;

            SqlParameter pUNSAmount = sqlComm.Parameters.Add("@UNSAmount", SqlDbType.Float);
            pUNSAmount.Direction = ParameterDirection.Input;
            pUNSAmount.Value = csm.UNSAmount;

            SqlParameter pFees = sqlComm.Parameters.Add("@FEES", SqlDbType.Float);
            pFees.Direction = ParameterDirection.Input;
            pFees.Value = csm.Fees;

            SqlParameter pTotalQP = sqlComm.Parameters.Add("@TotalAmount", SqlDbType.NVarChar, 10);
            pTotalQP.Direction = ParameterDirection.Input;
            pTotalQP.Value = csm.TotalQP;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;


            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion
        //public static DataSet GetHashStatementByUsername(string username, out int ok, out string message)
        //{
        //    SqlConnection sqlConn = DBOperator.GetConnection();
        //    SqlDataAdapter da = new SqlDataAdapter();

        //    SqlCommand sqlComm = new SqlCommand("SP_GetHashStatementByUsername", sqlConn);
        //    sqlComm.CommandType = CommandType.StoredProcedure;

        //    SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
        //    pUsername.Direction = ParameterDirection.Input;
        //    pUsername.Value = username;

        //    SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
        //    pOk.Direction = ParameterDirection.Output;

        //    SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
        //    pMessage.Direction = ParameterDirection.Output;

        //    da.SelectCommand = sqlComm;
        //    DataSet ds = new DataSet();

        //    sqlConn.Open();
        //    da.Fill(ds);

        //    ok = (int)pOk.Value;
        //    message = pMessage.Value.ToString();
        //    sqlConn.Close();

        //    return ds;
        //}

        public static DataSet GetHashStatementByUsername(string Username , string Type, int viewPage,  out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetHashStatementByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pType = sqlComm.Parameters.Add("@Type", SqlDbType.NVarChar, 10);
            pType.Direction = ParameterDirection.Input;
            pType.Value = Type;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }


        public static DataSet SP_GetHashStatementByUsernameFor300Token(string Username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetHashStatementByUsernameFor300Token", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
          

            sqlConn.Close();
            return ds;
        }



        public static void CheckMemberUpDownBinaryBased(string Username, string SearchUser, out int Status)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CheckMemberUpDownBinaryBased", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pSearchUser = sqlComm.Parameters.Add("@SEARCHUSER", SqlDbType.NVarChar, 50);
            pSearchUser.Direction = ParameterDirection.Input;
            pSearchUser.Value = SearchUser;

            SqlParameter pStatus = sqlComm.Parameters.Add("@STATUS", SqlDbType.Int);
            pStatus.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            Status = (int)pStatus.Value;
            sqlConn.Close();
        }


        #region Wallet Report
        public static DataSet GetBonusSummaryByUsername(string Username, int Year, int Month, int viewPage, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetBonusSummaryByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pYear = sqlComm.Parameters.Add("@YEAR", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = Year;

            SqlParameter pMonth = sqlComm.Parameters.Add("@MONTH", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = Month;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }

        public static DataSet GetSponsorBonusByUsername(string Username, int Year, int Month, int viewPage, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetSponsorBonusByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pYear = sqlComm.Parameters.Add("@YEAR", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = Year;

            SqlParameter pMonth = sqlComm.Parameters.Add("@MONTH", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = Month;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }

        public static DataSet GetPairingBonusByUsername(string username, int month, int year, int viewPage, out int pages, out int ok, out string msg)
        {
            var sqlConn = DBOperator.GetConnection();
            var dataAdapter = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberDetailsPairingBonusLog", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = month;

            SqlParameter pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = year;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            dataAdapter.SelectCommand = sqlComm;
            var dataset = new DataSet();

            sqlConn.Open();
            dataAdapter.Fill(dataset);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;

            sqlConn.Close();
            return dataset;
        }

        public static DataSet GetMatchingBonusByUsername(string username, int month, int year, int viewPage, out int pages, out int ok, out string msg)
        {
            var sqlConn = DBOperator.GetConnection();
            var dataAdapter = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMatchingBonusByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pMonth = sqlComm.Parameters.Add("@month", SqlDbType.Int);
            pMonth.Direction = ParameterDirection.Input;
            pMonth.Value = month;

            SqlParameter pYear = sqlComm.Parameters.Add("@year", SqlDbType.Int);
            pYear.Direction = ParameterDirection.Input;
            pYear.Value = year;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            dataAdapter.SelectCommand = sqlComm;
            var dataset = new DataSet();

            sqlConn.Open();
            dataAdapter.Fill(dataset);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPages.Value;

            sqlConn.Close();
            return dataset;
        }
        #endregion

        #region Wallet Transaction Log

        public static DataSet GetAllRegisterWalletLogByUsername(string Cashname, string Username, int viewPage, string StartDate, string EndDate, string languageCode, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllWalletRegisterLogByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pStartDate = sqlComm.Parameters.Add("@startdate", SqlDbType.NVarChar, 50);
            pStartDate.Direction = ParameterDirection.Input;
            pStartDate.Value = StartDate;

            SqlParameter pEndDate = sqlComm.Parameters.Add("@enddate", SqlDbType.NVarChar, 50);
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = EndDate;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pCashname = sqlComm.Parameters.Add("@Cashname", SqlDbType.NVarChar, 50);
            pCashname.Direction = ParameterDirection.Input;
            pCashname.Value = Cashname;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            //SqlParameter pExport = sqlComm.Parameters.Add("@export", SqlDbType.Int);
            //pExport.Direction = ParameterDirection.Input;
            //pExport.Value = export;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }

        public static DataSet GetAllActivationWalletLogByUsername(string Cashname, string Username, int viewPage, string StartDate, string EndDate, string languageCode, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllActivationWalletLogByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pStartDate = sqlComm.Parameters.Add("@startdate", SqlDbType.NVarChar, 50);
            pStartDate.Direction = ParameterDirection.Input;
            pStartDate.Value = StartDate;

            SqlParameter pEndDate = sqlComm.Parameters.Add("@enddate", SqlDbType.NVarChar, 50);
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = EndDate;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pCashname = sqlComm.Parameters.Add("@Cashname", SqlDbType.NVarChar, 50);
            pCashname.Direction = ParameterDirection.Input;
            pCashname.Value = Cashname;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            //SqlParameter pExport = sqlComm.Parameters.Add("@export", SqlDbType.Int);
            //pExport.Direction = ParameterDirection.Input;
            //pExport.Value = export;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }

        public static DataSet GetAllCashWalletLogByUsername(string Cashname, string Username, int viewPage, string StartDate, string EndDate, string languageCode, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllWalletCashLogByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pStartDate = sqlComm.Parameters.Add("@startdate", SqlDbType.NVarChar, 50);
            pStartDate.Direction = ParameterDirection.Input;
            pStartDate.Value = StartDate;

            SqlParameter pEndDate = sqlComm.Parameters.Add("@enddate", SqlDbType.NVarChar, 50);
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = EndDate;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pCashname = sqlComm.Parameters.Add("@Cashname", SqlDbType.NVarChar, 50);
            pCashname.Direction = ParameterDirection.Input;
            pCashname.Value = Cashname;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            //SqlParameter pExport = sqlComm.Parameters.Add("@export", SqlDbType.Int);
            //pExport.Direction = ParameterDirection.Input;
            //pExport.Value = export;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }

        public static DataSet GetAllShareUnitLogByUsername(string Username, int viewPage, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllShareUnitLogByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }

        public static DataSet GetAllShareWalletLogByUsername(string cashname, string Username, int viewPage, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllWalletShareLogByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pCashname = sqlComm.Parameters.Add("@cashname", SqlDbType.NVarChar, 50);
            pCashname.Direction = ParameterDirection.Input;
            pCashname.Value = cashname;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }

        #endregion

        #region Wallet Transfer

        public static void WalletSend(string username, float amount, string pin, string from, string remark, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_WalletSend", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pWallet = sqlComm.Parameters.Add("@amount", SqlDbType.Money);
            pWallet.Direction = ParameterDirection.Input;
            pWallet.Value = amount;

            SqlParameter pPin = sqlComm.Parameters.Add("@pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = pin;

            var pRemark = sqlComm.Parameters.Add("@remark", SqlDbType.NVarChar, 100);
            pRemark.Direction = ParameterDirection.Input;
            pRemark.Value = remark;

            SqlParameter pFrom = sqlComm.Parameters.Add("@from", SqlDbType.NVarChar, 20);
            pFrom.Direction = ParameterDirection.Input;
            pFrom.Value = from;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 4000);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void RegisterWalletTransfer(string username, float amount, string pin, string from, string remark, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_TransferRegisterWallet", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pWallet = sqlComm.Parameters.Add("@amount", SqlDbType.Money);
            pWallet.Direction = ParameterDirection.Input;
            pWallet.Value = amount;

            SqlParameter pPin = sqlComm.Parameters.Add("@pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = pin;

            var pRemark = sqlComm.Parameters.Add("@remark", SqlDbType.NVarChar, 100);
            pRemark.Direction = ParameterDirection.Input;
            pRemark.Value = remark;

            SqlParameter pFrom = sqlComm.Parameters.Add("@from", SqlDbType.NVarChar, 20);
            pFrom.Direction = ParameterDirection.Input;
            pFrom.Value = from;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 4000);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

       
        public static void CashWalletTransfer(string username, string Option, float amount, string pin, string from, string remark, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_TransferCashWallet", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pOption = sqlComm.Parameters.Add("@Option", SqlDbType.NVarChar, 20);
            pOption.Direction = ParameterDirection.Input;
            pOption.Value = Option;

            SqlParameter pWallet = sqlComm.Parameters.Add("@amount", SqlDbType.Money);
            pWallet.Direction = ParameterDirection.Input;
            pWallet.Value = amount;

            SqlParameter pPin = sqlComm.Parameters.Add("@pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = pin;

            var pRemark = sqlComm.Parameters.Add("@remark", SqlDbType.NVarChar, 100);
            pRemark.Direction = ParameterDirection.Input;
            pRemark.Value = remark;

            SqlParameter pFrom = sqlComm.Parameters.Add("@from", SqlDbType.NVarChar, 20);
            pFrom.Direction = ParameterDirection.Input;
            pFrom.Value = from;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 4000);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void ActivationWalletTransfer(string username, float amount, string pin, string from, string remark, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_TransferActivationWallet", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pWallet = sqlComm.Parameters.Add("@amount", SqlDbType.Money);
            pWallet.Direction = ParameterDirection.Input;
            pWallet.Value = amount;

            SqlParameter pPin = sqlComm.Parameters.Add("@pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = pin;

            var pRemark = sqlComm.Parameters.Add("@remark", SqlDbType.NVarChar, 100);
            pRemark.Direction = ParameterDirection.Input;
            pRemark.Value = remark;

            SqlParameter pFrom = sqlComm.Parameters.Add("@from", SqlDbType.NVarChar, 20);
            pFrom.Direction = ParameterDirection.Input;
            pFrom.Value = from;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 4000);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion

        #region Wallet Exchang 

        public static void ExchangeCashWalletToRegisterWallet(string username, int amount, string pin, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ExchangeCashWalletToRegisterWallet", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pWallet = sqlComm.Parameters.Add("@amount", SqlDbType.Money);
            pWallet.Direction = ParameterDirection.Input;
            pWallet.Value = amount;

            SqlParameter pPin = sqlComm.Parameters.Add("@pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = pin;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 4000);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void ExchangeProfitSharingWalletToCashWallet(string username, int amount, string pin, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ExchangeProfitSharingWalletToCashWallet", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pWallet = sqlComm.Parameters.Add("@amount", SqlDbType.Money);
            pWallet.Direction = ParameterDirection.Input;
            pWallet.Value = amount;

            SqlParameter pPin = sqlComm.Parameters.Add("@pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = pin;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 4000);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion

        #region Wallet Withdrawal

        public static void WithdrawCashWallet(string username, float amount, string pin, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_WithdrawCashWallet", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pWallet = sqlComm.Parameters.Add("@amount", SqlDbType.Money);
            pWallet.Direction = ParameterDirection.Input;
            pWallet.Value = amount;

            SqlParameter pPin = sqlComm.Parameters.Add("@pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = pin;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 4000);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }


        public static DataSet GetAllCashWithdrawalLogMember(int ViewPage,string Language, string Username, string Country, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllWalletCashWithdrawalLogMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@ViewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = ViewPage;
            
            SqlParameter pLanguage = sqlComm.Parameters.Add("@Language", SqlDbType.NVarChar, 50);
            pLanguage.Direction = ParameterDirection.Input;
            pLanguage.Value = Language;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.NVarChar, 50);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            pages = (int)pPages.Value;

            sqlConn.Close();

            return ds;
        }
        #endregion

        public static void RequestWalletTopUp(string username, float amount, string pin, string remark, string paymentmethod, string PaymentImage, float LocalAmount, string FromWallet, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_RequestWalletTopUp", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pWallet = sqlComm.Parameters.Add("@amount", SqlDbType.Money);
            pWallet.Direction = ParameterDirection.Input;
            pWallet.Value = amount;

            SqlParameter pPin = sqlComm.Parameters.Add("@pin", SqlDbType.NVarChar, 50);
            pPin.Direction = ParameterDirection.Input;
            pPin.Value = pin;

            SqlParameter pRemark = sqlComm.Parameters.Add("@remark", SqlDbType.NVarChar, 100);
            pRemark.Direction = ParameterDirection.Input;
            pRemark.Value = remark;

            SqlParameter ppaymentmethod = sqlComm.Parameters.Add("@paymentmethod", SqlDbType.NVarChar, 50);
            ppaymentmethod.Direction = ParameterDirection.Input;
            ppaymentmethod.Value = paymentmethod;

            SqlParameter pPaymentImage = sqlComm.Parameters.Add("@PaymentImage", SqlDbType.NVarChar, 500);
            pPaymentImage.Direction = ParameterDirection.Input;
            pPaymentImage.Value = PaymentImage;

            SqlParameter pLocalAmount = sqlComm.Parameters.Add("@LocalAmount", SqlDbType.Money, 100);
            pLocalAmount.Direction = ParameterDirection.Input;
            pLocalAmount.Value = LocalAmount;

            SqlParameter pFromWallet = sqlComm.Parameters.Add("@FromWallet", SqlDbType.NVarChar, 50);
            pFromWallet.Direction = ParameterDirection.Input;
            pFromWallet.Value = FromWallet;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 4000);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void CancelRequestTopUp(int? currencyId, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("[SP_CalcelRequestTopUp]", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = currencyId;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static DataSet GetAllRequestTopUpListByMemberID(int ViewPage, string Username, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllRequestTopUpListByMemberID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = ViewPage;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }

        public static DataSet GetMemberROIPackage(string Username)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberROIPackage", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;


            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();
            return ds;
        }
    }

}