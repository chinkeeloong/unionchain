﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ECFBase.Components
{
    public class PaymentModeSetupDB
    {
        #region Get

        public static DataSet GetAllPaymentMode(out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllPaymentMode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int) pOk.Value;
            msg = pMessage.Value.ToString();

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetPaymentModeByID(int payModeID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetPaymentModeByID", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPayModeID = sqlComm.Parameters.Add("@PaymentModeID", SqlDbType.Int);
            pPayModeID.Direction = ParameterDirection.Input;
            pPayModeID.Value = payModeID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        #endregion

        #region Create
        public static void CreateNewPaymentMode(string paymentType, string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreatePaymentMode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPaymentType = sqlComm.Parameters.Add("@PaymentModeType", SqlDbType.NVarChar, 100);
            pPaymentType.Direction = ParameterDirection.Input;
            pPaymentType.Value = paymentType;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        #region Update
        public static void UpdatePaymentMode(int? paymentModeID, string newPaymentModeType, 
                                                string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdatePaymentMode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryID = sqlComm.Parameters.Add("@PaymentModeID", SqlDbType.Int);
            pCategoryID.Direction = ParameterDirection.Input;
            pCategoryID.Value = paymentModeID;

            SqlParameter pCategoryCode = sqlComm.Parameters.Add("@NewPaymentModeType", SqlDbType.NVarChar, 100);
            pCategoryCode.Direction = ParameterDirection.Input;
            pCategoryCode.Value = newPaymentModeType;

            SqlParameter pCategoryName = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pCategoryName.Direction = ParameterDirection.Input;
            pCategoryName.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        #region Delete
        public static void DeletePaymentMode(int? paymentModeID, string updatedBy, 
                                            out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_DeletePaymentMode", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryID = sqlComm.Parameters.Add("@PaymentModeID", SqlDbType.Int);
            pCategoryID.Direction = ParameterDirection.Input;
            pCategoryID.Value = paymentModeID;

            SqlParameter pCategoryName = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pCategoryName.Direction = ParameterDirection.Input;
            pCategoryName.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion
    }
}