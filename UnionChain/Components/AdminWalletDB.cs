﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Net;

namespace ECFBase.Components
{
    public class AdminWalletDB
    {
        #region Utilities

        public static DataSet GetAllBill(int viewPage, out int pages, out int ok, out string message)
        {
            var ds = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    SqlCommand sqlComm = new SqlCommand("SP_GetAllBill", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(ds);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return ds;
        }

        public static DataSet GetAllBillApprove(int viewPage, out int pages, out int ok, out string message)
        {
            var ds = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    SqlCommand sqlComm = new SqlCommand("SP_GetAllBillApprove", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
                    pViewPage.Direction = ParameterDirection.Input;
                    pViewPage.Value = viewPage;

                    SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
                    pPages.Direction = ParameterDirection.Output;

                    var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
                    pOk.Direction = ParameterDirection.Output;

                    var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
                    pMessage.Direction = ParameterDirection.Output;

                    dataAdapter.Fill(ds);

                    ok = (int)pOk.Value;
                    message = pMessage.Value.ToString();
                    pages = (int)pPages.Value;
                    sqlConn.Close();
                }
            }

            return ds;
        }

        public static DataSet GetBillByBillID(int BillID)
        {
            var ds = new DataSet();
            using (SqlConnection sqlConn = DBOperator.GetConnection())
            {
                sqlConn.Open();
                using (var dataAdapter = new SqlDataAdapter())
                {
                    SqlCommand sqlComm = new SqlCommand("SP_GetBillByBillID", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    dataAdapter.SelectCommand = sqlComm;

                    SqlParameter pBillID = sqlComm.Parameters.Add("@BillID", SqlDbType.Int);
                    pBillID.Direction = ParameterDirection.Input;
                    pBillID.Value = BillID;


                    dataAdapter.Fill(ds);
                    sqlConn.Close();
                }
            }

            return ds;
        }

        public static void ApproveBill(int BillID)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ApproveBill", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pBillID = sqlComm.Parameters.Add("@BillID", SqlDbType.Int);
            pBillID.Direction = ParameterDirection.Input;
            pBillID.Value = BillID;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }
    
        public static void RejectBill(int BillID, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_RejectBill", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pBillID = sqlComm.Parameters.Add("@BillID", SqlDbType.Int);
            pBillID.Direction = ParameterDirection.Input;
            pBillID.Value = BillID;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }


        #endregion


        #region WalletAdjustment
        public static void WalletAdjustment(string storedProcedure, string username, float wallet, string cashName, string remark, string walletType, string from, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand(storedProcedure, sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pWallet = sqlComm.Parameters.Add("@Wallet", SqlDbType.Float);
            pWallet.Direction = ParameterDirection.Input;
            pWallet.Value = wallet;

            SqlParameter pFrom = sqlComm.Parameters.Add("@From", SqlDbType.NVarChar, 50);
            pFrom.Direction = ParameterDirection.Input;
            pFrom.Value = from;

            SqlParameter pCashName = sqlComm.Parameters.Add("@CashName", SqlDbType.NVarChar, 20);
            pCashName.Direction = ParameterDirection.Input;
            pCashName.Value = cashName;

            SqlParameter pRemark = sqlComm.Parameters.Add("@Remark", SqlDbType.NVarChar, 50);
            pRemark.Direction = ParameterDirection.Input;
            pRemark.Value = remark;


            //SqlParameter pmode = sqlComm.Parameters.Add("@paymentmode", SqlDbType.NVarChar, 50);
            //pmode.Direction = ParameterDirection.Input;
            //pmode.Value = paymentmode;

            //SqlParameter pCountryCode = sqlComm.Parameters.Add("@countryCode", SqlDbType.NVarChar, 10);
            //pCountryCode.Direction = ParameterDirection.Input;
            //pCountryCode.Value = countryCode;


            SqlParameter pOk = sqlComm.Parameters.Add("@OK", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@MSG", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;



            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        #region Withdrawal
        public static DataSet GetAllCashWithdrawal(int ViewPage, string Username, string Country, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllWalletCashWithdrawal", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@ViewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = ViewPage;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.NVarChar, 50);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            pages = (int)pPages.Value;

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCashWithdrawalLog(int ViewPage, string Username, string Country, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllWalletCashWithdrawalLog", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@ViewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = ViewPage;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.NVarChar, 50);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = Country;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            pages = (int)pPages.Value;

            sqlConn.Close();

            return ds;
        }

        public static void ApproveBonusWithdrawal(List<int> withdrawalListID, string admin)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm;
            foreach (int id in withdrawalListID)
            {
                sqlComm = new SqlCommand("SP_ApproveBonusWithdrawal", sqlConn);
                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlParameter pLogID = sqlComm.Parameters.Add("@logID", SqlDbType.Int);
                pLogID.Direction = ParameterDirection.Input;
                pLogID.Value = id;

                SqlParameter pAdmin = sqlComm.Parameters.Add("@admin", SqlDbType.NVarChar, 20);
                pAdmin.Direction = ParameterDirection.Input;
                pAdmin.Value = admin;

                sqlComm.ExecuteNonQuery();

            }

            sqlConn.Close();
        }

        public static void RejectBonusWithdrawal(List<int> withdrawalListID, string admin)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm;
            foreach (int id in withdrawalListID)
            {
                sqlComm = new SqlCommand("SP_RejectBonusWithdrawal", sqlConn);
                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlParameter pLogID = sqlComm.Parameters.Add("@logID", SqlDbType.Int);
                pLogID.Direction = ParameterDirection.Input;
                pLogID.Value = id;

                SqlParameter pAdmin = sqlComm.Parameters.Add("@admin", SqlDbType.NVarChar, 20);
                pAdmin.Direction = ParameterDirection.Input;
                pAdmin.Value = admin;

                sqlComm.ExecuteNonQuery();
            }

            sqlConn.Close();
        }

        public static void RefundBonusWithdrawal(List<int> withdrawalListID, string admin)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm;
            foreach (int id in withdrawalListID)
            {
                sqlComm = new SqlCommand("SP_RefundBonusWithdrawal", sqlConn);
                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlParameter pLogID = sqlComm.Parameters.Add("@logID", SqlDbType.Int);
                pLogID.Direction = ParameterDirection.Input;
                pLogID.Value = id;

                SqlParameter pAdmin = sqlComm.Parameters.Add("@admin", SqlDbType.NVarChar, 20);
                pAdmin.Direction = ParameterDirection.Input;
                pAdmin.Value = admin;

                sqlComm.ExecuteNonQuery();
            }

            sqlConn.Close();
        }

        public static DataSet GetBonusWithdrawalFullList(DateTime dtSelected, int export, int state, string languageCode)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetBonusWithdrawalFullList", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pSelectedDate = sqlComm.Parameters.Add("@selectedDate", SqlDbType.DateTime);
            pSelectedDate.Direction = ParameterDirection.Input;
            pSelectedDate.Value = dtSelected;

            SqlParameter pExport = sqlComm.Parameters.Add("@export", SqlDbType.Int);
            pExport.Direction = ParameterDirection.Input;
            pExport.Value = export;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pState = sqlComm.Parameters.Add("@state", SqlDbType.Int);
            pState.Direction = ParameterDirection.Input;
            pState.Value = state;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }
        #endregion

        #region View WalletLog       
        public static DataSet GetAllTradeableWalletLog(string Username, string Cashname, string StartDate, string EndDate, string languageCode, int export, int viewPage, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllTradeableWalletLog", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pStartDate = sqlComm.Parameters.Add("@startdate", SqlDbType.NVarChar, 50);
            pStartDate.Direction = ParameterDirection.Input;
            pStartDate.Value = StartDate;

            SqlParameter pEndDate = sqlComm.Parameters.Add("@enddate", SqlDbType.NVarChar, 50);
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = EndDate;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pCashname = sqlComm.Parameters.Add("@Cashname", SqlDbType.NVarChar, 50);
            pCashname.Direction = ParameterDirection.Input;
            pCashname.Value = Cashname;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pExport = sqlComm.Parameters.Add("@export", SqlDbType.Int);
            pExport.Direction = ParameterDirection.Input;
            pExport.Value = export;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }
        
        public static DataSet GetAllUntradeableWalletLog(string Username, string Cashname, string StartDate, string EndDate, string languageCode, int export, int viewPage, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllUntradeableWalletLog", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pStartDate = sqlComm.Parameters.Add("@startdate", SqlDbType.NVarChar, 50);
            pStartDate.Direction = ParameterDirection.Input;
            pStartDate.Value = StartDate;

            SqlParameter pEndDate = sqlComm.Parameters.Add("@enddate", SqlDbType.NVarChar, 50);
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = EndDate;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pCashname = sqlComm.Parameters.Add("@Cashname", SqlDbType.NVarChar, 50);
            pCashname.Direction = ParameterDirection.Input;
            pCashname.Value = Cashname;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pExport = sqlComm.Parameters.Add("@export", SqlDbType.Int);
            pExport.Direction = ParameterDirection.Input;
            pExport.Value = export;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }

        public static DataSet GetAllActivationPointWalletLog(string Username, string Cashname, string StartDate, string EndDate, string languageCode, int export, int viewPage, out int pages)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllWalletActivationLog", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pStartDate = sqlComm.Parameters.Add("@startdate", SqlDbType.NVarChar, 50);
            pStartDate.Direction = ParameterDirection.Input;
            pStartDate.Value = StartDate;

            SqlParameter pEndDate = sqlComm.Parameters.Add("@enddate", SqlDbType.NVarChar, 50);
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = EndDate;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pCashname = sqlComm.Parameters.Add("@Cashname", SqlDbType.NVarChar, 50);
            pCashname.Direction = ParameterDirection.Input;
            pCashname.Value = Cashname;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = languageCode;

            SqlParameter pExport = sqlComm.Parameters.Add("@export", SqlDbType.Int);
            pExport.Direction = ParameterDirection.Input;
            pExport.Value = export;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = viewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            pages = (int)pPages.Value;

            sqlConn.Close();
            return ds;
        }

        

        #endregion

    }
}