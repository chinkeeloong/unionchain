﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using ECFBase.Models;

namespace ECFBase.Components
{
    public class MemberShareDB
    {

        public static DataSet GetHashPerformanceBonusByUsername(string username, int selectedPage, string startdate, string enddate, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashPerformanceBonusByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;


            var pstartdate = sqlComm.Parameters.Add("@startdate", SqlDbType.NVarChar, 50);
            pstartdate.Direction = ParameterDirection.Input;
            pstartdate.Value = startdate;

            var pEnddate = sqlComm.Parameters.Add("@enddate", SqlDbType.NVarChar, 50);
            pEnddate.Direction = ParameterDirection.Input;
            pEnddate.Value = enddate;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetHashPowerBonusByUsername(string username, int selectedPage, string startdate, string enddate, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashPowerBonusByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            var pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pstartdate = sqlComm.Parameters.Add("@startdate", SqlDbType.NVarChar, 50);
            pstartdate.Direction = ParameterDirection.Input;
            pstartdate.Value = startdate;

            var pEnddate = sqlComm.Parameters.Add("@enddate", SqlDbType.NVarChar, 50);
            pEnddate.Direction = ParameterDirection.Input;
            pEnddate.Value = enddate;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetHashBonusByUsername(string username, int selectedPage, string startdate, string enddate, out int pages, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_GetHashBonusByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            var pSelectedPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pSelectedPage.Direction = ParameterDirection.Input;
            pSelectedPage.Value = selectedPage;

            var pstartdate = sqlComm.Parameters.Add("@startdate", SqlDbType.NVarChar, 50);
            pstartdate.Direction = ParameterDirection.Input;
            pstartdate.Value = startdate;

            var pEnddate = sqlComm.Parameters.Add("@enddate", SqlDbType.NVarChar, 50);
            pEnddate.Direction = ParameterDirection.Input;
            pEnddate.Value = enddate;

            var pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            var pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            pages = (int)pPage.Value;
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetTradingPriceValue()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_Get_Price", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }
        public static void SaveTradingPriceValue(string Price, int ID, string CreatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_Save_Price", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pPrice = sqlComm.Parameters.Add("@Price", SqlDbType.NVarChar, 50);
            pPrice.Direction = ParameterDirection.Input;
            pPrice.Value = Price;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = ID;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 50);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = CreatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }


        public static DataSet GetAllTradingBuyingQueue(string Username, string Status , string Rate , string StartDate , string EndDate,  int ViewPage , out int Page , out int ok , out string msg  )
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_Get_Buy_Queue", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pStatus = sqlComm.Parameters.Add("@STATUS", SqlDbType.NVarChar, 50);
            pStatus.Direction = ParameterDirection.Input;
            pStatus.Value = Status;

            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.NVarChar, 50);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            SqlParameter pStartDate = sqlComm.Parameters.Add("@STARTDATE", SqlDbType.NVarChar, 50);
            pStartDate.Direction = ParameterDirection.Input;
            pStartDate.Value = StartDate;

            SqlParameter pEndDate = sqlComm.Parameters.Add("@ENDDATE", SqlDbType.NVarChar, 50);
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = EndDate;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = ViewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pok = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pok.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            Page = (int)pPages.Value;
            msg = pMessage.Value.ToString();
            ok = (int)pok.Value;

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllTradingSellingQueue(string Username, string Status, string Rate, string StartDate, string EndDate, int ViewPage, out int Page, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_Get_Sell_Queue", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pStatus = sqlComm.Parameters.Add("@STATUS", SqlDbType.NVarChar, 50);
            pStatus.Direction = ParameterDirection.Input;
            pStatus.Value = Status;

            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.NVarChar, 50);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            SqlParameter pStartDate = sqlComm.Parameters.Add("@STARTDATE", SqlDbType.NVarChar, 50);
            pStartDate.Direction = ParameterDirection.Input;
            pStartDate.Value = StartDate;

            SqlParameter pEndDate = sqlComm.Parameters.Add("@ENDDATE", SqlDbType.NVarChar, 50);
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = EndDate;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = ViewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pok = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pok.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            Page = (int)pPages.Value;
            msg = pMessage.Value.ToString();
            ok = (int)pok.Value;

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllTradingMatchQueue(string Buyer, string Seller, string Status, string Rate, string StartDate, string EndDate, int ViewPage, out int Page, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_Get_Match_Queue", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pBuyer = sqlComm.Parameters.Add("@BUYER", SqlDbType.NVarChar, 50);
            pBuyer.Direction = ParameterDirection.Input;
            pBuyer.Value = Buyer;

            SqlParameter pSeller = sqlComm.Parameters.Add("@SELLER", SqlDbType.NVarChar, 50);
            pSeller.Direction = ParameterDirection.Input;
            pSeller.Value = Seller;

            SqlParameter pStatus = sqlComm.Parameters.Add("@STATUS", SqlDbType.NVarChar, 50);
            pStatus.Direction = ParameterDirection.Input;
            pStatus.Value = Status;

            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.NVarChar, 50);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            SqlParameter pStartDate = sqlComm.Parameters.Add("@STARTDATE", SqlDbType.NVarChar, 50);
            pStartDate.Direction = ParameterDirection.Input;
            pStartDate.Value = StartDate;

            SqlParameter pEndDate = sqlComm.Parameters.Add("@ENDDATE", SqlDbType.NVarChar, 50);
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = EndDate;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = ViewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pok = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pok.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            Page = (int)pPages.Value;
            msg = pMessage.Value.ToString();
            ok = (int)pok.Value;

            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllTradingMatchPendingQueue(string Buyer, string Seller, string Status, string Rate, string StartDate, string EndDate, int ViewPage, out int Page, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_Get_Match_PendingQueue", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pBuyer = sqlComm.Parameters.Add("@BUYER", SqlDbType.NVarChar, 50);
            pBuyer.Direction = ParameterDirection.Input;
            pBuyer.Value = Buyer;

            SqlParameter pSeller = sqlComm.Parameters.Add("@SELLER", SqlDbType.NVarChar, 50);
            pSeller.Direction = ParameterDirection.Input;
            pSeller.Value = Seller;

            SqlParameter pStatus = sqlComm.Parameters.Add("@STATUS", SqlDbType.NVarChar, 50);
            pStatus.Direction = ParameterDirection.Input;
            pStatus.Value = Status;

            SqlParameter pRate = sqlComm.Parameters.Add("@RATE", SqlDbType.NVarChar, 50);
            pRate.Direction = ParameterDirection.Input;
            pRate.Value = Rate;

            SqlParameter pStartDate = sqlComm.Parameters.Add("@STARTDATE", SqlDbType.NVarChar, 50);
            pStartDate.Direction = ParameterDirection.Input;
            pStartDate.Value = StartDate;

            SqlParameter pEndDate = sqlComm.Parameters.Add("@ENDDATE", SqlDbType.NVarChar, 50);
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = EndDate;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = ViewPage;

            SqlParameter pPages = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPages.Direction = ParameterDirection.Output;

            SqlParameter pok = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pok.Direction = ParameterDirection.Output;

            var pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            Page = (int)pPages.Value;
            msg = pMessage.Value.ToString();
            ok = (int)pok.Value;

            sqlConn.Close();

            return ds;
        }
        

        public static void CancelTradingMatch(string MatchTradingID, string Type, string CreatedBy)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Trading_Admin_Cancel", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = MatchTradingID;

            SqlParameter pType = sqlComm.Parameters.Add("@Type", SqlDbType.NVarChar,50);
            pType.Direction = ParameterDirection.Input;
            pType.Value = Type;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 50);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = CreatedBy;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        public static DataSet TradingRateRecord()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_Trading_Rate_Record", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

        public static DataSet TradingRecord()
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            var da = new SqlDataAdapter();

            var sqlComm = new SqlCommand("SP_Trading_Record", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            da.SelectCommand = sqlComm;
            var ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            sqlConn.Close();

            return ds;
        }

    }
}
