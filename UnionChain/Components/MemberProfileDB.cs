﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using ECFBase.Models;

namespace ECFBase.Components
{
    public class MemberProfileDB
    {

        #region "View"

        public static DataSet GetMemberBankByUsername(string Username, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetMemberBankByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.NVarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllProvinceByCountry(string LanguageCode, string CountryCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllProvinceByCountry", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = CountryCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllCityByProvince(string LanguageCode, string ProvinceCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllCityByProvince", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pProvinceCode = sqlComm.Parameters.Add("@ProvinceCode", SqlDbType.NVarChar, 10);
            pProvinceCode.Direction = ParameterDirection.Input;
            pProvinceCode.Value = ProvinceCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetAllDistrictByCity(string LanguageCode, string CityCode, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_GetAllDistrictByCity", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pLanguageCode = sqlComm.Parameters.Add("@LanguageCode", SqlDbType.NVarChar, 10);
            pLanguageCode.Direction = ParameterDirection.Input;
            pLanguageCode.Value = LanguageCode;

            SqlParameter pCityCode = sqlComm.Parameters.Add("@CityCode", SqlDbType.NVarChar, 10);
            pCityCode.Direction = ParameterDirection.Input;
            pCityCode.Value = CityCode;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        #endregion

        #region "Create"

        public static void CreateNewMemberBank(string CountryCode, string BankCode, string Username,
                                                string BranchName, string AccountNumber, string BeneficiaryName, string BeneficiaryIC,
                                                string createdBy, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMemberBank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = CountryCode;

            SqlParameter pBankCode = sqlComm.Parameters.Add("@BankCode", SqlDbType.NVarChar, 10);
            pBankCode.Direction = ParameterDirection.Input;
            pBankCode.Value = BankCode;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pBranchName = sqlComm.Parameters.Add("@BranchName", SqlDbType.NVarChar, 100);
            pBranchName.Direction = ParameterDirection.Input;
            pBranchName.Value = BranchName;

            SqlParameter pAccountNumber = sqlComm.Parameters.Add("@AccountNumber", SqlDbType.NVarChar, 100);
            pAccountNumber.Direction = ParameterDirection.Input;
            pAccountNumber.Value = AccountNumber;

            SqlParameter pBeneficiary = sqlComm.Parameters.Add("@Beneficiary", SqlDbType.NVarChar, 100);
            pBeneficiary.Direction = ParameterDirection.Input;
            pBeneficiary.Value = BeneficiaryName;

            SqlParameter pBeneficiaryIC = sqlComm.Parameters.Add("@BeneficiaryIC", SqlDbType.NVarChar, 100);
            pBeneficiaryIC.Direction = ParameterDirection.Input;
            pBeneficiaryIC.Value = BeneficiaryIC;

            SqlParameter pCreatedBy = sqlComm.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 20);
            pCreatedBy.Direction = ParameterDirection.Input;
            pCreatedBy.Value = createdBy;

            SqlParameter pCreatedOn = sqlComm.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
            pCreatedOn.Direction = ParameterDirection.Input;
            pCreatedOn.Value = DateTime.Now;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pDeletionState = sqlComm.Parameters.Add("@DeletionState", SqlDbType.Bit);
            pDeletionState.Direction = ParameterDirection.Input;
            pDeletionState.Value = false;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion

        #region "Update"


        #region PRE-MEMBER
        public static void ChangePreMemberPassword(string UserName, string CurrentPassword, string NewPassword, string updatedBy,
                                               out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ChangePasswordPreMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pCurrentPassword = sqlComm.Parameters.Add("@Current", SqlDbType.NVarChar, 50);
            pCurrentPassword.Direction = ParameterDirection.Input;
            pCurrentPassword.Value = CurrentPassword;

            SqlParameter pNewPassword = sqlComm.Parameters.Add("@New", SqlDbType.NVarChar, 50);
            pNewPassword.Direction = ParameterDirection.Input;
            pNewPassword.Value = NewPassword;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void ChangePreMemberPIN(string UserName, string CurrentPIN, string NewPIN, string updatedBy,
                                                out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ChangePINPreMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pCurrentPIN = sqlComm.Parameters.Add("@Current", SqlDbType.NVarChar, 50);
            pCurrentPIN.Direction = ParameterDirection.Input;
            pCurrentPIN.Value = CurrentPIN;

            SqlParameter pNewPIN = sqlComm.Parameters.Add("@New", SqlDbType.NVarChar, 50);
            pNewPIN.Direction = ParameterDirection.Input;
            pNewPIN.Value = NewPIN;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #endregion
        public static void ChangePassword(string UserName, string CurrentPassword, string NewPassword, string updatedBy,
                                                out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ChangePassword", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pCurrentPassword = sqlComm.Parameters.Add("@Current", SqlDbType.NVarChar, 50);
            pCurrentPassword.Direction = ParameterDirection.Input;
            pCurrentPassword.Value = CurrentPassword;

            SqlParameter pNewPassword = sqlComm.Parameters.Add("@New", SqlDbType.NVarChar, 50);
            pNewPassword.Direction = ParameterDirection.Input;
            pNewPassword.Value = NewPassword;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void ChangePIN(string UserName, string CurrentPIN, string NewPIN, string updatedBy,
                                                out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ChangePIN", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@UserName", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pCurrentPIN = sqlComm.Parameters.Add("@Current", SqlDbType.NVarChar, 50);
            pCurrentPIN.Direction = ParameterDirection.Input;
            pCurrentPIN.Value = CurrentPIN;

            SqlParameter pNewPIN = sqlComm.Parameters.Add("@New", SqlDbType.NVarChar, 50);
            pNewPIN.Direction = ParameterDirection.Input;
            pNewPIN.Value = NewPIN;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 20);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pUpdatedOn = sqlComm.Parameters.Add("@UpdatedOn", SqlDbType.DateTime);
            pUpdatedOn.Direction = ParameterDirection.Input;
            pUpdatedOn.Value = DateTime.Now;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateMemberBank(string CountryCode, string BankCode, string Username,
                                                string BranchName, string AccountNumber, string BeneficiaryName,
                                                string BeneficiaryIC, string BeneficiaryPhone, string BeneficiaryRelationship, string updatedBy,
                                                out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateMemberBank", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pCountryCode = sqlComm.Parameters.Add("@CountryCode", SqlDbType.NVarChar, 10);
            pCountryCode.Direction = ParameterDirection.Input;
            pCountryCode.Value = CountryCode;

            SqlParameter pBankCode = sqlComm.Parameters.Add("@BankCode", SqlDbType.NVarChar, 10);
            pBankCode.Direction = ParameterDirection.Input;
            pBankCode.Value = BankCode;

            SqlParameter pUsername = sqlComm.Parameters.Add("@Username", SqlDbType.NVarChar, 20);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = Username;

            SqlParameter pBranchName = sqlComm.Parameters.Add("@BranchName", SqlDbType.NVarChar, 100);
            pBranchName.Direction = ParameterDirection.Input;
            pBranchName.Value = BranchName;

            SqlParameter pAccountNumber = sqlComm.Parameters.Add("@AccountNumber", SqlDbType.NVarChar, 100);
            pAccountNumber.Direction = ParameterDirection.Input;
            pAccountNumber.Value = AccountNumber;

            SqlParameter pBeneficiary = sqlComm.Parameters.Add("@Beneficiary", SqlDbType.NVarChar, 100);
            pBeneficiary.Direction = ParameterDirection.Input;
            pBeneficiary.Value = BeneficiaryName;

            SqlParameter pBeneficiaryIC = sqlComm.Parameters.Add("@BeneficiaryIC", SqlDbType.NVarChar, 100);
            pBeneficiaryIC.Direction = ParameterDirection.Input;
            pBeneficiaryIC.Value = BeneficiaryIC;

            SqlParameter pBeneficiaryPhone = sqlComm.Parameters.Add("@BeneficiaryPhone", SqlDbType.NVarChar, 100);
            pBeneficiaryPhone.Direction = ParameterDirection.Input;
            pBeneficiaryPhone.Value = BeneficiaryPhone;

            SqlParameter pBeneficiaryRela = sqlComm.Parameters.Add("@BeneficiaryRela", SqlDbType.NVarChar, 100);
            pBeneficiaryRela.Direction = ParameterDirection.Input;
            pBeneficiaryRela.Value = " ";

            var pUpdatingUser = sqlComm.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar, 100);
            pUpdatingUser.Direction = ParameterDirection.Input;
            pUpdatingUser.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateMember(EditMemberModel mem, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = mem.Username;

            SqlParameter pCellPhone = sqlComm.Parameters.Add("@HPPHONE", SqlDbType.NVarChar, 50);
            pCellPhone.Direction = ParameterDirection.Input;
            pCellPhone.Value = (mem.NewCellPhone == null) ? "" : mem.NewCellPhone;

            SqlParameter pMobileCountry = sqlComm.Parameters.Add("@MobileCountry", SqlDbType.NVarChar, 100);
            pMobileCountry.Direction = ParameterDirection.Input;
            pMobileCountry.Value = (mem.MobileCountryCode == null) ? "" : mem.MobileCountryCode;

            SqlParameter pEmail = sqlComm.Parameters.Add("@Email", SqlDbType.NVarChar, 100);
            pEmail.Direction = ParameterDirection.Input;
            pEmail.Value = (mem.NewMemberEmail == null) ? "" : mem.NewMemberEmail; 
                        
            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }


        public static void UpdateMemberFromAdmin(EditMemberModel mem, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateMemberByAdmin", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = mem.Username;

            SqlParameter pFullName = sqlComm.Parameters.Add("@FULLNAME", SqlDbType.NVarChar, 100);
            pFullName.Direction = ParameterDirection.Input;
            pFullName.Value = mem.FullName;

            SqlParameter pIC = sqlComm.Parameters.Add("@IC", SqlDbType.NVarChar, 100);
            pIC.Direction = ParameterDirection.Input;
            pIC.Value = (mem.IdentityNumber == null) ? "" : mem.IdentityNumber;

            SqlParameter pCountry = sqlComm.Parameters.Add("@Country", SqlDbType.NVarChar, 50);
            pCountry.Direction = ParameterDirection.Input;
            pCountry.Value = (mem.SelectedCountry == null) ? "" : mem.SelectedCountry;

            SqlParameter pMobileCountry = sqlComm.Parameters.Add("@MobileCountry", SqlDbType.NVarChar, 50);
            pMobileCountry.Direction = ParameterDirection.Input;
            pMobileCountry.Value = (mem.MobileCountryCode == null) ? "" : mem.MobileCountryCode;

            SqlParameter pCellPhone = sqlComm.Parameters.Add("@HPPHONE", SqlDbType.NVarChar, 50);
            pCellPhone.Direction = ParameterDirection.Input;
            pCellPhone.Value = (mem.CellPhone == null) ? "" : mem.CellPhone;

            SqlParameter pEmail = sqlComm.Parameters.Add("@Email", SqlDbType.NVarChar, 100);
            pEmail.Direction = ParameterDirection.Input;
            pEmail.Value = (mem.MemberEmail == null) ? "" : mem.MemberEmail;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateMemberAutoPlacement(EditMemberModel mem, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateMemberMarketTree", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = mem.Username;

            SqlParameter pPosition = sqlComm.Parameters.Add("@SelectedPlace", SqlDbType.NVarChar, 50);
            pPosition.Direction = ParameterDirection.Input;
            pPosition.Value = mem.SelectedPosition;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }


        #region Get Member Verification
        public static DataSet GetMemberVerification(string member, int selectedPage, out int page, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Admin_GetMemberVerification", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pMember = sqlComm.Parameters.Add("@searchMember", SqlDbType.NVarChar, 50);
            pMember.Direction = ParameterDirection.Input;
            pMember.Value = member;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = selectedPage;

            SqlParameter pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            page = (int)pPage.Value;
            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetHGMMeber(string UserName, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Member_GetHGMMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetMemberVerificationByUserName(string UserName, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Member_GetMemberVerificationByUsername", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUserName = sqlComm.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            pUserName.Direction = ParameterDirection.Input;
            pUserName.Value = UserName;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }
        public static DataSet GetVerifiedMemberAuthentication(string member, string Status, int selectedPage, out int page, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Admin_GetMemberVerifiedAuthenticationRecord", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pMember = sqlComm.Parameters.Add("@searchMember", SqlDbType.NVarChar, 50);
            pMember.Direction = ParameterDirection.Input;
            pMember.Value = member;

            SqlParameter pStatus = sqlComm.Parameters.Add("@Status", SqlDbType.NVarChar, 50);
            pStatus.Direction = ParameterDirection.Input;
            pStatus.Value = Status;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = selectedPage;

            SqlParameter pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            page = (int)pPage.Value;
            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetVerifiedMemberAuthenticationByDate(DateTime datefrom, DateTime dateto, int selectedPage, out int page, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Admin_GetMemberVerifiedAuthenticationRecordByDate", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pdatefrom = sqlComm.Parameters.Add("@DateFrom", SqlDbType.DateTime);
            pdatefrom.Direction = ParameterDirection.Input;
            pdatefrom.Value = datefrom;

            SqlParameter pdateto = sqlComm.Parameters.Add("@DATETO", SqlDbType.DateTime);
            pdateto.Direction = ParameterDirection.Input;
            pdateto.Value = dateto;

            SqlParameter pViewPage = sqlComm.Parameters.Add("@viewPage", SqlDbType.Int);
            pViewPage.Direction = ParameterDirection.Input;
            pViewPage.Value = selectedPage;

            SqlParameter pPage = sqlComm.Parameters.Add("@pages", SqlDbType.Int);
            pPage.Direction = ParameterDirection.Output;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);

            page = (int)pPage.Value;
            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();

            return ds;
        }

        public static DataSet GetVerifiedMemberAuthenticationExport(string member, string Status)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter();

            SqlCommand sqlComm = new SqlCommand("SP_Admin_GetMemberVerifiedAuthenticationRecordExport", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pMember = sqlComm.Parameters.Add("@searchMember", SqlDbType.NVarChar, 50);
            pMember.Direction = ParameterDirection.Input;
            pMember.Value = member;

            SqlParameter pStatus = sqlComm.Parameters.Add("@Status", SqlDbType.NVarChar, 50);
            pStatus.Direction = ParameterDirection.Input;
            pStatus.Value = Status;

            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();

            sqlConn.Open();
            da.Fill(ds);
            sqlConn.Close();

            return ds;
        }

        public static void CreateMemberVerification(EditMemberModel mem, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_CreateMemberVerfification", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = mem.Username;

            SqlParameter pFirstName = sqlComm.Parameters.Add("@FULLNAME", SqlDbType.NVarChar, 50);
            pFirstName.Direction = ParameterDirection.Input;
            pFirstName.Value = mem.FullName;

            SqlParameter pIC = sqlComm.Parameters.Add("@IC", SqlDbType.NVarChar, 50);
            pIC.Direction = ParameterDirection.Input;
            pIC.Value = mem.IdentityNumber;

            SqlParameter pProtrait = sqlComm.Parameters.Add("@Protrait", SqlDbType.NVarChar, 1000);
            pProtrait.Direction = ParameterDirection.Input;
            pProtrait.Value = mem.ProtraitPhotoPath;

            SqlParameter pReverse = sqlComm.Parameters.Add("@Reverse", SqlDbType.NVarChar, 1000);
            pReverse.Direction = ParameterDirection.Input;
            pReverse.Value = mem.ReversePhotoPath;

            SqlParameter pHandled = sqlComm.Parameters.Add("@Handled", SqlDbType.NVarChar, 1000);
            pHandled.Direction = ParameterDirection.Input;
            pHandled.Value = mem.HandheldPhotoPath;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void UpdateMemberVerification(EditMemberModel mem, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdateMemberVerfification", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = mem.Username;

            SqlParameter pFirstName = sqlComm.Parameters.Add("@FULLNAME", SqlDbType.NVarChar, 50);
            pFirstName.Direction = ParameterDirection.Input;
            pFirstName.Value = mem.FullName;

            SqlParameter pIC = sqlComm.Parameters.Add("@IC", SqlDbType.NVarChar, 50);
            pIC.Direction = ParameterDirection.Input;
            pIC.Value = mem.IdentityNumber;

            SqlParameter pProtrait = sqlComm.Parameters.Add("@Protrait", SqlDbType.NVarChar, 1000);
            pProtrait.Direction = ParameterDirection.Input;
            pProtrait.Value = mem.ProtraitPhotoPath;

            SqlParameter pReverse = sqlComm.Parameters.Add("@Reverse", SqlDbType.NVarChar, 1000);
            pReverse.Direction = ParameterDirection.Input;
            pReverse.Value = mem.ReversePhotoPath;

            SqlParameter pHandled = sqlComm.Parameters.Add("@Handled", SqlDbType.NVarChar, 1000);
            pHandled.Direction = ParameterDirection.Input;
            pHandled.Value = mem.HandheldPhotoPath;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void ApproveMemberVerification(string username, int ID, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Admin_ApproveMemberVerification", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.NVarChar, 50);
            pID.Direction = ParameterDirection.Input;
            pID.Value = ID;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 50);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        public static void RejectMemberVerification(string username, int ID, string updatedBy, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_Admin_RejectMemberVerification", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.NVarChar, 50);
            pID.Direction = ParameterDirection.Input;
            pID.Value = ID;

            SqlParameter pUpdatedBy = sqlComm.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar, 50);
            pUpdatedBy.Direction = ParameterDirection.Input;
            pUpdatedBy.Value = updatedBy;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }
        #endregion

        #endregion

        public static void UpdatePreMember(string email, string username, out int ok, out string msg)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_UpdatePreMemberfromMember", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pUsername = sqlComm.Parameters.Add("@USERNAME", SqlDbType.NVarChar, 50);
            pUsername.Direction = ParameterDirection.Input;
            pUsername.Value = username;

            //SqlParameter pFirstName = sqlComm.Parameters.Add("@FIRSTNAME", SqlDbType.NVarChar, 50);
            //pFirstName.Direction = ParameterDirection.Input;
            //pFirstName.Value = mem.Member.FirstName;

            //SqlParameter pLastName = sqlComm.Parameters.Add("@LASTNAME", SqlDbType.NVarChar, 50);
            //pLastName.Direction = ParameterDirection.Input;
            //pLastName.Value = mem.Member.LastName;

            //SqlParameter pFullName = sqlComm.Parameters.Add("@FULLNAME", SqlDbType.NVarChar, 100);
            //pFullName.Direction = ParameterDirection.Input;
            //pFullName.Value = mem.Member.FullName;

            //SqlParameter pUpline = sqlComm.Parameters.Add("@UPLINE", SqlDbType.NVarChar, 50);
            //pUpline.Direction = ParameterDirection.Input;
            //pUpline.Value = mem.Member.Intro;

            //SqlParameter pUplineName = sqlComm.Parameters.Add("@UPLINENAME", SqlDbType.NVarChar, 100);
            //pUplineName.Direction = ParameterDirection.Input;
            //pUplineName.Value = mem.Member.IntroFullName;

            //SqlParameter pRegisteredBy = sqlComm.Parameters.Add("@REGISTEREDBY", SqlDbType.NVarChar, 50);
            //pRegisteredBy.Direction = ParameterDirection.Input;
            //pRegisteredBy.Value = mem.Member.RegisteredBy;

            SqlParameter pEmail = sqlComm.Parameters.Add("@EMAIL", SqlDbType.NVarChar, 100);
            pEmail.Direction = ParameterDirection.Input;
            pEmail.Value = email;

            //SqlParameter pUpdatedby = sqlComm.Parameters.Add("@UPDATEDBY", SqlDbType.NVarChar, 50);
            //pUpdatedby.Direction = ParameterDirection.Input;
            //pUpdatedby.Value = username;

            SqlParameter pOk = sqlComm.Parameters.Add("@ok", SqlDbType.Int);
            pOk.Direction = ParameterDirection.Output;

            SqlParameter pMessage = sqlComm.Parameters.Add("@msg", SqlDbType.VarChar, 50);
            pMessage.Direction = ParameterDirection.Output;

            sqlComm.ExecuteNonQuery();

            ok = (int)pOk.Value;
            msg = pMessage.Value.ToString();
            sqlConn.Close();
        }

        #region "Delete"
        #endregion

    }
}