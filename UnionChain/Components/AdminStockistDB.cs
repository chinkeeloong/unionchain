﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using ECFBase.Models;

namespace ECFBase.Components
{
    public class AdminStockistDB
    {
        public static void ApproveRequestTopUp(string ID, string AdminRemark, string ApproveBy)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_ApproveRequestTopUp", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = ID;

            SqlParameter pAdminRemark = sqlComm.Parameters.Add("@ADMINREMARK", SqlDbType.NVarChar, 50);
            pAdminRemark.Direction = ParameterDirection.Input;
            pAdminRemark.Value = AdminRemark;

            SqlParameter pApproveBy = sqlComm.Parameters.Add("@APPROVEBY", SqlDbType.NVarChar, 50);
            pApproveBy.Direction = ParameterDirection.Input;
            pApproveBy.Value = ApproveBy;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        public static void RejectRequestTopUp(string ID, string AdminRemark, string RejectBy)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_RejectRequestTopUp", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pStockRequestID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pStockRequestID.Direction = ParameterDirection.Input;
            pStockRequestID.Value = ID;

            SqlParameter pAdminRemark = sqlComm.Parameters.Add("@ADMINREMARK", SqlDbType.NVarChar, 50);
            pAdminRemark.Direction = ParameterDirection.Input;
            pAdminRemark.Value = AdminRemark;

            SqlParameter pRejectBy = sqlComm.Parameters.Add("@REJECTBY", SqlDbType.NVarChar, 50);
            pRejectBy.Direction = ParameterDirection.Input;
            pRejectBy.Value = RejectBy;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

        public static void GenerateRequestTopUp(string ID, string ApproveBy)
        {
            SqlConnection sqlConn = DBOperator.GetConnection();
            sqlConn.Open();

            SqlCommand sqlComm = new SqlCommand("SP_GenerateRequestTopUp", sqlConn);
            sqlComm.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = sqlComm.Parameters.Add("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = ID;

            SqlParameter pApproveBy = sqlComm.Parameters.Add("@APPROVEBY", SqlDbType.NVarChar, 50);
            pApproveBy.Direction = ParameterDirection.Input;
            pApproveBy.Value = ApproveBy;

            sqlComm.ExecuteNonQuery();
            sqlConn.Close();
        }

    }
}
