﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Xml;

/// <summary>
/// Connection 的摘要说明
/// </summary>
public class DBOperator
{
    private readonly static string ConfigurationFilePath = @"C:\Config\ConfigurationUnionChain.xml";

    public static SqlConnection GetConnection()
    {
        string localConnectionString = "";
        if (File.Exists(ConfigurationFilePath))
        {
            XmlDocument configDoc = new XmlDocument();
            configDoc.Load(ConfigurationFilePath);
            XmlNode databaseNode = configDoc.SelectSingleNode("/Configurations/DatabaseList/Database[Name='UnionChain']");
            if (databaseNode != null)
            {
                localConnectionString = databaseNode.SelectSingleNode("ConnectionString").InnerText;
            }
        }
        return new SqlConnection(localConnectionString);
    }
    
}
