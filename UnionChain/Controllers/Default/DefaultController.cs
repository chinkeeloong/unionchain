﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECFBase.Controllers.Default
{
    public class DefaultController : Controller
    {
        //
        // GET: /Default/
        public ActionResult Index()
        {
            if (Request.Url.AbsoluteUri.Contains("sys.unionslink.com"))
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }
            else if (Request.Url.AbsoluteUri.Contains("unionslink.com"))
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
            }
            else if (Request.Url.AbsoluteUri.Contains("www.unionslink.com"))
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
            }
            else if (Request.Url.AbsoluteUri.Contains("unionslink.Net"))
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
            }
            else if (Request.Url.AbsoluteUri.Contains("www.unionslink.Net"))
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
            }
            else
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
            }            
        }
    }
}
