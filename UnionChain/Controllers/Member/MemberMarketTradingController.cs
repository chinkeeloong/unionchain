﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Components;
using ECFBase.Models;
using System.Text.RegularExpressions;
using System.Data;
using System.IO;

namespace ECFBase.Controllers.Member
{
    public class MemberMarketTradingController : Controller
    {

        //
        // GET: /MemberMarketTrading/

        #region Dynamic Curve
        public ActionResult DynamicCurve()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            return PartialView("DynamicCurve");

        }

        #endregion

        #region C2C Transaction
        public ActionResult C2CTransaction()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = "";
            ExchangeModel model = new ExchangeModel();

            DataSet DSTradingInformation = MemberDB.GetTradingInformation();
            model.CurrentPrice = DSTradingInformation.Tables[0].Rows[0]["CTR_RATE"].ToString();
            model.LastPrice = DSTradingInformation.Tables[0].Rows[0]["CTR_RATE"].ToString();


            var dsMember = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);            
            model.username = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            model.AvailableUNS = dsMember.Tables[0].Rows[0]["CMEM_TRADEABLEWALLET"].ToString();
            model.FrozenUNS = dsMember.Tables[0].Rows[0]["CMEM_UNTRADEABLEWALLET"].ToString();
            model.TotalValue = (Math.Round(((float.Parse(model.AvailableUNS) + float.Parse(model.FrozenUNS)) * float.Parse(model.CurrentPrice)), 4)).ToString();
            model.ICORPASSWORD = dsMember.Tables[0].Rows[0]["CUSR_ICORPASSPORT"].ToString();
            model.ContactNumber = dsMember.Tables[0].Rows[0]["CUSR_CELLPHONE"].ToString();
            model.Email = dsMember.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
            model.CanSell = dsMember.Tables[0].Rows[0]["CMFS_TRADEABLE_SELLTRADE"].ToString();
            model.CanBuy = dsMember.Tables[0].Rows[0]["CMFS_TRADEABLE_BUYTRADE"].ToString();
            var length = model.ContactNumber.Length;
            if (length > 7)
            {
                var firstDigits = model.ContactNumber.Substring(0, 3);
                var middleDigit = model.ContactNumber.Substring(0, 3);
                var lastDigits = model.ContactNumber.Substring(model.ContactNumber.Length - 4, 4);

                var requiredMask = new String('X', model.ContactNumber.Length - firstDigits.Length - lastDigits.Length);

                var maskedString = string.Concat(firstDigits, requiredMask, lastDigits);
                var maskedCardNumberWithSpaces = Regex.Replace(maskedString, ".{4}", "$0 ");

                ViewBag.ContactNumber = maskedString;
            }


            #region Rate Record
            var dsRateRecord = MemberShareDB.TradingRateRecord();

            foreach (DataRow drs in dsRateRecord.Tables[0].Rows)
            {
                var RateLog = new RateModel();
                RateLog.Rate = string.Format("{0:f4}", double.Parse(drs["CTR_RATE"].ToString()));
                RateLog.Volume = string.Format("{0:f4}", double.Parse(drs["CTR_VOLUME"].ToString()));
                model.RateResultList.Add(RateLog);
            }
            #endregion

            #region Latest Trading Record
            var dsLatestTradingRecord = MemberShareDB.TradingRecord();

            foreach (DataRow drs in dsLatestTradingRecord.Tables[0].Rows)
            {
                var RateLog = new RateModel();
                RateLog.Rate = string.Format("{0:f4}", double.Parse(drs["CTSQ_RATE"].ToString()));
                RateLog.Volume = string.Format("{0:f4}", double.Parse(drs["CTMP_SELLER_VALUE"].ToString()));
                RateLog.Time = drs["CTMP_CREATEDON"].ToString();
                model.TradingResultList.Add(RateLog);
            }
            #endregion

            var dr = MemberProfileDB.GetMemberVerificationByUserName(Session["Username"].ToString(), out ok, out msg);

            var dar = MemberProfileDB.GetHGMMeber(Session["Username"].ToString(), out ok, out msg);

            if (dar.Tables[0].Rows.Count == 0)
            {
                model.HGMMemberFlag = "NO";
            }
            else
            {
                model.HGMMemberFlag = "YES";
            }

            if (dr.Tables[0].Rows.Count == 0)
            {
                model.RealNameVerificationFlag = "NO";
            }
            else
            {
                string Status = dr.Tables[0].Rows[0]["CMEMVER_STATUS"].ToString();

                if (Status != "Approve")
                {
                    model.RealNameVerificationFlag = "NO";
                }
                else
                {
                    model.RealNameVerificationFlag = "YES";
                }
            }



            return PartialView("C2CTransaction", model);

        }

        public ActionResult SellValidation(ExchangeModel model)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = string.Empty;
                DataSet dsUser = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                var dr = MemberProfileDB.GetMemberVerificationByUserName(Session["Username"].ToString(), out ok, out msg);

                if (dr.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.WarningMakeSureRNAareApprovedBTrading);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string Status = dr.Tables[0].Rows[0]["CMEMVER_STATUS"].ToString();

                    if (Status != "Approve")
                    {
                        Response.Write(Resources.Member.WarningMakeSureRNAareApprovedBTrading);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                DataSet dsSellTradingUser = MemberDB.GetMemberSellTradingByUsername(Session["Username"].ToString());

                if (dsSellTradingUser.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.Member.warningOnetimeOneTrading);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);

                }

                DataSet dsBuyTradingUser = MemberDB.GetMemberBuyTradingByUsername(Session["Username"].ToString());

                if (dsBuyTradingUser.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.Member.warningOnetimeOneTrading);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);

                }

                if (dsUser.Tables[0].Rows[0]["CMFS_TRADEABLE_SELLTRADE"].ToString() == "True")
                {
                    Response.Write(Resources.Member.WarningCantSellAtTheMoment);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.SellQuantity == 0)
                {
                    Response.Write(Resources.Member.warningMinSellQuantity);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);                   
                }

                if (string.IsNullOrEmpty(model.SellQuantity.ToString()))
                {                    
                    Response.Write(Resources.Member.warningMinSellQuantity);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);                    
                }


                if (model.SellQuantity % 50 > 0)
                {
                    Response.Write(Resources.Member.warningBuySellQuantityMtpOf50);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.SellQuantity < 50)
                {
                    Response.Write(Resources.Member.warningMinSellQuantity);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var SellingPrice = model.SellQuantity + ( model.SellQuantity * 10 / 100);
                model.TotalSellPrice = (model.SellQuantity * float.Parse(model.CurrentPrice)).ToString();

                if (SellingPrice > decimal.Parse(dsUser.Tables[0].Rows[0]["CMEM_TRADEABLEWALLET"].ToString()))
                {
                    Response.Write(Resources.Member.warningInsufficientTradeableWallet);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
             
                string pin = dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString();
                if (Authentication.Encrypt(model.SecurityPIN) != pin)
                {
                    Response.Write(Resources.Member.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }                

                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult BuyValidation(ExchangeModel model)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }
                int ok = 0;
                string msg = string.Empty;
                DataSet dsUser = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                var dr = MemberProfileDB.GetMemberVerificationByUserName(Session["Username"].ToString(), out ok, out msg);

                if (dr.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.WarningMakeSureRNAareApprovedBTrading);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string Status = dr.Tables[0].Rows[0]["CMEMVER_STATUS"].ToString();

                    if (Status != "Approve")
                    {
                        Response.Write(Resources.Member.WarningMakeSureRNAareApprovedBTrading);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                DataSet dsSellTradingUser = MemberDB.GetMemberSellTradingByUsername(Session["Username"].ToString());

                if (dsSellTradingUser.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.Member.warningOnetimeOneTrading);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);

                }

                DataSet dsBuyTradingUser = MemberDB.GetMemberBuyTradingByUsername(Session["Username"].ToString());

                if (dsBuyTradingUser.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.Member.warningOnetimeOneTrading);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);

                }

                if (dsUser.Tables[0].Rows[0]["CMFS_TRADEABLE_BUYTRADE"].ToString() == "True")
                {
                    Response.Write(Resources.Member.WarningCantBuyAtTheMoment);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.BuyQuantity == 0)
                {
                    Response.Write(Resources.Member.warningMinBuyQuantity);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                

                if (string.IsNullOrEmpty(model.SellQuantity.ToString()))
                {
                    if (string.IsNullOrEmpty(model.BuyQuantity.ToString()))
                    {
                        Response.Write(Resources.Member.warningMinBuyQuantity);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }
                
                if (model.BuyQuantity % 50 > 0)
                {
                    Response.Write(Resources.Member.warningBuySellQuantityMinMtp50);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.BuyQuantity < 50)
                {
                    Response.Write(Resources.Member.warningMinBuyQuantity);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                

                string pin = dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString();
                if (Authentication.Encrypt(model.SecurityPIN) != pin)
                {
                    Response.Write(Resources.Member.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                
                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult MobileDeliver(ExchangeModel model)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }
                int ok = 0;
                string msg = string.Empty;
                DataSet dsUser = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                if (model.SellQuantity == 0)
                {
                    if (model.BuyQuantity == 0)
                    {
                        Response.Write(Resources.Member.warningInvalidQuantity);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                if (string.IsNullOrEmpty(model.SellQuantity.ToString()))
                {
                    if (string.IsNullOrEmpty(model.BuyQuantity.ToString()))
                    {
                        Response.Write(Resources.Member.warningInvalidQuantity);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                if (model.SellQuantity != 0)
                { 

                    if (model.SellQuantity % 50 > 0)
                    {
                        Response.Write(Resources.Member.warningBuySellQuantityMtpOf50);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    if (model.SellQuantity < 50)
                    {
                        Response.Write(Resources.Member.warningMinSellQuantity);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    var SellingPrice = model.SellQuantity + (model.SellQuantity * 10 / 100);
                    model.TotalSellPrice = (model.SellQuantity * float.Parse(model.CurrentPrice)).ToString();

                    if (SellingPrice > decimal.Parse(dsUser.Tables[0].Rows[0]["CMEM_TRADEABLEWALLET"].ToString()))
                    {
                        Response.Write(Resources.Member.warningInsufficientTradeableWallet);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }
                if (model.BuyQuantity != 0)
                {
                    if (model.BuyQuantity % 50 > 0)
                    {
                        Response.Write(Resources.Member.warningBuySellQuantityMinMtp50);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    if (model.BuyQuantity < 50)
                    {
                        Response.Write(Resources.Member.warningMinBuyQuantity);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                string pin = dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString();
                if (Authentication.Encrypt(model.SecurityPIN) != pin)
                {
                    Response.Write(Resources.Member.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }



                var dr = AdminMemberDB.GetMemberByUserName(Session["Username"].ToString(), out ok, out msg);                             

                model.ContactNumber = dr.Tables[0].Rows[0]["CUSR_CELLPHONE"].ToString();
                model.SelectedCountry = dr.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

                #region Send SMS

                try
                {
                    var returnURL = string.Empty;
                    var content = string.Empty;
                    string cellPhone = Misc.PhoneParse(model.SelectedCountry.ToLower(), model.ContactNumber);

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);

                    if (model.SelectedCountry.ToUpper() == "MY")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "RM0.00", Code);
                    }
                    else if (model.SelectedCountry.ToUpper() == "SG")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "ADV", Code);

                    }
                    else if (model.SelectedCountry.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);

                    }

                    int type = 1;

                    if (Session["LanguageChosen"].ToString() == "en-US")
                    {
                        type = 1;
                    }
                    else if (Session["LanguageChosen"].ToString() == "zh-CN")
                    {
                        type = 3;
                        content = System.Text.Encoding.BigEndianUnicode.GetBytes(content).Aggregate("", (agg, val) => agg + val.ToString("X2"));
                    }

                    returnURL = string.Format("http://www.bulksms2u.com/websmsapi/ISendSMS.aspx?username={0}&password={1}&message={2}&mobile={3}&Sender={4}&type={5}", "UnionSMS", "juta1515", content, cellPhone, "68886", type);

                    MemberLoginDB.InsertSMSLog(returnURL, "UnionSMS", "juta1515", cellPhone, model.SelectedCountry.ToUpper());

                    using (var client = new System.Net.WebClient())
                    {
                        client.Encoding = System.Text.Encoding.UTF8;
                        client.UploadString(returnURL, string.Empty);
                    }
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion

                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EmailDeliver(ExchangeModel model)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }
                int ok = 0;
                string msg = string.Empty;
                DataSet dsUser = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                if (model.SellQuantity == 0)
                {
                    if (model.BuyQuantity == 0)
                    {
                        Response.Write(Resources.Member.warningInvalidQuantity);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                if (string.IsNullOrEmpty(model.SellQuantity.ToString()))
                {
                    if (string.IsNullOrEmpty(model.BuyQuantity.ToString()))
                    {
                        Response.Write(Resources.Member.warningInvalidQuantity);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                if (model.SellQuantity != 0)
                {

                    if (model.SellQuantity % 50 > 0)
                    {
                        Response.Write(Resources.Member.warningBuySellQuantityMtpOf50);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    if (model.SellQuantity < 50)
                    {
                        Response.Write(Resources.Member.warningMinSellQuantity);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    var SellingPrice = model.SellQuantity + (model.SellQuantity * 10 / 100);
                    model.TotalSellPrice = (model.SellQuantity * float.Parse(model.CurrentPrice)).ToString();

                    if (SellingPrice > decimal.Parse(dsUser.Tables[0].Rows[0]["CMEM_TRADEABLEWALLET"].ToString()))
                    {
                        Response.Write(Resources.Member.warningInsufficientTradeableWallet);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }
                if (model.BuyQuantity != 0)
                {
                    if (model.BuyQuantity % 50 > 0)
                    {
                        Response.Write(Resources.Member.warningBuySellQuantityMinMtp50);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    if (model.BuyQuantity < 50)
                    {
                        Response.Write(Resources.Member.warningMinBuyQuantity);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                string pin = dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString();
                if (Authentication.Encrypt(model.SecurityPIN) != pin)
                {
                    Response.Write(Resources.Member.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                var dr = AdminMemberDB.GetMemberByUserName(Session["Username"].ToString(), out ok, out msg);

                model.Email = dr.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
                model.SelectedCountry = dr.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

                #region Send Email

                try
                {
                    var Title = string.Empty;
                    var content = string.Empty;

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);


                    if (model.SelectedCountry.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);
                        Title = Resources.Member.lblEmailTitleTaiwan;

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);
                        Title = Resources.Member.lblEmailTitle;

                    }


                    string result = Misc.SendEmail("noreply@UnionsChain.com", model.Email, Title, content);
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion


                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult BuyTrading(ExchangeModel model)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";

                if (string.IsNullOrEmpty(model.VerficationCode))
                {
                    Response.Write(Resources.Member.warningInvalidVerificationCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                DataSet DSValification = MemberLoginDB.ValidVerificationCode(model.VerficationCode);

                if (DSValification.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.warningVerificationCodeExpired);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                DataSet dsSellTradingUser = MemberDB.GetMemberSellTradingByUsername(Session["Username"].ToString());

                if (dsSellTradingUser.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.Member.warningOnetimeOneTrading);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);

                }

                DataSet dsBuyTradingUser = MemberDB.GetMemberBuyTradingByUsername(Session["Username"].ToString());

                if (dsBuyTradingUser.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.Member.warningOnetimeOneTrading);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);

                }


                DataSet dsUser = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                if (dsUser.Tables[0].Rows[0]["CMFS_TRADEABLE_BUYTRADE"].ToString() == "True")
                {
                    Response.Write(Resources.Member.WarningCantBuyAtTheMoment);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.BuyQuantity < 50)
                {
                    Response.Write(Resources.Member.warningBuySellQuantityMinMtp50);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.BuyQuantity % 50 > 0)
                {
                    Response.Write(Resources.Member.warningBuySellQuantityMtpOf50);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var BuyingPrice = model.BuyQuantity * decimal.Parse(model.CurrentPrice);
                model.TotalBuyingPrice = BuyingPrice.ToString();


                var LocalAmount = BuyingPrice * decimal.Parse(dsUser.Tables[0].Rows[0]["CCURRENCY_BUY"].ToString()) ;
                model.TotalBuyingLocalPrice = LocalAmount.ToString();

                string pin = dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString();
                if (Authentication.Encrypt(model.SecurityPIN) != pin)
                {
                    Response.Write(Resources.Member.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }



                MemberMarketTradingDB.BuyTrading(model, out ok, out msg);

                return BuyOrderManagement();

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SellTrading(ExchangeModel model)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";

                if (string.IsNullOrEmpty(model.VerficationCode))
                {
                    Response.Write(Resources.Member.warningInvalidVerificationCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                DataSet DSValification = MemberLoginDB.ValidVerificationCode(model.VerficationCode);

                if (DSValification.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.warningVerificationCodeExpired);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                DataSet dsSellTradingUser = MemberDB.GetMemberSellTradingByUsername(Session["Username"].ToString());

                if (dsSellTradingUser.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.Member.warningOnetimeOneTrading);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);

                }

                DataSet dsBuyTradingUser = MemberDB.GetMemberBuyTradingByUsername(Session["Username"].ToString());

                if (dsBuyTradingUser.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.Member.warningOnetimeOneTrading);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);

                }

                DataSet dsUser = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);


                if (dsUser.Tables[0].Rows[0]["CMFS_TRADEABLE_SELLTRADE"].ToString() == "True")
                {
                    Response.Write(Resources.Member.WarningCantSellAtTheMoment);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                if (model.SellQuantity % 50 > 0)
                {
                    Response.Write(Resources.Member.warningBuySellQuantityMtpOf50);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.SellQuantity < 50)
                {
                    Response.Write(Resources.Member.warningMinSellQuantity);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var SellingPrice = model.SellQuantity + (model.SellQuantity * 10/100);
                model.TotalSellPrice = (model.SellQuantity * decimal.Parse(model.CurrentPrice)).ToString();

                if (SellingPrice > decimal.Parse(dsUser.Tables[0].Rows[0]["CMEM_TRADEABLEWALLET"].ToString()))
                {
                    Response.Write(Resources.Member.warningInsufficientTradeableWallet);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var LocalAmount = decimal.Parse(model.TotalSellPrice) * decimal.Parse(dsUser.Tables[0].Rows[0]["CCURRENCY_BUY"].ToString());
                model.TotalSellLocalPrice = LocalAmount.ToString();

                string pin = dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString();
                if (Authentication.Encrypt(model.SecurityPIN) != pin)
                {
                    Response.Write(Resources.Member.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                MemberMarketTradingDB.SellTrading(model, out ok, out msg);

                return SellOrderManagement();

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Seller
        public ActionResult SellOrderManagement()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = "";


            SellOrderManagement model = new SellOrderManagement();
            var dr = MemberMarketTradingDB.SP_GetSellingOrderMatchQueue(Session["Username"].ToString(), out ok, out msg);
            if (dr.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow drBuy in dr.Tables[0].Rows)
                {
                    var data = new SellOrderManagement();
                    data.ID = Convert.ToInt32(drBuy["CTSQ_ID"].ToString());
                    data.CreatedTime = Convert.ToDateTime(drBuy["CTSQ_CREATEDON"].ToString());
                    data.SellQuantity = Convert.ToInt32(drBuy["CTSQ_ORI_VALUE"]);
                    data.TotalSellingPrice = float.Parse(drBuy["CTSQ_RATE"].ToString()).ToString();
                    data.TotalSellingLocalPrice = float.Parse(drBuy["CTSQ_LOCAL_AMOUNT"].ToString()).ToString();
                    data.PendingQuantity = Convert.ToInt32(drBuy["CTSQ_VALUE"]);
                    data.OrderStatus = Misc.TradingStatus(drBuy["CTSQ_STATUS"].ToString());

                    model.SellOrderList.Add(data);
                }
            }


            return PartialView("SellOrder", model);

        }
        
        public ActionResult SellOrderDetails()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = "";

            SellOrderManagement model = new SellOrderManagement();
            var dr = MemberMarketTradingDB.SP_GetSellingOrderMatchDetails(Session["Username"].ToString(), out ok, out msg);
            if (dr.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow drBuy in dr.Tables[0].Rows)
                {
                    var data = new SellOrderManagement();
                    data.ID = Convert.ToInt32(drBuy["CTMP_ID"].ToString());
                    data.CreatedTime = Convert.ToDateTime(drBuy["CTMP_CREATEDON"].ToString());
                    data.MatchQuantity = Convert.ToInt32(drBuy["CTMP_BUYER_VALUE"]);
                    data.TotalSellingPrice = float.Parse(drBuy["CTMP_SELLER_AMOUNT"].ToString()).ToString();
                    data.TotalSellingLocalPrice = float.Parse(drBuy["CTMP_SELLER_LOCAL_AMOUNT"].ToString()).ToString();
                    data.OrderStatus = Misc.TradingStatus(drBuy["CTMP_STATUS"].ToString());
                    data.PaymentSlipPhotoPath = drBuy["CTMP_BUYER_RECEIPT"].ToString();

                    model.SellOrderList.Add(data);
                }
            }

            return PartialView("SellOrderDetails", model);

        }

        public ActionResult SellOrderDetailsByID(int BySellerTradingID)
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = "";

            SellOrderManagement model = new SellOrderManagement();
            var dr = MemberMarketTradingDB.SP_GetSellingOrderMatchDetailsByTradingID(BySellerTradingID, out ok, out msg);
            if (dr.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow drBuy in dr.Tables[0].Rows)
                {
                    var data = new SellOrderManagement();
                    data.ID = Convert.ToInt32(drBuy["CTMP_ID"].ToString());
                    data.TableID = Convert.ToInt32(drBuy["CTMP_ID"].ToString());
                    data.CreatedTime = Convert.ToDateTime(drBuy["CTMP_CREATEDON"].ToString());
                    data.MatchQuantity = Convert.ToInt32(drBuy["CTMP_BUYER_VALUE"]);
                    data.TotalSellingPrice = float.Parse(drBuy["CTMP_SELLER_AMOUNT"].ToString()).ToString();
                    data.TotalSellingLocalPrice = float.Parse(drBuy["CTMP_SELLER_LOCAL_AMOUNT"].ToString()).ToString();
                    data.OrderStatus = Misc.TradingStatus(drBuy["CTMP_STATUS"].ToString());
                    data.PaymentSlipPhotoPath = drBuy["CTMP_BUYER_RECEIPT"].ToString();

                    model.SellOrderList.Add(data);
                }
            }


            return PartialView("SellOrderDetails", model);

        }

        [HttpPost]
        public ActionResult CancelTradingfromSellOrder(int BySellerTradingID)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }


                var dss = MemberMarketTradingDB.GetSellerPendingMatch(BySellerTradingID);

                if (dss.Tables[0].Rows.Count != 0)
                {
                    Response.Write("Cancel Failed , Still have match havent complete or cancel.");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                MemberMarketTradingDB.CancelTradingfromSellOrderByID(BySellerTradingID);

               
                return SellOrderManagement();

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewBuyerDetails(int BuyTradingID)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                if (BuyTradingID.ToString() == null || BuyTradingID.ToString() == "0")
                {
                    Response.Write("Invalid");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok = 0;
                string msg = "";

                SellOrderManagement model = new SellOrderManagement();
                var dr = MemberMarketTradingDB.SP_GetBuyingOrderMatchDetailsByTableID(BuyTradingID, out ok, out msg);
                if (dr.Tables[0].Rows.Count != 0)
                {
                    foreach (DataRow drBuy in dr.Tables[0].Rows)
                    {
                        var data = new SellOrderManagement();
                        data.ID = Convert.ToInt32(drBuy["CTMP_ID"].ToString());                       
                        data.TotalSellingLocalPrice = float.Parse(drBuy["CTMP_SELLER_LOCAL_AMOUNT"].ToString()).ToString();
                        data.BuyerFullname = drBuy["BUYER_FULLNAME"].ToString();
                        data.BuyerContact = drBuy["BUYER_CELLPHONE"].ToString();
                        data.BuyerEmail = drBuy["BUYER_EMAIL"].ToString();

                        model.SellOrderList.Add(data);
                    }
                }


                return PartialView("ViewBuyerDetails", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApproveTrading(int BuyTradingID)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }
                
                MemberMarketTradingDB.ApproveTradingByID(BuyTradingID);

                return SellOrderDetails();

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Buyer
        public ActionResult BuyOrderManagement()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = "";

            BuyOrderManagement model = new BuyOrderManagement();
            var dr = MemberMarketTradingDB.SP_GetBuyingOrderMatchQueue(Session["Username"].ToString(), out ok, out msg);
            if(dr.Tables[0].Rows.Count != 0)
            {
                foreach(DataRow drBuy in dr.Tables[0].Rows)
                {
                    var data = new BuyOrderManagement();
                    data.ID = Convert.ToInt32(drBuy["CTBQ_ID"].ToString());
                    data.CreatedTime = Convert.ToDateTime(drBuy["CTBQ_CREATEDON"].ToString());
                    data.BuyQuantity = Convert.ToInt32(drBuy["CTBQ_ORI_VALUE"]);
                    data.TotalBuyingPrice = float.Parse(drBuy["CTBQ_RATE"].ToString()).ToString();
                    data.TotalBuyingLocalPrice = float.Parse(drBuy["CTBQ_LOCAL_AMOUNT"].ToString()).ToString();
                    data.PendingQuantity = Convert.ToInt32(drBuy["CTBQ_VALUE"]);
                    data.OrderStatus = Misc.TradingStatus(drBuy["CTBQ_STATUS"].ToString());

                    model.BuyOrderList.Add(data);
                }
            }


            return PartialView("BuyOrder", model);

        }

        public ActionResult BuyOrderDetails()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = "";

            BuyOrderManagement model = new BuyOrderManagement();
            var dr = MemberMarketTradingDB.SP_GetBuyingOrderMatchDetails(Session["Username"].ToString(), out ok, out msg);
            if (dr.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow drBuy in dr.Tables[0].Rows)
                {
                    var data = new BuyOrderManagement();
                    data.ID = Convert.ToInt32(drBuy["CTMP_BUYER_ID"].ToString());
                    data.TableID = Convert.ToInt32(drBuy["CTMP_ID"].ToString());
                    data.CreatedTime = Convert.ToDateTime(drBuy["CTMP_CREATEDON"].ToString());
                    data.MatchQuantity = Convert.ToInt32(drBuy["CTMP_BUYER_VALUE"]);
                    data.TotalBuyingPrice = float.Parse(drBuy["CTMP_SELLER_AMOUNT"].ToString()).ToString();
                    data.TotalBuyingLocalPrice = float.Parse(drBuy["CTMP_SELLER_LOCAL_AMOUNT"].ToString()).ToString();
                    data.OrderStatus = Misc.TradingStatus(drBuy["CTMP_STATUS"].ToString());

                    model.BuyOrderList.Add(data);
                }
            }


            return PartialView("BuyOrderDetails", model);

        }

        public ActionResult ViewPaymentDetails(int BuyTradingID)
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = "";

            BuyOrderManagement model = new BuyOrderManagement();
            var dr = MemberMarketTradingDB.SP_GetBuyingOrderMatchDetailsByTableID(BuyTradingID, out ok, out msg);
            if (dr.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow drBuy in dr.Tables[0].Rows)
                {
                    var data = new BuyOrderManagement();
                    data.ID = Convert.ToInt32(drBuy["CTMP_ID"].ToString());
                    data.TotalBuyingLocalPrice = float.Parse(drBuy["CTMP_SELLER_LOCAL_AMOUNT"].ToString()).ToString();
                    data.SellerFullname = drBuy["CUSR_FULLNAME"].ToString();
                    data.SellerCellPhone = drBuy["CUSR_CELLPHONE"].ToString();
                    data.SellerBranch = drBuy["CMEMBANK_BRANCHNAME"].ToString();
                    data.SellerBank = drBuy["CMULTILANGBANK_NAME"].ToString();
                    data.SellerBankAccount = drBuy["CMEMBANK_ACCOUNTNUMBER"].ToString();

                    model.BuyOrderList.Add(data);
                }
            }


            return PartialView("ViewSellerPaymentDetails", model);

        }

        public ActionResult BuyOrderDetailsByID(int BuyTradingID)
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = "";

            BuyOrderManagement model = new BuyOrderManagement();
            var dr = MemberMarketTradingDB.SP_GetBuyingOrderMatchDetailsByID(BuyTradingID, out ok, out msg);
            if (dr.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow drBuy in dr.Tables[0].Rows)
                {
                    var data = new BuyOrderManagement();
                    data.ID = Convert.ToInt32(drBuy["CTMP_BUYER_ID"].ToString());
                    data.TableID = Convert.ToInt32(drBuy["CTMP_ID"].ToString());
                    data.CreatedTime = Convert.ToDateTime(drBuy["CTMP_CREATEDON"].ToString());
                    data.MatchQuantity = Convert.ToInt32(drBuy["CTMP_BUYER_VALUE"]);
                    data.TotalBuyingPrice = float.Parse(drBuy["CTMP_SELLER_AMOUNT"].ToString()).ToString();
                    data.TotalBuyingLocalPrice = float.Parse(drBuy["CTMP_SELLER_LOCAL_AMOUNT"].ToString()).ToString();
                    data.OrderStatus = Misc.TradingStatus(drBuy["CTMP_STATUS"].ToString());


                    model.BuyOrderList.Add(data);
                }
            }


            return PartialView("BuyOrderDetails", model);

        }

        public ActionResult UploadPayemntSlip(int BuyTradingID)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                if(BuyTradingID.ToString() ==null || BuyTradingID.ToString() == "0")
                {
                    Response.Write("Invalid");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                BuyOrderManagement model = new BuyOrderManagement();
                model.ID = BuyTradingID;


                return PartialView("UploadPaymentSlip", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadPayemntSlipMethod(BuyOrderManagement model)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";
                string languageCode = Session["LanguageChosen"].ToString();

               
               
                #region Check Slip Photo

                if (model.PaymentSlipPhoto != null && model.PaymentSlipPhoto.FileName != string.Empty)
                {
                    if (Misc.IsFileExtensionValid(model.PaymentSlipPhoto.FileName))
                    {
                        string basePath = Server.MapPath("~/Trading/Buyer/" + model.ID);
                        if (Directory.Exists(basePath) == false)
                        {
                            Directory.CreateDirectory(basePath);
                        }
                        var filename = Path.GetFileName(model.PaymentSlipPhoto.FileName);
                        model.PaymentSlipPhotoPath = basePath + "\\" + filename;
                        model.PaymentSlipPhoto.SaveAs(model.PaymentSlipPhotoPath);
                        model.PaymentSlipPhotoPath = "../../Trading/Buyer/" + model.ID + "/" + filename;
                    }
                    else
                    {
                        Response.Write(Resources.Member.msgWrongPaymentSlipExt);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);

                    }
                }
                else if (model.PaymentSlipPhotoPath != null && model.PaymentSlipPhotoPath != "")
                {
                    model.PaymentSlipPhotoPath = model.PaymentSlipPhoto.FileName;
                }
                else
                {
                    Response.Write(Resources.Member.warningUploadPaymentSlip);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion
                              

                //var maxsize = 2 * 1024 * 1024;
                //if (member.ProtraitPhoto.ContentLength > maxsize || member.ReversePhoto.ContentLength > maxsize || member.HandheldPhoto.ContentLength > maxsize)
                //{
                //    Response.Write(Resources.Member.msgMaxFileSize);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}


                MemberMarketTradingDB.UploadPaymentSlip(model, out ok, out msg);

                if (ok == 1)
                {
                    return BuyOrderDetails();
                }
                else
                {
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CancelTradingfromBuyOrder(int BuyTradingID)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";
                
                var dss = MemberMarketTradingDB.GetBuyerPendingMatch(BuyTradingID);

                if (dss.Tables[0].Rows.Count != 0)
                {
                    Response.Write("Cancel Failed , Still have match havent complete or cancel.");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                BuyOrderManagement model = new BuyOrderManagement();
                model.ID = BuyTradingID;


                MemberMarketTradingDB.CancelTradingFromBuyOrderByID(BuyTradingID);
                
                return BuyOrderDetails();


            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult CancelTradingfromBuyOrderDetails(int BuyTradingID)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";

                BuyOrderManagement model = new BuyOrderManagement();
                model.ID = BuyTradingID;
                
                MemberMarketTradingDB.CancelTradingFromBuyOrderDetailsByID(BuyTradingID);


                return BuyOrderDetails();

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Sub Assets Sent
        public ActionResult SubAssetSent()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            return PartialView("SubAssetSent");

        }

        #endregion

        #region Sub Assets Records
        public ActionResult SubAssetRecords()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            return PartialView("SubAssetRecords");

        }

        #endregion

    }
}
