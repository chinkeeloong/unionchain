﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using ECFBase.Components;
using ECFBase.Models;
using System.Web.Services;
using ECFBase.Helpers;
using System.IO;
using System.Text.RegularExpressions;

namespace ECFBase.Controllers.Member
{
    public class MemberWalletController : Controller
    {


        #region Utility

        public ActionResult BillHistory(int selectedPage = 1, string SelectedUtilityList = "0", string SelectedStatusList = "ALL")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true, ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }


                int pages = 0;
                var model = new UtilitySetupModel();

                DataSet dsBillLog = MemberWalletDB.GetBillByUsername(Session["Username"].ToString(), SelectedUtilityList, SelectedStatusList, selectedPage, out pages);

                selectedPage = ConstructPageList(selectedPage, pages, model);
              

                foreach (DataRow dr in dsBillLog.Tables[0].Rows)
                {
                    var billLog = new UtilityModel();
                    billLog.BillID = Convert.ToInt32(dr["CUTI_BILLID"].ToString());
                    billLog.UtilityDescription = dr["CMULTILANGUTILITY_DESCRIPTION"].ToString();
                    billLog.BillAccount = dr["CUTI_BILLACCOUNT"].ToString();
                    billLog.MobileNumber = dr["CUTI_MOBILENUMBER"].ToString();
                    billLog.ProcessingTime = dr["CUTI_PROCESSTIME"].ToString();
                    billLog.BillAmount = float.Parse(dr["CUTI_BILLAMOUNTRM"].ToString()).ToString("n4");
                    billLog.UNSAmount = float.Parse(dr["CUTI_UNSAMOUNT"].ToString()).ToString("n4");
                    billLog.Fees = float.Parse(dr["CUTI_FEES"].ToString()).ToString("n4");
                    billLog.TotalQP = float.Parse(dr["CUTI_TOTALAMOUNT"].ToString()).ToString("n4");
                    billLog.CreatedDate = Convert.ToDateTime(dr["CUTI_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    billLog.Status = Misc.GetUtilityStatus(dr["CUTI_STATUS"].ToString());

                    if (dr["CUTI_STATUS"].ToString() == "0")
                    {
                        billLog.PaymentDate = "";
                    }
                    else
                    {
                        billLog.PaymentDate = Convert.ToDateTime(dr["CUTI_UPDATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    }
                    model.BillList.Add(billLog);
                }
                int ok = 0;
                var msg = string.Empty;
                List<UtilitySetupModel> utilities = Misc.GetAllUtilityList(Session["LanguageChosen"].ToString(), Session["CountryCode"].ToString(), ref ok, ref msg);

                model.UtilityList = from c in utilities
                                    select new SelectListItem
                                    {
                                        Text = c.UtilityName,
                                        Value = Convert.ToString(c.UtilityID),
                                    };

                List<UtilitySetupModel> Status = Misc.GetUtilityStatus();

                model.StatusList = from c in Status
                                   select new SelectListItem
                                   {
                                       Text = c.Status,
                                       Value = Convert.ToString(c.Status),
                                   };


               
                return PartialView("UtilityBillHistory", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CancelUtilityBill(string ID, string ExpiredSession = "")
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true, ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            try
            {

                int ok = 0;
                string msg = "";
                MemberWalletDB.CancelBill(Convert.ToInt32(ID), out ok, out msg);

                if(ok == -1)
                {
                    Response.Write(Resources.Member.WarningUtilityRecordNotFound);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                ViewBag.SessionExpired = ExpiredSession;
                return BillHistory();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UtilityMall(int UtilityID = 0)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            var msg = string.Empty;
            var csm = new UtilitySetupModel();

            #region UtilityBox
            //combobox for Utility
            List<UtilitySetupModel> utilities = Misc.GetAllUtilityList(Session["LanguageChosen"].ToString(), Session["CountryCode"].ToString(), ref ok, ref msg);

            csm.UtilityList = from c in utilities
                              select new SelectListItem
                              {
                                  Text = c.UtilityName,
                                  Value = Convert.ToString(c.UtilityID),
                              };

            #endregion


            // If admin dint add Ulitiy it cant go in that page, because the dropdown box is empty.
            if (csm.UtilityList.Count() == 0)
            {
                Response.Write("No Utility can bill , please contact admin.");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            int totaldays = 0;
            DateTime startdate = DateTime.Now;
            DateTime today = DateTime.Now;
            float CurrentOwn = 0;
            var token = MemberWalletDB.SP_GetHashStatementByUsernameFor300Token(Session["Username"].ToString());
            if(token.Tables[0].Rows.Count!=0)
            {
                startdate = Convert.ToDateTime(token.Tables[0].Rows[0]["CREATEDDATE"].ToString());

                if(startdate != DateTime.Now)
                {
                    TimeSpan difference = today - startdate;
                    totaldays = difference.Days;
                }
           
            }
            if (token.Tables[1].Rows.Count != 0)
            {
                CurrentOwn = float.Parse(token.Tables[1].Rows[0]["WALLETBALANCE"].ToString());
                
            }

            if(totaldays < 30 && CurrentOwn < 300)
            {
                Response.Write(Resources.Member.warningBillPaymentAvailableSuperUser);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            else if (totaldays < 30 && CurrentOwn > 300)
            {
                Response.Write(Resources.Member.warningBillPaymentAvailableSuperUser);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            else if (totaldays > 30 && CurrentOwn < 300)
            {
                Response.Write(Resources.Member.warningMinUNSAvailable);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            DataSet dsBillSetupData = AdminGeneralDB.GetUtilityByID(UtilityID, Session["LanguageChosen"].ToString(), Session["CountryCode"].ToString(), out ok, out msg);
            if (dsBillSetupData.Tables[0].Rows.Count != 0)
            {
                csm.UtilityID = Convert.ToInt32(dsBillSetupData.Tables[0].Rows[0]["CUTI_ID"].ToString());
                csm.UtilityDescription = dsBillSetupData.Tables[0].Rows[0]["CMULTILANGUTILITY_DESCRIPTION"].ToString();
                csm.ProcessingTime = dsBillSetupData.Tables[0].Rows[0]["CUTI_PROCESS"].ToString();
                csm.Mobile = Convert.ToBoolean(dsBillSetupData.Tables[0].Rows[0]["CUTI_MOBILEBILL"].ToString());
                var CurrencyCode = dsBillSetupData.Tables[0].Rows[0]["CCURRENCY_CODE"].ToString();
                csm.MaxAmount = CurrencyCode + dsBillSetupData.Tables[0].Rows[0]["CUTI_MAXAMOUNT"].ToString();
                csm.MinAmount = CurrencyCode + dsBillSetupData.Tables[0].Rows[0]["CUTI_MINAMOUNT"].ToString();
               
                //csm.TotalQP = dsBillSetupData.Tables[0].Rows[0]["CUTI_TOTALQP"].ToString();
                csm.TotalDigit = Convert.ToInt32(dsBillSetupData.Tables[0].Rows[0]["CUTI_DIGIT"].ToString());

                string Code = dsBillSetupData.Tables[0].Rows[0]["CUTI_CODE"].ToString();

                int extensionIndex = dsBillSetupData.Tables[0].Rows[0]["CUTI_IMAGEPATH"].ToString().LastIndexOf('\\');

                string imageName = dsBillSetupData.Tables[0].Rows[0]["CUTI_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                csm.UtilityImagePath = "http://" + Request.Url.Authority + "/Utility/" + Code + "/" + imageName;
                ViewBag.Currency = "( " + dsBillSetupData.Tables[0].Rows[0]["CCURRENCY_CODE"].ToString() + " )";

            }

            csm.SelectedUtilityList = Convert.ToString(UtilityID);

            DataSet DSTradingInformation = MemberDB.GetTradingInformation();
            csm.CurrentPrice = DSTradingInformation.Tables[0].Rows[0]["CTR_RATE"].ToString();

            DataSet dsUser = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            csm.Currency = dsUser.Tables[0].Rows[0]["CCURRENCY_BUY"].ToString();
            csm.AvailableUNS = dsUser.Tables[0].Rows[0]["CMEM_TRADEABLEWALLET"].ToString();


            var dr = MemberProfileDB.GetMemberVerificationByUserName(Session["Username"].ToString(), out ok, out msg);
            if (dr.Tables[0].Rows.Count == 0)
            {
                csm.RealNameVerificationFlag = "NO";
            }
            else
            {
                string Status = dr.Tables[0].Rows[0]["CMEMVER_STATUS"].ToString();

                if (Status != "Approve")
                {
                    csm.RealNameVerificationFlag = "NO";
                }
                else
                {
                    csm.RealNameVerificationFlag = "YES";
                }
            }

            return PartialView("UtilityMall", csm);
        }

        [HttpPost]
        public ActionResult UtilityMallMethod(UtilitySetupModel csm, int BillID = 0)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                if (csm.SelectedUtilityList == null || csm.SelectedUtilityList == "")
                {
                    Response.Write(Resources.Member.warningPlsSelectUtilityType);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (csm.BillAmount == null)
                {
                    Response.Write(Resources.Member.warningKeyInBillAmt);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                if (csm.UtilityDescription == null)
                {
                    csm.UtilityDescription = "";
                }
                if (csm.ProcessingTime == null)
                {
                    csm.ProcessingTime = "";
                }

                if (csm.Mobile == true)
                {
                    if (csm.MobileNumber == null)
                    {
                        Response.Write(Resources.Member.warningPlsKeyInMobileNumber);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    if (csm.MobileNumber == "")
                    {
                        Response.Write(Resources.Member.warningPlsKeyInMobileNumber);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (csm.MobileNumber == null)
                    {
                        csm.MobileNumber = "";
                    }
                }

                if (csm.BillAccount == null)
                {
                    csm.BillAccount = "";
                }
                if(csm.MobileNumber == "" && csm.BillAccount == "")
                {
                    Response.Write(Resources.Member.warningAtLeastAccNumOrMobile);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex phonereg2 = new Regex(@"^[A-Za-z]$");

                if (phonereg2.IsMatch(csm.BillAmount) == true || phonereg2.IsMatch(csm.MobileNumber) == true)
                {
                    Response.Write(Resources.Member.warningNotAllowCharUtility1);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                if (csm.BillAmount.Contains(" ") || csm.BillAccount.Contains(" ") || csm.MobileNumber.Contains(" "))
                {
                    Response.Write(Resources.Member.warningNotAllowSpacingUtility1);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                if (float.Parse(csm.AvailableUNS) < 300)
                {
                    Response.Write(Resources.Member.warningMinUNSAvailable);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (float.Parse(csm.BillAmount) <= 0)
                {
                    Response.Write(Resources.Member.warningKeyInBillAmt);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }




                int ok = 0;
                string msg = string.Empty;
                float QP = 0;
                var ds = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                QP = float.Parse(ds.Tables[0].Rows[0]["CMEM_TRADEABLEWALLET"].ToString());

                if (QP < float.Parse(csm.TotalQP))
                {
                    Response.Write(Resources.Member.warningNotEnoughtUNS);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                DataSet dsBillSetupData = MemberWalletDB.GetBillByID(BillID, out ok, out msg);

                var dsUtilityChecking = MemberWalletDB.UtilityChecking(csm.SelectedUtilityList);

                float Min = 0;
                float Max = 0;
                string CurrencyCode = string.Empty;

                if (dsUtilityChecking.Tables[0].Rows.Count != 0)
                {
                    Min = float.Parse(dsUtilityChecking.Tables[0].Rows[0]["CUTI_MINAMOUNT"].ToString());
                    Max = float.Parse(dsUtilityChecking.Tables[0].Rows[0]["CUTI_MAXAMOUNT"].ToString());
                    CurrencyCode = dsUtilityChecking.Tables[0].Rows[0]["CCURRENCY_CODE"].ToString();
                }

                if (float.Parse(csm.BillAmount) < Min)
                {
                    Response.Write(Resources.Member.warningAlertMinUtilityAmt + CurrencyCode + Min);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (float.Parse(csm.BillAmount) > Max)
                {
                    Response.Write(Resources.Member.warningAlertMaxUtilityAmt + CurrencyCode + Max);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int pages;
                DataSet dsBillLog = MemberWalletDB.GetBillByUsername(Session["Username"].ToString(), csm.SelectedUtilityList, "All", 1, out pages);
                if(dsBillLog.Tables[0].Rows.Count != 0)
                {
                    foreach(DataRow dr in dsBillLog.Tables[0].Rows)
                    {
                        var checking = new UtilitySetupModel();
                        checking.Status = dr["CUTI_STATUS"].ToString();
                        checking.CreatedDate = Convert.ToDateTime(dr["CUTI_CREATEDON"]).ToString("MMM");

                        if (checking.Status == "Pending" || checking.Status == "Approve")
                        {                          
                            var CurrentMonth = DateTime.Now.ToString("MMM");
                            if(checking.CreatedDate == CurrentMonth)
                            {
                                Response.Write(Resources.Member.warningSameMonthSameBillType);
                                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
              


                if (dsBillSetupData.Tables[0].Rows.Count == 0)
                {
                    MemberWalletDB.CreateBill(csm, Session["Username"].ToString(), Session["Username"].ToString(), out ok, out msg);
                }
                else
                {
                   // MemberWalletDB.UpdateBill(csm, out ok, out msg);
                }

                return BillHistory();
                //return Json(string.Empty, JsonRequestBehavior.AllowGet);

                
            }
            catch (Exception e)
            {
                Response.Write("1|" + e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


        }

        


        private int ConstructPageList(int selectedPage, int pages, UtilitySetupModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 0; z < pages; z++)
            {
                pageList.Add(z);
            }

            if (pages == 0)
            {
                pageList.Add(0);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = (c + 1).ToString(),
                              Value = c.ToString()
                          };

            //if the selected page is -1, then set the first selected page
            if (model.Pages.Count() != 0 && selectedPage == -1)
            {
                model.Pages.First().Selected = true;
                selectedPage = int.Parse(model.Pages.First().Value);
            }
            return selectedPage;
        }


        #endregion 

        #region HashStatement
        //[HttpPost]
        //public ActionResult WalletForce(string type ="")
        //{

        //    if (Session["Username"] == null || Session["Username"].ToString() == "")
        //    {
        //        return RedirectToAction("MemberLogin", "Member", new { reloadPage = true, ExpiredSession = "Yes", FromLoginPage = "No" });
        //    }
        //    int ok = 0;
        //    string msg = "";

        //    HashWalletModel model = new HashWalletModel();

        //    var dsWallet = MemberWalletDB.GetHashStatementByUsername(Session["Username"].ToString(), out ok, out msg);
        //    if (dsWallet.Tables[0].Rows.Count != 0)
        //    {
        //        if (type == "OUT")
        //        {
        //            foreach (DataRow dr in dsWallet.Tables[0].Rows)
        //            {
        //                if (float.Parse(dr["CASHOUT"].ToString()) != 0)
        //                {
        //                    var records = new HashWalletModel();
        //                    records.Number = Convert.ToInt32(dr["rownumber"].ToString());
        //                    records.Username = dr["CUSR_USERNAME"].ToString();
        //                    records.CashIN = dr["CASHOUT"].ToString();
        //                    records.TransactionType = "-";
        //                    records.Remarks = Misc.GetReadableCashName(dr["UNIONTYPE"].ToString()) + " : " + dr["WALLETBALANCE"].ToString();
        //                    records.CreatedDate = Convert.ToDateTime(dr["CREATEDDATE"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt");

        //                    if(dr["CASHNAME"].ToString() == "Utility Bill")
        //                    {
        //                        records.UnionType = "Bill " + dr["BILLNAME"].ToString() + " " + dr["BILLSTATUS"].ToString();
        //                    }
        //                    else
        //                    {
        //                        records.UnionType = Misc.GetReadableCashName(dr["CASHNAME"].ToString());
        //                    }
                            
        //                    model.WalletLogList.Add(records);
        //                }
        //            }
        //        }
        //        else if (type == "IN")
        //        {
        //            foreach (DataRow dr in dsWallet.Tables[0].Rows)
        //            {
        //                if (float.Parse(dr["CASHIN"].ToString()) != 0)
        //                {
        //                    var records = new HashWalletModel();
        //                    records.Number = Convert.ToInt32(dr["rownumber"].ToString());
        //                    records.Username = dr["CUSR_USERNAME"].ToString();
        //                    records.CashIN = dr["CASHIN"].ToString();
        //                    //records.CashOUT = float.Parse(dr["CASHOUT"].ToString());
        //                    records.TransactionType = "+";
        //                    records.Remarks = Misc.GetReadableCashName(dr["UNIONTYPE"].ToString())+ " : " + dr["WALLETBALANCE"].ToString();
        //                    records.CreatedDate = Convert.ToDateTime(dr["CREATEDDATE"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt");

        //                    if (dr["CASHNAME"].ToString() == "Utility Bill")
        //                    {
        //                        records.UnionType = "Bill " + dr["BILLNAME"].ToString() + " " + dr["BILLSTATUS"].ToString();
        //                    }
        //                    else
        //                    {
        //                        records.UnionType = Misc.GetReadableCashName(dr["CASHNAME"].ToString());
        //                    }
        //                    model.WalletLogList.Add(records);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            foreach (DataRow dr in dsWallet.Tables[0].Rows)
        //            {
        //                var records = new HashWalletModel();
        //                records.Number = Convert.ToInt32(dr["rownumber"].ToString());
        //                records.Username = dr["CUSR_USERNAME"].ToString();
                        
        //                if (float.Parse(dr["CASHIN"].ToString()) != 0)
        //                {
        //                    records.TransactionType = "+";
        //                    records.CashIN = dr["CASHIN"].ToString();
        //                }
        //                else if (float.Parse(dr["CASHOUT"].ToString()) != 0)
        //                {
        //                    records.TransactionType = "-";
        //                    records.CashIN = dr["CASHOUT"].ToString();
        //                }
        //                //records.CashOUT = float.Parse(dr["CASHOUT"].ToString());
        //                records.Remarks = Misc.GetReadableCashName(dr["UNIONTYPE"].ToString()) + " : " + dr["WALLETBALANCE"].ToString();
        //                records.CreatedDate = Convert.ToDateTime(dr["CREATEDDATE"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt");

        //                if (dr["CASHNAME"].ToString() == "Utility Bill")
        //                {
        //                    records.UnionType = "Bill " + dr["BILLNAME"].ToString() + " " + dr["BILLSTATUS"].ToString();
        //                }
        //                else
        //                {
        //                    records.UnionType = Misc.GetReadableCashName(dr["CASHNAME"].ToString());
        //                }
        //                model.WalletLogList.Add(records);

        //            }
        //        }
        //    }


        //    return PartialView("WalletForce", model);

        //}

        [HttpPost]
        public ActionResult WalletForce(int selectedPage = -1 , string Type = "")
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int pages = 0;
                HashWalletModel model = new HashWalletModel();

                var dsWallet = MemberWalletDB.GetHashStatementByUsername(Session["Username"].ToString(), Type, selectedPage, out pages);

                selectedPage = ConstructPageList(selectedPage, pages, model);


                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                        var records = new HashWalletModel();
                        records.Number = Convert.ToInt32(dr["rownumber"].ToString());
                        records.Username = dr["CUSR_USERNAME"].ToString();

                        if (float.Parse(dr["CASHIN"].ToString()) != 0)
                        {
                            records.TransactionType = "+";
                            records.CashIN = dr["CASHIN"].ToString();
                        }
                        else if (float.Parse(dr["CASHOUT"].ToString()) != 0)
                        {
                            records.TransactionType = "-";
                            records.CashIN = dr["CASHOUT"].ToString();
                        }
                        //records.CashOUT = float.Parse(dr["CASHOUT"].ToString());
                        records.Remarks = Misc.GetReadableCashName(dr["UNIONTYPE"].ToString()) + " : " + dr["WALLETBALANCE"].ToString();
                        records.CreatedDate = Convert.ToDateTime(dr["CREATEDDATE"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt");
                        records.UnionType = Misc.GetReadableCashName(dr["CASHNAME"].ToString());
                        model.WalletLogList.Add(records);
                    
                }

                model.UnionType = Type;

                return PartialView("WalletForce", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        private int ConstructPageList(int selectedPage, int pages, HashWalletModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 0; z < pages; z++)
            {
                pageList.Add(z);
            }

            if (pages == 0)
            {
                pageList.Add(0);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = (c + 1).ToString(),
                              Value = c.ToString()
                          };

            //if the selected page is -1, then set the first selected page
            if (model.Pages.Count() != 0 && selectedPage == -1)
            {
                model.Pages.First().Selected = true;
                selectedPage = int.Parse(model.Pages.First().Value);
            }
            return selectedPage;
        }
        #endregion

        #region U Po Send
        public ActionResult WalletSend()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }


            int ok = 0;
            string msg = "";
            DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

            DataRow memberRow = member.Tables[0].Rows[0];

            var model = new TransferWalletModel { Charge = 0 };

            model.Wallet = memberRow["CMEM_TRADEABLEWALLET"].ToString();

            model.CanTransfer = memberRow["CMFS_TRADEABLE_TRANSFER"].ToString();

            model.PhoneNumber = memberRow["CUSR_CELLPHONE"].ToString();

            string HP = model.PhoneNumber.Remove(3, 4);

            HP = HP.Insert(3, "****");

            model.PhoneNumber = HP;


            return PartialView("WalletSend",model);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult WalletSendMethod(string WalletAddress, string Amount, string PIN,  string Remark)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }


                int ok = 0;
                string msg = string.Empty;
                string Username = string.Empty;

                var dr = MemberProfileDB.GetMemberVerificationByUserName(Session["Username"].ToString(), out ok, out msg);

                if (dr.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.WarningMakeSureRNAareApprovedBExc);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string Status = dr.Tables[0].Rows[0]["CMEMVER_STATUS"].ToString();

                    if (Status != "Approve")
                    {
                        Response.Write(Resources.Member.WarningMakeSureRNAareApprovedBExc);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }


                float transferAmount = 0;
                float.TryParse(Amount, out transferAmount);

                var dsMember = MemberDB.GetMemberByWalletAddress(WalletAddress);

                var dsOwnAcc = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                if (float.Parse(Amount) > float.Parse(dsOwnAcc.Tables[0].Rows[0]["CMEM_TRADEABLEWALLET"].ToString()))
                {
                    Response.Write(Resources.Member.WarningInvalidAmount);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string testing = dsOwnAcc.Tables[0].Rows[0]["CMFS_TRADEABLE_TRANSFER"].ToString();

                if (dsOwnAcc.Tables[0].Rows[0]["CMFS_TRADEABLE_TRANSFER"].ToString() == "True")
                {
                    Response.Write(Resources.Member.WarningCantTransfer);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (dsMember.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.WarningInvalidAddress);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Username = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();

                if (Username.ToUpper() == Session["Username"].ToString().ToUpper())
                {
                    Response.Write(Resources.Member.WarningCantTransferToOwnAcc);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }



                string DBPin = dsOwnAcc.Tables[0].Rows[0]["CUSR_PIN"].ToString();
                
                PIN = Authentication.Encrypt(PIN);

                if (DBPin != PIN)
                {
                    Response.Write(Resources.Member.WarningInvalidTransfactionPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //DataSet DSValification = MemberLoginDB.ValidVerificationCode(VerificationCode);

                //if (DSValification.Tables[0].Rows.Count == 0)
                //{
                //    Response.Write("Verification Code Expired, Please Try Again");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}
                
                if (transferAmount <= 0)
                {
                    Response.Write(Resources.Member.WarningInvalidAmount);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (transferAmount < 50)
                {
                    Response.Write(Resources.Member.warningSendQuantityMinMtp50);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (transferAmount % 50 > 0)
                {
                    Response.Write(Resources.Member.warningSendQuantityMinMtp50);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                MemberWalletDB.WalletSend(Username, transferAmount, PIN, Session["Username"].ToString(), Remark, out ok, out msg);


                return WalletForce();
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Wallet Option
        public ActionResult WalletOption()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            return PartialView("WalletOption");

        }

        #endregion

        #region Option Transfer Out
        public ActionResult TransferOut()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            return PartialView("TransferOut");

        }

        #endregion

        public ActionResult MobileDeliver()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);


                var dr = MemberProfileDB.GetMemberVerificationByUserName(Session["Username"].ToString(), out ok, out msg);

                if (dr.Tables[0].Rows.Count == 0)
                {
                    Response.Write("Please Make Sure Real Name Authentication Are Approve Before Exchange");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string Status = dr.Tables[0].Rows[0]["CMEMVER_STATUS"].ToString();

                    if (Status != "Approve")
                    {
                        Response.Write("Please Make Sure Real Name Authentication Are Approve Before Exchange");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                string Country = member.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                string Phone = member.Tables[0].Rows[0]["CUSR_CELLPHONE"].ToString();

                #region Send SMS

                try
                {
                    var returnURL = string.Empty;
                    var content = string.Empty;
                    string cellPhone = Misc.PhoneParse(Country.ToLower(), Phone);

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);

                    if (Country.ToUpper() == "MY")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "RM0.00", Code);
                    }
                    else if (Country.ToUpper() == "SG")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "ADV", Code);

                    }
                    else if (Country.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);

                    }

                    int type = 1;

                    if (Session["LanguageChosen"].ToString() == "en-US")
                    {
                        type = 1;
                    }
                    else if (Session["LanguageChosen"].ToString() == "zh-CN")
                    {
                        type = 3;
                        content = System.Text.Encoding.BigEndianUnicode.GetBytes(content).Aggregate("", (agg, val) => agg + val.ToString("X2"));
                    }


                    returnURL = string.Format("http://www.bulksms2u.com/websmsapi/ISendSMS.aspx?username={0}&password={1}&message={2}&mobile={3}&Sender={4}&type={5}", "UnionSMS", "juta1515", content, cellPhone, "68886", type);

                    MemberLoginDB.InsertSMSLog(returnURL, "UnionSMS", "juta1515", cellPhone, Country.ToUpper());

                    using (var client = new System.Net.WebClient())
                    {
                        client.Encoding = System.Text.Encoding.UTF8;
                        client.UploadString(returnURL, string.Empty);
                    }
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion

                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EmailDeliver()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);


                var dr = MemberProfileDB.GetMemberVerificationByUserName(Session["Username"].ToString(), out ok, out msg);

                if (dr.Tables[0].Rows.Count == 0)
                {
                    Response.Write("Please Make Sure Real Name Authentication Are Approve Before Exchange");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string Status = dr.Tables[0].Rows[0]["CMEMVER_STATUS"].ToString();

                    if (Status != "Approve")
                    {
                        Response.Write("Please Make Sure Real Name Authentication Are Approve Before Exchange");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }




                

                string Country = member.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                string Email = member.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();

                #region Send Email

                try
                {
                    var Title = string.Empty;
                    var content = string.Empty;

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);


                    if (Country.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);
                        Title = Resources.Member.lblEmailTitleTaiwan;

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);
                        Title = Resources.Member.lblEmailTitle;

                    }


                    string result = Misc.SendEmail("noreply@UnionsChain.com", Email, Title, content);
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion


                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
