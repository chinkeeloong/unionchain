﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using ECFBase.Helpers;
using System.Text.RegularExpressions;

namespace ECFBase.Controllers.Member
{
    public class MemberProfileController : Controller
    {

        #region Edit Profile

        [HttpPost]
        public JsonResult FindProvince(string CountryCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetProvinceList(LanguageCode, CountryCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetProvinceList(string LanguageCode, string CountryCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<ProvinceModel> provinces = Misc.GetAllProvinceByCountry(LanguageCode, CountryCode, ref ok, ref message);

            var result = from c in provinces
                         select new SelectListItem
                         {
                             Selected = false,
                             Text = c.ProvinceName,
                             Value = c.ProvinceCode
                         };
            return result.OrderBy(m => m.Value).ToList();
        }

        [HttpPost]
        public JsonResult CheckFileExtension(string file)
        {
            var isValid = Misc.IsFileExtensionValid(file);

            return Json(isValid.ToString(), JsonRequestBehavior.AllowGet);
        }

        #endregion
        
        #region Bank Account Info

        [HttpPost]
        public JsonResult FindBank(string CountryCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetBankList(LanguageCode, CountryCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetBankList(string LanguageCode, string CountryCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<BankModel> banks = Misc.GetAllBankByCountry(LanguageCode, CountryCode, ref ok, ref message);

            var result = from c in banks
                         select new SelectListItem
                         {
                             Selected = false,
                             Text = c.BankName,
                             Value = c.BankCode
                         };
            return result.OrderBy(m => m.Value).ToList();
        }

        public ActionResult BankAccInfo()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            int ok = 0;
            string msg = "";
            string language = Session["LanguageChosen"].ToString();

            MemberBankModel model = new MemberBankModel();
            model.Username = Session["Username"].ToString();

            //combobox for country
            List<CountrySetupModel> countries = Misc.GetAllCountryList(language, ref ok, ref msg);
            model.CountryList = from c in countries
                                select new SelectListItem
                                {
                                    Selected = false,
                                    Text = c.CountryName,
                                    Value = c.CountryCode
                                };

            DataSet dsMemberBank = MemberProfileDB.GetMemberBankByUsername(model.Username, out ok, out msg);

            DataSet dsmember = AdminMemberDB.GetMemberByUserName(model.Username, out ok, out msg);

            if (dsMemberBank.Tables[0].Rows.Count == 0)
            {
                string selectedcountry = dsmember.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                model.SelectedCountry = dsmember.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                model.SelectedCountryStraing = countries.Find(item => item.CountryCode == selectedcountry).CountryName;    

                //combobox for bank
                //string selectedcountry = dsMemberBank.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                //model.SelectedCountryStraing = countries.First().CountryName;                  
                // model.SelectedCountry = dsMemberBank.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                string countryCode = model.SelectedCountry;
                //model.MemberBankID = Convert.ToInt32(dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ID"]);
                List<BankModel> banks = Misc.GetAllBankByCountry("en-US", model.SelectedCountry, ref ok, ref msg);
                model.BankList = from c in banks
                                 select new SelectListItem
                                 {
                                     Selected = false,
                                     Text = c.BankName,
                                     Value = c.BankCode
                                 };
                model.SelectedBank = "null";
                model.MemberBankID = 0;
                model.BranchName = "";
                model.AccountNumber = "";
                model.BeneficiaryIC = dsmember.Tables[0].Rows[0]["CUSRINFO_IC"].ToString();
                model.BeneficiaryName = dsmember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                model.EnableEditAll = true;
            }
            else
            {
                string selectedcountry = dsMemberBank.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                model.SelectedCountry = dsMemberBank.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                model.SelectedCountryStraing = countries.Find(item => item.CountryCode == selectedcountry).CountryName;               
                model.MemberBankID = Convert.ToInt32(dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ID"]);
               // model.SelectedCountry = dsMemberBank.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                model.SelectedBank = dsMemberBank.Tables[0].Rows[0]["CBANK_CODE"].ToString();

                //combobox for bank
                List<BankModel> banks = Misc.GetAllBankByCountry("en-us", model.SelectedCountry, ref ok, ref msg);
                model.BankList = from c in banks
                                 select new SelectListItem
                                 {
                                     Selected = c.BankCode == model.SelectedBank ? true : false,
                                     Text = c.BankName,
                                     Value = c.BankCode
                                 };

                model.Username = dsMemberBank.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                model.BranchName = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_BRANCHNAME"].ToString();
                model.AccountNumber = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ACCOUNTNUMBER"].ToString();
                model.SelectedBank = dsMemberBank.Tables[0].Rows[0]["CBANK_CODE"].ToString();
                model.BeneficiaryName = dsmember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                model.BeneficiaryIC = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_BENEFICIARYIC"].ToString();
                model.BeneficiaryPhone = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_BENEFICIARYPHONE"].ToString();
                model.BeneficiaryRelationship = dsmember.Tables[0].Rows[0]["CUSRINFO_RELATIONSHIP"].ToString();

                if (model.BranchName == "" || model.AccountNumber == "" || model.BeneficiaryName == "")
                {
                    model.EnableEdit = true;
                }
            }

            return PartialView("BankAccInfo", model);
        }

        [HttpPost]
        public ActionResult BankAccInfoMethod(MemberBankModel mbm)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (mbm.BranchName == null)
                {
                    Response.Write(Resources.Member.warningPlsEnterBranchName);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mbm.AccountNumber == null)
                {
                    Response.Write(Resources.Member.warningPlsEnterAccNumber);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mbm.BeneficiaryName == null)
                {
                    Response.Write(Resources.Member.warningPlsEnterAccName);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mbm.BeneficiaryIC == null)
                {
                    Response.Write(Resources.Member.warningPlsEnterNRIC);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mbm.SelectedBank == "null")
                {
                    Response.Write(Resources.Member.warningPlsSelectBank);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                int ok;
                string msg;
                DataSet dsMemberBank = MemberProfileDB.GetMemberBankByUsername(mbm.Username, out ok, out msg);
                if (dsMemberBank.Tables[0].Rows.Count == 0)
                {
                    //Create New Record
    
                        MemberProfileDB.CreateNewMemberBank(mbm.SelectedCountry, mbm.SelectedBank, mbm.Username, Helper.NVL(mbm.BranchName), Helper.NVL(mbm.AccountNumber), Helper.NVL(mbm.BeneficiaryName), Helper.NVL(mbm.BeneficiaryIC), Session["Username"].ToString(), Session["Username"].ToString(), out ok, out msg);
                }
                else
                {
                    //Update Existing Record

                        MemberProfileDB.UpdateMemberBank(mbm.SelectedCountry, mbm.SelectedBank, mbm.Username, Helper.NVL(mbm.BranchName), Helper.NVL(mbm.AccountNumber), Helper.NVL(mbm.BeneficiaryName), Helper.NVL(mbm.BeneficiaryIC), Helper.NVL(mbm.BeneficiaryPhone), Helper.NVL(mbm.BeneficiaryRelationship), Helper.NVL(mbm.Username), out ok, out msg);
                }

                return BankAccInfo();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        
        #region Data Settings
        public ActionResult DataSettings()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = "";

            EditMemberModel member = new EditMemberModel();
            string languageCode = Session["LanguageChosen"].ToString();
            string selectedcountry = Session["CountryCode"].ToString();

            var dsMember = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            member.FullName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            member.Username = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            member.CellPhone = dsMember.Tables[0].Rows[0]["CUSR_CELLPHONE"].ToString();
            member.MemberEmail = dsMember.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
            member.CurrPassword = dsMember.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString();
            
            #region Country

            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

            member.MobileCountries = from c in countries
                              select new SelectListItem
                              {
                                  Text = c.CountryName + " (+" + c.MobileCode + ")",
                                  Value = c.MobileCode
                              };

            member.SelectedCountry = selectedcountry;
            DataSet dsCountrySetup = AdminGeneralDB.GetCountryByCountryCode(selectedcountry, out ok, out msg);

            member.MobileCountryCode = dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_MOBILECODE"].ToString();


            #endregion

            #region Position
            member.PositionList.Clear();

            SelectListItem loc = new SelectListItem();
            loc.Text = ECFBase.Resources.Member.lblSelected;
            loc.Value = "";
            member.PositionList.Add(loc);

            loc = new SelectListItem();
            loc.Text = ECFBase.Resources.Member.lblPositionA;
            loc.Value = "A";
            member.PositionList.Add(loc);

            loc = new SelectListItem();
            loc.Text = ECFBase.Resources.Member.lblPositionB;
            loc.Value = "B";
            member.PositionList.Add(loc);

            member.SelectedPosition = dsMember.Tables[0].Rows[0]["CMTREE_AUTO_PLACEMENT"].ToString();
            #endregion

            return PartialView("DataSettings",member);

        }

        public ActionResult UpdateCellPhone(EditMemberModel member)
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            if (string.IsNullOrEmpty(member.VerficationCode))
            {
                Response.Write("Invalid Verification Code");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            DataSet DSValification = MemberLoginDB.ValidVerificationCode(member.VerficationCode);

            if (DSValification.Tables[0].Rows.Count == 0)
            {
                Response.Write("Verification Code Expired, Please Try Again");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(member.MobileCountryCode))
            {
                Response.Write(Resources.Member.warningPleaseSelectPhoneCode);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            Regex phonereg = new Regex(@"^([a-zA-Z])$");

            if (string.IsNullOrEmpty(member.NewCellPhone))
            {
                Response.Write(Resources.Member.warningPleaseKeyInPhoneNumber);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (phonereg.IsMatch(member.NewCellPhone) == true)
            {
                Response.Write(Resources.Member.warningPhoneNotAllowChar);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            int ok = 0;
            string msg = "";
            MemberProfileDB.UpdateMember(member, out ok, out msg);


            Session["page"] = "EditProfile";
            ViewBag.page = "EditProfile";
            return DataSettings();

        }

        public ActionResult UpdateLoginPassword(EditMemberModel member)
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }


            if (member.NewPassword == null)
            {
                Response.Write(Resources.Member.msgReqNewPwd);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (member.RetypeNewPassword == null)
            {
                Response.Write(Resources.Member.msgReqNewPwd);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (member.RetypeNewPassword != member.NewPassword)
            {
                Response.Write(Resources.Member.msgPwdNotMatch);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            int ok = 0; string msg = "";

            DataSet dsUser = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            string userPassword = dsUser.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString();
            if (Authentication.Encrypt(member.CurrPassword) != userPassword)
            {
                Response.Write(Resources.Member.msgInvalidCurrPwd);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            MemberProfileDB.ChangePassword(member.Username, Authentication.Encrypt(member.CurrPassword), Authentication.Encrypt(member.NewPassword), member.Username, out ok, out msg);
            if (ok == -1)
            {
                Response.Write(Resources.Member.msgInvalidCurrPwd);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            else if (ok == -2)
            {
                Response.Write(Resources.Member.msgPasswordAndPinMustDifferent);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            Session["page"] = "EditProfile";
            ViewBag.page = "EditProfile";
            return DataSettings();

        }

        public ActionResult UpdateSecurityPin(EditMemberModel member)
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = "";

            DataSet dsUser = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            string userPin = dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString();
            if (Authentication.Encrypt(member.CurrPayPassword) != userPin)
            {
                Response.Write(Resources.Member.msgInvalidCurrPIN);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (member.NewPayPassword != member.RetypePayPassword)
            {
                Response.Write(Resources.Member.msgPINNotMatch);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            if (member.NewPayPassword == null)
            {
                Response.Write(Resources.Member.msgReqPIN);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            if (member.RetypePayPassword == null)
            {
                Response.Write(Resources.Member.warningConfirmNewPIN);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            MemberProfileDB.ChangePIN(member.Username, Authentication.Encrypt(member.CurrPayPassword), Authentication.Encrypt(member.NewPayPassword), member.Username, out ok, out msg);
            if (ok == -1)
            {
                Response.Write(Resources.Member.msgInvalidCurrPIN);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            Session["page"] = "EditProfile";
            ViewBag.page = "EditProfile";
            return RedirectToAction("DataSettings", "MemberProfile");

        }

        public ActionResult UpdateEmail(EditMemberModel member)
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }


            if (member.NewMemberEmail == null)
            {
                Response.Write(Resources.Member.msgReqEmail);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            Regex reg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

            if (reg.IsMatch(member.NewMemberEmail) == false)
            {
                Response.Write(Resources.Member.msgEmailInvalid);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            int ok = 0;
            string msg = "";
            MemberProfileDB.UpdateMember(member, out ok, out msg);

            Session["page"] = "EditProfile";
            ViewBag.page = "EditProfile";
            return DataSettings();

        }

        public ActionResult UpdateAutoPlacement(EditMemberModel member)
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            if (string.IsNullOrEmpty(member.SelectedPosition))
            {
                Response.Write(Resources.Member.warningSelectPosition);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            int ok = 0;
            string msg = "";
            MemberProfileDB.UpdateMemberAutoPlacement(member, out ok, out msg);

            Session["page"] = "EditProfile";
            ViewBag.page = "EditProfile";
            return DataSettings();

        }

        public ActionResult MobileDeliver()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);


                var dr = MemberProfileDB.GetMemberVerificationByUserName(Session["Username"].ToString(), out ok, out msg);

                if (dr.Tables[0].Rows.Count == 0)
                {
                    Response.Write("Please Make Sure Real Name Authentication Are Approve Before Exchange");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string Status = dr.Tables[0].Rows[0]["CMEMVER_STATUS"].ToString();

                    if (Status != "Approve")
                    {
                        Response.Write("Please Make Sure Real Name Authentication Are Approve Before Exchange");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                string Country = member.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                string Phone = member.Tables[0].Rows[0]["CUSR_CELLPHONE"].ToString();

                #region Send SMS

                try
                {
                    var returnURL = string.Empty;
                    var content = string.Empty;
                    string cellPhone = Misc.PhoneParse(Country.ToLower(), Phone);

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);

                    if (Country.ToUpper() == "MY")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "RM0.00", Code);
                    }
                    else if (Country.ToUpper() == "SG")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "ADV", Code);

                    }
                    else if (Country.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);

                    }

                    int type = 1;

                    if (Session["LanguageChosen"].ToString() == "en-US")
                    {
                        type = 1;
                    }
                    else if (Session["LanguageChosen"].ToString() == "zh-CN")
                    {
                        type = 3;
                        content = System.Text.Encoding.BigEndianUnicode.GetBytes(content).Aggregate("", (agg, val) => agg + val.ToString("X2"));
                    }


                    returnURL = string.Format("http://www.bulksms2u.com/websmsapi/ISendSMS.aspx?username={0}&password={1}&message={2}&mobile={3}&Sender={4}&type={5}", "UnionSMS", "juta1515", content, cellPhone, "68886", type);

                    MemberLoginDB.InsertSMSLog(returnURL, "UnionSMS", "juta1515", cellPhone, Country.ToUpper());

                    using (var client = new System.Net.WebClient())
                    {
                        client.Encoding = System.Text.Encoding.UTF8;
                        client.UploadString(returnURL, string.Empty);
                    }
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion

                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EmailDeliver()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);


                var dr = MemberProfileDB.GetMemberVerificationByUserName(Session["Username"].ToString(), out ok, out msg);

                if (dr.Tables[0].Rows.Count == 0)
                {
                    Response.Write("Please Make Sure Real Name Authentication Are Approve Before Exchange");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string Status = dr.Tables[0].Rows[0]["CMEMVER_STATUS"].ToString();

                    if (Status != "Approve")
                    {
                        Response.Write("Please Make Sure Real Name Authentication Are Approve Before Exchange");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }






                string Country = member.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                string Email = member.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();

                #region Send Email

                try
                {
                    var Title = string.Empty;
                    var content = string.Empty;

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);


                    if (Country.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);
                        Title = Resources.Member.lblEmailTitleTaiwan;

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);
                        Title = Resources.Member.lblEmailTitle;

                    }


                    string result = Misc.SendEmail("noreply@UnionsChain.com", Email, Title, content);
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion


                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ChangeHPMobileDeliver()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

                string Country = member.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                string Phone = member.Tables[0].Rows[0]["CUSR_CELLPHONE"].ToString();

                #region Send SMS

                try
                {
                    var returnURL = string.Empty;
                    var content = string.Empty;
                    string cellPhone = Misc.PhoneParse(Country.ToLower(), Phone);

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);

                    if (Country.ToUpper() == "MY")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "RM0.00", Code);
                    }
                    else if (Country.ToUpper() == "SG")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "ADV", Code);

                    }
                    else if (Country.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);

                    }

                    int type = 1;

                    if (Session["LanguageChosen"].ToString() == "en-US")
                    {
                        type = 1;
                    }
                    else if (Session["LanguageChosen"].ToString() == "zh-CN")
                    {
                        type = 3;
                        content = System.Text.Encoding.BigEndianUnicode.GetBytes(content).Aggregate("", (agg, val) => agg + val.ToString("X2"));
                    }


                    returnURL = string.Format("http://www.bulksms2u.com/websmsapi/ISendSMS.aspx?username={0}&password={1}&message={2}&mobile={3}&Sender={4}&type={5}", "UnionSMS", "juta1515", content, cellPhone, "68886", type);

                    MemberLoginDB.InsertSMSLog(returnURL, "UnionSMS", "juta1515", cellPhone, Country.ToUpper());

                    using (var client = new System.Net.WebClient())
                    {
                        client.Encoding = System.Text.Encoding.UTF8;
                        client.UploadString(returnURL, string.Empty);
                    }
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion

                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ChangeHPEmailDeliver()
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";
                DataSet member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                 



                string Country = member.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                string Email = member.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();

                #region Send Email

                try
                {
                    var Title = string.Empty;
                    var content = string.Empty;

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);


                    if (Country.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);
                        Title = Resources.Member.lblEmailTitleTaiwan;

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);
                        Title = Resources.Member.lblEmailTitle;

                    }


                    string result = Misc.SendEmail("noreply@UnionsChain.com", Email, Title, content);
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion


                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Real Name Authentication
        public ActionResult RealNameAuthentication()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = "";

            EditMemberModel member = new EditMemberModel();
            member.ProtraitPhotoPath = "";
            member.ReversePhotoPath = "";
            member.HandheldPhotoPath = "";

            var dr = MemberProfileDB.GetMemberVerificationByUserName(Session["Username"].ToString(), out ok, out msg);
            if (dr.Tables[0].Rows.Count != 0)
            {
                //member.VerificationID = Convert.ToInt32(dr.Tables[0].Rows[0]["CMEMVERVER_ID"].ToString());
                member.Username = dr.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                member.FullName = dr.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                member.IdentityNumber = dr.Tables[0].Rows[0]["IdentityNumber"].ToString();
             
                member.Status = Misc.Status(dr.Tables[0].Rows[0]["CMEMVER_STATUS"].ToString());

                if (member.Status == Resources.Member.lblStatusRejected)
                {
                    member.ProtraitPhotoPath = "";
                    member.ReversePhotoPath = "";
                    member.HandheldPhotoPath = "";
                }
                else
                {
                    member.ProtraitPhotoPath = dr.Tables[0].Rows[0]["CMEMVER_PROTRAIT_PHOTO"].ToString();
                    member.ReversePhotoPath = dr.Tables[0].Rows[0]["CMEMVER_REVERSE_PHOTO"].ToString();
                    member.HandheldPhotoPath = dr.Tables[0].Rows[0]["CMEMVER_HANDHELD_PHOTO"].ToString();
                }

                Session["Status"] = member.Status;
            }

            DataSet dsCountrySetup = AdminGeneralDB.GetCountryByCountryCode(Session["CountryCode"].ToString(), out ok, out msg);
            if (dsCountrySetup.Tables[0].Rows.Count != 0)
            {
                member.ICLength = dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_ICLENGTH"].ToString();
                member.SelectedICType = dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_ICTYPE"].ToString();
            }

            return PartialView("RealNameAuthentication", member);

        }
        [HttpPost]
        public ActionResult RealNameAuthenticationMethod(EditMemberModel member)
        {
            try
            {

                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int ok = 0;
                string msg = "";
                string languageCode = Session["LanguageChosen"].ToString();

                #region Check Fullname
                if (string.IsNullOrEmpty(member.FullName))
                {
                    Response.Write(Resources.Member.warningFullname);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Check IC
                if (string.IsNullOrEmpty(member.IdentityNumber))
                {
                    Response.Write(Resources.Member.warningPlsKeyInIC);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //Regex digits = new Regex("^[0-9]*$");
                //Regex alphadigits = new Regex("^[a-zA-Z0-9]*$");

                //if (member.SelectedICType == "True")
                //{
                //    if (!alphadigits.IsMatch(member.IdentityNumber))
                //    {
                //        Response.Write(Resources.Member.warningICAlpha);
                //        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //    }
                //}
                //else
                //{
                //    if (!digits.IsMatch(member.IdentityNumber))
                //    {
                //        Response.Write(Resources.Member.warningICNumeric);
                //        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //    }
                //}

                //if (member.IdentityNumber.Length != Convert.ToInt32(member.ICLength))
                //{
                //    Response.Write(Resources.Member.warningICLength);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                #endregion

                #region Check Protrait Photo

                if (member.ProtraitPhoto != null && member.ProtraitPhoto.FileName != string.Empty)
                {
                    if (Misc.IsFileExtensionValid(member.ProtraitPhoto.FileName))
                    {
                        string basePath = Server.MapPath("~/MemberProfile/" + member.Username + "/Protrait/");
                        if (Directory.Exists(basePath) == false)
                        {
                            Directory.CreateDirectory(basePath);
                        }
                        var filename = Path.GetFileName(member.ProtraitPhoto.FileName);
                        member.ProtraitPhotoPath = basePath + "\\" + filename;
                        member.ProtraitPhoto.SaveAs(member.ProtraitPhotoPath);
                        member.ProtraitPhotoPath = "../../MemberProfile/" + member.Username + "/Protrait/" +filename;
                    }
                    else
                    {
                        Response.Write(Resources.Member.msgWrongProtraitExt);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);

                    }
                }
                else if (member.ProtraitPhotoPath != null && member.ProtraitPhotoPath != "")
                {
                    member.ProtraitPhotoPath = member.ProtraitPhoto.FileName;
                }
                else
                {
                    Response.Write(Resources.Member.warningUploadProtraitPhoto);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Check Reverse Photo

                if (member.ReversePhoto != null && member.ReversePhoto.FileName != string.Empty)
                {
                    if (Misc.IsFileExtensionValid(member.ReversePhoto.FileName))
                    {
                        string basePath = Server.MapPath("~/MemberProfile/" + member.Username + "/Reverse/");
                        if (Directory.Exists(basePath) == false)
                        {
                            Directory.CreateDirectory(basePath);
                        }
                        var filename = Path.GetFileName(member.ReversePhoto.FileName);
                        member.ReversePhotoPath = basePath + "\\" + filename;
                        member.ReversePhoto.SaveAs(member.ReversePhotoPath);
                        member.ReversePhotoPath = "../../MemberProfile/" + member.Username + "/Reverse/" + filename;
                    }
                    else
                    {
                        Response.Write(Resources.Member.msgWrongReverseExt);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);

                    }
                }
                else if (member.ReversePhotoPath != null && member.ReversePhotoPath != "")
                {
                    member.ReversePhotoPath = member.ReversePhoto.FileName;
                }
                else
                {
                    Response.Write(Resources.Member.warningUploadReversePhoto);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Check Handheld Photo

                if (member.HandheldPhoto != null && member.HandheldPhoto.FileName != string.Empty)
                {
                    if (Misc.IsFileExtensionValid(member.HandheldPhoto.FileName))
                    {
                        string basePath = Server.MapPath("~/MemberProfile/" + member.Username + "/Handheld/");
                        if (Directory.Exists(basePath) == false)
                        {
                            Directory.CreateDirectory(basePath);
                        }
                        var filename = Path.GetFileName(member.HandheldPhoto.FileName);
                        member.HandheldPhotoPath = basePath + "\\" + filename;
                        member.HandheldPhoto.SaveAs(member.HandheldPhotoPath);
                        member.HandheldPhotoPath = "../../MemberProfile/" + member.Username + "/Handheld/" + filename;
                    }
                    else
                    {
                        Response.Write(Resources.Member.msgWrongHandheldExt);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);

                    }
                }
                else if (member.HandheldPhotoPath != null && member.HandheldPhotoPath != "")
                {
                    member.HandheldPhotoPath = member.HandheldPhoto.FileName;
                }
                else
                {
                    Response.Write(Resources.Member.warningUploadHandheldPhoto);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                //var maxsize = 2 * 1024 * 1024;
                //if (member.ProtraitPhoto.ContentLength > maxsize || member.ReversePhoto.ContentLength > maxsize || member.HandheldPhoto.ContentLength > maxsize)
                //{
                //    Response.Write(Resources.Member.msgMaxFileSize);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}


                MemberProfileDB.CreateMemberVerification(member, out ok, out msg);

                //if (member.VerificationID == 0)
                //{
                //    MemberProfileDB.CreateMemberVerification(member, out ok, out msg);
                //}
                //else
                //{
                //    MemberProfileDB.UpdateMemberVerification(member, out ok, out msg);
                //}

                if(ok == 1)
                {
                    return RealNameAuthentication();
                }
                else
                {
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Bingding Bank Cards
        public ActionResult BindBankCards()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }
            MemberBankModel model = new MemberBankModel();

            model.MemberID = Session["Username"].ToString();

            int ok = 0;
            string msg = string.Empty;
            string language = Session["LanguageChosen"].ToString();

            DataSet dsMemberBank = MemberProfileDB.GetMemberBankByUsername(model.MemberID, out ok, out msg);
            DataSet dsMember = MemberDB.GetMemberByUsername(model.MemberID, out ok, out msg);

            List<CountrySetupModel> countries = Misc.GetAllCountryList(language, ref ok, ref msg);
            model.CountryList = from c in countries
                                select new SelectListItem
                                {
                                    Selected = false,
                                    Text = c.CountryName,
                                    Value = c.CountryCode
                                };

            string Country = dsMember.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
            model.SelectedCountry = Country;
            model.SelectedCountryStraing = countries.Find(item => item.CountryCode == Country).CountryName;

            List<BankModel> banks = Misc.GetAllBankByCountry(language, model.SelectedCountry, ref ok, ref msg);
            model.BankList = from c in banks
                             select new SelectListItem
                             {
                                 Selected = false,
                                 Text = c.BankName,
                                 Value = c.BankCode
                             };

            if (dsMemberBank.Tables[0].Rows.Count == 0)
            {
                model.SelectedBank = "null";
                model.BranchName = "";
                model.AccountNumber = "";
            }
            else
            {
                model.BranchName = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_BRANCHNAME"].ToString();
                model.AccountNumber = dsMemberBank.Tables[0].Rows[0]["CMEMBANK_ACCOUNTNUMBER"].ToString();
                model.SelectedBank = dsMemberBank.Tables[0].Rows[0]["CBANK_CODE"].ToString();

            }




            return PartialView("BankCardsBinding",model);

        }

        [HttpPost]
        public ActionResult BindBankCardsMethod(MemberBankModel mbm)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }


                mbm.MemberID = Session["Username"].ToString();

                if (mbm.BranchName == null)
                {
                    Response.Write(Resources.Member.warningPlsEnterBranchName);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mbm.AccountNumber == null)
                {
                    Response.Write(Resources.Member.warningPlsEnterAccNumber);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (mbm.BeneficiaryName == null)
                {
                    mbm.BeneficiaryName = "";
                }
                if (mbm.BeneficiaryIC == null)
                {
                    mbm.BeneficiaryIC = "";
                }
                if (mbm.SelectedBank == "null")
                {
                    Response.Write(Resources.Member.warningPlsSelectBank);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                int ok;
                string msg;
                DataSet dsMemberBank = MemberProfileDB.GetMemberBankByUsername(mbm.MemberID, out ok, out msg);

                DataSet dsMember = MemberDB.GetMemberByUsername(mbm.MemberID, out ok, out msg);

                string Country = dsMember.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                mbm.SelectedCountry = Country;

                if (dsMemberBank.Tables[0].Rows.Count == 0)
                {
                    //Create New Record

                    MemberProfileDB.CreateNewMemberBank(mbm.SelectedCountry, mbm.SelectedBank, mbm.MemberID, Helper.NVL(mbm.BranchName), Helper.NVL(mbm.AccountNumber), Helper.NVL(mbm.BeneficiaryName), Helper.NVL(mbm.BeneficiaryIC), Session["Username"].ToString(), Session["Username"].ToString(), out ok, out msg);
                }
                else
                {
                    //Update Existing Record

                    MemberProfileDB.UpdateMemberBank(mbm.SelectedCountry, mbm.SelectedBank, mbm.MemberID, Helper.NVL(mbm.BranchName), Helper.NVL(mbm.AccountNumber), Helper.NVL(mbm.BeneficiaryName), Helper.NVL(mbm.BeneficiaryIC), Helper.NVL(mbm.BeneficiaryPhone), Helper.NVL(mbm.BeneficiaryRelationship), Helper.NVL(mbm.Username), out ok, out msg);
                }

                return BindBankCards();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

    }
}