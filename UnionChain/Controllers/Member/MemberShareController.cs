﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Globalization;
using System.IO;
using iTextSharp.text.pdf;
using System.Net;
using ECFBase.Helpers;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using ECFBase.ThirdParties;
using Newtonsoft.Json;

namespace ECFBase.Controllers.Member
{
    public class MemberShareController : Controller
    {

        private int ConstructPageList(int selectedPage, int pages, PaginationWalletLogModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 0; z < pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = (c + 1).ToString(),
                              Value = c.ToString()
                          };

            //if the selected page is -1, then set the first selected page
            if (model.Pages.Count() != 0 && selectedPage == -1)
            {
                model.Pages.First().Selected = true;
                selectedPage = int.Parse(model.Pages.First().Value);
            }
            return selectedPage;
        }
        private static int ConstructPageList(int selectedPage, int pages, PaginationMemberShareModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }


            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        public ActionResult HashBonusList(int selectedPage = 1, string startdate = "2018-01-01", string enddate ="2200-01-01")
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }



            int ok;
            string msg;
            int pages;

            if (startdate == "")
            {
                startdate = "2018-01-01";
            }

            if (startdate != "2018-01-01")
            {
                DateTime sdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", null);
                startdate = sdate.ToString("yyyy-MM-dd");
            }

            if (enddate == "")
            {
                enddate = "2200-01-01";
            }

            if (enddate != "2200-01-01")
            {
                DateTime edate = DateTime.ParseExact(enddate, "dd-MM-yyyy", null);
                enddate = edate.ToString("yyyy-MM-dd");
            }


            var model = new PaginationMemberShareModel();
            
            var dsHashBonus = MemberShareDB.GetHashBonusByUsername(Session["Username"].ToString(), selectedPage, startdate, enddate, out pages, out ok, out msg);
            if (dsHashBonus.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    var csm = new MemberHashBonusModel();
                    //csm.MemberID = dr["CUSR_USERNAME"].ToString();
                    csm.SalesAmount = float.Parse(dr["CROIWL_CASHIN"].ToString());
                    csm.SalesDate = Convert.ToDateTime(dr["CROIWL_CREATEDON"].ToString()).ToString("dd-MM-yyyy");
                    csm.CashName = Misc.GetReadableCashName(dr["CROIWL_CASHNAME"].ToString());
                    csm.AppUser = dr["CROIWL_APPUSER"].ToString();
                    csm.Remarks = dr["CROIWL_APPOTHER"].ToString();
                    model.MemberHashBonusModel.Add(csm);
                }
            }
            

            selectedPage = ConstructPageList(selectedPage, pages, model);



            return PartialView("HashBonusSummary", model);
        }

        public ActionResult HashPowerBonusList(int selectedPage = 1, string startdate = "2018-01-01", string enddate = "2200-01-01")
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }


            int ok;
            string msg;
            int pages;

            if (startdate == "")
            {
                startdate = "2018-01-01";
            }

            if (startdate != "2018-01-01")
            {
                DateTime sdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", null);
                startdate = sdate.ToString("yyyy-MM-dd");
            }

            if (enddate == "")
            {
                enddate = "2200-01-01";
            }

            if (enddate != "2200-01-01")
            {
                DateTime edate = DateTime.ParseExact(enddate, "dd-MM-yyyy", null);
                enddate = edate.ToString("yyyy-MM-dd");
            }

            var model = new PaginationMemberShareModel();
                       
            var dsHashBonus = MemberShareDB.GetHashPowerBonusByUsername(Session["Username"].ToString(), selectedPage, startdate, enddate, out pages, out ok, out msg);
            if (dsHashBonus.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    var csm = new MemberHashPowerModel();
                    //csm.MemberID = dr["CUSR_USERNAME"].ToString();
                    csm.SalesAmount = float.Parse(dr["CPHWL_CASHIN"].ToString());
                    csm.SalesDate = Convert.ToDateTime(dr["CPHWL_CREATEDON"].ToString()).ToString("dd-MM-yyyy");
                    csm.CashName = dr["CPHWL_CASHNAME"].ToString();
                    csm.AppUser = dr["CPHWL_APPUSER"].ToString();
                    csm.Remarks = dr["CPHWL_APPOTHER"].ToString();
                    model.MemberHashPowerModel.Add(csm);
                }
            }
            
            selectedPage = ConstructPageList(selectedPage, pages, model);



            return PartialView("HashPowerSummary", model);
        }


        public ActionResult HashPerformanceBonusList(int selectedPage = 1, string startdate = "2018-01-01", string enddate = "2200-01-01")
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok;
            string msg;
            int pages;

            if(startdate == "")
            {
                startdate = "2018-01-01";
            }

            if (startdate != "2018-01-01")
            {
                DateTime sdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", null);
                startdate = sdate.ToString("yyyy-MM-dd");
            }

            if (enddate == "")
            {
                enddate = "2200-01-01";
            }

            if (enddate != "2200-01-01")
            {
                DateTime edate = DateTime.ParseExact(enddate, "dd-MM-yyyy", null);
                enddate = edate.ToString("yyyy-MM-dd");
            }

            var model = new PaginationMemberShareModel();
            
            var dsHashBonus = MemberShareDB.GetHashPerformanceBonusByUsername(Session["Username"].ToString(), selectedPage, startdate, enddate, out pages, out ok, out msg);
            if (dsHashBonus.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    var csm = new MemberHashPerformanceBonusModel();
                    ViewBag.MemberID = dr["CUSR_USERNAME"].ToString();
                    csm.SalesAmount = float.Parse(dr["CMHWL_CASHIN"].ToString());
                    csm.SalesDate = Convert.ToDateTime(dr["CMHWL_CREATEDON"].ToString()).ToString("dd-MM-yyyy");
                    csm.CashName = dr["CMHWL_CASHNAME"].ToString();
                    csm.AppUser = dr["CMHWL_APPUSER"].ToString();
                    csm.Remarks = dr["CMHWL_APPOTHER"].ToString();
                    model.MemberHashPerformanceBonusModel.Add(csm);
                }
            }
            
            selectedPage = ConstructPageList(selectedPage, pages, model);



            return PartialView("HashPerformanceBonusSummary", model);
        }
        public void ExportHashBonus(string type = "", int selectedPage = 1, string startdate = "2018-01-01", string enddate = "2200-01-01", string Export = "1")
        {

            if (startdate == "")
            {
                startdate = "2018-01-01";
            }

            if (startdate != "2018-01-01")
            {
                DateTime sdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", null);
                startdate = sdate.ToString("yyyy-MM-dd");
            }

            if (enddate == "")
            {
                enddate = "2200-01-01";
            }

            if (enddate != "2200-01-01")
            {
                DateTime edate = DateTime.ParseExact(enddate, "dd-MM-yyyy", null);
                enddate = edate.ToString("yyyy-MM-dd");
            }

            string Title = string.Empty;

            if (type == "Hash")
            {
                Title = "HashBonus-Log.csv";
            }
            if (type == "HashPower")
            {
                Title = "PerformanceHashBonus-Log.csv";
            }
            if (type == "HashPerformance")
            {
                Title = "ManagementHashBonus-Log.csv";
            }

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + Title + "");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintBonus(type, Session["Username"].ToString(), startdate, enddate, Export, selectedPage));
            sw.Close();
            Response.End();
        }
        private string PrintBonus(string type, string username, string startdate, string enddate, string Export, int SelectedPage)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Date,Amount,Remark,Extra Info"));

            int pages = 0;
            int ok = 0;
            string msg = string.Empty;

            if (type == "Hash")
            {

                DataSet dsHashBonus = MemberShareDB.GetHashBonusByUsername(Session["Username"].ToString(), SelectedPage, startdate, enddate, out pages, out ok, out msg);

                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    sb.AppendLine(
                                    Convert.ToDateTime(dr["CROIWL_CREATEDON"]).ToString("dd-MM-yyyy") + "," +
                                   dr["CROIWL_CASHIN"].ToString() + "," +
                                    dr["CROIWL_CASHNAME"].ToString() + "," +
                                   dr["CROIWL_APPOTHER"].ToString());
                }

            }
            else if (type == "HashPower")
            {
                DataSet dsHashBonus = MemberShareDB.GetHashPowerBonusByUsername(Session["Username"].ToString(), SelectedPage, startdate, enddate, out pages, out ok, out msg);

                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    sb.AppendLine(
                                    Convert.ToDateTime(dr["CPHWL_CREATEDON"]).ToString("dd-MM-yyyy") + "," +
                                   dr["CPHWL_CASHIN"].ToString() + "," +
                                    dr["CPHWL_CASHNAME"].ToString() + "," +
                                   dr["CPHWL_APPOTHER"].ToString());
                }
            }
            else if (type == "HashPerformance")
            {
                DataSet dsHashBonus = MemberShareDB.GetHashPerformanceBonusByUsername(Session["Username"].ToString(), SelectedPage, startdate, enddate, out pages, out ok, out msg);

                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    sb.AppendLine(
                                    Convert.ToDateTime(dr["CMHWL_CREATEDON"]).ToString("dd-MM-yyyy") + "," +
                                   dr["CMHWL_CASHIN"].ToString() + "," +
                                    dr["CMHWL_CASHNAME"].ToString() + "," +
                                   dr["CMHWL_APPOTHER"].ToString());
                }
            }
            return sb.ToString();
        }


    }
}
