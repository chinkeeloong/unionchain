﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ECFBase.Models;
using System.Data;
using ECFBase.Components;
using System.Text.RegularExpressions;
using System.IO;
using ECFBase.Helpers;
using ECFBase.ThirdParties;
using System.Net;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace ECFBase.Controllers.Member
{
    public class MemberShopCenterController : Controller
    {

        #region OLD Sponsor Chart

        //[WebMethod]
        //[ScriptMethod(UseHttpGet = true)]

        //public ActionResult SearchSponsor(string Username)
        //{
        //    try { 
        //    if (Session["Username"] == null || Session["Username"].ToString() == "")
        //    {
        //        return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
        //    }

        //    if (Username == null || Username == "")
        //    {
        //        Username = Session["Username"].ToString();
        //    }


        //    var model = new MemberSponsorChartModel();

        //        int ok;
        //        string msg;

        //        DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(Username, out ok, out msg);

        //    if (dsNetworkTree.Tables[0].Rows.Count > 0)
        //    {
        //        model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
        //        model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
        //        model.FirstLevelJoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
        //        model.FirstLevelRanking = dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString();
        //        model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();
        //        model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();

        //        foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
        //        {
        //            MemberList resultList = new MemberList();
        //            var info = GetMemberSponsorInfo(dr);
        //            resultList.Level = dr["LEVEL"].ToString();
        //            resultList.Member = dr["USERNAME"].ToString();
        //            resultList.Ranking = dr["RANK"].ToString();
        //            resultList.JoinedDate = dr["DATE"].ToString();
        //            resultList.FullName = dr["FULLNAME"].ToString();
        //            resultList.Intro = dr["INTRO"].ToString();

        //            model.resultList.Add(resultList);

        //        }

        //        ViewBag.MemberCount = 0;
        //    }
        //    else
        //    {
        //            Response.Write(Resources.Member.warningPlsKeyInSearchID);
        //            return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //    }

        //        return PartialView("SponsorChartTree", model);
        //    }
        //    catch (Exception e)
        //    {
        //        Response.Write(e.Message);
        //        return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public ActionResult SponsorChartTree(string memberUsername, int? memberLevel, int selectedPage = 1)
        //{
        //    if (Session["Username"] == null || Session["Username"].ToString() == "")
        //    {
        //        return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
        //    }

        //    string username;
        //    if (memberUsername == null)
        //    {
        //        username = Session["Username"].ToString();
        //        ViewBag.MemberLevel = 1;
        //        ViewBag.NextLevel = 4;
        //        Session["PreviousView"] = null;
        //    }
        //    else
        //    {
        //        username = memberUsername;
        //        ViewBag.MemberLevel = memberLevel;
        //        ViewBag.NextLevel = memberLevel + 3;
        //    }

        //    //Store Previous username and level
        //    List<String> userNameList = new List<String>();
        //    if (Session["PreviousView"] == null)
        //    {
        //        userNameList.Add(username + ";" + ViewBag.MemberLevel);
        //        Session["PreviousView"] = userNameList;
        //    }
        //    else
        //    {
        //        userNameList = (List<string>)Session["PreviousView"];
        //        userNameList.Add(memberUsername + ";" + memberLevel);
        //        Session["PreviousView"] = userNameList;
        //    }

        //    var model = new MemberSponsorChartModel();

        //    int ok;
        //    string msg;
        //    DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(username, out ok, out msg);
        //    ViewBag.MemberCount = 0;

        //    var member = MemberDB.GetMemberByUsername(username, out ok, out msg);

        //    model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
        //    model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
        //    model.FirstLevelJoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
        //    model.FirstLevelRanking = Misc.GetMemberRanking(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());
        //    //model.FirstLevelRanking = dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString();
        //    model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();
        //    model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();
        //    model.FirstLevelRankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

        //    foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
        //    {
        //        MemberList resultList = new MemberList();
        //        var info = GetMemberSponsorInfo(dr);
        //        resultList.Level = dr["LEVEL"].ToString();
        //        resultList.Member = dr["USERNAME"].ToString();
        //        resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
        //        //resultList.Ranking = dr["RANK"].ToString();
        //        resultList.JoinedDate = dr["DATE"].ToString();
        //        resultList.FullName = dr["FULLNAME"].ToString();
        //        resultList.Intro = dr["INTRO"].ToString();
        //        resultList.RankIcon = dr["ICON"].ToString();

        //        model.resultList.Add(resultList);

        //    }

        //    DataSet logo = AdminGeneralDB.GetRankIcon();
        //    foreach (DataRow dr in logo.Tables[0].Rows)
        //    {
        //        var IFile = new ImageFile();
        //        IFile.FileName = dr["CRANKSET_ICON"].ToString();
        //        RankModel RankList = new RankModel();
        //        RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
        //        //RankList.RankIconName = dr["AMOUNT"].ToString();
        //        RankList.RankName = dr["CRANKSET_NAME"].ToString();
        //        ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

        //        model.RankList.Add(RankList);
        //    }

        //    //Sponsor List start
        //    int pages = 0;
        //    //PaginationIntroListModel model = new PaginationIntroListModel();
        //    DataSet dsSponsorList = MemberShopCenterDB.GetAllSponsorListByUsername(Session["Username"].ToString(), selectedPage, out pages);

        //    selectedPage = SponsorListConstructPageList(selectedPage, pages, model);

        //    foreach (DataRow dr in dsSponsorList.Tables[0].Rows)
        //    {
        //        IntroListModel sponsorlist = new IntroListModel();
        //        sponsorlist.No = dr["rownumber"].ToString();
        //        sponsorlist.Username = dr["CUSR_MEMBERID"].ToString();
        //        sponsorlist.Name = dr["CUSR_FULLNAME"].ToString();
        //        sponsorlist.Rank = Misc.RankNumber(dr["CRANK_CODE"].ToString());
        //        sponsorlist.Sponsor = dr["CUSR_INTRO"].ToString();
        //        sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
        //        sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_DATEJOINED"]).ToString("dd/MM/yyyy hh:mm:ss tt");

        //        model.SponsorList.Add(sponsorlist);
        //    }

        //    return PartialView("SponsorChartTree", model);
        //}

        //private MemberSponsorChartModel GetMemberSponsorInfo(DataRow dr)
        //{
        //    var mntm = new MemberSponsorChartModel();

        //    mntm.Level = dr["LEVEL"].ToString();
        //    mntm.Member = dr["USERNAME"].ToString();
        //    mntm.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
        //    mntm.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToShortDateString();
        //    mntm.FullName = dr["FULLNAME"].ToString();
        //    mntm.Intro = dr["INTRO"].ToString();
        //    mntm.RankIcon = dr["ICON"].ToString();

        //    return mntm;
        //}
        #endregion

        #region SponsorChart Ver1

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ActionResult SearchSponsor(string Username)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                if (Username == null || Username == "")
                {
                    Username = Session["Username"].ToString();
                }

                var model = new MemberSponsorChartModel();

                int ok;
                string msg;
                DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(Username, out ok, out msg);

                if (dsNetworkTree.Tables[0].Rows.Count > 0)
                {
                    model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
                    model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
                    //model.FirstLevelJoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
                    model.FirstLevelJoinedDate = DateTime.Parse(dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString()).ToString("dd-MM-yyyy");

                    model.FirstLevelRanking = dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString();
                    model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();
                    model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();

                    foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
                    {
                        MemberList resultList = new MemberList();
                        var info = GetMemberSponsorInfo(dr);
                        resultList.Level = dr["LEVEL"].ToString();
                        resultList.Member = dr["USERNAME"].ToString();
                        resultList.Ranking = dr["RANK"].ToString();
                        //resultList.JoinedDate = dr["DATE"].ToString();
                        //model.FirstLevelJoinedDate = dr["DATE"].ToString();
                        model.FirstLevelJoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToString("dd-MM-yyyy");
                        resultList.FullName = dr["FULLNAME"].ToString();
                        resultList.Intro = dr["INTRO"].ToString();
                        model.resultList.Add(resultList);
                    }

                    ViewBag.MemberCount = 0;
                }
                else
                {
                    Response.Write("Invalid Sponsor");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return PartialView("SponsorChartTreeVer1", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        private MemberSponsorChartModel GetMemberSponsorInfo(DataRow dr)
        {
            var mntm = new MemberSponsorChartModel();

            mntm.Level = dr["LEVEL"].ToString();
            mntm.Member = dr["USERNAME"].ToString();
            mntm.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
            mntm.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToString("dd-MM-yyyy");
            mntm.FullName = dr["FULLNAME"].ToString();
            mntm.Intro = dr["INTRO"].ToString();
            mntm.RankIcon = dr["ICON"].ToString();

            return mntm;
        }
        public ActionResult FindSponsorDownline(string Username)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int isBelongTo;
            MemberDB.MemberIsInDownlineByUsername(Username, Session["Username"].ToString(), out isBelongTo);
            if (isBelongTo == 0)//not belong to
            {
                Response.Write(string.Format("Invalid User", Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            return SponsorChartTree(Username, null);
        }

        public ActionResult SponsorChartTree(string memberUsername, int? memberLevel, int selectedpage = 1)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            string username;
            if (memberUsername == null)
            {
                username = Session["Username"].ToString();
                ViewBag.MemberLevel = 1;
                ViewBag.NextLevel = 4;
                Session["PreviousView"] = null;
            }
            else
            {
                username = memberUsername;
                ViewBag.MemberLevel = memberLevel;
                ViewBag.NextLevel = memberLevel + 3;
            }

            //Store Previous username and level
            var userNameList = new List<String>();
            if (Session["PreviousView"] == null)
            {
                userNameList.Add(username + ";" + ViewBag.MemberLevel);
                Session["PreviousView"] = userNameList;
            }
            else
            {
                userNameList = (List<string>)Session["PreviousView"];
                userNameList.Add(memberUsername + ";" + memberLevel);
                Session["PreviousView"] = userNameList;
            }

            var model = new MemberSponsorChartModel();
            int ok;
            string msg;
            DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChartVer1(username, out ok, out msg);
            ViewBag.MemberCount = 0;

            var member = MemberDB.GetMemberByUsername(username, out ok, out msg);
            model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
            model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
            //model.FirstLevelJoinedDate = DateTime.Parse(dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString()).ToShortDateString();

            //var Date = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
            //model.FirstLevelJoinedDate = Convert.ToDateTime(Date).ToString("dd/mm/yyyy");
            model.FirstLevelJoinedDate = Convert.ToDateTime(dsNetworkTree.Tables[0].Rows[0]["DATE"]).ToString("dd-MM-yyyy");

            model.FirstLevelRanking = Misc.GetMemberRanking(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());

            if (dsNetworkTree.Tables[0].Rows[0]["UPGRADEDATE"].ToString() != "")
            {
                model.FirstLevelRanking += string.Format(" (Upgrade:{0})", DateTime.Parse(dsNetworkTree.Tables[0].Rows[0]["UPGRADEDATE"].ToString()).ToShortDateString());
            }

            model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();

            model.FirstLevelIntro = string.Format("{0} ({1})", model.FirstLevelIntro, dsNetworkTree.Tables[0].Rows[0]["GROUPSALES"].ToString());

            model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();
            model.FirstLevelRankIcon = Misc.GetMemberRankingIcon(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());

            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                //var resultList = new MemberList();
                MemberList resultList = new MemberList();
                resultList.Level = dr["LEVEL"].ToString();
                resultList.Member = dr["USERNAME"].ToString();
                resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());

                if (dr["UPGRADEDATE"].ToString() != "")
                {
                    model.Ranking += string.Format(" (Upgrade:{0})", DateTime.Parse(dr["UPGRADEDATE"].ToString()).ToShortDateString());
                }

                //resultList.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToShortDateString();

                var JoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
                resultList.JoinedDate = Convert.ToDateTime(JoinedDate).ToString("dd-MM-yyyy");

                resultList.FullName = dr["FULLNAME"].ToString();
                resultList.Intro = dr["INTRO"].ToString();

                model.Intro = string.Format("{0} ({1})", model.Intro, dr["GROUPSALES"].ToString());

                resultList.RankIcon = Misc.GetMemberRankingIcon(dr["RANK"].ToString());
                resultList.MemberCount = dr["MEMBERCOUNT"].ToString();
                resultList.GoingToExpended = int.Parse(dr["LEVEL"].ToString()) % 3 == 0;
                model.resultList.Add(resultList);
            }

            DataSet logo = AdminGeneralDB.GetRankIcon();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["CRANKSET_ICON"].ToString();
                RankModel RankList = new RankModel();
                RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
                RankList.RankName = dr["CRANKSET_NAME"].ToString();
                ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                model.RankList.Add(RankList);
            }

          
            return PartialView("SponsorChartTree", model);
        }


        private MemberSponsorChartModel GetMemberSponsorInfoVer2(DataRow dr)
        {
            var mntm = new MemberSponsorChartModel();
            mntm.Level = dr["LEVEL"].ToString();
            mntm.Member = dr["USERNAME"].ToString();
            mntm.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
            mntm.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToString("dd-MM-yyyy");
            mntm.FullName = dr["FULLNAME"].ToString();
            mntm.Intro = dr["INTRO"].ToString();
            mntm.RankIcon = dr["ICON"].ToString();
            return mntm;
        }

        [HttpPost]
        public JsonResult FindNextSponsorList(string username, int level)
        {
            int ok = 0;
            string msg = "";

            DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChartVer1Fixed2Level(username, out ok, out msg);
            ViewBag.MemberCount = 0;

            var member = MemberDB.GetMemberByUsername(username, out ok, out msg);
            List<MemberList> list = new List<MemberList>();

            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                var resultList = new MemberList();
                //resultList.Level = dr["LEVEL"].ToString();
                resultList.Level = string.Format("{0}", level + 1);
                resultList.Member = dr["USERNAME"].ToString();
                resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());

                if (dr["UPGRADEDATE"].ToString() != "")
                {
                    resultList.Ranking += string.Format(" (Upgrade:{0})", DateTime.Parse(dr["UPGRADEDATE"].ToString()).ToString("dd-MM-yyyy"));
                }

                resultList.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToString("dd-MM-yyyy");
                resultList.FullName = dr["FULLNAME"].ToString();
                resultList.Intro = dr["INTRO"].ToString();

                resultList.Intro = string.Format("{0} ({1})", resultList.Intro, dr["GROUPSALES"].ToString());

                resultList.RankIcon = Misc.GetMemberRankingIcon(dr["RANK"].ToString());
                resultList.MemberCount = dr["MEMBERCOUNT"].ToString();
                resultList.GoingToExpended = int.Parse(dr["LEVEL"].ToString()) % 3 == 0;

                if (dr["LEVEL"].ToString() == "2")
                {
                    list.Add(resultList);
                }
            }

            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        #endregion    

        #region CheckEmail
        [HttpPost]
        public JsonResult CheckEmail(string Email)
        {
            var result = false;
            Regex reg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

            if (reg.IsMatch(Email))
            {
                result = true;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
        
        #region Invoice Listing

        public ActionResult PrintInvoice(int InvoiceID)
        {
            ViewBag.InvoiceID = InvoiceID;
            return View("PrintInvoice");
        }

        public ActionResult InvoiceListing(int selectedPage = 1)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok;
            string msg;
            int pages = 0;
            var model = new PaginationInvoiceListingModel();

            var dsInvoice = AdminSalesOrderDB.GetAllInvoicesByUsername(Session["Username"].ToString(), Session["LanguageChosen"].ToString(),
                                                                        selectedPage, out pages, out ok, out msg);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsInvoice.Tables[0].Rows)
            {
                var invoice = new InvoiceModel();

                //invoice info
                invoice.RowNumber = dr["rownumber"].ToString();
                invoice.Id = Convert.ToInt32(dr["CINVNO_ID"]);
                invoice.InvoiceNo = dr["CINVNO_NO"].ToString();
                invoice.InvoiceCashName = Misc.GetReadableCashName(dr["CINVNO_CASHNAME"].ToString());
                invoice.Username = dr["CUSR_USERNAME"].ToString();
                invoice.Date = (DateTime)dr["CINVNO_CREATEDON"];
                invoice.Status = dr["DOSTATUS"].ToString();
                invoice.Investment = float.Parse(dr["PKGINVEST"].ToString());
                invoice.PackageQuantity = 1; //currently only 1 Pkg is sold during register
                invoice.TotalAmount = (invoice.PackageQuantity * invoice.Investment);

                model.InvoiceLists.Add(invoice);
            }

            return PartialView("InvoiceListing", model);
        }

        public ActionResult InvoiceView(int Id)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok;
            string msg;
            var dsInvoice = AdminSalesOrderDB.GetInvoiceRelatedInfoById(Id, Session["LanguageChosen"].ToString(),
                                                                        out ok, out msg);
            var invoice = new InvoiceModel();

            //invoice info
            invoice.RowNumber = dsInvoice.Tables[0].Rows[0]["rownumber"].ToString();
            invoice.Id = Convert.ToInt32(dsInvoice.Tables[0].Rows[0]["CINVNO_ID"]);
            invoice.InvoiceNo = dsInvoice.Tables[0].Rows[0]["CINVNO_NO"].ToString();
            invoice.InvoiceCashName = Misc.GetReadableCashName(dsInvoice.Tables[0].Rows[0]["CINVNO_CASHNAME"].ToString());
            invoice.CountryCode = dsInvoice.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
            invoice.Date = (DateTime)dsInvoice.Tables[0].Rows[0]["CINVNO_CREATEDON"];
            var shippingAddress = dsInvoice.Tables[0].Rows[0]["CINVNO_ADDRESS"].ToString();
            if (!string.IsNullOrEmpty(shippingAddress.Trim()))
            {

                var address = shippingAddress.Split(new char[] { '\r' });
                for (int i = 0; i < address.Length; i++)
                {
                    if (i == 0) invoice.ShipAddress = address[0];
                    else if (i == 1) invoice.ShipAddress1 = address[1];
                    else if (i == 2) invoice.ShipAddress2 = address[2];
                    else if (i == 3) invoice.ShipAddress3 = address[3];
                }
            }

            //user and member info
            invoice.Username = dsInvoice.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            invoice.CellPhone = dsInvoice.Tables[0].Rows[0]["CELLPHONE"].ToString();
            invoice.Phone = dsInvoice.Tables[0].Rows[0]["PHONE"].ToString();
            invoice.IntroPerson = dsInvoice.Tables[0].Rows[0]["INTRO"].ToString();

            //package info
            invoice.PackageCode = dsInvoice.Tables[0].Rows[0]["CPKG_CODE"].ToString();
            invoice.Investment = float.Parse(dsInvoice.Tables[0].Rows[0]["PKGINVEST"].ToString());
            invoice.PackageQuantity = 1; //currently only 1 Pkg is sold during register
            invoice.PackageDescription = dsInvoice.Tables[0].Rows[0]["PKGDESC"].ToString();
            invoice.TotalAmount = (invoice.PackageQuantity * invoice.Investment);

            return PartialView("InvoiceView", invoice);
        }

        #endregion

        #region Sponsor List
        public ActionResult SponsorListing(int selectedPage = 1)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }

                int pages = 0;
                MemberSponsorChartModel model = new MemberSponsorChartModel();

                DataSet dsSponsorList = MemberShopCenterDB.GetAllSponsorListByUsername(Session["Username"].ToString(), selectedPage, out pages);

                selectedPage = SponsorListConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsSponsorList.Tables[0].Rows)
                {
                    IntroListModel sponsorlist = new IntroListModel();
                    sponsorlist.No = dr["rownumber"].ToString();
                    sponsorlist.Username = dr["CUSR_MEMBERID"].ToString();
                    sponsorlist.Name = dr["CUSR_FULLNAME"].ToString();
                    sponsorlist.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
                    //sponsorlist.Sponsor = dr["CUSR_INTRO"].ToString();

                    string firstupline = dr["CUSR_INTRO"].ToString();
                    if (sponsorlist.Username == Session["Username"].ToString())
                    {
                        sponsorlist.Sponsor = "";
                    }
                    else
                    {
                        sponsorlist.Sponsor = firstupline;
                    }


                    sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
                    sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_DATEJOINED"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.SponsorList.Add(sponsorlist);
                }

                return PartialView("SponsorListing", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Binary List

        public ActionResult BinaryListing(int selectedPage = 1)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
                }


                int pages = 0;
                PaginationIntroListModel model = new PaginationIntroListModel();
                DataSet dsBinaryList = MemberShopCenterDB.GetAllBinaryListByUsername(Session["Username"].ToString(), selectedPage, out pages);

                selectedPage = BinaryConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsBinaryList.Tables[0].Rows)
                {
                    IntroListModel sponsorlist = new IntroListModel();
                    sponsorlist.No = dr["rownumber"].ToString();
                    sponsorlist.Username = dr["CUSR_USERNAME"].ToString();
                    sponsorlist.FullName = dr["CUSR_FIRSTNAME"].ToString();
                    sponsorlist.Sponsor = dr["CMTREE_UNDER"].ToString();
                    sponsorlist.Upline = dr["CMTREE_UPMEMBER"].ToString();
                    sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
                    sponsorlist.Rank = Misc.RankNumber(dr["CRANK_CODE"].ToString());
                    sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    model.SponsorList.Add(sponsorlist);
                }

                return PartialView("BinaryListing", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        
        #region Shared

        private int BinaryConstructPageList(int selectedPage, int pages, PaginationIntroListModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int SponsorConstructPageList(int selectedPage, int pages, PaginationIntroListModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int SponsorListConstructPageList(int selectedPage, int pages, MemberSponsorChartModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            if (pages == 0)
            {
                pageList.Add(1);
            }
            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationInvoiceListingModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        [HttpPost]
        public JsonResult FindCountryCode(string CountryCode)
        {
            int ok = 0;
            string msg = "";
            DataSet dsCountrySetup = AdminGeneralDB.GetCountryByCountryCode(CountryCode, out ok, out msg);

            return Json(dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_MOBILECODE"].ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FindCity(string ProvinceCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetCityList(LanguageCode, ProvinceCode);
            ((RegisterNewMemberModel)Session["regMember"]).UserInfo.City = result;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetCityList(string LanguageCode, string ProvinceCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<CityModel> cities = Misc.GetAllCityByProvince(LanguageCode, ProvinceCode, ref ok, ref message);

            var result = new[] { new SelectListItem { Text = "" } }.Concat(from c in cities
                                                                           select new SelectListItem
                                                                           {
                                                                               Selected = false,
                                                                               Text = c.CityName,
                                                                               Value = c.CityCode
                                                                           });
            return result.OrderBy(m => m.Value).ToList();
        }

        [HttpPost]
        public JsonResult FindProvince(string CountryCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetProvinceList(LanguageCode, CountryCode);
            ((RegisterNewMemberModel)Session["regMember"]).UserInfo.Province = result;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetProvinceList(string LanguageCode, string CountryCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<ProvinceModel> provinces = Misc.GetAllProvinceByCountry(LanguageCode, CountryCode, ref ok, ref message);

            var result = new[] { new SelectListItem { Text = "" } }.Concat(from c in provinces
                                                                           select new SelectListItem
                                                                           {
                                                                               Selected = false,
                                                                               Text = c.ProvinceName,
                                                                               Value = c.ProvinceCode
                                                                           });
            return result.OrderBy(m => m.Value).ToList();
        }

        [HttpPost]
        public JsonResult CheckAvailability(string Username)
        {
            int ok = 0;
            MemberShopCenterDB.CheckUsernameAvailability(Username, out ok);

            return Json(ok.ToString(), JsonRequestBehavior.AllowGet);
        }
        #endregion

    }

}