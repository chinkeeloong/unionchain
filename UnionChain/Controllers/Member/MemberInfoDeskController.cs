using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ECFBase.Components;
using ECFBase.Models;
using System.Data;
using ECFBase.Helpers;
using System.IO;
using System.Net;

namespace ECFBase.Controllers.Member
{
    public class MemberInfoDeskController : Controller
    {

        #region Corporate News
        
        public ActionResult CorporateNews(int selectedPage = 1)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            int ok;
            string msg;
            int pages = 0;
            PaginationInfoDeskModel model = new PaginationInfoDeskModel();
            DataSet dsCorpNews = AdminInfoDeskDB.GetAllInfoDesk("C", Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsCorpNews.Tables[0].Rows)
            {
                var idm = new InfoDeskModel();
                idm.Number = dr["rownumber"].ToString();
                idm.InfoDeskID = Convert.ToInt32(dr["CINF_ID"]);
                idm.InfoDeskTitle = dr["CMULTILANGINFODESK_TITLE"].ToString();
                idm.InfoDeskPostDate = Convert.ToDateTime(dr["CINF_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                model.InfoDeskList.Add(idm);
            }

            return PartialView("CorporateNews", model);
        }

        public ActionResult CorporateNewsData(int CorporateNewsID)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            int ok = 0;
            string msg = "";
            var idm = new InfoDeskModel();

            //combobox for category
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            idm.LanguageList = from c in languages
                               select new SelectListItem
                               {
                                   Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                   Text = c.Text,
                                   Value = c.Value
                               };
            idm.SelectedLanguage = Session["LanguageChosen"].ToString();
            string B = string.Empty;
            if (CorporateNewsID != 0)
            {
                //View Existing Corporate News
                DataSet dsCorpNews = AdminInfoDeskDB.GetInfoDeskByID(CorporateNewsID, out ok, out msg);
                if (dsCorpNews.Tables[0].Rows.Count != 0)
                {
                    idm.InfoDeskID = Convert.ToInt32(dsCorpNews.Tables[0].Rows[0]["CINF_ID"]);
                    idm.InfoDeskType = dsCorpNews.Tables[0].Rows[0]["CINF_TYPE"].ToString();
                    idm.Created = Convert.ToDateTime(dsCorpNews.Tables[0].Rows[0]["CINF_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    B = dsCorpNews.Tables[0].Rows[0]["CINF_CODE"].ToString();
                    DataSet dsMultiLanguageInfoDesk = AdminInfoDeskDB.GetMultiLanguageInfoDeskByID(idm.InfoDeskID, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = System.Net.WebUtility.HtmlDecode(language.Text) ;
                        DataRow[] dr = dsMultiLanguageInfoDesk.Tables[0].Select("CINF_ID='" + idm.InfoDeskID + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGINFODESK_TITLE"].ToString();
                        int extensionIndex = dr[0]["CINF_IMAGEPATH"].ToString().LastIndexOf('\\');
                        string imageName = dr[0]["CINF_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                        idm.Image = "http://" + Request.Url.Authority + "/Products/" + B + "/" + imageName;

                        if(idm.SelectedLanguage == language.Value)
                            idm.TitleLanguageList.Add(tlm);
                    }

                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = System.Net.WebUtility.HtmlDecode(language.Text);
                        DataRow[] dr = dsMultiLanguageInfoDesk.Tables[0].Select("CINF_ID='" + idm.InfoDeskID + "' AND CLANG_CODE='" + language.Value + "'");


                        tlm.Text = System.Net.WebUtility.HtmlDecode(dr[0]["CMULTILANGINFODESK_CONTENT"].ToString());

                        if (idm.SelectedLanguage == language.Value)
                            idm.ContentLanguageList.Add(tlm);
                    }
                }
            }

            return PartialView("CorporateNewsData", idm);
        }

        #endregion
        
        #region Download Center

        public ActionResult DownloadCenter()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            PaginationFileCenterModel model = new PaginationFileCenterModel();

            DirectoryInfo directory = null;
            FileInfo[] files = null;

            string basePath = Server.MapPath("~/FileCenter/");
            if (Directory.Exists(basePath) == false)
            {
                Directory.CreateDirectory(basePath);
            }

            string dirPath = @"/FileCenter/";
            directory = new DirectoryInfo(Server.MapPath(dirPath));
            files = directory.GetFiles();
            files = files.OrderBy(f => f.Name).ToArray();

            int ok;
            string msg;
            foreach (FileInfo file in files)
            {
                DataSet dsFile = AdminInfoDeskDB.GetFileByName(file.Name, out ok, out msg);
                if (dsFile.Tables[0].Rows.Count != 0)
                {
                    var ucm = new FileCenterModel();
                    ucm.FileName = file.Name;
                    ucm.FileSize = (file.Length / 1000) + 1 + " kb";
                    ucm.FileTitle = dsFile.Tables[0].Rows[0]["CFILE_TITLE"].ToString();

                    model.FileCenterList.Add(ucm);
                }
            }
            return PartialView("DownloadCenter", model);
        }

        //public void Download(string fileName)
        //{
        //    Response.Buffer = false; //transmitfile self buffers
        //    Response.Clear();
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.ContentType = "application/pdf";
        //    //Response.AddHeader("Content-Disposition", string.Format("attachment; filename=myfile.pdf"));
        //    Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", System.Web.HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8))); 
        //    Response.TransmitFile(Request.MapPath("~/FileCenter/") + fileName); //transmitfile keeps entire file from loading into memory
        //    Response.End();
        //}
        public void Download(string filePath, string fileName)
        {
            Response.Buffer = false; //transmitfile self buffers
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            //Response.AddHeader("Content-Disposition", string.Format("attachment; filename=myfile.pdf"));
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", System.Web.HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8)));
            Response.TransmitFile(Request.MapPath("/FileCenter/") + fileName); //transmitfile keeps entire file from loading into memory
            Response.End();
        }
        #endregion

        #region MyNews
        public ActionResult MyNews(int selectedPage = 1)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            int ok;
            string msg;
            int pages = 0;
            PaginationInfoDeskModel model = new PaginationInfoDeskModel();

            var dsMyNews = AdminHelpDeskDB.GetAllNoticeByUsername(Session["Username"].ToString(), Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };


            foreach (DataRow dr in dsMyNews.Tables[0].Rows)
            {
                var idm = new InfoDeskModel();
                idm.Number = dr["rownumber"].ToString();
                idm.InfoDeskID = Convert.ToInt32(dr["CNOT_ID"]);
                idm.InfoDeskTitle = dr["CMULTILANGNOT_TITLE"].ToString();
                idm.InfoDeskPostDate = Convert.ToDateTime(dr["CNOT_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                model.InfoDeskList.Add(idm);
            }

            return PartialView("MyNews", model);
        }

        public ActionResult MyNewsData(int NewsID)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            int pages = 0;
            int ok = 0;
            string msg = "";
            var idm = new InfoDeskModel();

            var dsMyNews = AdminHelpDeskDB.GetAllNoticeByUsername(Session["Username"].ToString(), Session["LanguageChosen"].ToString(), -1, out pages, out ok, out msg);
            for (var i = 0; i < dsMyNews.Tables[0].Rows.Count; i++)
            {
                var item = new InfoDeskModel
                {
                    InfoDeskTitle = dsMyNews.Tables[0].Rows[i]["CMULTILANGNOT_TITLE"].ToString(),
                    InfoDeskContent = dsMyNews.Tables[0].Rows[i]["CMULTILANGNOT_CONTENT"].ToString(),
                    Created = Convert.ToDateTime(dsMyNews.Tables[0].Rows[i]["CNOT_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"),
                    InfoDeskID = int.Parse(dsMyNews.Tables[0].Rows[i]["CNOT_ID"].ToString())
                };

                if (item.InfoDeskID == NewsID)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageName = item.InfoDeskTitle;
                    idm.TitleLanguageList.Add(tlm);

                    tlm = new TextLanguageModel();
                    tlm.LanguageName = item.InfoDeskContent;
                    idm.ContentLanguageList.Add(tlm);
                    idm.Created = item.Created;

                    break;
                }
                
            }

            

            return PartialView("MyNewsData", idm);
        }
        #endregion


        #region Feedback
        public ActionResult FeedBack()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            return PartialView("Feedback");

        }

        #endregion

        #region Feedback
        public ActionResult ContactUs()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            return PartialView("ContactUs");

        }

        #endregion

        #region Notice Bulletin
        public ActionResult NoticeBulletin()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }
            int ok = 0;
            int pages = 0;
            string msg = "";

            MemberModel model = new MemberModel();

            var dsMyNews = AdminHelpDeskDB.GetAllNoticeByUsername(Session["Username"].ToString(), Session["LanguageChosen"].ToString(), 1, out pages, out ok, out msg);
            for (var i = 0; i < dsMyNews.Tables[0].Rows.Count && i < 3; i++)
            {
                var item = new InfoDeskModel
                {
                    InfoDeskTitle = dsMyNews.Tables[0].Rows[i]["CMULTILANGNOT_TITLE"].ToString(),
                    InfoDeskID = int.Parse(dsMyNews.Tables[0].Rows[i]["CNOT_ID"].ToString()),
                    InfoDeskContent = dsMyNews.Tables[0].Rows[i]["CMULTILANGNOT_CONTENT"].ToString(),
                    IsNewInfo = Convert.ToDateTime(dsMyNews.Tables[0].Rows[i]["CNOT_CREATEDON"]).AddDays(14) > DateTime.Now,
                    Created = Convert.ToDateTime(dsMyNews.Tables[0].Rows[i]["CNOT_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt")
                };

                model.MyNews.Add(item);
            }


            return PartialView("NoticeBulletin", model);

        }

        #endregion

    }
}