﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Helpers;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.SessionState;
using System.Web.UI.HtmlControls;
using System.IO;
using ECFBase.Components;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using ECFBase.Models;
using System.Text.RegularExpressions;
using System.Globalization;

namespace ECFBase.Controllers.Member
{
    public class MemberLoginController : Controller
    {
        public ActionResult CaptchaImage(string prefix, bool noisy = true)
        {
            Session["CaptchaImageText"] = LoginDB.GenerateRandomCode();
            CaptchaImage ci = new CaptchaImage(Session["CaptchaImageText"].ToString(), 280, 50, "Miriam");
            
            FileContentResult img = null;
            var mem = new MemoryStream();

            ci.Image.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
            img = this.File(mem.GetBuffer(), "image/Jpeg");

            return img;
        }

        [HttpPost]
        public ActionResult ValidateLogin(string userName, string password, string captcha)
        {
            try
            {   
                int ok = 0;
                string msg = "";

                string Status = "";

                MemberDB.GetLoginTime(out Status);

                if (Status == "NO")
                {
                    Response.Write(Resources.Member.WarningSystemMaintainance);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if(string.IsNullOrEmpty(userName))
                {
                    Response.Write(Resources.Member.warningUsername);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(password))
                {
                    Response.Write(Resources.Member.WarningPassword);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (captcha != Session["CaptchaImageText"].ToString())
                {
                    Response.Write(Resources.Member.msgInvalidCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                DataSet dsUser = MemberDB.GetMemberByUsername(userName, out ok, out msg);

                if (dsUser.Tables[0].Rows[0]["CMFS_ACCESSRIGHT"].ToString() == "True")
                {
                    Response.Write(Resources.Member.msgUserBlocked);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (dsUser.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.msgInvalidLogin);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string userPassword = dsUser.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString();
                if (Authentication.Encrypt(password) != userPassword)
                {
                    Response.Write(Resources.Member.msgInvalidLogin);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var isBlocked = !Convert.ToBoolean(dsUser.Tables[0].Rows[0]["CUSR_ACTIVE"]);
                if (isBlocked)
                {
                    Response.Write(Resources.Member.msgUserBlocked);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Session["CountryCode"] = dsUser.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                Session["Username"] = userName;

                //track member IP during login
                MemberDB.InsertUserOperation(userName, "Login", HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToString(), 0);

                //Login Successful
                //TODO: access right for the login user.
                //TODO: validate if login user is a member.
                string languageUsed = string.Empty;

                if (dsUser.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString() == "CN")
                {
                    languageUsed = "zh-CN";
                }
                else
                {
                    languageUsed = "en-US";
                }
                Session["LanguageChosen"] = languageUsed;
                               

                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", e.Message);
                return Json(new { success = false });
            }
        }

        [HttpPost]
        public ActionResult SendPassword(string userName, string userEmail)
        {
            try
            {
                DataSet dsUser = LoginDB.GetUserByUserName(userName);
                if (dsUser.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.msgInvalidMemberID);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string email = dsUser.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
                if (email != userEmail)
                {
                    Response.Write(Resources.Member.msgEmailNotMatch);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string userFirstname = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                string userPassword = dsUser.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString();
                string userPin = dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString();

                string mailSubject = Resources.Member.EmailSubject;

                string welcomeContent = string.Format("<html>" +
                                                        "<head>" +
                                                        "   <title></title>" +
                                                        "</head>" +
                                                        "<body>" +
                                                        "    <table>" +
                                                        //"        <tr>" +
                                                        //"            <td><img alt=\"\" src=cid:myImageID style=\"height: 113px; width: 274px\" /></td>" +
                                                        //"        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td>{0}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td>{1}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td>{2}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td>{3}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td>{4}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                         "        <tr>" +
                                                        "            <td>{5}</td>" +
                                                        "        </tr>" +
                                                        "        <tr>" +
                                                        "            <td></td>" +
                                                        "        </tr>" +
                                                        "    </table>" +
                                                        "</body>" +
                                                        "</html>",
                                                        Resources.Member.EmailContent1,
                                                        Resources.Member.EmailContent2, 
                                                        Resources.Member.EmailContent3 + " " + userName,
                                                        Resources.Member.EmailContent4 + " " + Authentication.Decrypt(userPassword),
                                                        Resources.Member.EmailContent6 + " " + Authentication.Decrypt(userPin),
                                                        Resources.Member.EmailContent5);

                string imagePath = string.Empty; //Path.Combine(Server.MapPath("~/Images/"), "Logo.jpg");

                string result = Misc.SendEmail("noreply@unionschain.com", email, mailSubject, welcomeContent);

                if (result == "Successfully")
                {
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Response.Write(result);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", e.Message);
                return Json(new { success = false });
            }
        }

        public ActionResult ForgetPassword()
        {
            SignUpModel model = new SignUpModel();

            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }
            int ok = 0;
            string msg = "";
            string languageCode = "en-us";
            string selectedcountry = "CN";

            #region Country

            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

            model.Countries = from c in countries
                              select new SelectListItem
                              {
                                  Text = c.CountryName,
                                  Value = c.CountryCode
                              };

            model.SelectedCountry = selectedcountry;
            DataSet dsCountrySetup = AdminGeneralDB.GetCountryByCountryCode(selectedcountry, out ok, out msg);

            model.MobileCountryCode = dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_MOBILECODE"].ToString();


            #endregion

            return PartialView("ForgetPassword", model);
        }


        #region Forget password

        public ActionResult MobileDeliver(SignUpModel model)
        {
            try
            {
                int ok = 0;
                string msg = "";

                #region Username
                if (string.IsNullOrEmpty(model.Username))
                {
                    Response.Write(Resources.Member.warningUsername);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Username.Length < 6)
                {
                    Response.Write(Resources.Member.WarningUsernameSixChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Username.Contains(" "))
                {
                    Response.Write(Resources.Member.warningNotAllowSpacing);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                var dr = AdminMemberDB.GetMemberByUserName(model.Username, out ok, out msg);

                if (dr.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.msgInvalidMemberID);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                #endregion

                #region Captcha

                if (Session["CaptchaImageText"] == null)
                {
                    Response.Write(Resources.Member.warningRefreshCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Captcha != Session["CaptchaImageText"].ToString())
                {
                    Response.Write(Resources.Member.msgInvalidCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                model.Phone = dr.Tables[0].Rows[0]["CUSR_CELLPHONE"].ToString();
                model.SelectedCountry = dr.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                
                #region Send SMS

                try
                {
                    var returnURL = string.Empty;
                    var content = string.Empty;
                    string cellPhone = Misc.PhoneParse(model.SelectedCountry.ToLower(), model.Phone);

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);

                    if (model.SelectedCountry.ToUpper() == "MY")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "RM0.00", Code);
                    }
                    else if (model.SelectedCountry.ToUpper() == "SG")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "ADV", Code);

                    }
                    else if (model.SelectedCountry.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);

                    }

                    int type = 1;

                    if (Session["LanguageChosen"].ToString() == "en-US")
                    {
                        type = 1;
                    }
                    else if (Session["LanguageChosen"].ToString() == "zh-CN")
                    {
                        type = 3;
                        content = System.Text.Encoding.BigEndianUnicode.GetBytes(content).Aggregate("", (agg, val) => agg + val.ToString("X2"));
                    }

                    returnURL = string.Format("http://www.bulksms2u.com/websmsapi/ISendSMS.aspx?username={0}&password={1}&message={2}&mobile={3}&Sender={4}&type={5}", "UnionSMS", "juta1515", content, cellPhone, "68886", type);

                    MemberLoginDB.InsertSMSLog(returnURL, "UnionSMS", "juta1515", cellPhone, model.SelectedCountry.ToUpper());

                    using (var client = new System.Net.WebClient())
                    {
                        client.Encoding = System.Text.Encoding.UTF8;
                        client.UploadString(returnURL, string.Empty);
                    }
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion

                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EmailDeliver(SignUpModel model)
        {
            try
            {
                int ok = 0;
                string msg = "";

                #region Username
                if (string.IsNullOrEmpty(model.Username))
                {
                    Response.Write(Resources.Member.warningUsername);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Username.Length < 6)
                {
                    Response.Write(Resources.Member.WarningUsernameSixChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Username.Contains(" "))
                {
                    Response.Write(Resources.Member.warningNotAllowSpacing);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var dr = AdminMemberDB.GetMemberByUserName(model.Username, out ok, out msg);

                if (dr.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.msgInvalidMemberID);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                #endregion

                #region Captcha

                if (Session["CaptchaImageText"] == null)
                {
                    Response.Write(Resources.Member.warningRefreshCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Captcha != Session["CaptchaImageText"].ToString())
                {
                    Response.Write(Resources.Member.msgInvalidCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                model.Email = dr.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
                model.SelectedCountry = dr.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
               
                #region Send Email

                try
                {
                    var Title = string.Empty;
                    var content = string.Empty;

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);


                    if (model.SelectedCountry.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);
                        Title = Resources.Member.lblEmailTitleTaiwan;

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);
                        Title = Resources.Member.lblEmailTitle;

                    }


                    string result = Misc.SendEmail("noreply@UnionsChain.com", model.Email, Title, content);
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion


                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult MobileAndEmailDeliver(SignUpModel model)
        {
            try
            {
                int ok = 0;
                string msg = "";

                #region Username
                if (string.IsNullOrEmpty(model.Username))
                {
                    Response.Write(Resources.Member.warningUsername);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

             

                if (model.Username.Contains(" "))
                {
                    Response.Write(Resources.Member.warningNotAllowSpacing);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var dr = AdminMemberDB.GetMemberByUserName(model.Username, out ok, out msg);

                if (dr.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.msgInvalidMemberID);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                #endregion

                #region Captcha

                if (Session["CaptchaImageText"] == null)
                {
                    Response.Write(Resources.Member.warningRefreshCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Captcha != Session["CaptchaImageText"].ToString())
                {
                    Response.Write(Resources.Member.msgInvalidCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                model.Email = dr.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
                model.SelectedCountry = dr.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                model.Phone = dr.Tables[0].Rows[0]["CUSR_CELLPHONE"].ToString();
                string Code = string.Empty;

                MemberLoginDB.VerificationCode(out Code);
                #region Send SMS

                try
                {
                    var returnURL = string.Empty;
                    var content = string.Empty;
                    string cellPhone = Misc.PhoneParse(model.SelectedCountry.ToLower(), model.Phone);

 

                    if (model.SelectedCountry.ToUpper() == "MY")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "RM0.00", Code);
                    }
                    else if (model.SelectedCountry.ToUpper() == "SG")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "ADV", Code);

                    }
                    else if (model.SelectedCountry.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);

                    }

                    int type = 1;

                    if (Session["LanguageChosen"].ToString() == "en-US")
                    {
                        type = 1;
                    }
                    else if (Session["LanguageChosen"].ToString() == "zh-CN")
                    {
                        type = 3;
                        content = System.Text.Encoding.BigEndianUnicode.GetBytes(content).Aggregate("", (agg, val) => agg + val.ToString("X2"));
                    }

                    returnURL = string.Format("http://www.bulksms2u.com/websmsapi/ISendSMS.aspx?username={0}&password={1}&message={2}&mobile={3}&Sender={4}&type={5}", "UnionSMS", "juta1515", content, cellPhone, "68886", type);

                    MemberLoginDB.InsertSMSLog(returnURL, "UnionSMS", "juta1515", cellPhone, model.SelectedCountry.ToUpper());

                    using (var client = new System.Net.WebClient())
                    {
                        client.Encoding = System.Text.Encoding.UTF8;
                        client.UploadString(returnURL, string.Empty);
                    }
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion

                #region Send Email

                try
                {
                    var Title = string.Empty;
                    var content = string.Empty;


                    if (model.SelectedCountry.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);
                        Title = Resources.Member.lblEmailTitleTaiwan;

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);
                        Title = Resources.Member.lblEmailTitle;

                    }


                    string result = Misc.SendEmail("noreply@UnionsChain.com", model.Email, Title, content);
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion


                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ResetPassword(SignUpModel model)
        {
            try
            {

                if (string.IsNullOrEmpty(model.VerficationCode))
                {
                    Response.Write("Invalid Verification Code");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                DataSet DSValification = MemberLoginDB.ValidVerificationCode(model.VerficationCode);

                if (DSValification.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Member.warningVerificationCodeExpired);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                DataSet logo = AdminGeneralDB.GetCompanySetup();
                foreach (DataRow dr in logo.Tables[0].Rows)
                {
                    var IFile = new ImageFile();
                    IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                    ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
                }

                Session["Username"] = model.Username;
                return PartialView("ResetPassword", model);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ResetPasswordMethod(SignUpModel cpm)
        {
            try
            {

            if (cpm.NewPassword == null)
            {
                Response.Write(Resources.Admin.msgReqNewPwd);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            if (cpm.RetypeNewPassword == null)
            {
                Response.Write(Resources.Admin.msgReqConfirmNewPwd);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (cpm.NewPassword != cpm.RetypeNewPassword)
            {
                Response.Write(Resources.Admin.msgPwdNotMatch);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

                if (cpm.Username == null)
                {
                    cpm.Username = Session["Username"].ToString();
                }

            int ok;
            string msg;
            MemberLoginDB.ResetPassword("SP_MemberResetPassword", cpm.Username,  Authentication.Encrypt(cpm.NewPassword), cpm.Username, out ok, out msg);

            return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion


        public ActionResult TermsAndCondition()
        {

            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }

            return PartialView("TermsAndCondition");
        }


        [HttpPost]
        public JsonResult findMobileCode(string selectedcountry)
        {
            int ok = 0;
            string msg = string.Empty;
            string SelectedCountryName = string.Empty;
            string MobileCountryCode = string.Empty;
            string languageCode = "en-us";

            if (string.IsNullOrEmpty(selectedcountry))
            {
                Response.Write(Resources.Member.warningSelectCountry);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);
            SelectedCountryName = countries.Find(item => item.CountryCode == selectedcountry).CountryName;

            DataSet dsCountrySetup = AdminGeneralDB.GetCountryByCountryCode(selectedcountry, out ok, out msg);
            MobileCountryCode = dsCountrySetup.Tables[0].Rows[0]["CMULTILANGCOUNTRY_NAME"].ToString() + " (" + dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_MOBILECODE"].ToString() + ") ";


            var result = MobileCountryCode;
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        [HttpPost]
        public JsonResult findSponsor(string memberID)
        {
            int ok = 0;
            string msg = string.Empty;

            if (string.IsNullOrEmpty(memberID))
            {
                Response.Write(Resources.Admin.warningInvalidUsername2);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            var dsMember = MemberDB.GetMemberByUsername(memberID, out ok, out msg);

            if (dsMember.Tables[0].Rows.Count == 0)
            {
                Response.Write(Resources.Admin.warningInvalidUsername2);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            var result = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        #region Check Email
        [HttpPost]
        public JsonResult CheckEmail(string Email)
        {
            var result = false;
            Regex reg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

            if (reg.IsMatch(Email))
            {
                result = true;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion      

    }
}