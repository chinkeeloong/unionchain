﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Globalization;
using System.IO;
using iTextSharp.text.pdf;
using System.Net;
using ECFBase.Helpers;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using ECFBase.ThirdParties;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace ECFBase.Controllers.Member
{
    public class MemberController : Controller
    {
        #region Home
        public ActionResult Home(string username, int selectedPage = -1, bool fresh = true, string type = "")
        {
            int ok = 0;
            string msg = "";

            if (!string.IsNullOrEmpty(username) && Session["Admin"] != null)
            {
                Session["Username"] = username;
            }

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            string languageUsed = string.Empty;
            

            var dsMember = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);

            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["LanguageChosen"].ToString());
            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["LanguageChosen"].ToString());

            MemberModel model = new MemberModel();
      
                var DS = AdminInfoDeskDB.GetAllImage();
                foreach (DataRow dr in DS.Tables[0].Rows)
                {
                    var IFile = new ImageFile();
                    IFile.FileName = dr["CINF_IMAGEPATH"].ToString();

                    model.ImageList.Add(IFile);
                }

                int pages = 0;
            


                DataSet logo = AdminGeneralDB.GetCompanySetup();
                foreach (DataRow dr in logo.Tables[0].Rows)
                {
                    var IFile = new ImageFile();
                    IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                    ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
                }


                var dsLog = MemberDB.GetMemberOperationLog(Session["Username"].ToString());

            

                string RankCode = dsMember.Tables[0].Rows[0]["CRANK_CODE"].ToString();
                string Package = dsMember.Tables[0].Rows[0]["CPKG_CODE"].ToString();

                if (dsLog.Tables[0].Rows.Count == 0)
                {
                    model.OperationLogShow = false;
                }
                else
                {
                    model.OperationLogShow = true;
                    string Name = Session["Username"].ToString();
                
                    string ActiveDate = DateTime.Parse(dsLog.Tables[0].Rows[0]["CUSEROPLOG_CREATEDON"].ToString()).ToString("dd-MM-yyyy");
                    string FullName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                    model.OperationLog = string.Format(Resources.Member.msgWELCOME, FullName, ActiveDate);

                }

                model.FirstName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                model.JoinedDateString = dsMember.Tables[0].Rows[0]["CMEM_CREATEDON"].ToString();
                model.Rank = Misc.GetMemberRanking(dsMember.Tables[0].Rows[0]["CRANK_CODE"].ToString());
                model.Phone = dsMember.Tables[0].Rows[0]["CUSR_CELLPHONE"].ToString();



                model.Username = Session["Username"].ToString();
                model.MemberSelectedLanguage = languageUsed;
                model.InvoiceID = dsMember.Tables[0].Rows[0]["CINVNO_ID"].ToString();

                var languageList = Misc.GetLanguageList(ref ok, ref msg);
                model.LanguageList = from c in languageList
                                     select new SelectListItem
                                     {
                                         Selected = (c.Value == languageUsed) ? true : false,
                                         Text = c.Text,
                                         Value = c.Value
                                     };

                if (dsMember.Tables[0].Rows.Count != 0)
                {
                    Session["CountryCode"] = dsMember.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                    Session["MemberCurrency"] = dsMember.Tables[0].Rows[0]["CCURRENCY_CODE"].ToString();   
                    model.MemberCurrency = dsMember.Tables[0].Rows[0]["CCURRENCY_NAME"].ToString();          
                    model.JoinedDate = (DateTime)dsMember.Tables[0].Rows[0]["CMEM_CREATEDON"];
                    model.FirstName = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                    model.MemberType = dsMember.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();                   
                    model.ACommunity = dsMember.Tables[0].Rows[0]["CMTREE_GROUPAYJ"].ToString();
                    model.BCommunity = dsMember.Tables[0].Rows[0]["CMTREE_GROUPBYJ"].ToString();
                    model.AvailableUNS = dsMember.Tables[0].Rows[0]["CMEM_TRADEABLEWALLET"].ToString();
                    
                    decimal Total = decimal.Parse(dsMember.Tables[0].Rows[0]["CMEM_TRADEABLEWALLET"].ToString()) + decimal.Parse(dsMember.Tables[0].Rows[0]["CMEM_UNTRADEABLEWALLET"].ToString());
                    if (Total >= 300)
                    {
                        model.CurrentTokenFlag = "No";
                    }
                    else
                    {
                        model.CurrentTokenFlag = "Yes";
                    }

                    model.FrozenUNS = dsMember.Tables[0].Rows[0]["CMEM_UNTRADEABLEWALLET"].ToString();
                    model.WalletAddress = dsMember.Tables[0].Rows[0]["CMEM_WALLET_ADDRESS"].ToString();
                    model.Rank = Misc.GetMemberRanking(dsMember.Tables[0].Rows[0]["CRANK_CODE"].ToString());
                    Session["IsActivated"] = (dsMember.Tables[0].Rows[0]["CPKG_CODE"].ToString() == "FREE001") ? 0 : 1;                
                }

                var dsMemberBonus = MemberDB.GetMemberBonusByUsername(Session["Username"].ToString(), out ok, out msg);
                if (dsMemberBonus.Tables[0].Rows.Count != 0)
                {
                    if (dsMemberBonus.Tables[0].Rows[0]["CMB_STATUS"].ToString() == "Receive")
                    {
                        model.HashRate = "0";
                        model.HashPower = "0";
                        model.HashPerformance = "0";
                    }
                    else {
                        model.HashRate = dsMemberBonus.Tables[0].Rows[0]["CMB_HASH"].ToString();
                        model.HashPower = dsMemberBonus.Tables[0].Rows[0]["CMB_PERFORMANCE_HASH"].ToString();
                        model.HashPerformance = dsMemberBonus.Tables[0].Rows[0]["CMB_MANAGEMENT_HASH"].ToString();
                    }                    
                }
                else
                {
                    model.HashRate = "0";
                    model.HashPower = "0";
                    model.HashPerformance = "0";
                }

            DataSet DSTradingInformation = MemberDB.GetTradingInformation();
            model.NewMessage = 1;
            model.TotalMessages = 100;
            model.UNSPrice = DSTradingInformation.Tables[0].Rows[0]["CTR_RATE"].ToString();
            model.LastPrice = DSTradingInformation.Tables[1].Rows[1]["CTR_RATE"].ToString();
            model.TotalValue =  (Math.Round(((float.Parse(model.AvailableUNS) + float.Parse(model.FrozenUNS)) * float.Parse(model.UNSPrice)),4)).ToString();

            model.TodayPriceDifferent = "0.0000";
            model.LastPriceDifferent = (decimal.Parse(DSTradingInformation.Tables[0].Rows[0]["CTR_RATE"].ToString()) - decimal.Parse(DSTradingInformation.Tables[1].Rows[1]["CTR_RATE"].ToString())).ToString("f4");


                #region InfoDesk


                    var dsEventsPromo = AdminInfoDeskDB.GetAllInfoDesk("E", Session["LanguageChosen"].ToString(), 1, out pages, out ok, out msg);
                    for (var i = 0; i < dsEventsPromo.Tables[0].Rows.Count && i < 3; i++)
                    {
                        var item = new InfoDeskModel
                        {
                            InfoDeskTitle = dsEventsPromo.Tables[0].Rows[i]["CMULTILANGINFODESK_TITLE"].ToString(),
                            InfoDeskID = int.Parse(dsEventsPromo.Tables[0].Rows[i]["CINF_ID"].ToString()),
                            InfoDeskContent = dsEventsPromo.Tables[0].Rows[i]["CMULTILANGINFODESK_CONTENT"].ToString(),
                            IsNewInfo = Convert.ToDateTime(dsEventsPromo.Tables[0].Rows[i]["CINF_CREATEDON"]).AddDays(14) > DateTime.Now,
                            Created = Convert.ToDateTime(dsEventsPromo.Tables[0].Rows[i]["CINF_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt")
                        };

                        model.EventAndPromotions.Add(item);
                    }
                    string bB = string.Empty;
                    var dsCorporateNews = AdminInfoDeskDB.GetAllInfoDesk("C", Session["LanguageChosen"].ToString(), 1, out pages, out ok, out msg);

                    for (var i = 0; i < dsCorporateNews.Tables[0].Rows.Count && i < 5; i++)
                    {
                        bB = dsCorporateNews.Tables[0].Rows[i]["CINF_CODE"].ToString();
                        var item = new InfoDeskModel
                        {
                            InfoDeskTitle = dsCorporateNews.Tables[0].Rows[i]["CMULTILANGINFODESK_TITLE"].ToString(),
                            InfoDeskID = int.Parse(dsCorporateNews.Tables[0].Rows[i]["CINF_ID"].ToString()),
                            InfoDeskContent =  dsCorporateNews.Tables[0].Rows[i]["CMULTILANGINFODESK_CONTENT"].ToString(),
                            IsNewInfo = Convert.ToDateTime(dsCorporateNews.Tables[0].Rows[i]["CINF_CREATEDON"]).AddDays(14) > DateTime.Now,
                            Created = Convert.ToDateTime(dsCorporateNews.Tables[0].Rows[i]["CINF_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"),
                            A = dsCorporateNews.Tables[0].Rows[i]["CINF_CODE"].ToString()
                        };
                        List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
                        DataSet dsMultiLanguageInfoDesk = AdminInfoDeskDB.GetMultiLanguageInfoDeskByID(item.InfoDeskID, out ok, out msg);


                        foreach (SelectListItem language in languages)
                        {
                            DataRow[] dr = dsMultiLanguageInfoDesk.Tables[0].Select("CINF_ID='" + item.InfoDeskID + "' AND CLANG_CODE='" + language.Value + "'");
                            int extensionIndex = dr[0]["CINF_IMAGEPATH"].ToString().LastIndexOf('\\');

                            string imageName = dr[0]["CINF_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                            item.Image = "http://" + Request.Url.Authority + "/Products/" +bB + "/" + imageName;
                        }

                        model.CorporateNews.Add(item);
                    }

                    var dsMyNews = AdminHelpDeskDB.GetAllNoticeByUsername(Session["Username"].ToString(), Session["LanguageChosen"].ToString(), 1, out pages, out ok, out msg);
                    for (var i = 0; i < dsMyNews.Tables[0].Rows.Count && i < 3; i++)
                    {
                        var item = new InfoDeskModel
                        {
                            InfoDeskTitle = dsMyNews.Tables[0].Rows[i]["CMULTILANGNOT_TITLE"].ToString(),
                            InfoDeskID = int.Parse(dsMyNews.Tables[0].Rows[i]["CNOT_ID"].ToString()),
                            InfoDeskContent = dsMyNews.Tables[0].Rows[i]["CMULTILANGNOT_CONTENT"].ToString(),
                            IsNewInfo = Convert.ToDateTime(dsMyNews.Tables[0].Rows[i]["CNOT_CREATEDON"]).AddDays(14) > DateTime.Now,
                            Created = Convert.ToDateTime(dsMyNews.Tables[0].Rows[i]["CNOT_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt")
                        };

                        model.MyNews.Add(item);
                    }


                    #endregion

                #region SeasionArea

                if (Session["page"] != null)
                {
                    ViewBag.page = Session["page"];
                    Session["page"] = null;
                }

                if (Session["selectedPage"] != null)
                {
                    ViewBag.selectedPage = Session["selectedPage"];
                    Session["selectedPage"] = null;
                }

                if (Session["walletType"] != null)
                {
                    ViewBag.walletType = Session["walletType"];
                    Session["walletType"] = null;
                }

                if (Session["date"] != null)
                {
                    ViewBag.date = Session["date"];
                    Session["date"] = null;
                }

                if (Session["searchUsername"] != null)
                {
                    ViewBag.searchUsername = Session["searchUsername"];
                    Session["searchUsername"] = null;
                }

                ViewBag.ReferenceID = dsMember.Tables[0].Rows[0]["CMEM_PERSONAL_ID"].ToString();

                ViewBag.WalletAddress = dsMember.Tables[0].Rows[0]["CMEM_WALLET_ADDRESS"].ToString() + dsMember.Tables[0].Rows[0]["CMEM_PERSONAL_ID"].ToString();


            ViewBag.PreMember = "No";
            #endregion

            string ExchangeFlag = string.Empty;

            MemberDB.GetTime(out ExchangeFlag);

            if (ExchangeFlag == "YES")
            {
                model.ExchangeFlag = "YES";
            }
            else if (ExchangeFlag == "NO")
            {
                model.ExchangeFlag = "NO";
            }


            var drMem = MemberProfileDB.GetMemberVerificationByUserName(Session["Username"].ToString(), out ok, out msg);
            if (drMem.Tables[0].Rows.Count != 0)
            {
                model.Username = drMem.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                model.FullName = drMem.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                model.Status = drMem.Tables[0].Rows[0]["CMEMVER_STATUS"].ToString();
                if(model.Status != "")
                {
                    model.RealnameUpdatedDate = Convert.ToDateTime(drMem.Tables[0].Rows[0]["CMEMVER_UPDATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                }                

            }


            var dsWallet = MemberWalletDB.GetHashStatementByUsername(Session["Username"].ToString(), type, -1, out pages);
            if (dsWallet.Tables[0].Rows.Count != 0)
            {
                    foreach (DataRow dr in dsWallet.Tables[0].Rows)
                    {
                            var records = new HashWalletModel();
                            records.Number = Convert.ToInt32(dr["rownumber"].ToString());
                            records.Username = dr["CUSR_USERNAME"].ToString();
                            
                            if (float.Parse(dr["CASHIN"].ToString()) != 0)
                            {
                                records.TransactionType = "+" ;
                                records.CashIN = dr["CASHIN"].ToString();
                            }
                            else if(float.Parse(dr["CASHOUT"].ToString()) != 0)
                            {
                                records.TransactionType = "-" ;
                                records.CashIN = dr["CASHOUT"].ToString();
                            }
                            //records.CashOUT = float.Parse(dr["CASHOUT"].ToString());
                            records.Remarks = Misc.GetReadableCashName(dr["UNIONTYPE"].ToString()) + " : " + dr["WALLETBALANCE"].ToString();
                            records.CreatedDate = Convert.ToDateTime(dr["CREATEDDATE"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt");
                            records.UnionType = Misc.GetReadableCashName(dr["CASHNAME"].ToString());
                        model.WalletLogList.Add(records);
                       
                    }
                
            }

            ViewBag.Chinese = "中文";
            ViewBag.English = "English";
            return View("Home", model);
        }
        #endregion

        #region Recommendation Structure
        public ActionResult RecommendationStructure(string memberUsername, int? memberLevel, int selectedpage = 1)
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { ReloadPage = "Yes", Expired = "Yes" });
            }

            string username;
            if (memberUsername == null)
            {
                username = Session["Username"].ToString();
                ViewBag.MemberLevel = 1;
                ViewBag.NextLevel = 4;
                Session["PreviousView"] = null;
            }
            else
            {
                username = memberUsername;
                ViewBag.MemberLevel = memberLevel;
                ViewBag.NextLevel = memberLevel + 3;
            }

            //Store Previous username and level
            var userNameList = new List<String>();
            if (Session["PreviousView"] == null)
            {
                userNameList.Add(username + ";" + ViewBag.MemberLevel);
                Session["PreviousView"] = userNameList;
            }
            else
            {
                userNameList = (List<string>)Session["PreviousView"];
                userNameList.Add(memberUsername + ";" + memberLevel);
                Session["PreviousView"] = userNameList;
            }

            var model = new MemberSponsorChartModel();
            int ok;
            string msg;
            DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChartVer1(username, out ok, out msg);
            ViewBag.MemberCount = 0;

            var member = MemberDB.GetMemberByUsername(username, out ok, out msg);
            model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
            model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
            model.FirstLevelJoinedDate = Convert.ToDateTime(dsNetworkTree.Tables[0].Rows[0]["DATE"]).ToString("dd/MM/yyyy");

            model.FirstLevelRanking = Misc.GetMemberRanking(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());

            if (dsNetworkTree.Tables[0].Rows[0]["UPGRADEDATE"].ToString() != "")
            {
                model.FirstLevelRanking += string.Format(" (Upgrade:{0})", DateTime.Parse(dsNetworkTree.Tables[0].Rows[0]["UPGRADEDATE"].ToString()).ToShortDateString());
            }

            model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();

            model.FirstLevelIntro = string.Format("{0} ({1})", model.FirstLevelIntro, dsNetworkTree.Tables[0].Rows[0]["GROUPSALES"].ToString());

            model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();
            model.FirstLevelRankIcon = Misc.GetMemberRankingIcon(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());

            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                MemberList resultList = new MemberList();
                resultList.Level = dr["LEVEL"].ToString();
                resultList.Member = dr["USERNAME"].ToString();
                resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());

                if (dr["UPGRADEDATE"].ToString() != "")
                {
                    model.Ranking += string.Format(" (Upgrade:{0})", DateTime.Parse(dr["UPGRADEDATE"].ToString()).ToShortDateString());
                }

                var JoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
                resultList.JoinedDate = Convert.ToDateTime(JoinedDate).ToString("dd/mm/yyyy");

                resultList.FullName = dr["FULLNAME"].ToString();
                resultList.Intro = dr["INTRO"].ToString();

                model.Intro = string.Format("{0} ({1})", model.Intro, dr["GROUPSALES"].ToString());

                resultList.RankIcon = Misc.GetMemberRankingIcon(dr["RANK"].ToString());
                resultList.MemberCount = dr["MEMBERCOUNT"].ToString();
                resultList.GoingToExpended = int.Parse(dr["LEVEL"].ToString()) % 3 == 0;
                model.resultList.Add(resultList);
            }

            DataSet logo = AdminGeneralDB.GetRankIcon();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["CRANKSET_ICON"].ToString();
                RankModel RankList = new RankModel();
                RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
                RankList.RankName = dr["CRANKSET_NAME"].ToString();
                ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                model.RankList.Add(RankList);
            }

            return PartialView("RecommendationStructure" , model);

        }

        [HttpPost]
        public JsonResult FindNextRecommendationStructure(string username, int level)
        {
            int ok = 0;
            string msg = "";

            DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChartVer1Fixed2Level(username, out ok, out msg);
            ViewBag.MemberCount = 0;

            var member = MemberDB.GetMemberByUsername(username, out ok, out msg);
            List<MemberList> list = new List<MemberList>();

            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                var resultList = new MemberList();
                //resultList.Level = dr["LEVEL"].ToString();
                resultList.Level = string.Format("{0}", level + 1);
                resultList.Member = dr["USERNAME"].ToString();
                resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());

                if (dr["UPGRADEDATE"].ToString() != "")
                {
                    resultList.Ranking += string.Format(" (Upgrade:{0})", DateTime.Parse(dr["UPGRADEDATE"].ToString()).ToShortDateString());
                }

                resultList.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToShortDateString();


                var Date = dr["DATE"].ToString();
                //resultList.JoinedDate = Convert.ToDateTime(Date).ToString("dd/mm/yyyy");
                resultList.JoinedDate = Convert.ToDateTime(dr["DATE"]).ToString("dd/MM/yyyy");


                resultList.FullName = dr["FULLNAME"].ToString();
                resultList.Intro = dr["INTRO"].ToString();

                resultList.Intro = string.Format("{0} ({1})", resultList.Intro, dr["GROUPSALES"].ToString());

                resultList.RankIcon = Misc.GetMemberRankingIcon(dr["RANK"].ToString());
                resultList.MemberCount = dr["MEMBERCOUNT"].ToString();
                resultList.GoingToExpended = int.Parse(dr["LEVEL"].ToString()) % 3 == 0;

                if (dr["LEVEL"].ToString() == "2")
                {
                    list.Add(resultList);
                }
            }

            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Personal Team
        public ActionResult PersonalTeam()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0, pages = 0;
            string msg = "";
            var member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            string TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();

            

            MarketTreeModel memberMarketTree = GetAllMarketTreeMember(TopMember);
            memberMarketTree.TotalDirectSponsor = member.Tables[1].Rows[0]["CMEM_TOTAL_DIRECT_SPONSOR"].ToString();

            return PartialView("PersonalTeam", memberMarketTree);

        }

        private MarketTreeModel GetAllMarketTreeMember(string TopMember)
        {
            MarketTreeModel treeMC = new MarketTreeModel();

            int nCount = 0;
            string fullName = "";

            DataSet dsFollow = MemberDB.GetFollowID(TopMember);

            //Loop for 15 times
            while (nCount <= 14)
            {
                if (dsFollow.Tables[nCount].Rows.Count == 0)
                {
                    treeMC.MemberList.Add("0");
                    treeMC.LogoList.Add("~");
                    treeMC.TooltipList.Add(ConstructTooltipTree("", "", "0", "0", "0", "0", "", ""));
                    treeMC.PackageList.Add("");
                    treeMC.DateList.Add("");
                    treeMC.AccYJLeftList.Add("");
                    treeMC.AccYJRightList.Add("");
                    treeMC.CFBalLeftList.Add("");
                    treeMC.CFBalRightList.Add("");
                    treeMC.SalesLeftList.Add("");
                    treeMC.SalesRightList.Add("");
                    treeMC.TotalDownlineLeft.Add("");
                    treeMC.TotalDownlineRight.Add("");
                    treeMC.Balance.Add("");

                    if (nCount == 0)
                    {
                        treeMC.MainLeftYJ = "0";
                        treeMC.MainLeftBalance = "0";
                        treeMC.MainRightYJ = "0";
                        treeMC.MainRightBalance = "0";
                    }
                }
                else
                {
                    DataRow dr = dsFollow.Tables[nCount].Rows[0];
                    DateTime joinedDate = DateTime.Parse(dr["CUSR_CREATEDON"].ToString());
                    fullName = string.Format("{0}", dr["CUSR_FULLNAME"].ToString());
                    treeMC.MemberList.Add(dr["CUSR_USERNAME"].ToString());

                    treeMC.TooltipList.Add(
                        ConstructTooltipTree(
                            fullName, joinedDate.ToShortDateString(), dr["CMTREE_GROUPAYJ"].ToString(), dr["CMTREE_GROUPBYJ"].ToString(), 
                            dr["CMTREE_GROUPAGP"].ToString(), dr["CMTREE_GROUPBGP"].ToString(), dr["CMEM_INTRO"].ToString(), 
                            string.Format("人民币{0}", dr["CPKG_AMOUNT"].ToString())
                            )
                         );

                    treeMC.DateList.Add(string.Format("{0}", joinedDate.ToString("dd/MM/yyyy")));


                    float leftYJ1 = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                    float rightYJ1 = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                    float leftGP1 = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                    float rightGP1 = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                    float leftsales1 = float.Parse(dr["LEFTSALES"].ToString());
                    float rightsales1 = float.Parse(dr["RIGHTSALES"].ToString());
                    int lefttotaldownline1 = Convert.ToInt32(dr["CMTREE_TGROUPAMEMBER"].ToString());
                    int righttotaldownline1 = Convert.ToInt32(dr["CMTREE_TGROUPBMEMBER"].ToString());
                    float Balance = float.Parse(dr["Balance"].ToString());

                    treeMC.AccYJLeftList.Add(string.Format("{0}", Helper.NVL(leftYJ1.ToString("###,##0"))));
                    treeMC.AccYJRightList.Add(string.Format("{0}", Helper.NVL(rightYJ1.ToString("###,##0"))));
                    treeMC.CFBalLeftList.Add(string.Format("{0}", Helper.NVL(leftGP1.ToString("###,##0"))));
                    treeMC.CFBalRightList.Add(string.Format("{0}", Helper.NVL(rightGP1.ToString("###,##0"))));
                    treeMC.SalesLeftList.Add(string.Format("{0}", Helper.NVL(leftsales1.ToString("###,##0"))));
                    treeMC.SalesRightList.Add(string.Format("{0}", Helper.NVL(rightsales1.ToString("###,##0"))));
                    treeMC.TotalDownlineLeft.Add(string.Format("{0}", Helper.NVL(lefttotaldownline1.ToString())));
                    treeMC.TotalDownlineRight.Add(string.Format("{0}", Helper.NVL(righttotaldownline1.ToString())));
                    treeMC.Balance.Add(string.Format("{0}", Helper.NVL(Balance.ToString("###,###0"))));


                    if (nCount == 0)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());

                        treeMC.MainLeftYJ = leftYJ.ToString("###,##0");
                        treeMC.MainRightYJ = rightYJ.ToString("###,##0");

                        treeMC.MainLeftBalance = leftGP.ToString("###,##0");
                        treeMC.MainRightBalance = rightGP.ToString("###,##0");

                        treeMC.MainLeftSales = leftsales.ToString("###,##0");
                        treeMC.MainRightSales = rightsales.ToString("###,##0");
                    }
                    else if (nCount == 1)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());

                        treeMC.FirstLeftYJ = leftYJ.ToString("###,##0");
                        treeMC.FirstRightYJ = rightYJ.ToString("###,##0");

                        treeMC.FirstLeftBalance = leftGP.ToString("###,##0");
                        treeMC.FirstRightBalance = rightGP.ToString("###,##0");

                        treeMC.FirstLeftSales = leftsales.ToString("###,##0");
                        treeMC.FirstRightSales = rightsales.ToString("###,##0");
                    }
                    else if (nCount == 2)
                    {
                        float leftYJ = float.Parse(dr["CMTREE_GROUPAYJ"].ToString());
                        float rightYJ = float.Parse(dr["CMTREE_GROUPBYJ"].ToString());
                        float leftGP = float.Parse(dr["CMTREE_GROUPAGP"].ToString());
                        float rightGP = float.Parse(dr["CMTREE_GROUPBGP"].ToString());
                        float leftsales = float.Parse(dr["LEFTSALES"].ToString());
                        float rightsales = float.Parse(dr["RIGHTSALES"].ToString());
                        int lefttotaldownline = Convert.ToInt32(dr["CMTREE_TGROUPAMEMBER"].ToString());
                        int righttotaldownline = Convert.ToInt32(dr["CMTREE_TGROUPBMEMBER"].ToString());

                        treeMC.SecondLeftYJ = leftYJ.ToString("###,##0");
                        treeMC.SecondRightYJ = rightYJ.ToString("###,##0");

                        treeMC.SecondLeftBalance = leftGP.ToString("###,##0");
                        treeMC.SecondRightBalance = rightGP.ToString("###,##0");

                        treeMC.SecondLeftSales = leftsales.ToString("###,##0");
                        treeMC.SecondRightSales = rightsales.ToString("###,##0");

                        treeMC.SecondLeftTotalDownline = lefttotaldownline.ToString("###,##0");
                        treeMC.SecondRightTotalDownline = righttotaldownline.ToString("###,##0");
                    }
                }

                nCount++;
            }

            return treeMC;
        }

        #endregion

        #region Payment Code
        public ActionResult PaymentCode()
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0; string msg = string.Empty;

            var dsMember = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
            if(dsMember.Tables[0].Rows.Count != 0)
            {
                ViewBag.ReferenceIDA = dsMember.Tables[0].Rows[0]["CMEM_PERSONAL_ID"].ToString() + "1";
                ViewBag.ReferenceIDB = dsMember.Tables[0].Rows[0]["CMEM_PERSONAL_ID"].ToString() + "2";
                ViewBag.WalletAddress = dsMember.Tables[0].Rows[0]["CMEM_WALLET_ADDRESS"].ToString() + dsMember.Tables[0].Rows[0]["CMEM_PERSONAL_ID"].ToString();
                ViewBag.PositionA = "A";
                ViewBag.PositionB = "B";
            }
            else
            {
                Response.Write(Resources.Member.messageError);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            


            return PartialView("PaymentCode");

        }

        #endregion

        #region ChangeLanguage

        public ActionResult ChangeLanguage(string languageCode)
        {
            Session["LanguageChosen"] = languageCode;
            var username = Session["Username"] == null ? string.Empty : Session["Username"].ToString();
            return Home(username,-1,false);
        }

        #endregion

        #region MemberLogin
        public ActionResult MemberLogin(string selectedLanguage = null, bool reloadPage = false, string Activate = "", string ExpiredSession = "")
        {
            if (reloadPage)
                ViewBag.ReloadPage = "Y";

            if (!string.IsNullOrEmpty(selectedLanguage))
            {
                Session["LanguageChosen"] = selectedLanguage;
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["LanguageChosen"].ToString());
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["LanguageChosen"].ToString());
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
            }

            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }


            //Clear all session except selected language.
            string language = Session["LanguageChosen"].ToString();
            Session.RemoveAll();
            Session["LanguageChosen"] = language;
            ViewBag.Language = language;
            ViewBag.Activate = Activate;
            ViewBag.SessionExpired = ExpiredSession;
            ViewBag.ReloadPage = reloadPage;
            return View();
        }

        public ActionResult TermsAndCondition(string selectedLanguage = null, bool reloadPage = false, string Activate = "", string ExpiredSession = "")
        {
            if (reloadPage)
                ViewBag.ReloadPage = "Y";

            if (!string.IsNullOrEmpty(selectedLanguage))
            {
                Session["LanguageChosen"] = selectedLanguage;
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["LanguageChosen"].ToString());
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["LanguageChosen"].ToString());
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
            }

            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }


            //Clear all session except selected language.
            string language = Session["LanguageChosen"].ToString();
            Session.RemoveAll();
            Session["LanguageChosen"] = language;
            ViewBag.Language = language;
            ViewBag.Activate = Activate;
            ViewBag.SessionExpired = ExpiredSession;
            ViewBag.ReloadPage = reloadPage;

            if (Session["LanguageChosen"].ToString() == "zh-CN")
            {
                return View("TermsAndConditionCN");
            }
            else
            {
                return View("TermsAndCondition");
            }
           
        }
        #endregion

        #region PINValidation
        public ActionResult PINValidation(string Type)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
            }

            if (Type == ProjectStaticString.EditProfile)
            {
                ViewBag.MainModule = ECFBase.Resources.Member.mnuMember;
                ViewBag.SubModule = ECFBase.Resources.Member.mnuProfile;
                ViewBag.TypeResource = ECFBase.Resources.Member.lblEditProfile;
            }
            else if (Type == ProjectStaticString.BankAccInfo)
            {
                ViewBag.MainModule = ECFBase.Resources.Member.mnuMember;
                ViewBag.SubModule = ECFBase.Resources.Member.mnuProfile;
                ViewBag.TypeResource = ECFBase.Resources.Member.lblChgBankInfo;
            }

            ViewBag.Type = Type;
            PINValidationModel model = new PINValidationModel();
            return PartialView("PINValidation", model);
        }

        public ActionResult ValidatePIN(string PIN, string Type)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
            }

            int ok;
            string msg;

            //MemberDB.ValidateUserPIN(Session["Username"].ToString(), Authentication.Encrypt(PIN), out ok, out msg);
             MemberDB.ValidateUserPIN(Session["Username"].ToString(), Authentication.Encrypt(PIN), out ok, out msg);
            if (ok == 0)
            {
                Response.Write(Resources.Member.msgInvalidPIN);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            MemberDB.ValidatePreMemberPIN(Session["Username"].ToString(), Authentication.Encrypt(PIN), out ok, out msg);

            if (ok == 0)
                {
                    Response.Write(Resources.Member.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            
          

            if (Type == ProjectStaticString.EditProfile)
            {
                //redirect to Controller
                return RedirectToAction("EditProfile", "MemberProfile");
            }
            else if (Type == ProjectStaticString.BankAccInfo)
            {
                return RedirectToAction("BankAccInfo", "MemberProfile");
            }

            return Home(Session["Username"].ToString());
        }
        #endregion
        
        #region Binary List

        //public ActionResult BinaryListing(int selectedPage = 1)
        //{
        //try
        //{
        //        if (Session["Username"] == null || Session["Username"].ToString() == "")
        //        {
        //            return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
        //        }

        //        int pages = 0;
        //        PaginationIntroListModel model = new PaginationIntroListModel();
        //        DataSet dsBinaryList = MemberShopCenterDB.GetAllBinaryListByUsername(Session["Username"].ToString(), selectedPage, out pages);

        //        selectedPage = BinaryConstructPageList(selectedPage, pages, model);

        //        foreach (DataRow dr in dsBinaryList.Tables[0].Rows)
        //        {
        //            IntroListModel sponsorlist = new IntroListModel();
        //            sponsorlist.No = dr["rownumber"].ToString();
        //            sponsorlist.Username = dr["CUSR_USERNAME"].ToString();
        //            sponsorlist.FullName = dr["CUSR_FIRSTNAME"].ToString();
        //            sponsorlist.Sponsor = dr["CMTREE_UNDER"].ToString();
        //            sponsorlist.Upline = dr["CMTREE_UPMEMBER"].ToString();
        //            sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
        //            sponsorlist.Rank = Misc.RankNumber(dr["CRANK_CODE"].ToString());
        //            sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
        //            model.SponsorList.Add(sponsorlist);
        //        }

        //        return PartialView("BinaryListing", model);
//    }
//            catch (Exception e)
//            {

//                Response.Write(e.Message);
//                return Json(string.Empty, JsonRequestBehavior.AllowGet);
//}
//}

private int BinaryConstructPageList(int selectedPage, int pages, MarketTreeModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            if (pages == 0)
            {
                pageList.Add(1);
            }
            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        #endregion

        #region MarketTree

        //[HttpPost]
        //public ActionResult MarketTree(int selectedPage = 1)
        //{
        //    if (Session["Username"] == null || Session["Username"].ToString() == "")
        //    {
        //        return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
        //    }

        //    int ok = 0, pages = 0;
        //    string msg = "";
        //    var member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
        //    string TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();


        //    MarketTreeModel memberMarketTree = GetAllMarketTreeMember(TopMember);

        //    memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();

        //    var Stockist = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
        //    memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();


        //    DataSet logo = AdminGeneralDB.GetRankIcon();
        //    foreach (DataRow dr in logo.Tables[0].Rows)
        //    {
        //        var IFile = new ImageFile();
        //        IFile.FileName = dr["CRANKSET_ICON"].ToString();
        //        RankModel RankList = new RankModel();
        //        RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
        //        //RankList.RankIconName = float.Parse(dr["AMOUNT"].ToString());
        //        //RankList.RankAmount = float.Parse(dr["AMOUNT"].ToString());
        //        ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
        //        RankList.RankName = dr["CRANKSET_NAME"].ToString();

        //        memberMarketTree.RankList.Add(RankList);
        //    }


            //string keke = Session["LanguageChosen"].ToString();
            //if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
            //{
            //    memberMarketTree.Picture = "RegisterEn.png";
            //}
            //else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
            //{
            //    memberMarketTree.Picture = "RegisterEn.png";
            //}
            //else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
            //{
            //    memberMarketTree.Picture = "RegisterEn.png";
            //}
            //else if (Session["LanguageChosen"].ToString().Equals("ja-JP", StringComparison.InvariantCultureIgnoreCase))
            //{
            //    memberMarketTree.Picture = "RegisterEn.png";
            //}
                       


        //    return PartialView("MarketTree", memberMarketTree);
        //}

        //public ActionResult GroupListing(int selectedPage = 1)
        //{
        //    if (Session["Username"] == null || Session["Username"].ToString() == "")
        //    {
        //        return RedirectToAction("MemberLogin", "Member", new { ExpiredSession = "Yes" });
        //    }


        //    int ok = 0, pages = 0;
        //    string msg = "";
        //    var member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
        //    string TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();


        //    MarketTreeModel memberMarketTree = new MarketTreeModel();

        //    DataSet dsBinaryList = MemberShopCenterDB.GetAllBinaryListByUsername(TopMember, selectedPage, out pages);

        //    selectedPage = BinaryConstructPageList(selectedPage, pages, memberMarketTree);

        //    if (dsBinaryList.Tables[0].Rows.Count != 0)
        //    {
        //        memberMarketTree.FirstLevelUsername = dsBinaryList.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
        //        memberMarketTree.FirstLevelFullName = dsBinaryList.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
        //        //memberMarketTree.FirstLevelSponsor = dsBinaryList.Tables[0].Rows[0]["CMTREE_UNDER"].ToString();
        //        memberMarketTree.FirstLevelUpline = dsBinaryList.Tables[0].Rows[0]["CMEM_INTRO"].ToString();
        //        memberMarketTree.FirstLevelLevel = dsBinaryList.Tables[0].Rows[0]["CMTREE_LEVEL"].ToString();
        //        memberMarketTree.FirstLevelRank = Misc.GetMemberRanking(dsBinaryList.Tables[0].Rows[0]["CRANK_CODE"].ToString());
        //        memberMarketTree.FirstLevelCreatedDate = Convert.ToDateTime(dsBinaryList.Tables[0].Rows[0]["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
        //        memberMarketTree.FirstLevelRankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

        //    }

        //    foreach (DataRow dr in dsBinaryList.Tables[0].Rows)
        //    {


        //        IntroListModel sponsorlist = new IntroListModel();
        //        sponsorlist.No = dr["rownumber"].ToString();
        //        sponsorlist.Username = dr["CUSR_USERNAME"].ToString();
        //        sponsorlist.FullName = dr["CUSR_FULLNAME"].ToString();
        //        //sponsorlist.Sponsor = dr["CMTREE_UNDER"].ToString();

        //        string firstmember = dr["CUSR_USERNAME"].ToString();
        //        if (firstmember == Session["Username"].ToString())
        //        {

        //            sponsorlist.Upline = "";
        //        }
        //        else
        //        {
        //            sponsorlist.Upline = dr["CMEM_INTRO"].ToString();
        //        }

        //        sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
        //        sponsorlist.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
        //        sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
        //        //sponsorlist.RankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
        //        sponsorlist.RankIcon = dr["CRANKSET_ICON"].ToString();

        //        if (dr["CMTREE_LEVEL"].ToString() != "0")
        //        {
        //            memberMarketTree.SponsorList.Add(sponsorlist);
        //        }


        //    }
        //    //var binary = GetBinaryListing(Session["Username"].ToString());
        //    //memberMarketTree.FirstLevelUsername = binary.FirstLevelUsername;
        //    //memberMarketTree.FirstLevelFullName = binary.FirstLevelFullName;
        //    //memberMarketTree.FirstLevelUpline = binary.FirstLevelUpline;
        //    //memberMarketTree.FirstLevelLevel = binary.FirstLevelLevel;
        //    //memberMarketTree.FirstLevelRank = binary.FirstLevelRank;
        //    //memberMarketTree.FirstLevelCreatedDate = binary.FirstLevelCreatedDate;
        //    //memberMarketTree.FirstLevelRankIcon = binary.FirstLevelRankIcon;
        //    //memberMarketTree.SponsorList = binary.SponsorList;



        //    //selectedPage = BinaryConstructPageList(selectedPage, pages, memberMarketTree);


        //    return PartialView("GroupListing", memberMarketTree);
        //}

        //public ActionResult GroupChart(int selectedPage = 1)
        //{
        //    if (Session["Username"] == null || Session["Username"].ToString() == "")
        //    {
        //        return RedirectToAction("MemberLogin", "Member", new { ExpiredSession = "Yes" });
        //    }

        //    int ok = 0, pages = 0;
        //    string msg = "";
        //    var member = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
        //    string TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();

        //    MarketTreeModel memberMarketTree = new MarketTreeModel();

        //    DataSet dsBinaryList = MemberShopCenterDB.GetAllBinaryListByUsername(TopMember, selectedPage, out pages);

        //    if (dsBinaryList.Tables[0].Rows.Count != 0)
        //    {
        //        memberMarketTree.FirstLevelUsername = dsBinaryList.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
        //        memberMarketTree.FirstLevelFullName = dsBinaryList.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
        //        memberMarketTree.FirstLevelUpline = dsBinaryList.Tables[0].Rows[0]["CMEM_INTRO"].ToString();
        //        memberMarketTree.FirstLevelLevel = dsBinaryList.Tables[0].Rows[0]["CMTREE_LEVEL"].ToString();
        //        memberMarketTree.FirstLevelRank = Misc.GetMemberRanking(dsBinaryList.Tables[0].Rows[0]["CRANK_CODE"].ToString());
        //        memberMarketTree.FirstLevelCreatedDate = Convert.ToDateTime(dsBinaryList.Tables[0].Rows[0]["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
        //        memberMarketTree.FirstLevelRankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

        //    }

        //    DataSet logo = AdminGeneralDB.GetRankIcon();
        //    foreach (DataRow dr in logo.Tables[0].Rows)
        //    {
        //        var IFile = new ImageFile();
        //        IFile.FileName = dr["CRANKSET_ICON"].ToString();
        //        RankModel RankList = new RankModel();
        //        RankList.RankIconPath = dr["CRANKSET_ICON"].ToString();
        //        ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
        //        RankList.RankName = dr["CRANKSET_NAME"].ToString();

        //        memberMarketTree.RankList.Add(RankList);
        //    }

        //    return PartialView("GroupChart", memberMarketTree);
        //}

        [HttpPost]
        public JsonResult FindNextGroupChart(string username)
        {
            int ok = 0;
            string msg = "";

            DataSet dsNetworkTree = MemberShopCenterDB.GetBinaryChartLevel2(username, out ok, out msg);
            ViewBag.MemberCount = 0;

            var member = MemberDB.GetMemberByUsername(username, out ok, out msg);
            List<MemberList> list = new List<MemberList>();

            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                var resultList = new MemberList();
                //resultList.No = dr["rownumber"].ToString();
                resultList.Username = dr["CUSR_USERNAME"].ToString();
                resultList.FullName = dr["CUSR_FULLNAME"].ToString();

                string firstmember = dr["CUSR_USERNAME"].ToString();

                if (firstmember == Session["Username"].ToString())
                {

                    resultList.Upline = "";
                }
                else
                {
                    resultList.Upline = dr["CMEM_INTRO"].ToString();
                }

                resultList.Level = dr["CMTREE_LEVEL"].ToString();
                resultList.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
                resultList.CreatedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                resultList.RankIcon = dr["CRANKSET_ICON"].ToString();

                if (dr["CMTREE_LEVEL"].ToString() == "1")
                {
                    list.Add(resultList);
                }
            }

            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        //private MarketTreeModel GetBinaryListing(string TopMember, int selectedPage = 1)
        //{

        //    int pages = 0, ok = 0;
        //    string msg = "";

        //    MarketTreeModel memberMarketTree = new MarketTreeModel();

        //    var member = MemberDB.GetMemberByUsername(TopMember, out ok, out msg);
        //    TopMember = member.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
        //    DataSet dsBinaryList = MemberShopCenterDB.GetAllBinaryListByUsername(TopMember, selectedPage, out pages);

        //    if (dsBinaryList.Tables[0].Rows.Count != 0)
        //    {
        //        memberMarketTree.FirstLevelUsername = dsBinaryList.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
        //        memberMarketTree.FirstLevelFullName = dsBinaryList.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
        //        //memberMarketTree.FirstLevelSponsor = dsBinaryList.Tables[0].Rows[0]["CMTREE_UNDER"].ToString();
        //        memberMarketTree.FirstLevelUpline = dsBinaryList.Tables[0].Rows[0]["CMEM_INTRO"].ToString();
        //        memberMarketTree.FirstLevelLevel = dsBinaryList.Tables[0].Rows[0]["CMTREE_LEVEL"].ToString();
        //        memberMarketTree.FirstLevelRank = Misc.RankNumber(dsBinaryList.Tables[0].Rows[0]["CRANK_CODE"].ToString());
        //        memberMarketTree.FirstLevelCreatedDate = Convert.ToDateTime(dsBinaryList.Tables[0].Rows[0]["CMEM_CREATEDON"]).ToString("dd-MM-yyyy");
        //        memberMarketTree.FirstLevelRankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToSt--ring();
        //    }

        //    selectedPage = BinaryConstructPageList(selectedPage, pages, memberMarketTree);

        //    foreach (DataRow dr in dsBinaryList.Tables[0].Rows)
        //    {
        //        IntroListModel sponsorlist = new IntroListModel();
        //        sponsorlist.No = dr["rownumber"].ToString();
        //        sponsorlist.Username = dr["CUSR_USERNAME"].ToString();
        //        sponsorlist.FullName = dr["CUSR_FULLNAME"].ToString();
        //        //sponsorlist.Sponsor = dr["CMTREE_UNDER"].ToString();

        //        string firstmember = dr["CUSR_USERNAME"].ToString();
        //        if (firstmember == Session["Username"].ToString())
        //        {

        //            sponsorlist.Upline = "";
        //        }
        //        else
        //        {
        //            sponsorlist.Upline = dr["CMEM_INTRO"].ToString();
        //        }

        //        sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
        //        sponsorlist.Rank = Misc.RankNumber(dr["CRANK_CODE"].ToString());
        //        sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd-MM-yyyy");
        //        //sponsorlist.RankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
        //        sponsorlist.RankIcon = dr["CRANKSET_ICON"].ToString();

        //        memberMarketTree.SponsorList.Add(sponsorlist);
        //    }
        //        return memberMarketTree;

        //}

        public string ConstructTooltipTree(string fullName, string joinedDate, string strLeftYJ, string strRightYJ, string LeftGP, string RightGP, string sponsor, string investStar)
        {
            float fLeftYJ = float.Parse(strLeftYJ);
            float fRightYJ = float.Parse(strRightYJ);
            float fLeftGP = float.Parse(LeftGP);
            float fRightGP = float.Parse(RightGP);
            string leftSales = Resources.Member.lblLeftSale;
            string rightSales = Resources.Member.lblRightSale;
            string sponsorLabel = Resources.Member.lblSponsor;
            string tooltip = string.Format("{0}({1})\n{2}\n{3} : {4}\n{5}     :{6}\t\t{7}     ", fullName, joinedDate,  sponsorLabel, sponsor, leftSales, strLeftYJ, rightSales, strRightYJ);
            return tooltip;
        }

        public ActionResult GetMarketTreeMostLeft(string Username)
        {

            int isBelongTo;
            MemberDB.MemberIsInMarketTreeByUsername(Username, Session["Username"].ToString(), out isBelongTo);

            if (isBelongTo == 0)//not belong to
            {
                Response.Write(string.Format(Resources.Member.msgNotInTree, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            DataSet dsFollow = MemberDB.GetMarketTreeMostLeft(Username);
            DataRow dr = dsFollow.Tables[0].Rows[0];
            string upMember = dr["CUSR_USERNAME"].ToString();


            return MarketTreeByUsername(upMember);
        }

        public ActionResult GetMarketTreeMostRight(string Username)
        {
            int isBelongTo;
            MemberDB.MemberIsInMarketTreeByUsername(Username, Session["Username"].ToString(), out isBelongTo);

            if (isBelongTo == 0)//not belong to
            {
                Response.Write(string.Format(Resources.Member.msgNotInTree, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            DataSet dsFollow = MemberDB.GetMarketTreeMostRight(Username);
            DataRow dr = dsFollow.Tables[0].Rows[0];
            string upMember = dr["CUSR_USERNAME"].ToString();



            return MarketTreeByUsername(upMember);
        }

        [HttpPost]
        public ActionResult MarketTreeByUsername(string TopMember)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
            }
            if (TopMember =="")
            {
                TopMember = Session["Username"].ToString();
            }
            int isBelongTo;
            MemberDB.MemberIsInMarketTreeByUsername(TopMember, Session["Username"].ToString(), out isBelongTo);

            if (isBelongTo == 0)//not belong to
            {
                Response.Write(string.Format(Resources.Member.msgNotInTree, TopMember));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            if (TopMember == "Main")
            {
                TopMember = Session["Username"].ToString();
            }

            try
            {
                MarketTreeModel memberMarketTree = GetAllMarketTreeMember(TopMember);

                return PartialView("PersonalTeam", memberMarketTree);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FindMemberTree(string Username)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                int isBelongTo;

                MemberDB.MemberIsInMarketTreeByUsername(Username, Session["Username"].ToString(), out isBelongTo);
                MarketTreeModel memberMarketTree;

                if (isBelongTo == 0)//not belong to
                {
                    Response.Write(string.Format(Resources.Member.msgNotInTree, Username));
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    memberMarketTree = GetAllMarketTreeMember(Username);

                    int ok = 0;
                    string msg = "";
                    var member = MemberDB.GetMemberByUsername(Username, out ok, out msg);
                    memberMarketTree.country = member.Tables[0].Rows[0]["CCOUNTRY_ID"].ToString();
                    var Stockist = MemberDB.GetMemberByUsername(Session["Username"].ToString(), out ok, out msg);
                    memberMarketTree.Stockist = Stockist.Tables[0].Rows[0]["CMEM_MEMBER_TYPE"].ToString();
                    if (Session["LanguageChosen"].ToString().Equals("zh-cn", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("zh-tw", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }
                    else if (Session["LanguageChosen"].ToString().Equals("zh-JP", StringComparison.InvariantCultureIgnoreCase))
                    {
                        memberMarketTree.Picture = "RegisterEn.png";
                    }

                    DataSet logo = AdminGeneralDB.GetRankIcon();
                    foreach (DataRow dr2 in logo.Tables[0].Rows)
                    {
                        var IFile = new ImageFile();
                        IFile.FileName = dr2["CRANKSET_ICON"].ToString();
                        RankModel RankList = new RankModel();
                        RankList.RankIconPath = dr2["CRANKSET_ICON"].ToString();
                        //RankList.RankIconName = dr2["AMOUNT"].ToString();
                        RankList.RankAmount = float.Parse(dr2["AMOUNT"].ToString());
                        ViewBag.Rank = logo.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                        memberMarketTree.RankList.Add(RankList);
                    }

                }

                return PartialView("MarketTree", memberMarketTree);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GoUpOneMember(string Username)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
                }

                DataSet dsFollow = MemberDB.GetMarketTreeByUsername(Username);
                DataRow dr = dsFollow.Tables[0].Rows[0];
                string upMember = dr["CMTREE_UPMEMBER"].ToString();

                string CurrentUser = Session["Username"].ToString();

                if (Username.Replace(" ", "") == CurrentUser.ToUpper())
                {
                    Response.Write(Resources.Member.msgTopMost);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (upMember == "0")
                {
                    Response.Write(Resources.Member.msgTopMost);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return MarketTreeByUsername(upMember);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public ActionResult ReceiveBonus()
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }

            int ok = 0;
            string msg = string.Empty;

            var dsMemberBonus = MemberDB.GetMemberBonusByUsername(Session["Username"].ToString(), out ok, out msg);
            if (dsMemberBonus.Tables[0].Rows.Count == 0)
            {
                Response.Write(Resources.Member.warningNoBonusReceiveToday);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            string Status = dsMemberBonus.Tables[0].Rows[0]["CMB_STATUS"].ToString();

            if (Status == "Receive")
            {
                Response.Write(Resources.Member.warningBonusAlreadyReceive);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            MemberDB.ReceiveBonus(Session["Username"].ToString());


            return Json(string.Empty, JsonRequestBehavior.AllowGet);

        }


    }
}