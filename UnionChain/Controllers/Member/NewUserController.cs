﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Components;
using ECFBase.Models;
using System.Data;
using System.Text.RegularExpressions;
using System.Globalization;

namespace ECFBase.Controllers.Member
{
    public class NewUserController : Controller
    {


        #region Sign up
        public ActionResult SignUp(string ReferenceID = "")
        {
            Session.Clear();
            SignUpModel model = new SignUpModel();
            string Position = "";

            if (ReferenceID != "")
            {
                Position = ReferenceID.Remove(0, 8);
                ReferenceID = ReferenceID.Remove(8, 1);
                model.Username = "";

                var dsMember = MemberDB.GetMemberByPersonalID(ReferenceID);

                model.IntroName = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                model.SponsorName = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            }

            

            int ok = 0;
            string msg = "";
            string languageCode = "en-us";
            string selectedcountry = "CN";

            #region Country

            List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

            model.Countries = from c in countries
                              select new SelectListItem
                              {
                                  Text = c.CountryName + " (+" + c.MobileCode + ")",
                                  Value = c.CountryCode
                              };

            model.SelectedCountry = selectedcountry;
            DataSet dsCountrySetup = AdminGeneralDB.GetCountryByCountryCode(selectedcountry, out ok, out msg);

            model.MobileCountryCode = dsCountrySetup.Tables[0].Rows[0]["CMULTILANGCOUNTRY_NAME"].ToString() + " (" +  dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_MOBILECODE"].ToString() + ") ";


            #endregion


            #region Position

            if(Position != "")
            {
                if(Position == "1")
                {
                    SelectListItem loc = new SelectListItem();
                    loc.Text = ECFBase.Resources.Member.lblPositionA;
                    loc.Value = "0";
                    model.PositionList.Add(loc);
                }
                else if (Position == "2")
                {
                    SelectListItem loc = new SelectListItem();
                    loc.Text = ECFBase.Resources.Member.lblPositionB;
                    loc.Value = "1";
                    model.PositionList.Add(loc);
                }
            }
            else
            {
                model.PositionList.Clear();

                SelectListItem loc = new SelectListItem();
                loc.Text = ECFBase.Resources.Member.lblSelected;
                loc.Value = "";
                model.PositionList.Add(loc);

                loc = new SelectListItem();
                loc.Text = ECFBase.Resources.Member.lblPositionA;
                loc.Value = "0";
                model.PositionList.Add(loc);

                loc = new SelectListItem();
                loc.Text = ECFBase.Resources.Member.lblPositionB;
                loc.Value = "1";
                model.PositionList.Add(loc);

            }

            #endregion


            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }

            return PartialView("SignUp", model);

        }

        public ActionResult SignUpMethod(SignUpModel model)
        {
            try
            {
                int ok = 0;
                string msg = "";

                #region Username
                if (string.IsNullOrEmpty(model.Username))
                {
                    Response.Write(Resources.Member.warningUsername);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Username.Length < 6)
                {
                    Response.Write(Resources.Member.WarningUsernameSixChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Username.Contains(" "))
                {
                    Response.Write(Resources.Member.warningNotAllowSpacing);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                //Regex phonereg = new Regex(@"^([a-zA-Z])$");
                //Regex nameReg = new Regex(@"^[\p{L}\s]+$");

                //if (nameReg.IsMatch(model.Username) == true)
                //{
                //    Response.Write(Resources.Member.warningNotAllowChineseCharacter);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                Regex nameReg2 = new Regex(@"\W|_");

                if (nameReg2.IsMatch(model.Username) == true)
                {
                    Response.Write(Resources.Member.warningNotAllowSymbolCharacter);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var dr = AdminMemberDB.GetMemberByUserName(model.Username, out ok, out msg);

                if (dr.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.Member.warningMemberIDExisted);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                #endregion

                #region Fullname
                if (string.IsNullOrEmpty(model.FullName))
                {
                    Response.Write(Resources.Member.warningFullname);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Intro
                if (string.IsNullOrEmpty(model.IntroName))
                {
                    Response.Write(Resources.Member.msgIntroNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Sponsor
                if (string.IsNullOrEmpty(model.SponsorName))
                {
                    Response.Write(Resources.Member.msginvalidupline);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Check Password/PIN
                if (string.IsNullOrEmpty(model.Password))
                {
                    Response.Write(Resources.Member.msgInvalidPassword);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (string.IsNullOrEmpty(model.PayPassword))
                {
                    Response.Write(Resources.Member.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Password.Length < 6 || model.PayPassword.Length < 6)
                {
                    Response.Write(string.Format(Resources.Member.WarningPassSixChar));
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                model.EncryptPassword = Authentication.Encrypt(model.Password);
                model.EncryptPayPassword = Authentication.Encrypt(model.PayPassword);

                #endregion

                #region Captcha

                if (Session["CaptchaImageText"] == null)
                {
                    Response.Write(Resources.Member.warningRefreshCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Captcha != Session["CaptchaImageText"].ToString())
                {
                    Response.Write(Resources.Member.msgInvalidCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Phone
                if (string.IsNullOrEmpty(model.MobileCountryCode))
                {
                    Response.Write(Resources.Member.warningPleaseSelectPhoneCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(model.Phone))
                {
                    Response.Write(Resources.Member.warningPleaseKeyInPhoneNumber);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex phonereg = new Regex(@"^([a-zA-Z])$");

                if (phonereg.IsMatch(model.Phone) == true)
                {
                    Response.Write(Resources.Member.warningPhoneNotAllowChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Email
                if (string.IsNullOrEmpty(model.Email))
                {
                    Response.Write(Resources.Member.msgInvalidEmail);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex reg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

                if (reg.IsMatch(model.Email) == false)
                {
                    Response.Write(Resources.Member.msgEmailInvalid);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Position
                if (string.IsNullOrEmpty(model.SelectedPosition))
                {
                    Response.Write(Resources.Member.warningSelectPosition);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Country
                if (string.IsNullOrEmpty(model.SelectedCountry))
                {
                    Response.Write(Resources.Member.warningSelectCountry);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                //DataSet DSValification = MemberLoginDB.ValidVerificationCode(model.VerficationCode);

                //if (DSValification.Tables[0].Rows.Count == 0)
                //{
                //    Response.Write("Verification Code Expired, Please Try Again");
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                string invoiceNumber = "";
                string MemberUsername = model.Username;

                MemberLoginDB.RegisterMember(model, out ok, out msg, out invoiceNumber);

                //string MsgRsuult = Resources.Member.msgRegMemberSucceed1 + "\n" + model.Username + " " + model.FullName + "\n" + Resources.Member.msgRegMemberSucceed2;

                if (ok == 1) //success
                {

                    #region Send Email

                    string massege = string.Empty;
                    string Title = string.Empty;
                    string MemberEmail = model.Email;
                    string subject = string.Empty;
                    string fullname = model.FullName;
                    string username = model.Username;
                    string password = model.Password;
                    string pin = model.PayPassword;

                    try
                    {
                        string path = string.Empty;
                        DataSet logo = AdminGeneralDB.GetCompanySetup();
                        foreach (DataRow dr2 in logo.Tables[0].Rows)
                        {
                            var IFile = new ImageFile();
                            IFile.FileName = dr2["COM_IMAGEPATH"].ToString();
                            string TempPath = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString().Replace("../..", "~");
                            path = Server.MapPath(TempPath);
                        }

                        Title = "Welcome to UnionsChain";
                        massege = "<img height=\"200\" width=\"200\" src=\"http://www.unionslink.net/Images/company.png\"><br/><br/>{1}<br/><br/>{4},<br/><br/>恭喜你已经成功注册成为优亚链平台的会员！<br/>优亚链平台欢迎大家的参与，并致力于大家一起创造财富！迎接辉煌！<br/><br/>请登录  www.unionslink.net  会员后台后，更改您的 一级密码和二级密码，以防被人盗用。 <br/><br/> 会员账号          :{1} <br/>登陆密码 ：{2} <br/> 安全密码 ：{3} <br/><br/>Congratulations and welcome to Union Chain !<br/>We will do our best toward all the success. Let us work together to create wealth.<br/><br/>Please login to www.unionslink.net and change your login and security password to prevent unauthorized access. ";
                        subject = "优亚链 UnionsChain";
                        fullname = model.FullName;

                        var contentSMS = string.Format(massege, username, username, password, pin, fullname);
                        string result = Misc.SendEmailGood("noreply@unionschain.com", MemberEmail, subject, contentSMS, path);
                    }
                    catch (Exception)
                    {
                    }

                    #endregion
                    Session["Username"] = MemberUsername;
                    string languageUsed = Session["LanguageChosen"].ToString();
                    Session["LanguageChosen"] = languageUsed;
                    return RedirectToAction("Home", "Member", new { username = MemberUsername });
                    //return Json(MsgRsuult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (msg == "-1")
                        Response.Write(Resources.Member.msgMemberExist);
                    else if (msg == "-2")
                        Response.Write(Resources.Member.msgIntroNotExist);
                    else if (msg == "-3")
                        Response.Write(Resources.Member.msgUpNotExist);
                    else if (msg == "-4")
                        Response.Write(Resources.Member.msgLeftOccupied);
                    else if (msg == "-5")
                        Response.Write(Resources.Member.msgRightOccupied);

                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MobileDeliver(SignUpModel model)
        {
            try
            {
                int ok = 0;
                string msg = "";

                #region Username
                if (string.IsNullOrEmpty(model.Username))
                {
                    Response.Write(Resources.Member.warningUsername);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Username.Length < 6)
                {
                    Response.Write(Resources.Member.WarningUsernameSixChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Username.Contains(" "))
                {
                    Response.Write(Resources.Member.warningNotAllowSpacing);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var dr = AdminMemberDB.GetMemberByUserName(model.Username, out ok, out msg);

                if (dr.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.Member.warningMemberIDExisted);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                #endregion

                #region Fullname
                if (string.IsNullOrEmpty(model.FullName))
                {
                    Response.Write(Resources.Member.warningFullname);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Intro
                if (string.IsNullOrEmpty(model.IntroName))
                {
                    Response.Write(Resources.Member.msgIntroNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Sponsor
                if (string.IsNullOrEmpty(model.SponsorName))
                {
                    Response.Write(Resources.Member.msginvalidupline);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Check Password/PIN
                if (string.IsNullOrEmpty(model.Password))
                {
                    Response.Write(Resources.Member.msgInvalidPassword);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (string.IsNullOrEmpty(model.PayPassword))
                {
                    Response.Write(Resources.Member.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Password.Length < 6 || model.PayPassword.Length < 6)
                {
                    Response.Write(string.Format(Resources.Member.WarningPassSixChar));
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                model.EncryptPassword = Authentication.Encrypt(model.Password);
                model.EncryptPayPassword = Authentication.Encrypt(model.PayPassword);

                #endregion

                #region Captcha

                if (Session["CaptchaImageText"] == null)
                {
                    Response.Write(Resources.Member.warningRefreshCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Captcha != Session["CaptchaImageText"].ToString())
                {
                    Response.Write(Resources.Member.msgInvalidCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Phone
                if (string.IsNullOrEmpty(model.MobileCountryCode))
                {
                    Response.Write(Resources.Member.warningPleaseSelectPhoneCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(model.Phone))
                {
                    Response.Write(Resources.Member.warningPleaseKeyInPhoneNumber);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex phonereg = new Regex(@"^([a-zA-Z])$");

                if (phonereg.IsMatch(model.Phone) == true)
                {
                    Response.Write(Resources.Member.warningPhoneNotAllowChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Email
                if (string.IsNullOrEmpty(model.Email))
                {
                    Response.Write(Resources.Member.msgInvalidEmail);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex reg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

                if (reg.IsMatch(model.Email) == false)
                {
                    Response.Write(Resources.Member.msgEmailInvalid);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Position
                if (string.IsNullOrEmpty(model.SelectedPosition))
                {
                    Response.Write(Resources.Member.warningSelectPosition);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Country
                if (string.IsNullOrEmpty(model.SelectedCountry))
                {
                    Response.Write(Resources.Member.warningSelectCountry);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Send SMS

                try
                {
                    var returnURL = string.Empty;
                    var content = string.Empty;
                    string cellPhone = Misc.PhoneParse(model.SelectedCountry.ToLower(), model.Phone);

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);

                    if (model.SelectedCountry.ToUpper() == "MY")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "RM0.00", Code);
                    }
                    else if (model.SelectedCountry.ToUpper() == "SG")
                    {
                        content = string.Format(Resources.Member.lblSMSContent, "ADV", Code);

                    }
                    else if (model.SelectedCountry.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);

                    }

                    int type = 1;
                    
                    if (Session["LanguageChosen"].ToString() == "en-US")
                    {
                        type = 1;
                    }
                    else if (Session["LanguageChosen"].ToString() == "zh-CN")
                    {
                        type = 3;
                        content = System.Text.Encoding.BigEndianUnicode.GetBytes(content).Aggregate("", (agg, val) => agg + val.ToString("X2"));
                    }

                    returnURL = string.Format("http://www.bulksms2u.com/websmsapi/ISendSMS.aspx?username={0}&password={1}&message={2}&mobile={3}&Sender={4}&type={5}", "UnionSMS", "juta1515", content, cellPhone, "68886", type);

                    MemberLoginDB.InsertSMSLog(returnURL, "UnionSMS", "juta1515" , cellPhone , model.SelectedCountry.ToUpper());

                    using (var client = new System.Net.WebClient())
                    {
                        client.Encoding = System.Text.Encoding.UTF8;
                        client.UploadString(returnURL, string.Empty);
                    }
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion

                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EmailDeliver(SignUpModel model)
        {
            try
            {
                int ok = 0;
                string msg = "";

                #region Username
                if (string.IsNullOrEmpty(model.Username))
                {
                    Response.Write(Resources.Member.warningUsername);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Username.Length < 6)
                {
                    Response.Write(Resources.Member.WarningUsernameSixChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Username.Contains(" "))
                {
                    Response.Write(Resources.Member.warningNotAllowSpacing);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var dr = AdminMemberDB.GetMemberByUserName(model.Username, out ok, out msg);

                if (dr.Tables[0].Rows.Count != 0)
                {
                    Response.Write(Resources.Member.warningMemberIDExisted);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                #endregion

                #region Fullname
                if (string.IsNullOrEmpty(model.FullName))
                {
                    Response.Write(Resources.Member.warningFullname);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Intro
                if (string.IsNullOrEmpty(model.IntroName))
                {
                    Response.Write(Resources.Member.msgIntroNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Sponsor
                if (string.IsNullOrEmpty(model.SponsorName))
                {
                    Response.Write(Resources.Member.msginvalidupline);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Check Password/PIN
                if (string.IsNullOrEmpty(model.Password))
                {
                    Response.Write(Resources.Member.msgInvalidPassword);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (string.IsNullOrEmpty(model.PayPassword))
                {
                    Response.Write(Resources.Member.msgInvalidPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Password.Length < 6 || model.PayPassword.Length < 6)
                {
                    Response.Write(string.Format(Resources.Member.WarningPassSixChar));
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                model.EncryptPassword = Authentication.Encrypt(model.Password);
                model.EncryptPayPassword = Authentication.Encrypt(model.PayPassword);

                #endregion

                #region Captcha

                if (Session["CaptchaImageText"] == null)
                {
                    Response.Write(Resources.Member.warningRefreshCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (model.Captcha != Session["CaptchaImageText"].ToString())
                {
                    Response.Write(Resources.Member.msgInvalidCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Phone
                if (string.IsNullOrEmpty(model.MobileCountryCode))
                {
                    Response.Write(Resources.Member.warningPleaseSelectPhoneCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(model.Phone))
                {
                    Response.Write(Resources.Member.warningPleaseKeyInPhoneNumber);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex phonereg = new Regex(@"^([a-zA-Z])$");

                if (phonereg.IsMatch(model.Phone) == true)
                {
                    Response.Write(Resources.Member.warningPhoneNotAllowChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Email
                if (string.IsNullOrEmpty(model.Email))
                {
                    Response.Write(Resources.Member.msgInvalidEmail);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex reg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

                if (reg.IsMatch(model.Email) == false)
                {
                    Response.Write(Resources.Member.msgEmailInvalid);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Position
                if (string.IsNullOrEmpty(model.SelectedPosition))
                {
                    Response.Write(Resources.Member.warningSelectPosition);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Country
                if (string.IsNullOrEmpty(model.SelectedCountry))
                {
                    Response.Write(Resources.Member.warningSelectCountry);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Send Email

                try
                {
                    var Title = string.Empty;
                    var content = string.Empty;

                    string Code = string.Empty;

                    MemberLoginDB.VerificationCode(out Code);


                    if (model.SelectedCountry.ToUpper() == "TW")
                    {
                        content = string.Format(Resources.Member.lblSMSContentTaiwan, Code);
                        Title = Resources.Member.lblEmailTitleTaiwan;

                    }
                    else
                    {
                        content = string.Format(Resources.Member.lblSMSContentOtherCountry, Code);
                        Title = Resources.Member.lblEmailTitle;

                    }
                    

                    string result = Misc.SendEmail("noreply@UnionsChain.com", model.Email, Title, content);
                }
                catch (Exception)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                }

                #endregion
                

                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Get Verification Code

        public ActionResult GetVerificationCode(string Email, string Phone)
        {
            Session.Clear();
            SignUpModel model = new SignUpModel();
            model.Email = Email;
            model.Phone = Phone;

            return PartialView("GetVerificationCode", model);

        }

        public ActionResult GetVerificationCodeByEmail(string Email)
        {
            Session.Clear();
            SignUpModel model = new SignUpModel();
            model.Email = Email;

            return PartialView("GetVerificationCode", model);

        }

        public ActionResult GetVerificationCodeByPhone(string Phone)
        {
            Session.Clear();
            SignUpModel model = new SignUpModel();
            model.Phone = Phone;

            return PartialView("GetVerificationCode", model);

        }


        #endregion

    }
}
