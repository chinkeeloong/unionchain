﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using ECFBase.Helpers;

namespace ECFBase.Controllers.Member
{
    public class MemberHelpDeskController : Controller
    {
        #region AllInbox
        public ActionResult AllInbox(int selectedPage = 1, string type = "")
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            int ok;
            string msg;
            int pages = 0;
            PaginationInboxModel model = new PaginationInboxModel();

            if (type == "")
            {
                type = ECFBase.Resources.Member.lblGeneral;
            }

            DataSet dsCorpNews = AdminHelpDeskDB.GetAllInboxByUsername(Session["Username"].ToString(), Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsCorpNews.Tables[0].Rows)
            {
                var idm = new InboxModel();
                idm.Username = dr["CUSR_USERNAME"].ToString();
                idm.InboxID = Convert.ToInt32(dr["CMSG_ID"]);
                idm.Title = dr["CMULTILANGMSG_TITLE"].ToString();

                if (dr["CMULTILANGMSG_REPLY"].ToString() != "")
                {
                    //idm.Reply = dr["CMULTILANGMSG_REPLY"].ToString();
                    idm.Reply = ECFBase.Resources.Member.lblReplied;
                }

                idm.PostDate = Convert.ToDateTime(dr["CMSG_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                idm.State = int.Parse(dr["CMSG_STATE"].ToString());

                if (idm.State == 1)
                {
                    idm.ReplyDate = Convert.ToDateTime(dr["CMSG_UPDATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                }

                model.InboxList.Add(idm);
            }

            return PartialView("AllInbox", model);
        }
        #endregion

        #region InboxData
        public ActionResult InboxData(int InboxID = 0)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            var inboxModel = new InboxModel();
            int ok = 0;
            string msg = "";

            //combobox for question
            List<string> questionTypeList = new List<string>();
            questionTypeList.Add(ECFBase.Resources.Member.lblGeneral);

            inboxModel.QuestionTypes = from c in questionTypeList
                                       select new SelectListItem
                                       {
                                           Text = c.ToString(),
                                           Value = c.ToString()
                                       };

            inboxModel.SelectedType = "General";

            //combobox for language
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            inboxModel.LanguageList = from c in languages
                                      select new SelectListItem
                                      {
                                          Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                          Text = c.Text,
                                          Value = c.Value
                                      };
            inboxModel.SelectedLanguage = Session["LanguageChosen"].ToString();

            if (InboxID == 0)
            {
                //Create New Corporate News
                inboxModel.Title = "";
                inboxModel.SendTo = "";

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    inboxModel.TitleLanguageList.Add(tlm);
                }

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    inboxModel.ContentLanguageList.Add(tlm);
                }
            }

            return PartialView("InboxData", inboxModel);

        }
        #endregion

        #region CreateNewMessageMethod
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateNewMessageMethod(InboxModel idm)
        {
            try
            {
                if (Session["Username"] == null || Session["Username"].ToString() == "")
                {
                    return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
                }

                if (idm.InboxID == null || idm.InboxID == 0)
                {
                    int ok;
                    string msg;
                    int messageID;

                    //user must enter title and content
                    if (!IsInboxMsgContentAndTitleEmpty(idm))
                    {
                        Response.Write(string.Format(ECFBase.Resources.Member.msgFailToSent));
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    //Create New Record
                    AdminHelpDeskDB.CreateNewMessage(Session["Username"].ToString(), idm.SelectedType, Session["Username"].ToString(), Session["Username"].ToString(), out messageID, out ok, out msg);
                    if (ok == 1)
                    {
                        for (int i = 0; i < idm.TitleLanguageList.Count; i++)
                        {
                            AdminHelpDeskDB.CreateNewMultiLanguageMessage(messageID, idm.TitleLanguageList[i].LanguageCode, Helper.NVL(idm.TitleLanguageList[i].Text), Helper.NVL(idm.ContentLanguageList[i].Text), "", Session["Username"].ToString(), Session["Username"].ToString(), out ok, out msg);
                        }
                    }
                }
                return AllInbox();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ViewMessage
        public ActionResult ViewMessage(int InboxID = 0)
        {
            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString() });
            }

            int ok = 0;
            string msg = "";

            var inboxModel = new InboxModel();

            //combobox for language
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            inboxModel.LanguageList = from c in languages
                                      select new SelectListItem
                                      {
                                          Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                          Text = c.Text,
                                          Value = c.Value
                                      };
            inboxModel.SelectedLanguage = Session["LanguageChosen"].ToString();

            DataSet ds = AdminHelpDeskDB.GetMessageByID(InboxID, out ok, out msg);
            if (ds.Tables[0].Rows.Count != 0)
            {
                inboxModel.SelectedType = ds.Tables[0].Rows[0]["CMSG_TYPE"].ToString();
                inboxModel.SendTo = ds.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                inboxModel.State = int.Parse(ds.Tables[0].Rows[0]["CMSG_STATE"].ToString());
                inboxModel.InboxID = InboxID;

                DataSet dsMultiLanguageMessage = AdminHelpDeskDB.GetMultiLanguageMessageByID(inboxModel.InboxID, out ok, out msg);

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    DataRow[] dr = dsMultiLanguageMessage.Tables[0].Select("CMSG_ID='" + inboxModel.InboxID + "' AND CLANG_CODE='" + language.Value + "'");
                    tlm.Text = dr[0]["CMULTILANGMSG_TITLE"].ToString();

                    if(inboxModel.SelectedLanguage == language.Value)
                        inboxModel.TitleLanguageList.Add(tlm);
                }

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    DataRow[] dr = dsMultiLanguageMessage.Tables[0].Select("CMSG_ID='" + inboxModel.InboxID + "' AND CLANG_CODE='" + language.Value + "'");
                    tlm.Text = dr[0]["CMULTILANGMSG_CONTENT"].ToString();

                    if (inboxModel.SelectedLanguage == language.Value)
                        inboxModel.ContentLanguageList.Add(tlm);
                }

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    DataRow[] dr = dsMultiLanguageMessage.Tables[0].Select("CMSG_ID='" + inboxModel.InboxID + "' AND CLANG_CODE='" + language.Value + "'");
                    tlm.Text = dr[0]["CMULTILANGMSG_REPLY"].ToString();

                    if (inboxModel.SelectedLanguage == language.Value)
                        inboxModel.ReplyLanguageList.Add(tlm);
                }
            }

            return PartialView("ViewMessage", inboxModel);

        }
        #endregion

        #region private
        private bool IsInboxMsgContentAndTitleEmpty(InboxModel inbox)
        {
            bool isNotContentEmpty = false;
            bool isNotTitleEmpty = false;
            foreach (var content in inbox.ContentLanguageList)
            {
                //test for null or empty
                if (!string.IsNullOrEmpty(content.Text))
                {
                    isNotContentEmpty = true;
                    break;
                }
            }

            foreach (var title in inbox.TitleLanguageList)
            {
                //test for null or empty
                if (!string.IsNullOrEmpty(title.Text))
                {
                    isNotTitleEmpty = true;
                    break;
                }
            }

            return isNotContentEmpty && isNotTitleEmpty;
        }
        #endregion
    }
}
