﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;

namespace ECFBase.Controllers.Member
{
    public class MemberReportsController : Controller
    {

        #region Bonus Summary Report
        public ActionResult BonusSummary(int selectedPage = 1, int selectedYear = -1, int selectedMonth = -1, int selectedDay = -1)
        {

            if (Session["Username"] == null || Session["Username"].ToString() == "")
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true,ExpiredSession = Session["ExpiredSession"].ToString(), FromLoginPage = "No" });
            }


            int ok;
            string msg;
            int pages;

            if (selectedYear == -1)
                selectedYear = DateTime.Now.Year;
            if (selectedMonth == -1)
                selectedMonth = DateTime.Now.Month;
            if (selectedDay == -1)
                selectedDay = DateTime.Now.Day;

            var model = new PaginationCurrentSalesModel();

            var dsMemberCurrentSales = MemberReportsDB.GetMemberCurrentSales(selectedPage, Session["Username"].ToString(), selectedYear, selectedMonth, selectedDay, out pages, out ok, out msg);
            selectedPage = ConstructPageList(selectedPage, pages, model);

            //add in new register
            foreach (DataRow dr in dsMemberCurrentSales.Tables[0].Rows)
            {
                var csm = new CurrentSalesModel();
                csm.SalesDate = dr["SalesDate"].ToString();
                csm.NewRegSalesAmount = float.Parse(dr["SalesAmount"].ToString());
                csm.TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                model.CurrentSalesModel.Add(csm);
            }

            //add in BonusWallet topup
            foreach (DataRow dr in dsMemberCurrentSales.Tables[1].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].TopupBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.TopupBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //add in PVWallet topup
            foreach (DataRow dr in dsMemberCurrentSales.Tables[2].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].TopupPVSalesAmount = float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.TopupPVSalesAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //add in maintainance
            foreach (DataRow dr in dsMemberCurrentSales.Tables[3].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].MaintainanceSalesAmount = float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.MaintainanceSalesAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //add in sponsor bonus
            foreach (DataRow dr in dsMemberCurrentSales.Tables[4].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].SponsorBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.SponsorBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //add in pairing bonus
            foreach (DataRow dr in dsMemberCurrentSales.Tables[5].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].PairingBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.PairingBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //add in matching bonus
            foreach (DataRow dr in dsMemberCurrentSales.Tables[6].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].MatchingBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.MatchingBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //add in worldpool bonus
            foreach (DataRow dr in dsMemberCurrentSales.Tables[7].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].WorldPoolBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.WorldPoolBonusAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);
                }
            }

            //sort first before add total
            var sort = new sortOnSalesDate();
            model.CurrentSalesModel.Sort(sort);

            //add up the total
            float totalRegSales = 0;
            float totalMaintainanceSales = 0;
            float totalPVTopupSales = 0;
            float totalTopupBonusSales = 0;
            float totalSponsorSales = 0;
            float totalPairingSales = 0;
            float totalMatchingSales = 0;
            float totalWorldPoolSales = 0;

            foreach (CurrentSalesModel salesModel in model.CurrentSalesModel)
            {
                totalRegSales += salesModel.NewRegSalesAmount;
                totalMaintainanceSales += salesModel.MaintainanceSalesAmount;
                totalPVTopupSales += salesModel.TopupPVSalesAmount;
                totalTopupBonusSales += salesModel.TopupBonusAmount;
                totalSponsorSales += salesModel.SponsorBonusAmount;
                totalPairingSales += salesModel.PairingBonusAmount;
                totalMatchingSales += salesModel.MatchingBonusAmount;
                totalWorldPoolSales += salesModel.WorldPoolBonusAmount;
            }
            var totalCSM = new CurrentSalesModel();
            totalCSM.SalesDate = Resources.Member.lblTotal;
            totalCSM.NewRegSalesAmount = totalRegSales;
            totalCSM.MaintainanceSalesAmount = totalMaintainanceSales;
            totalCSM.TopupPVSalesAmount = totalPVTopupSales;
            totalCSM.TopupBonusAmount = totalTopupBonusSales;
            totalCSM.SponsorBonusAmount = totalSponsorSales;
            totalCSM.PairingBonusAmount = totalPairingSales;
            totalCSM.MatchingBonusAmount = totalMatchingSales;
            totalCSM.WorldPoolBonusAmount = totalWorldPoolSales;
            totalCSM.TotalSalesAmount = totalRegSales;

            model.CurrentSalesModel.Add(totalCSM);

            ViewBag.selectedYear = selectedYear;
            ViewBag.selectedMonth = selectedMonth;
            ViewBag.selectedDay = selectedDay;

            return PartialView("BonusSummary", model);
        }
        #endregion

        #region Shared
        private static int ConstructPageList(int selectedPage, int pages, PaginationCurrentSalesModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationInvoiceListingModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        #endregion

    }
}
