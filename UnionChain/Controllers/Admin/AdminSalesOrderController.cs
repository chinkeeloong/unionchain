﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Components;
using ECFBase.Models;
using System.Globalization;
using System.IO;
using System.Text;

namespace ECFBase.Controllers.Admin
{
    public class AdminSalesOrderController : Controller
    {

        #region DeliveryOrder Listing

        public ActionResult PrintDO(string DONO)
        {
            ViewBag.DONO = DONO;
            return View("PrintDO");
        }

        /// <summary>
        /// Search Delivery Order By FC
        /// </summary>
        /// <param name="fc"></param>
        /// <param name="searchFC"></param>
        /// <param name="selectedPage"></param>
        /// <returns></returns>
        public ActionResult SearchDeliveryOrderByFC(string fc, string searchFC, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            var dsDeliveryOrder = AdminSalesOrderDB.GetAllDeliveryOrdersByFilteringCriteria(Session["LanguageChosen"].ToString(), fc, searchFC, selectedPage, out pages, out ok, out msg);

            var model = ContructDeliveryOrderEntity(pages, selectedPage, dsDeliveryOrder);
            return PartialView("DeliveryOrderSetup", model);
        }

        /// <summary>
        /// Search Delivery Order By FC
        /// </summary>
        /// <param name="fc"></param>
        /// <param name="searchFC"></param>
        /// <param name="selectedPage"></param>
        /// <returns></returns>
        public ActionResult SearchDeliveryOrderByStatus(string fc, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            var dsDeliveryOrder = AdminSalesOrderDB.GetAllDeliveryOrdersByStatus(Session["LanguageChosen"].ToString(), Misc.DeliveryStatus(fc), selectedPage, out pages, out ok, out msg);

            var model = ContructDeliveryOrderEntity(pages, selectedPage, dsDeliveryOrder);
            model.SelectedFilteringCriteria = fc;
            model.SelectedStatus = fc;
            return PartialView("DeliveryOrderSetup", model);
        }

        /// <summary>
        /// Search Delivery Order By Date
        /// </summary>
        /// <param name="fc"></param>
        /// <param name="searchFC"></param>
        /// <param name="selectedPage"></param>
        /// <returns></returns>
        public ActionResult SearchDeliveryOrderByDate(int FromDay, int FromMonth, int FromYear, int ToDay, int ToMonth, int ToYear, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;

            DateTime fromDate = new DateTime(FromYear, FromMonth, FromDay);
            DateTime toDate = new DateTime(ToYear, ToMonth, ToDay, 23, 59, 59);
            var dsDeliveryOrder = AdminSalesOrderDB.GetAllDeliveryOrdersByDate(Session["LanguageChosen"].ToString(), fromDate, toDate, selectedPage, out pages, out ok, out msg);

            var model = ContructDeliveryOrderEntity(pages, selectedPage, dsDeliveryOrder);
            model.SelectedFromDay = FromDay.ToString();
            model.SelectedToDay = ToDay.ToString();
            model.SelectedFromMonth = FromMonth.ToString();
            model.SelectedToMonth = ToMonth.ToString();
            model.SelectedFromYear = FromYear.ToString();
            model.SelectedToYear = ToYear.ToString();
            return PartialView("DeliveryOrderSetup", model);
        }

        /// <summary>
        /// Search Delivery Order Setup
        /// </summary>
        /// <param name="fc"></param>
        /// <param name="searchFC"></param>
        /// <param name="selectedPage"></param>
        /// <returns></returns>
        public ActionResult DeliveryOrderSetup(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;

            var dsDeliveryOrder = AdminSalesOrderDB.GetAllDeliveryOrders(Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

            var model = ContructDeliveryOrderEntity(pages, selectedPage, dsDeliveryOrder);
            return PartialView("DeliveryOrderSetup", model);
        }

        public ActionResult RedirectToEditDOPage(string orderNo)
        {
            Session["page"] = "RedirectToEditDOPage";
            Session["orderNo"] = orderNo;
            return RedirectToAction("Home", "Admin");
        }

        public ActionResult EditDO()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            if (Session["orderNo"] == null || Session["orderNo"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var orderNo = Session["orderNo"].ToString();
            Session["orderNo"] = null;

            int ok;
            string msg;
            var model = new DeliveryOrderModel();
            var dsDeliveryOrder = AdminSalesOrderDB.GetDeliveryOrderDetailsByDONumber(Session["LanguageChosen"].ToString(), orderNo, out ok, out msg);

            foreach (DataRow dr in dsDeliveryOrder.Tables[0].Rows)
            {
                model.ContactPerson = dr["CUSRINFO_SHIP_NAME"].ToString();
                model.ContactPhone = dr["MOBILE"].ToString();
                model.CourierNumber = dr["CDLVORDER_COURIERNO"].ToString();
                model.ShippingPhoneNo = dr["CUSRINFO_SHIP_PHONENO"].ToString();
                var shippingAddress = dr["SHIPADDRESS"].ToString();
                if (!string.IsNullOrEmpty(shippingAddress.Trim()))
                {
                    var address = shippingAddress.Split(new char[] { '\r' });
                    for (int i = 0; i < address.Length; i++)
                    {
                        if (i == 0) model.ShippingAddress1 = address[0];
                        else if (i == 1) model.ShippingAddress2 = address[1];
                        else if (i == 2) model.ShippingAddress3 = address[2];
                        else if (i == 3) model.ShippingAddress4 = address[3];
                    }
                }
                else
                {
                    model.ShippingAddress1 = string.Empty;
                    model.ShippingAddress2 = string.Empty;
                    model.ShippingAddress3 = string.Empty;
                    model.ShippingAddress4 = string.Empty;
                }

                model.DeliveryOrderNo = dr["CDLVORDER_NO"].ToString();
                model.DeliveryOrderDate = dr["CDLVORDER_DATE"] == null || dr["CDLVORDER_DATE"].ToString() == string.Empty ? string.Empty : DateTime.Parse(dr["CDLVORDER_DATE"].ToString()).ToString("D");
                model.InvoiceNo = dr["INVOICENUMBER"].ToString();
                model.InvoiceDate = dr["INVOICEDATE"] == null || dr["INVOICEDATE"].ToString() == string.Empty ? string.Empty : DateTime.Parse(dr["INVOICEDATE"].ToString()).ToString("D");

                if (dr["CPKG_CODE"].ToString() == "MAINPKG")
                {
                    var dsShoppingInv = AdminSalesOrderDB.GetShoppingInvoiceById(Convert.ToInt32(dr["CINVNO_ID"].ToString()), out ok, out msg);
                    int row = 1;
                    float total = 0;
                    int quantity = 0;
                    foreach (DataRow rw in dsShoppingInv.Tables[0].Rows)
                    {
                        model.OrderList.Add(
                        new OrderModel
                        {
                            Number = row,
                            Quantity = Int32.Parse(rw["CINVPRO_QUANTITY"].ToString()),
                            ProductNameAndDescription = rw["CMULTILANGPACKAGE_NAME"].ToString(),
                            SingleUnitPrice = float.Parse(rw["CPKG_AMOUNT"].ToString()),
                            TotalPrice = Int32.Parse(rw["CINVPRO_QUANTITY"].ToString()) * float.Parse(rw["CPKG_AMOUNT"].ToString())
                        });

                        total += Int32.Parse(rw["CINVPRO_QUANTITY"].ToString()) * float.Parse(rw["CPKG_AMOUNT"].ToString());
                        row += 1;
                        quantity += Int32.Parse(rw["CINVPRO_QUANTITY"].ToString());
                    }

                    model.OrderList.Add(
                      new OrderModel
                      {
                          Number = 2,
                          Quantity = Int32.Parse(dr["qtyRD"].ToString()),
                          ProductNameAndDescription = "Additional RD",
                          SingleUnitPrice = float.Parse(dr["addRD"].ToString()),
                      });

                    model.TotalPrice = total + float.Parse(dr["addRD"].ToString());
                    var charge = Misc.GetMaintainProductCharge(quantity);
                    model.TotalCharge = charge;
                    model.GrandTotal = model.TotalPrice + charge;
                }
                else
                {
                    model.OrderList.Add(
                      new OrderModel
                      {
                          Number = 1,
                          Quantity = Int32.Parse(dr["QUANTITY"].ToString()),
                          ProductNameAndDescription = dr["PRODUCTDESC"].ToString(),
                          SingleUnitPrice = float.Parse(dr["CPKG_AMOUNT"].ToString()),
                      });

                    model.OrderList.Add(
                      new OrderModel
                      {
                          Number = 2,
                          Quantity = Int32.Parse(dr["qtyRD"].ToString()),
                          ProductNameAndDescription = "Additional RD",
                          SingleUnitPrice = float.Parse(dr["addRD"].ToString()),
                      });

                    model.TotalPrice = float.Parse(dr["CPKG_AMOUNT"].ToString()) + float.Parse(dr["addRD"].ToString());
                }

                model.DeliveryType = dr["DELIVERYMODE"].ToString();
                model.DeliveryCourier = dr["COURIERCODE"] == null ? string.Empty : dr["COURIERCODE"].ToString();
                model.Remark = dr["CDLVORDER_REMARK"].ToString();
                model.DeliveryOrderApprovedBy = dr["CDLVORDER_APPROVEDBY"].ToString();
                model.DeliveryOrderApprovedOn = dr["CDLVORDER_APPROVEDDATE"] == null || dr["CDLVORDER_APPROVEDDATE"].ToString() == string.Empty ? string.Empty : DateTime.Parse(dr["CDLVORDER_APPROVEDDATE"].ToString()).ToString("D");
                model.Country = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                model.DeliveryStatus = dr["CDLVORDER_STATUS"].ToString();
                if (model.DeliveryStatus == "1")
                {
                    model.IsApproved = "1";
                    model.IsNotApproved = "0";
                }
                else
                {
                    model.IsApproved = "0";
                    model.IsNotApproved = "1";
                }

                SelectListItem loc = new SelectListItem();
                loc.Text = "Member";
                loc.Value = "0";
                model.DeliveryModes.Add(loc);
                SelectListItem loc2 = new SelectListItem();
                loc2 = new SelectListItem();
                loc2.Text = "Mobile";
                loc2.Value = "1";
                model.DeliveryModes.Add(loc2);

                int pages;
                var dsCourierModes = AdminGeneralDB.GetAllCourier(Session["LanguageChosen"].ToString(), -1, out pages, out ok, out msg);
                foreach (DataRow dsCourier in dsCourierModes.Tables[0].Rows)
                {
                    var courierModel = new SelectListItem { Text = dsCourier["CMULTILANGCRCOM_COMPANYNAME"].ToString(), Value = dsCourier["CRCOM_CODE"].ToString() };
                    model.CourierCompanies.Add(courierModel);
                }
            }

            return View("EditDO", model);
        }

        public ActionResult UpdateDOStatus(string DOId, string courierCode, string deliveryDate, string deliveryType, string remark, string address, string courierNumber)
        {
            int ok;
            string msg;
            AdminSalesOrderDB.UpdateDeliveryOrderStatus(DOId, courierCode == "null" ? null : courierCode, deliveryDate, deliveryType, remark, address, courierNumber, Session["Admin"].ToString(), out ok, out msg);
            Session["orderNo"] = DOId;
            return EditDO();
        }

        [HttpPost]
        public ActionResult EditDOMethod(DeliveryOrderModel model)
        {
            int ok;
            string msg;
            var fullAddress = Helpers.Helper.NVL(model.ShippingAddress1) + "\r" + Helpers.Helper.NVL(model.ShippingAddress2) + "\r" + Helpers.Helper.NVL(model.ShippingAddress3) + "\r" + Helpers.Helper.NVL(model.ShippingAddress4);
            AdminSalesOrderDB.UpdateDeliveryOrder(model.DeliveryOrderNo, model.DeliveryType, model.DeliveryCourier, Helpers.Helper.NVL(model.CourierNumber), model.Remark, fullAddress, Helpers.Helper.NVL(model.DeliveryOrderDate), Session["Admin"].ToString(), out ok, out msg);

            Session["page"] = "DeliveryOrderSetup";
            return RedirectToAction("Home", "Admin");
        }

        private PaginationDeliveryOrderModel ContructDeliveryOrderEntity(int pages, int selectedPage, DataSet dsDeliveryOrder)
        {
            var model = new PaginationDeliveryOrderModel();
            var pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();
            model.StatusCriteria = Misc.ConstructsStatusCriteria().ToList();
            model.FromDays = Misc.ConstructsDay().ToList();
            model.ToDays = Misc.ConstructsDay().ToList();
            model.FromMonths = Misc.ConstructsMonth().ToList();
            model.ToMonths = Misc.ConstructsMonth().ToList();
            model.FromYears = Misc.ConstructYears().ToList();
            model.ToYears = Misc.ConstructYears().ToList();

            model.SelectedFromDay = DateTime.Now.Day.ToString();
            model.SelectedToDay = DateTime.Now.Day.ToString();
            model.SelectedFromMonth = DateTime.Now.Month.ToString();
            model.SelectedToMonth = DateTime.Now.Month.ToString();
            model.SelectedFromYear = DateTime.Now.Year.ToString();
            model.SelectedToYear = DateTime.Now.Year.ToString();
            model.SelectedStatus = "ALL";

            foreach (DataRow dr in dsDeliveryOrder.Tables[0].Rows)
            {
                var dom = new DeliveryOrderModel();
                dom.Number = dr["rownumber"].ToString();
                dom.Id = dr["CDLVORDER_ID"].ToString();
                dom.InvoiceDate = DateTime.Parse(dr["CINVNO_CREATEDON"].ToString()).ToString("MM/dd/yyyy");
                dom.DeliveryOrderNo = dr["CDLVORDER_NO"].ToString();
                dom.DeliveryOrderDate = dr["CDLVORDER_DATE"] == null || dr["CDLVORDER_DATE"].ToString() == string.Empty ? Resources.Admin.lblUnknown : DateTime.Parse(dr["CDLVORDER_DATE"].ToString()).ToString("MM/dd/yyyy");
                dom.Username = dr["CUSR_USERNAME"].ToString();
                dom.DeliveryAmount = (float.Parse(dr["CPKG_AMOUNT"].ToString()) + float.Parse(dr["addRD"].ToString())).ToString("#0.00");
                dom.DeliveryType = Misc.GetDeliveryType(dr["CDELIMD_ID"].ToString());
                dom.DeliveryCourier = dr["CDELIMD_ID"] == null || dr["CMULTILANGCRCOM_COMPANYNAME"].ToString() == string.Empty ? string.Empty : dr["CMULTILANGCRCOM_COMPANYNAME"].ToString();
                dom.DeliveryStatus = Misc.DeliveryStatus(dr["CDLVORDER_STATUS"].ToString());
                dom.DeliveryOrderApprovedBy = dr["CDLVORDER_APPROVEDBY"] == null || dr["CDLVORDER_APPROVEDBY"].ToString() == string.Empty ? Resources.Admin.lblPendingApproval : dr["CDLVORDER_APPROVEDBY"].ToString();
                dom.DeliveryOrderApprovedOn = dr["CDLVORDER_APPROVEDDATE"] == null || dr["CDLVORDER_APPROVEDDATE"].ToString() == string.Empty ? Resources.Admin.lblUnknown : DateTime.Parse(dr["CDLVORDER_APPROVEDDATE"].ToString()).ToString("D");
                dom.StockistName = dr["STOCKISTNAME"].ToString();
                model.DeliveryOrderList.Add(dom);
            }

            return model;
        }

        #endregion

        #region InvoiceListing

        public ActionResult PrintInvoice(int InvoiceID)
        {
            ViewBag.InvoiceID = InvoiceID;
            return View("PrintInvoice");
        }

        public ActionResult InvoiceListing(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            var model = new PaginationInvoiceListingModel();

            var dsInvoice = AdminSalesOrderDB.GetAllInvoices(Session["LanguageChosen"].ToString(),
                                                            selectedPage, out pages, out ok, out msg);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsInvoice.Tables[0].Rows)
            {
                var invoice = new InvoiceModel();

                //invoice info
                invoice.RowNumber = dr["rownumber"].ToString();
                invoice.Id = Convert.ToInt32(dr["CINVNO_ID"]);
                invoice.InvoiceNo = dr["CINVNO_NO"].ToString();
                invoice.InvoiceCashName = Misc.GetReadableCashName(dr["CINVNO_CASHNAME"].ToString());
                invoice.Username = dr["CUSR_USERNAME"].ToString();
                invoice.Date = (DateTime)dr["CINVNO_CREATEDON"];
                invoice.Status = Misc.DeliveryStatus(dr["DOSTATUS"].ToString());
                invoice.Investment = float.Parse(dr["PKGINVEST"].ToString());
                invoice.TotalAmount = invoice.Investment;
                invoice.DeliveryCharge = float.Parse(dr["CPKG_DELIVERYCHARGE"].ToString());
                invoice.PackagePV = float.Parse(dr["PKGPV"].ToString());
                invoice.MobileMember = dr["MobileMember"].ToString();

                model.InvoiceLists.Add(invoice);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();
            model.StatusFilteringCriteria = Misc.ConstructDeliveryStatusFilteringCriteria().ToList();

            model.FromDay = Misc.ConstructsDay().ToList();
            model.FromMonth = Misc.ConstructsMonth().ToList();
            model.FromYear = Misc.ConstructYears(DateTime.Now.Year).ToList();
            model.ToDay = Misc.ConstructsDay().ToList();
            model.ToMonth = Misc.ConstructsMonth().ToList();
            model.ToYear = Misc.ConstructYears(DateTime.Now.Year).ToList();

            model.SelectedFromDay = DateTime.Now.Day.ToString();
            model.SelectedToDay = DateTime.Now.Day.ToString();
            model.SelectedFromMonth = DateTime.Now.Month.ToString();
            model.SelectedToMonth = DateTime.Now.Month.ToString();
            model.SelectedFromYear = DateTime.Now.Year.ToString();
            model.SelectedToYear = DateTime.Now.Year.ToString();

            return PartialView("InvoiceListing", model);
        }

        public ActionResult InvoiceView(int Id)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            var dsInvoice = AdminSalesOrderDB.GetInvoiceRelatedInfoById(Id, Session["LanguageChosen"].ToString(),
                                                                        out ok, out msg);
            var invoice = new InvoiceModel();

            //invoice info
            invoice.RowNumber = dsInvoice.Tables[0].Rows[0]["rownumber"].ToString();
            invoice.Id = Convert.ToInt32(dsInvoice.Tables[0].Rows[0]["CINVNO_ID"]);
            invoice.InvoiceNo = dsInvoice.Tables[0].Rows[0]["CINVNO_NO"].ToString();
            invoice.InvoiceCashName = Misc.GetReadableCashName(dsInvoice.Tables[0].Rows[0]["CINVNO_CASHNAME"].ToString());
            invoice.CountryCode = dsInvoice.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
            invoice.Date = (DateTime)dsInvoice.Tables[0].Rows[0]["CINVNO_CREATEDON"];
            var shippingAddress = dsInvoice.Tables[0].Rows[0]["CINVNO_ADDRESS"].ToString();
            if (!string.IsNullOrEmpty(shippingAddress.Trim()))
            {

                var address = shippingAddress.Split(new char[] { '\r' });
                for (int i = 0; i < address.Length; i++)
                {
                    if (i == 0) invoice.ShipAddress = address[0];
                    else if (i == 1) invoice.ShipAddress1 = address[1];
                    else if (i == 2) invoice.ShipAddress2 = address[2];
                    else if (i == 3) invoice.ShipAddress3 = address[3];
                }
            }

            //user and member info
            invoice.Username = dsInvoice.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            invoice.CellPhone = dsInvoice.Tables[0].Rows[0]["CELLPHONE"].ToString();
            invoice.Phone = dsInvoice.Tables[0].Rows[0]["PHONE"].ToString();
            invoice.IntroPerson = dsInvoice.Tables[0].Rows[0]["INTRO"].ToString();

            //package info
            invoice.PackageCode = dsInvoice.Tables[0].Rows[0]["CPKG_CODE"].ToString();
            invoice.Investment = float.Parse(dsInvoice.Tables[0].Rows[0]["PKGINVEST"].ToString());
            invoice.PackageQuantity = 1; //currently only 1 Pkg is sold during register
            invoice.PackageDescription = dsInvoice.Tables[0].Rows[0]["PKGDESC"].ToString();
            invoice.TotalAmount = (invoice.PackageQuantity * invoice.Investment);

            return PartialView("InvoiceView", invoice);
        }

        public ActionResult InvoiceSetup(int Id)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            var dsInvoice = AdminSalesOrderDB.GetInvoiceRelatedInfoById(Id, Session["LanguageChosen"].ToString(),
                                                                        out ok, out msg);
            var invoice = new InvoiceModel();

            //invoice info
            invoice.RowNumber = dsInvoice.Tables[0].Rows[0]["rownumber"].ToString();
            invoice.Id = Convert.ToInt32(dsInvoice.Tables[0].Rows[0]["CINVNO_ID"]);
            invoice.InvoiceNo = dsInvoice.Tables[0].Rows[0]["CINVNO_NO"].ToString();
            invoice.InvoiceCashName = Misc.GetReadableCashName(dsInvoice.Tables[0].Rows[0]["CINVNO_CASHNAME"].ToString());
            invoice.CountryCode = dsInvoice.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
            invoice.Date = (DateTime)dsInvoice.Tables[0].Rows[0]["CINVNO_CREATEDON"];
            var shippingAddress = dsInvoice.Tables[0].Rows[0]["CINVNO_ADDRESS"].ToString();
            if (!string.IsNullOrEmpty(shippingAddress.Trim()))
            {

                var address = shippingAddress.Split(new char[] { '\r' });
                for (int i = 0; i < address.Length; i++)
                {
                    if (i == 0) invoice.ShipAddress = address[0];
                    else if (i == 1) invoice.ShipAddress1 = address[1];
                    else if (i == 2) invoice.ShipAddress2 = address[2];
                    else if (i == 3) invoice.ShipAddress3 = address[3];
                }
            }

            //user and member info
            invoice.Username = dsInvoice.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            invoice.CellPhone = dsInvoice.Tables[0].Rows[0]["CELLPHONE"].ToString();
            invoice.Phone = dsInvoice.Tables[0].Rows[0]["PHONE"].ToString();
            invoice.IntroPerson = dsInvoice.Tables[0].Rows[0]["INTRO"].ToString();

            //package info
            invoice.PackageCode = dsInvoice.Tables[0].Rows[0]["CPKG_CODE"].ToString();
            invoice.Investment = float.Parse(dsInvoice.Tables[0].Rows[0]["PKGINVEST"].ToString());
            invoice.PackageQuantity = 1; //currently only 1 Pkg is sold during register
            invoice.PackageDescription = dsInvoice.Tables[0].Rows[0]["PKGDESC"].ToString();
            invoice.TotalAmount = (invoice.PackageQuantity * invoice.Investment);

            return PartialView("InvoiceSetup", invoice);
        }

        [HttpPost]
        public ActionResult InvoiceMethod(InvoiceModel model)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }
            //Currently this method is only updating shipping address into invoice table
            int ok;
            string msg;
            AdminSalesOrderDB.UpdateInvoiceById(model, out ok, out msg);

            return InvoiceListing();
        }

        public ActionResult SearchInvoiceByDate(int FromDay, int FromMonth, int FromYear, int ToDay, int ToMonth, int ToYear, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;

            DateTime fromDate = new DateTime(FromYear, FromMonth, FromDay);
            DateTime toDate = new DateTime(ToYear, ToMonth, ToDay, 23, 59, 59);

            var model = new PaginationInvoiceListingModel();
            var dsInvoice = AdminSalesOrderDB.SP_GetAllInvoicesByDate(Session["LanguageChosen"].ToString(), fromDate, toDate, selectedPage, out pages, out ok, out msg);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsInvoice.Tables[0].Rows)
            {
                var invoice = new InvoiceModel();

                //invoice info
                invoice.RowNumber = dr["rownumber"].ToString();
                invoice.Id = Convert.ToInt32(dr["CINVNO_ID"]);
                invoice.InvoiceNo = dr["CINVNO_NO"].ToString();
                invoice.InvoiceCashName = Misc.GetReadableCashName(dr["CINVNO_CASHNAME"].ToString());
                invoice.Username = dr["CUSR_USERNAME"].ToString();
                invoice.Date = (DateTime)dr["CINVNO_CREATEDON"];
                invoice.Status = Misc.DeliveryStatus(dr["DOSTATUS"].ToString());
                invoice.Investment = float.Parse(dr["PKGINVEST"].ToString());
                invoice.TotalAmount = invoice.Investment;
                invoice.DeliveryCharge = float.Parse(dr["CPKG_DELIVERYCHARGE"].ToString());
                invoice.PackagePV = float.Parse(dr["PKGPV"].ToString());

                model.InvoiceLists.Add(invoice);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();
            model.StatusFilteringCriteria = Misc.ConstructDeliveryStatusFilteringCriteria().ToList();

            model.FromDay = Misc.ConstructsDay().ToList();
            model.FromMonth = Misc.ConstructsMonth().ToList();
            model.FromYear = Misc.ConstructYears(DateTime.Now.Year).ToList();
            model.SelectedFromDay = FromDay.ToString();
            model.SelectedFromMonth = FromMonth.ToString();
            model.SelectedFromYear = FromYear.ToString();
            model.ToDay = Misc.ConstructsDay().ToList();
            model.ToMonth = Misc.ConstructsMonth().ToList();
            model.ToYear = Misc.ConstructYears(DateTime.Now.Year).ToList();
            model.SelectedToDay = ToDay.ToString();
            model.SelectedToMonth = ToMonth.ToString();
            model.SelectedToYear = ToYear.ToString();

            return PartialView("InvoiceListing", model);
        }

        public ActionResult SearchInvoiceListByFC(string fc, string searchFC, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            var model = new PaginationInvoiceListingModel();
            var dsInvoice = AdminSalesOrderDB.GetAllInvoicesByFilteringCriteria(Session["LanguageChosen"].ToString(), fc, searchFC, selectedPage, out pages, out ok, out msg);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsInvoice.Tables[0].Rows)
            {
                var invoice = new InvoiceModel();

                //invoice info
                invoice.RowNumber = dr["rownumber"].ToString();
                invoice.Id = Convert.ToInt32(dr["CINVNO_ID"]);
                invoice.InvoiceNo = dr["CINVNO_NO"].ToString();
                invoice.InvoiceCashName = Misc.GetReadableCashName(dr["CINVNO_CASHNAME"].ToString());
                invoice.Username = dr["CUSR_USERNAME"].ToString();
                invoice.Date = (DateTime)dr["CINVNO_CREATEDON"];
                invoice.Status = dr["DOSTATUS"].ToString();
                invoice.Investment = float.Parse(dr["PKGINVEST"].ToString());
                invoice.PackageQuantity = 1; //currently only 1 Pkg is sold during register
                invoice.TotalAmount = (invoice.PackageQuantity * invoice.Investment);
                invoice.PackagePV = float.Parse(dr["PKGPV"].ToString());

                model.InvoiceLists.Add(invoice);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();
            model.StatusFilteringCriteria = Misc.ConstructDeliveryStatusFilteringCriteria().ToList();

            model.FromDay = Misc.ConstructsDay().ToList();
            model.FromMonth = Misc.ConstructsMonth().ToList();
            model.FromYear = Misc.ConstructYears(DateTime.Now.Year).ToList();
            model.ToDay = Misc.ConstructsDay().ToList();
            model.ToMonth = Misc.ConstructsMonth().ToList();
            model.ToYear = Misc.ConstructYears(DateTime.Now.Year).ToList();

            model.SelectedFromDay = DateTime.Now.Day.ToString();
            model.SelectedToDay = DateTime.Now.Day.ToString();
            model.SelectedFromMonth = DateTime.Now.Month.ToString();
            model.SelectedToMonth = DateTime.Now.Month.ToString();
            model.SelectedFromYear = DateTime.Now.Year.ToString();
            model.SelectedToYear = DateTime.Now.Year.ToString();

            return PartialView("InvoiceListing", model);
        }

        public ActionResult SearchInvoicesByStatus(string fc, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            var model = new PaginationInvoiceListingModel();
            DataSet dsInvoice;

            if (String.Compare(fc, "ALL", System.StringComparison.OrdinalIgnoreCase) != 0)
            {
                //if its not ALL fc
                dsInvoice = AdminSalesOrderDB.GetAllInvoicesByStatus(Session["LanguageChosen"].ToString(), Misc.DeliveryStatus(fc), selectedPage, out pages, out ok, out msg);
            }
            else
            {
                //if its ALL/ default search for all invoices
                dsInvoice = AdminSalesOrderDB.GetAllInvoices(Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);
            }
            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsInvoice.Tables[0].Rows)
            {
                var invoice = new InvoiceModel();

                //invoice info
                invoice.RowNumber = dr["rownumber"].ToString();
                invoice.Id = Convert.ToInt32(dr["CINVNO_ID"]);
                invoice.InvoiceNo = dr["CINVNO_NO"].ToString();
                invoice.InvoiceCashName = Misc.GetReadableCashName(dr["CINVNO_CASHNAME"].ToString());
                invoice.Username = dr["CUSR_USERNAME"].ToString();
                invoice.Date = (DateTime)dr["CINVNO_CREATEDON"];
                invoice.Status = Misc.DeliveryStatus(dr["DOSTATUS"].ToString());
                invoice.Investment = float.Parse(dr["PKGINVEST"].ToString());
                invoice.TotalAmount = invoice.Investment;
                invoice.DeliveryCharge = float.Parse(dr["CPKG_DELIVERYCHARGE"].ToString());
                invoice.PackagePV = float.Parse(dr["PKGPV"].ToString());
                invoice.MobileMember = dr["MobileMember"].ToString();

                model.InvoiceLists.Add(invoice);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();
            model.StatusFilteringCriteria = Misc.ConstructDeliveryStatusFilteringCriteria().ToList();

            model.FromDay = Misc.ConstructsDay().ToList();
            model.FromMonth = Misc.ConstructsMonth().ToList();
            model.FromYear = Misc.ConstructYears(DateTime.Now.Year).ToList();
            model.ToDay = Misc.ConstructsDay().ToList();
            model.ToMonth = Misc.ConstructsMonth().ToList();
            model.ToYear = Misc.ConstructYears(DateTime.Now.Year).ToList();

            model.SelectedFromDay = DateTime.Now.Day.ToString();
            model.SelectedToDay = DateTime.Now.Day.ToString();
            model.SelectedFromMonth = DateTime.Now.Month.ToString();
            model.SelectedToMonth = DateTime.Now.Month.ToString();
            model.SelectedFromYear = DateTime.Now.Year.ToString();
            model.SelectedToYear = DateTime.Now.Year.ToString();
            model.SelectedStatusFilteringCriteria = fc;

            return PartialView("InvoiceListing", model);
        }

        #endregion

        #region SharedMethod
        public static IEnumerable<SelectListItem> ConstructsMonth()
        {
            List<string> monthsList = new List<string>();
            monthsList.Add(Resources.Admin.lblAll);
            for (int z = 1; z <= 12; z++)
            {
                monthsList.Add(z.ToString());
            }

            var months = from c in monthsList
                         select new SelectListItem
                         {
                             Text = c.ToString(),
                             Value = c.ToString()
                         };

            return months;
        }

        public static IEnumerable<SelectListItem> ConstructYears(int year = 1920)
        {
            var yearsList = new List<string>();
            DateTime dt = new DateTime(year, 1, 1);

            yearsList.Add(Resources.Admin.lblAll);
            while (true)
            {
                yearsList.Add(dt.Year.ToString());
                dt = dt.AddYears(1);
                if (dt.Year > DateTime.Now.Year)
                {
                    break;
                }
            }

            var years = from c in yearsList
                        select new SelectListItem
                        {
                            Text = c,
                            Value = c
                        };

            return years;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationRegistrationPackageLog model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationInvoiceListingModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        #endregion
    }
}
