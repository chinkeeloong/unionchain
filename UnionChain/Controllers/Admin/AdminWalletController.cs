﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using ECFBase.Resources;

namespace ECFBase.Controllers.Admin
{
    public class AdminWalletController : Controller
    {
        #region Utilities Bill
        public ActionResult UtilityBillListing(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            UtilitySetupModel model = new UtilitySetupModel();
            DataSet dsMembers = AdminWalletDB.GetAllBill(selectedPage, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);
            foreach (DataRow dr in dsMembers.Tables[0].Rows)
            {
                var Bill = new UtilityModel();
                Bill.CreatedDate = Convert.ToDateTime(dr["CUTI_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                Bill.MemberID = dr["CUSR_USERNAME"].ToString();
                Bill.MemberName = dr["CUSR_FULLNAME"].ToString();
                Bill.BillID = Convert.ToInt32(dr["CUTI_BILLID"].ToString());
                Bill.UtilityDescription = dr["CMULTILANGUTILITY_DESCRIPTION"].ToString();
                Bill.BillAccount = dr["CUTI_BILLACCOUNT"].ToString();
                Bill.MobileNumber = dr["CUTI_MOBILENUMBER"].ToString();
                Bill.BillAmount = float.Parse(dr["CUTI_BILLAMOUNTRM"].ToString()).ToString("n4");
                Bill.UNSAmount = float.Parse(dr["CUTI_UNSAMOUNT"].ToString()).ToString("n4");
                Bill.Fees = float.Parse(dr["CUTI_FEES"].ToString()).ToString("n4");
                Bill.TotalQP = float.Parse(dr["CUTI_TOTALAMOUNT"].ToString()).ToString("n4");
                model.BillList.Add(Bill);
            }

            return PartialView("UtilityBillListing", model);
        }



        public ActionResult UtilityBillByID(string ID)
        {

            UtilityModel model = new UtilityModel();
            DataSet dr = AdminWalletDB.GetBillByBillID(Convert.ToInt32(ID));

            model.CreatedDate = DateTime.Now.ToString();
            model.MemberID = dr.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            model.MemberName = dr.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.BillID = Convert.ToInt32(dr.Tables[0].Rows[0]["CUTI_BILLID"].ToString());
            model.UtilityDescription = dr.Tables[0].Rows[0]["CMULTILANGUTILITY_DESCRIPTION"].ToString();
            model.BillAccount = dr.Tables[0].Rows[0]["CUTI_BILLACCOUNT"].ToString();
            model.MobileNumber = dr.Tables[0].Rows[0]["CUTI_MOBILENUMBER"].ToString();           
            model.BillAmount = float.Parse(dr.Tables[0].Rows[0]["CUTI_BILLAMOUNTRM"].ToString()).ToString("n4");
            model.UNSAmount = float.Parse(dr.Tables[0].Rows[0]["CUTI_UNSAMOUNT"].ToString()).ToString("n4");
            model.Fees = float.Parse(dr.Tables[0].Rows[0]["CUTI_FEES"].ToString()).ToString("n4");
            model.TotalQP = float.Parse(dr.Tables[0].Rows[0]["CUTI_TOTALAMOUNT"].ToString()).ToString("n4");


            return PartialView("UtilityBillListingData", model);

        }

        public ActionResult ApproveUtilityBill(string ID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            AdminWalletDB.ApproveBill(Convert.ToInt32(ID));


            return UtilityBillListing();
        }

        public ActionResult RejectUtilityBill(string ID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            try
            {

                int ok = 0;
                string msg = "";
                AdminWalletDB.RejectBill(Convert.ToInt32(ID), out ok, out msg);


                if (ok == -1)
                {
                    Response.Write(Resources.Member.WarningUtilityRecordNotFound);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return UtilityBillListing();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
           
        }

        public ActionResult UtilityBillsListing(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            UtilitySetupModel model = new UtilitySetupModel();
            DataSet dsMembers = AdminWalletDB.GetAllBillApprove(selectedPage, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);
            foreach (DataRow dr in dsMembers.Tables[0].Rows)
            {
                var Bill = new UtilityModel();
                Bill.CreatedDate = Convert.ToDateTime(dr["CUTI_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                Bill.MemberID = dr["CUSR_USERNAME"].ToString();
                Bill.MemberName = dr["CUSR_FULLNAME"].ToString();
                Bill.BillID = Convert.ToInt32(dr["CUTI_BILLID"].ToString());
                Bill.UtilityDescription = dr["CMULTILANGUTILITY_DESCRIPTION"].ToString();
                Bill.BillAccount = dr["CUTI_BILLACCOUNT"].ToString();
                Bill.MobileNumber = dr["CUTI_MOBILENUMBER"].ToString();

                Bill.BillAmount = float.Parse(dr["CUTI_BILLAMOUNTRM"].ToString()).ToString("n4");
                Bill.UNSAmount = float.Parse(dr["CUTI_UNSAMOUNT"].ToString()).ToString("n4");
                Bill.Fees = float.Parse(dr["CUTI_FEES"].ToString()).ToString("n4");
                Bill.TotalQP = float.Parse(dr["CUTI_TOTALAMOUNT"].ToString()).ToString("n4");

                Bill.Status =  dr["CUTI_STATUS"].ToString();
                Bill.PaymentDate = Convert.ToDateTime(dr["CUTI_UPDATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                model.BillList.Add(Bill);
            }

            return PartialView("UtilityBillApprovedListing ", model);
        }

        private int ConstructPageList(int selectedPage, int pages, UtilitySetupModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        #endregion


        #region WalletAdjustment
        public ActionResult WalletAdjustment(string Username, string Type)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            WalletAdjustmentModel model = new WalletAdjustmentModel();
            model.Username = Username;
            var dataset = MemberDB.GetMemberByUsername(Username, out ok, out msg);
            if (dataset.Tables[0].Rows.Count == 0)
            {
                Response.Write(Resources.Admin.msgInvalidUser1);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            DataRow memberDetails = dataset.Tables[0].Rows[0];

            model.Type = Type;

            if (Type == ProjectStaticString.RegisterwalletTopUp)
            {
                model.TypeResource = Resources.Admin.mnuRegisterTopUp;
                model.WalletBalanceTypeResource = "Register Point Balance";
                model.Nickname = memberDetails["CUSR_FULLNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_REGISTERWALLET"].ToString());
            }
            else if (Type == ProjectStaticString.CashwalletTopUp)
            {
                model.TypeResource = Resources.Admin.mnuCashTopUp;
                model.WalletBalanceTypeResource = "Cash Point Balance";
                model.Nickname = memberDetails["CUSR_FULLNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_CASHWALLET"].ToString());
            }
            else if (Type == ProjectStaticString.ActivationPointTopUp)
            {
                model.TypeResource = Resources.Admin.mnuActivationPointTopUp;
                model.WalletBalanceTypeResource = "Activation Point Balance";
                model.Nickname = memberDetails["CUSR_FULLNAME"].ToString();
                model.CurrentWallet = float.Parse(memberDetails["CMEM_ACTIVATIONWALLET"].ToString());
            }
           
            model.SelectedCountry = memberDetails["CCOUNTRY_CODE"].ToString();

            #region Transaction type
            string add = Resources.Admin.btnAdd;
            string deduct = Resources.Admin.cntranDeduct;
            List<string> typeList = new List<string>();
            typeList.Add(add);
            typeList.Add(deduct);

            model.Transaction = from c in typeList
                                select new SelectListItem
                                {
                                    Selected = false,
                                    Text = c.ToString(),
                                    Value = (c.ToString() == add) ? "TOPUP" : "DED" //this is for cashname
                                };

            #endregion

            //#region PaymentMode
            //List<string> modelist = new List<string>();
            //var ds = MemberDB.GetAllPaymentMode(out ok, out msg);
            //foreach (DataTable table in ds.Tables)
            //{
            //    foreach (DataRow dr in table.Rows)
            //    {
            //        string mode = dr["CPAYMD_TYPE"].ToString();
            //        modelist.Add(mode);
            //    }
            //}

            //model.paymentmode = from c in modelist
            //                    select new SelectListItem
            //                    {
            //                        Selected = false,
            //                        Text = c.ToString(),
            //                        Value = (c.ToString())
            //                    };
            //#endregion

            return PartialView("WalletAdjustment", model);
        }

        [HttpPost]
        public JsonResult FindBank(string CountryCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetBankList(LanguageCode, CountryCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetBankList(string LanguageCode, string CountryCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<BankModel> banks = Misc.GetAllBankByCountry(LanguageCode, CountryCode, ref ok, ref message);

            var result = from c in banks
                         select new SelectListItem
                         {
                             Selected = false,
                             Text = c.BankName,
                             Value = c.BankCode
                         };
            return result.OrderBy(m => m.Value).ToList();
        }

        [HttpPost]
        public ActionResult WalletAdjustmentMethod(WalletAdjustmentModel walletMem)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok = 0;
                string msg = "";
                string abc = walletMem.selectedmode;
                if (walletMem.Remark == "")
                {
                    walletMem.Remark = "";
                }
                if (walletMem.SelectedTransaction == "DED")
                {
                    walletMem.TransactionWallet = 0 - walletMem.TransactionWallet;
                }              

                if (walletMem.TransactionWallet % 1 > 0)
                {
                    Response.Write(Resources.Admin.lblNoDecimal);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (walletMem.Type == ProjectStaticString.TradeableWalletTopUp)
                {
                    AdminWalletDB.WalletAdjustment("SP_WalletTradeableAdjustment", walletMem.Username, walletMem.TransactionWallet, walletMem.SelectedTransaction, walletMem.Remark, walletMem.Type, Session["Admin"].ToString(), out ok, out msg);
                }
                else if (walletMem.Type == ProjectStaticString.UntradeableWalletTopUp)
                {
                    AdminWalletDB.WalletAdjustment("SP_WalletUntradeableAdjustment", walletMem.Username, walletMem.TransactionWallet, walletMem.SelectedTransaction, walletMem.Remark, walletMem.Type, Session["Admin"].ToString(), out ok, out msg);
                }
              
               
                if (ok == -1)
                {
                    Response.Write(Resources.Admin.msgInvalidBank);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (ok == 1)//success
                {
                    if (walletMem.Type == ProjectStaticString.TradeableWalletTopUp)
                    {
                        return ViewTradeableWallet();
                    }
                    else if (walletMem.Type == ProjectStaticString.UntradeableWalletTopUp)
                    {
                        return ViewUntradeableWallet();
                    }

                    return PartialView("");
                }
                else
                {
                    //throw new System.InvalidOperationException(string.Format("Failed to topup. Error message : [{0}]", msg));
                    throw new System.InvalidOperationException(string.Format(Resources.Admin.msgFailedtoTopUp, msg));
                }
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Withdrawal

        #region CashWallet
        public ActionResult WithdrawalCashLog(int selectedPage = 1, string Username = "0", string Country = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int pages = 0;
                var model = new PaginationWalletLogModel();

                var dsCashWalletLog = new DataSet();
                dsCashWalletLog = AdminWalletDB.GetAllCashWithdrawalLog(selectedPage, Username, Country, out pages);

                selectedPage = ConstructPageList(selectedPage, pages, model);

                if (dsCashWalletLog.Tables.Count != 0)
                {
                    foreach (DataRow dr in dsCashWalletLog.Tables[0].Rows)
                    {
                        WalletLogModel walletLog = new WalletLogModel();
                        walletLog.No = dr["rownumber"].ToString();
                        walletLog.ID = int.Parse(dr["CCSHWL_ID"].ToString());
                        walletLog.Username = dr["CUSR_USERNAME"].ToString();
                        walletLog.IsChecked = false;

                        walletLog.CashOut = float.Parse(dr["CCSHWL_CASHOUT"].ToString());
                        walletLog.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                        walletLog.Bank = dr["CBANK_CODE"].ToString();
                        walletLog.AccNo = dr["CCSHWL_USERBANKACC"].ToString();
                        walletLog.FullName = dr["CUSR_FULLNAME"].ToString();
                        walletLog.TranState = Misc.GetWithdrawalStatus(dr["CCSHWL_TRANSTATE"].ToString());
                        walletLog.CreatedDate = Convert.ToDateTime(dr["CCSHWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                        walletLog.TranState = Convert.ToDateTime(dr["CCSHWL_UPDATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                        walletLog.refference = dr["REF_CONTENT"].ToString();
                        walletLog.refferenceID = dr["REF_ID"].ToString();
                        string Sta = dr["CCSHWL_TRANSTATE"].ToString();

                        if (Sta == "1")
                        {
                            walletLog.status = "Approve";
                        }
                        else if (Sta == "-1")
                        {
                            walletLog.status = "Refund";
                        }
                        model.WalletLogList.Add(walletLog);
                    }
                }

                return PartialView("WithdrawalCashLog", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult WithdrawCashList(int selectedPage = 1, string searchName = "0", string selectedCountry = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int pages = 0;
                var model = new PaginationWalletLogModel();
                model.TypeOfWallet = "Cash";


                //select the withdrawal date 
                DataSet dsCashWalletLog = new DataSet();

                dsCashWalletLog = AdminWalletDB.GetAllCashWithdrawal(selectedPage, searchName, selectedCountry, out pages);

                selectedPage = ConstructPageList(selectedPage, pages, model);

                string totalWithdraw = "";
                string totalAdminCharge = "";
                string totalNettWithdraw = "";
                string IC = string.Empty;
                string PreIC = "";
                float TotalAmount = 0;
                int i = 0;

                WalletLogModel walletLog = new WalletLogModel();
                if (dsCashWalletLog.Tables.Count != 0)
                {
                    foreach (DataRow dr in dsCashWalletLog.Tables[0].Rows)
                    {

                        IC = dr["CUSRINFO_IC"].ToString();

                        if (i == 0)
                        {

                        }
                        else
                        {
                            if (PreIC == IC)
                            {
                                walletLog.ShowSubTotal = false;
                                model.WalletLogList.Add(walletLog);
                            }
                            else if (PreIC != IC)
                            {
                                model.WalletLogList.Add(walletLog);
                                WalletLogModel ELog = new WalletLogModel();
                                ELog.TotalAmout = TotalAmount.ToString("n2");
                                ELog.Subtotal = "SubTotal :";
                                ELog.ShowSubTotal = true;
                                model.WalletLogList.Add(ELog);
                                TotalAmount = 0;
                            }
                        }

                        i += 1;

                        walletLog = new WalletLogModel();
                        walletLog.No = dr["rownumber"].ToString();
                        walletLog.ID = int.Parse(dr["CCSHWL_ID"].ToString());
                        walletLog.Username = dr["CUSR_USERNAME"].ToString();
                        walletLog.FullName = dr["CUSR_FULLNAME"].ToString();
                        walletLog.IsChecked = false;
                        walletLog.BankCharges = float.Parse(dr["CCSHWL_APPNUMBER"].ToString());
                        walletLog.NetAmount = float.Parse(dr["CCSHWL_CASHOUT"].ToString());
                        walletLog.CashOut = walletLog.BankCharges + walletLog.NetAmount;
                        string test = dr["SumWithdrawAmt"].ToString();
                        totalWithdraw = double.Parse(dr["SumWithdrawAmt"].ToString()).ToString("n2");
                        walletLog.AdminFee = float.Parse(dr["CCSHWL_APPNUMBER"].ToString());
                        totalAdminCharge = double.Parse(dr["SumAdminFee"].ToString()).ToString("n2");
                        walletLog.NetWithdrawal = walletLog.CashOut - walletLog.AdminFee;
                        totalNettWithdraw = double.Parse(dr["SumNettWithdraw"].ToString()).ToString("n2");
                        walletLog.BranchName = dr["CMEMBANK_BRANCHNAME"] == null ? string.Empty : dr["CMEMBANK_BRANCHNAME"].ToString();
                        walletLog.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                        walletLog.Bank = dr["CBANK_CODE"].ToString();
                        walletLog.AccNo = dr["CCSHWL_USERBANKACC"].ToString();
                        walletLog.MobileNumber = dr["CUSRINFO_CELLPHONE"].ToString();
                        walletLog.TranState = Misc.GetWithdrawalStatus(dr["CCSHWL_TRANSTATE"].ToString());
                        walletLog.CreatedDate = Convert.ToDateTime(dr["CCSHWL_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                        walletLog.IC = dr["CUSRINFO_IC"].ToString();
                        TotalAmount += walletLog.BankCharges + walletLog.NetAmount;
                        PreIC = IC;

                        if (i == 50)
                        {

                            model.WalletLogList.Add(walletLog);
                            WalletLogModel ELog = new WalletLogModel();
                            ELog.TotalAmout = TotalAmount.ToString("n2");
                            ELog.Subtotal = "SubTotal :";
                            ELog.ShowSubTotal = true;
                            model.WalletLogList.Add(ELog);
                        }

                        if (dsCashWalletLog.Tables[0].Rows.Count == i)
                        {

                            model.WalletLogList.Add(walletLog);
                            WalletLogModel ELog = new WalletLogModel();
                            ELog.TotalAmout = TotalAmount.ToString("n2");
                            ELog.Subtotal = "SubTotal :";
                            ELog.ShowSubTotal = true;
                            model.WalletLogList.Add(ELog);
                        }
                    }
                }
                ViewBag.totalWithdraw = totalWithdraw;
                ViewBag.totalAdminCharge = totalAdminCharge;
                ViewBag.totalNettWithdraw = totalNettWithdraw;

                //0 Pending
                //1 Approved
                //-1 Refunded
                //-3 Rejected
                List<string> stateList = new List<string>();
                Misc.InsertWithdrawalStatus(ref stateList);


                #region Country
                int ok = 0;
                string msg = "";
                string languageCode = Session["LanguageChosen"].ToString();
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                model.Countries = from c in countries
                                  select new SelectListItem
                                  {
                                      Selected = false,
                                      Text = c.CountryName,
                                      Value = c.CountryCode
                                  };
                #endregion

                return PartialView("WithdrawCashList", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApproveWithdrawal(string ParameterValue)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (ParameterValue == "")
                {
                    Response.Write("No Record Found.");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string[] parameters = ParameterValue.Split(new string[] { "&" }, StringSplitOptions.None);
                List<int> withdrawalListID = new List<int>();

                foreach (string parameter in parameters)
                {
                    string[] value = parameter.Split(new string[] { "=" }, StringSplitOptions.None);

                    //Since i added datelist and statelist, when serialize, this gets to pass in, I want to ignore this
                    if (value[0] == "dateList" || value[0] == "stateList")
                        continue;

                    if (value[0] != "item.IsChecked" && value[0] != "page" && value[0] != "countryList" && value[0] != "dateList" && value[0] != "stateList")
                    {

                        withdrawalListID.Add(int.Parse(value[0]));
                    }
                    //var parameterValue = Convert.ToBoolean(value[1]);
                }

                AdminWalletDB.ApproveBonusWithdrawal(withdrawalListID, Session["Admin"].ToString());

                //if (Request.IsAjaxRequest())
                //{
                //    return Json(new { redirectToUrl = Url.Action("WithdrawBonusList", "AdminWallet") });
                //}

                return WithdrawCashList();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RejectWithdrawal(string ParameterValue)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (ParameterValue == "")
                {
                    Response.Write("No Record Found.");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string[] parameters = ParameterValue.Split(new string[] { "&" }, StringSplitOptions.None);
                List<int> withdrawalListID = new List<int>();

                foreach (string parameter in parameters)
                {
                    string[] value = parameter.Split(new string[] { "=" }, StringSplitOptions.None);

                    //Since i added datelist and statelist, when serialize, this gets to pass in, I want to ignore this
                    if (value[0] == "dateList" || value[0] == "stateList")
                        continue;

                    if (value[0] != "item.IsChecked" && value[0] != "page" && value[0] != "countryList" && value[0] != "dateList" && value[0] != "stateList")
                    {
                        withdrawalListID.Add(int.Parse(value[0]));
                    }
                    //var parameterValue = Convert.ToBoolean(value[1]);
                }

                AdminWalletDB.RejectBonusWithdrawal(withdrawalListID, Session["Admin"].ToString());

                //if (Request.IsAjaxRequest())
                //{
                //    return Json(new { redirectToUrl = Url.Action("WithdrawBonusList", "AdminWallet") });
                //}

                return WithdrawCashList();
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RefundWithdrawal(string ParameterValue)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (ParameterValue == "")
                {
                    Response.Write("No Record Found.");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string[] parameters = ParameterValue.Split(new string[] { "&" }, StringSplitOptions.None);
                List<int> withdrawalListID = new List<int>();

                foreach (string parameter in parameters)
                {
                    string[] value = parameter.Split(new string[] { "=" }, StringSplitOptions.None);

                    //Since i added datelist and statelist, when serialize, this gets to pass in, I want to ignore this
                    if (value[0] == "dateList" || value[0] == "stateList")
                        continue;

                    if (value[0] != "item.IsChecked" && value[0] != "page" && value[0] != "countryList" && value[0] != "dateList" && value[0] != "stateList")
                    {
                        withdrawalListID.Add(int.Parse(value[0]));
                    }
                    //var parameterValue = Convert.ToBoolean(value[1]);
                }

                AdminWalletDB.RefundBonusWithdrawal(withdrawalListID, Session["Admin"].ToString());

                //if (Request.IsAjaxRequest())
                //{
                //    return Json(new { redirectToUrl = Url.Action("WithdrawBonusList", "AdminWallet") });
                //}

                return WithdrawCashList();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public void ExportWithdrawalList(string selectedDate, string state)
        {
            int stateInt = int.Parse(state);
            DateTime dtSelected = DateTime.ParseExact(selectedDate, "M/d/yyyy", CultureInfo.InvariantCulture);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=WithdrawalBonusList.csv");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            string style = @"<style> .text { mso-number-format:\@; } </style> ";
            Response.Write(style);
            sw.Write(PrintBonusWithdrawalList(dtSelected, stateInt));
            sw.Close();
            Response.End();
        }

        private string PrintBonusWithdrawalList(DateTime dtSelected, int state)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Flow ID, Username, {0}, {1}, {2}, Country, Bank, Bank Account No, Account Holder Name, Status, Transaction Date", Resources.Admin.lblAmountRD, Resources.Admin.lblBankCharges, Resources.Admin.lblNetAmount));

            DataSet dsCashWalletLog = AdminWalletDB.GetBonusWithdrawalFullList(dtSelected, 1, state, Session["LanguageChosen"].ToString());

            foreach (DataRow dr in dsCashWalletLog.Tables[0].Rows)
            {
                float BankCharges = float.Parse(dr["CBONUSWL_APPNUMBER"].ToString());
                float NetAmount = float.Parse(dr["CBONUSWL_CASHOUT"].ToString());
                float CashOut = BankCharges + NetAmount;

                sb.AppendLine(dr["CBONUSWL_FLOWID"].ToString() + "," +
                              dr["CUSR_USERNAME"].ToString() + "," +
                              CashOut + "," +
                              BankCharges + "," +
                              NetAmount + "," +
                              dr["CMULTILANGCOUNTRY_NAME"].ToString() + "," +
                              dr["CMULTILANGBANK_NAME"].ToString() + "," +
                              dr["CBONUSWL_USERBANKACC"].ToString() + "\t," +
                              dr["CMEMBANK_BENEFICIARYNAME"].ToString() + "," +
                              Misc.GetWithdrawalStatus(dr["CBONUSWL_TRANSTATE"].ToString()) + "," +
                              dr["CBONUSWL_CREATEDON"].ToString());
            }

            return sb.ToString();
        }
        #endregion


        #endregion

        #region Wallet Log

        public ActionResult ViewTradeableWallet(int selectedPage = 1, string cashname = "0", string startdate = "01-01-1990", string enddate = "01-01-3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int pages = 0;
                PaginationWalletLogModel model = new PaginationWalletLogModel();
                model.TypeOfWallet = "Tradeable";
                ViewBag.wallet = model.TypeOfWallet; ;
                model.show = true;

                DateTime sdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd-MM-yyyy", null);
                //DateTime ENDDATE = edate.AddDays(1);
                string ED = edate.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("TW");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };

                DataSet dsWallet = AdminWalletDB.GetAllTradeableWalletLog(Username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), selectedPage, out pages);

                selectedPage = ConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    WalletLogModel walletLog = new WalletLogModel();
                    walletLog.FlowID = dr["CTDBWL_ID"].ToString();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CTDBWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CTDBWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CTDBWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CTDBWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CTDBWL_APPUSER"].ToString();

                    walletLog.AppOther = Misc.ConvertToAppOtherBasedOnCashName(dr["CTDBWL_APPOTHER"].ToString(), dr["CTDBWL_CASHNAME"].ToString());
                  
                    walletLog.AppNumber = dr["CTDBWL_APPNUMBER"].ToString();
                    
                    walletLog.Bank = dr["CMULTILANGBANK_NAME"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CTDBWL_CREATEDON"]).ToString("dd-MM-yyyy hh:mm:ss tt");
                    //walletLog.refference = dr["REF_CONTENT"].ToString();
                    //walletLog.refferenceID = dr["REF_ID"].ToString();
                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Title = Resources.Admin.mnuTradeableWalletLog;
                model.selectedname = cashname;
                if (startdate != "01/01/1990")
                {
                    model.startdate = startdate;
                }

                if (enddate != "01/01/3000")
                {
                    model.enddate = enddate;
                }
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewUntradeableWallet(int selectedPage = 1, string cashname = "0", string startdate = "01-01-1990", string enddate = "01-01-3000", string Username = "0", string Export = "0")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int pages = 0;
                PaginationWalletLogModel model = new PaginationWalletLogModel();
                model.TypeOfWallet = "Untradeable";
                ViewBag.wallet = model.TypeOfWallet; ;
                model.show = true;

                DateTime sdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", null);
                string SD = sdate.ToString("yyyy-MM-dd");

                DateTime edate = DateTime.ParseExact(enddate, "dd-MM-yyyy", null);
                //DateTime ENDDATE = edate.AddDays(1);
                string ED = edate.ToString("yyyy-MM-dd");

                List<CashName> CName = Misc.DBGetAllCashNameList("UTW");

                model.SelectedCashName = from c in CName
                                         select new SelectListItem
                                         {
                                             Text = c.cashnamedisplay,
                                             Value = c.cashname
                                         };

                DataSet dsWallet = AdminWalletDB.GetAllUntradeableWalletLog(Username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), selectedPage, out pages);

                selectedPage = ConstructPageList(selectedPage, pages, model);
                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    WalletLogModel walletLog = new WalletLogModel();
                    walletLog.Username = dr["CUSR_USERNAME"].ToString();
                    walletLog.CashName = Misc.GetReadableCashName(dr["CUTWL_CASHNAME"].ToString());
                    walletLog.CashIn = float.Parse(dr["CUTWL_CASHIN"].ToString());
                    walletLog.CashOut = float.Parse(dr["CUTWL_CASHOUT"].ToString());
                    walletLog.Wallet = float.Parse(dr["CUTWL_WALLET"].ToString());
                    walletLog.AppUser = dr["CUTWL_APPUSER"].ToString();
                    walletLog.AppOther = Misc.ConvertToAppOtherBasedOnCashName(dr["CUTWL_APPOTHER"].ToString(), dr["CUTWL_CASHNAME"].ToString());
                  
                    walletLog.AppNumber = dr["CUTWL_APPNUMBER"].ToString();
                    walletLog.Bank = dr["CMULTILANGBANK_NAME"].ToString();
                    walletLog.CreatedDate = Convert.ToDateTime(dr["CUTWL_CREATEDON"]).ToString("dd-MM-yyyy hh:mm:ss tt");
                    model.WalletLogList.Add(walletLog);
                }

                ViewBag.Title = Resources.Admin.mnuUntradeableWalletLog;
                model.selectedname = cashname;
                if (startdate != "01-01-1990")
                {
                    model.startdate = startdate;
                }

                if (enddate != "01-01-3000")
                {
                    model.enddate = enddate;
                }
                return PartialView("WalletTransaction", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

  
        public void Exportwallet(string WalletType = "", string cashname = "0", string username = "0", string startdate = "01-01-1990", string enddate = "01-01-3000", string Export = "1")
        {

            string Title = string.Empty;

            if (WalletType == "Tradeable")
            {
                Title = "TradeableWallet-Log.csv";
            }
           
            if (WalletType == "Untradeable")
            {
                Title = "UntradeableWallet-Log.csv";
            }        

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + Title + "");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(Printwallet(WalletType, cashname, username, startdate, enddate, Export));
            sw.Close();
            Response.End();
        }
        private string Printwallet(string WalletType, string cashname, string username, string startdate, string enddate, string Export)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Username,Remark,Credit,Debit,Balance,Extra Info,Transaction Date "));

            int pages = 0;
            string message = string.Empty;
            DataSet dsWallet = new DataSet();

            DateTime sdate = DateTime.ParseExact(startdate, "MM-dd-yyyy", null);
            string SD = sdate.ToString("yyyy-MM-dd");

            DateTime edate = DateTime.ParseExact(enddate, "MM-dd-yyyy", null);
            DateTime ENDDATE = edate.AddDays(0);
            string ED = ENDDATE.ToString("yyyy-MM-dd");

            if (WalletType == "Tradeable")
            {
                dsWallet = AdminWalletDB.GetAllTradeableWalletLog(username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), 1, out pages);
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    sb.AppendLine(
                                  dr["CUSR_USERNAME"].ToString() + "," +
                                  //dr["Member_Fullname"].ToString() + "," +
                                  Misc.GetReadableCashName(dr["CTDBWL_CASHNAME"].ToString()) + "," +
                                  float.Parse(dr["CTDBWL_CASHIN"].ToString()) + "," +
                                  float.Parse(dr["CTDBWL_CASHOUT"].ToString()) + "," +
                                  float.Parse(dr["CTDBWL_WALLET"].ToString()) + "," +
                                  Misc.ConvertToAppOtherBasedOnCashName(dr["CTDBWL_APPOTHER"].ToString(), dr["CTDBWL_CASHNAME"].ToString()) + "," +                                 
                    Convert.ToDateTime(dr["CTDBWL_CREATEDON"]).ToString("dd-MM-yyyy"));
                }
            }

            if (WalletType == "Untradeable")
            {
                dsWallet = AdminWalletDB.GetAllUntradeableWalletLog(username, cashname, SD, ED, Session["LanguageChosen"].ToString(), Convert.ToInt32(Export), 1, out pages);
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    sb.AppendLine(
                                  dr["CUSR_USERNAME"].ToString() + "," +
                                  //dr["Member_Fullname"].ToString() + "," +
                                  Misc.GetReadableCashName(dr["CUTWL_CASHNAME"].ToString()) + "," +
                                  float.Parse(dr["CUTWL_CASHIN"].ToString()) + "," +
                                  float.Parse(dr["CUTWL_CASHOUT"].ToString()) + "," +
                                  float.Parse(dr["CUTWL_WALLET"].ToString()) + "," +
                                  //dr["CCSHWL_APPUSER"].ToString() + "," +
                                  //dr["APPUSER_Fulname"].ToString() + "," +
                                  Misc.ConvertToAppOtherBasedOnCashName(dr["CUTWL_APPOTHER"].ToString(), dr["CUTWL_CASHNAME"].ToString()) + "," +
                                   //dr["CUTWL_PAYMENTMODE"].ToString() + "," +
                    Convert.ToDateTime(dr["CUTWL_CREATEDON"]).ToString("dd-MM-yyyy"));
                }
            }

       
            return sb.ToString();
        }

        #endregion

        #region General for View Next Page Log
        public ActionResult NextWalletTransaction(string selectedPage, string username, string walletType, string cashname, string datetime, string startdate, string enddate)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }
               
                if(walletType == "Tradeable")
                {
                    return RedirectToAction("ViewTradeableWallet", "AdminWallet", new { selectedPage = selectedPage, username = username, cashname = cashname, startdate = startdate, enddate = enddate });
                }

                if (walletType == "Untradeable")
                {
                    return RedirectToAction("ViewUntradeableWallet", "AdminWallet", new { selectedPage = selectedPage, username = username, cashname = cashname, startdate = startdate, enddate = enddate });
                }


                //purposely set to unknown, so that we know there is some wallet that is not yet add in the if condition
                return RedirectToAction("UnknownWalletTransaction", "AdminWallet", new { selectedPage = selectedPage });

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region SharedMethod
        private int ConstructPageList(int selectedPage, int pages, PaginationWalletLogModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationShareCertPurchaseModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        #endregion

    }
}
