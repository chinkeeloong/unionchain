﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Text;

namespace ECFBase.Controllers.Admin
{
    public class AdminController : Controller
    {
        #region AdminLogin
        public ActionResult AdminLogin(bool reloadPage = false)
        {
            if (reloadPage)
                ViewBag.ReloadPage = "Y";

            if (Request.Url.AbsoluteUri.Contains("www.unionslink.com"))
            {
                return RedirectToAction("MemberLogin", "Member", new { reloadPage = true });
            }

            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }


            string language = Session["LanguageChosen"].ToString();
            //Clear all session.
            Session.RemoveAll();       
            Session["LanguageChosen"] = language;
            


            return View();
        }
        #endregion

        #region ChangeLanguage

        public ActionResult ChangeLanguage(string languageCode)
        {
            Session["LanguageChosen"] = languageCode;
            return Home();
        }

        #endregion

        #region Home
        public ActionResult Home()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var ahm = new AdminHomeModel();


            var DS = AdminInfoDeskDB.GetAllImage();
            foreach (DataRow dr in DS.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["CINF_IMAGEPATH"].ToString();

                ahm.ImageList.Add(IFile);
            }

            DataSet logo = AdminGeneralDB.GetCompanySetup();
            foreach (DataRow dr in logo.Tables[0].Rows)
            {
                var IFile = new ImageFile();
                IFile.FileName = dr["COM_IMAGEPATH"].ToString();

                ViewBag.Logo = logo.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
            }

            var languageUsed = Session["LanguageChosen"] == null ? ProjectStaticString.DefaultLanguage : Session["LanguageChosen"].ToString();
            ahm.AdminSelectedLanguage = languageUsed;

            int ok = 0;
            string msg = string.Empty;
            var languageList = Misc.GetLanguageList(ref ok, ref msg);
            ahm.LanguageList = from c in languageList
                               select new SelectListItem
                               {
                                   Selected = (c.Value == languageUsed) ? true : false,
                                   Text = c.Text,
                                   Value = c.Value
                               };

            ViewBag.UserName = Session["Admin"].ToString();

            if (Session["page"] != null)
            {
                ViewBag.page = Session["page"];
                Session["page"] = null;
            }
            
            


            return View("Home", ahm);
        }
        #endregion

        #region AdminSearchUser
        public ActionResult AdminSearchUser(string Type)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            if (Type == ProjectStaticString.ChangePassword)
            {
                ViewBag.MainModule = ECFBase.Resources.Admin.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.Admin.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.Admin.mnuChgPassword;
            }
            else if (Type == ProjectStaticString.ChangePin)
            {
                ViewBag.MainModule = ECFBase.Resources.Admin.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.Admin.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.Admin.mnuChgPIN;
            }
            else if (Type == ProjectStaticString.ChangeMemberPermission)
            {
                ViewBag.MainModule = ECFBase.Resources.Admin.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.Admin.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.Admin.mnuChgMemPer;
            }
            else if (Type == ProjectStaticString.TradeableWalletTopUp)
            {
                ViewBag.MainModule = ECFBase.Resources.Admin.lblWallet;
                ViewBag.SubModule = ECFBase.Resources.Admin.mnuUtility;
                ViewBag.TypeResource = Resources.Admin.mnuTradeableWallet;
            }
            else if (Type == ProjectStaticString.UntradeableWalletTopUp)
            {
                ViewBag.MainModule = ECFBase.Resources.Admin.lblWallet;
                ViewBag.SubModule = ECFBase.Resources.Admin.mnuUtility;
                ViewBag.TypeResource = Resources.Admin.mnuUntradeableWallet;
            }
            else if (Type == ProjectStaticString.ChangeOwnership)
            {
                ViewBag.MainModule = ECFBase.Resources.Admin.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.Admin.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.Admin.mnuChangeOwnership;
            }
            else if (Type == ProjectStaticString.ChangeRanking)
            {
                ViewBag.MainModule = ECFBase.Resources.Admin.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.Admin.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.Admin.mnuChangeRanking;
            }
            else if (Type == ProjectStaticString.ChangeSponsor)
            {
                ViewBag.MainModule = ECFBase.Resources.Admin.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.Admin.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.Admin.mnuChangeSponsor;
            }
            else if (Type == ProjectStaticString.ChangeSponsor)
            {
                ViewBag.MainModule = ECFBase.Resources.Admin.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.Admin.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.Admin.mnuChangeSponsor;
            }
            else if (Type == ProjectStaticString.ChangeSponsor)
            {
                ViewBag.MainModule = ECFBase.Resources.Admin.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.Admin.mnuUtility;
                ViewBag.TypeResource = ECFBase.Resources.Admin.mnuChangeSponsor;
            }
            else if (Type == ProjectStaticString.FunctionSetting)
            {
                ViewBag.MainModule = ECFBase.Resources.Admin.mnuMembers;
                ViewBag.SubModule = ECFBase.Resources.Admin.mnuUtility;
                ViewBag.TypeResource = "Function Setting";
            }

            ViewBag.Type = Type;
            UsernameSearchModel model = new UsernameSearchModel();
            return PartialView("AdminSearchUser", model);
        }

        public ActionResult SearchUser(string Username, string Type)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            if (Type == ProjectStaticString.ChangePassword)
            {
                //redirect to Controller
                return RedirectToAction("ChangePassword", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.ChangePin)
            {
                return RedirectToAction("ChangePin", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.ChangeMemberPermission)
            {
                return RedirectToAction("ChangeMemberPermission", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.TradeableWalletTopUp || Type == ProjectStaticString.UntradeableWalletTopUp)
            {
                return RedirectToAction("WalletAdjustment", "AdminWallet", new { Username = Username, Type = Type });
            }
            else if (Type == ProjectStaticString.ChangeOwnership)
            {
                return RedirectToAction("ChangeOwnership", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.ChangeRanking)
            {
                return RedirectToAction("ChangeRanking", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.ChangeSponsor)
            {
                return RedirectToAction("ChangeSponsor", "AdminMember", new { Username = Username });
            }
            else if (Type == ProjectStaticString.FunctionSetting)
            {
                return RedirectToAction("GetFunctionSettingList", "AdminMember", new { Member = Username });
            }
            return Home();
        }
        #endregion


        public ActionResult SendEmail()
        {



            DataSet dsMembers = MemberShopCenterDB.GetAllMemberList();

            foreach (DataRow dr in dsMembers.Tables[0].Rows)
            {

                try
                {
                    string path = Server.MapPath(@"~/Images/Company/nlh99newlogo.png"); // my logo is placed in images folder

                    string massege = string.Empty;
                    string Title = string.Empty;

                    string nickname = dr["CUSR_FULLNAME"].ToString();
                    string Name = dr["CUSR_USERNAME"].ToString();
                    string password = dr["CUSR_PASSWORD"].ToString();
                    string Email = dr["CUSR_EMAIL"].ToString();



                    Title = "NLH 99";
                    massege = "<img height=\"200\" width=\"200\" src=cid:myImageID /><br/><br/>{0}<br/><br/>Congratulations and welcome to NLH99 !<br/>We will do our best toward all the success, Let us work together to create wealth.<br/><br/>恭喜你已经成功注册成为NLH99平台的会员！NLH99平台欢迎大家的参与，并致力于大家一起创造财富！迎接辉煌！<br/><br/> 会员账号 Member ID         :{1} <br/>登录密码 Login Password ：{2} <br/><br/>Please login to www.nlh99.com and change your login password and security PIN to prevent unauthorized access.<br/><br/>请登录 www.nlh99.com 会员后台后，更改您的登录密码和安全密码，以防被人盗用。";


                    var contentSMS = string.Format(massege, nickname, Name, Authentication.Decrypt(password));

                    string result = Misc.SendEmailGood("noreply@nlh99.com", Email, Title, contentSMS, path);

                }
                catch (Exception e)
                {
                    // failed to send sms is ok, but better dont block the registration flow
                    return Json(e.ToString(), JsonRequestBehavior.AllowGet);
                }
                

            }

            return Json("All Send", JsonRequestBehavior.AllowGet);
        }

        

    }
}


