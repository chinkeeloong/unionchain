﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Web.Services;
using ECFBase.Resources;
using System.Collections.Generic;
using ECFBase.Helpers;

namespace ECFBase.Controllers.Admin
{
    public class AdminStockController : Controller
    {
        #region Mobile
        public ActionResult AddMobileList(int selectedPage = 1, string searchMemberList = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            string languageCode = Session["LanguageChosen"].ToString();

            PaginationMobileAgent model = new PaginationMobileAgent();
            DataSet dsAgents = MemberDB.GetAllMobileAgents(searchMemberList, selectedPage, languageCode, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            RetrieveMobilePosition(model);

            foreach (DataRow dr in dsAgents.Tables[0].Rows)
            {
                var mobileAgent = new MobileAgent();
                mobileAgent.Number = dr["rownumber"].ToString();
                mobileAgent.AgentID = dr["CUSR_USERNAME"].ToString();
                mobileAgent.FullName = dr["CUSR_FULLNAME"].ToString();
                mobileAgent.JoinedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToShortDateString();
                mobileAgent.Country = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                mobileAgent.AdminApproved = dr["CMEM_APPROVEBY"].ToString();
                mobileAgent.DateApproved = Convert.ToDateTime(dr["CMEM_UPDATEDON"]).ToShortDateString();
                model.MobileList.Add(mobileAgent);
            }

            return PartialView("AddMobile", model);
        }

        private static void RetrieveMobilePosition(PaginationMobileAgent model)
        {
            List<SelectListItem> positions = new List<SelectListItem>();

            //SelectListItem fcPos = new SelectListItem();
            //fcPos.Text = Resources.Admin.lblMobile;
            //fcPos.Value = "1";
            //positions.Add(fcPos);

            SelectListItem fcPos = new SelectListItem();
            fcPos.Text = ECFBase.Resources.Admin.lblStockist;
            fcPos.Value = "2";
            positions.Add(fcPos);

            fcPos = new SelectListItem();
            fcPos.Text = Resources.Admin.lblRegional;
            fcPos.Value = "3";
            positions.Add(fcPos);

            fcPos = new SelectListItem();
            fcPos.Text = Resources.Admin.lblCountry;
            fcPos.Value = "4";
            positions.Add(fcPos);

            model.Positions = positions;
        }

        public ActionResult DisqualifyMobile(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            MemberDB.DisqualifiedMobile(username, Session["Admin"].ToString());

            return AddMobileList(1, "");
        }

        public ActionResult AppointMobileUser(string username, string percentage)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";

            var ds = MemberDB.GetMemberByUsername(username, out ok, out msg);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Response.Write("Invalid Username");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            MemberDB.AppointMobileUser(username, Session["Admin"].ToString(), "0");
            return AddMobileList();
        }

        [HttpPost]
        public JsonResult findUserName(string memberID)
        {
            int ok = 0;
            string msg = string.Empty;

            if (string.IsNullOrEmpty(memberID))
            {
                Response.Write(Resources.Admin.warningInvalidUsername2);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            var dsMember = MemberDB.GetMemberByUsername(memberID, out ok, out msg);

            if (dsMember.Tables[0].Rows.Count == 0)
            {
                Response.Write(Resources.Admin.warningInvalidUsername2);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            var result = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RequestTopUp()
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                PaginationMobileAgent model = new PaginationMobileAgent();


                DataSet dsWallet = AdminGeneralDB.GetAllPendingRequest();

                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    MobileRequestList Request = new MobileRequestList();
                    Request.PinID = dr["CTUR_ID"].ToString();
                    Request.Date = dr["CTUR_CREATEDON"].ToString();
                    Request.ReferenceOrder = dr["CTUR_REFFERALID"].ToString();
                    Request.MemberID = dr["CUSR_USERNAME"].ToString();
                    Request.FullName = dr["CTUR_FULLNAME"].ToString();
                    Request.TopUpFor = dr["CTUR_FROM"].ToString();
                    Request.Currency = dr["CTUR_CURRENCY"].ToString();
                    Request.LocalAmount = float.Parse(dr["CTUR_LOCAL_AMOUNT"].ToString()).ToString("n2");
                    Request.Amount = float.Parse(dr["CTUR_AMOUNT"].ToString()).ToString("n2");
                    Request.Remark = dr["CTUR_REMARK"].ToString();
                    Request.AdminRemark = dr["CTUR_ADMIN_REMARK"].ToString();
                    Request.VerifyBy = dr["CTUR_APPROVEBY"].ToString();

                    Request.ApprovedBy = dr["CTUR_GENERATEBY"].ToString();


                    if (dr["CTUR_STATUS"].ToString() == "Pending")
                    {
                        Request.VerifyOn = "";
                        Request.ApprovedOn = "";
                    }
                    else
                    {
                        if (dr["CTUR_STATUS"].ToString() == "Approve")
                        {
                            Request.VerifyOn = dr["CTUR_APPROVETIME"].ToString();
                            Request.ApprovedOn = "";
                        }
                        else
                        {
                            Request.VerifyOn = dr["CTUR_APPROVETIME"].ToString();
                            Request.ApprovedOn = dr["CTUR_GENERATEON"].ToString();
                        }
                    }

                    Request.Status = dr["CTUR_STATUS"].ToString();

                    model.MobileTopUpList.Add(Request);
                }

                return PartialView("RequestTopUp", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult VeryfyRequestTopUp(string ID)
        {
            int ok = 0;
            string msg = string.Empty;

            PaginationMobileAgent model = new PaginationMobileAgent();


            DataSet dr = AdminGeneralDB.GetRequestTopUpByID(Convert.ToInt32(ID));
            model.PinID = dr.Tables[0].Rows[0]["CTUR_ID"].ToString();
            model.Date = dr.Tables[0].Rows[0]["CTUR_CREATEDON"].ToString();
            model.Username = dr.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            var ds = MemberDB.GetMemberByUsername(model.Username, out ok, out msg);
            model.Name = ds.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.USDAmount = float.Parse(dr.Tables[0].Rows[0]["CTUR_AMOUNT"].ToString()).ToString("n2");
            model.LocalAmount = float.Parse(dr.Tables[0].Rows[0]["CTUR_LOCAL_AMOUNT"].ToString()).ToString("n2");
            model.Country = Misc.GetCountryNameByCountryCode(ds.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString());
            model.Status = dr.Tables[0].Rows[0]["CTUR_STATUS"].ToString();
            int extensionIndex = dr.Tables[0].Rows[0]["CTUR_IMAGE"].ToString().LastIndexOf('\\');
            string imageName = dr.Tables[0].Rows[0]["CTUR_IMAGE"].ToString().Substring(extensionIndex + 1);
            model.ImagePath = "http://" + Request.Url.Authority + "/Images/Payment" + "/" + imageName;
            model.PaymentMethod = dr.Tables[0].Rows[0]["CTUR_PAYMENT_METHOD"].ToString();
            model.Remark = dr.Tables[0].Rows[0]["CTUR_REMARK"].ToString();



            return PartialView("RequestTopUpDataByID", model);

        }

        public ActionResult GenerateRequestTopUp(string ID)
        {
            int ok = 0;
            string msg = string.Empty;

            PaginationMobileAgent model = new PaginationMobileAgent();


            DataSet dr = AdminGeneralDB.GetRequestTopUpByID(Convert.ToInt32(ID));
            model.PinID = dr.Tables[0].Rows[0]["CTUR_ID"].ToString();
            model.Date = dr.Tables[0].Rows[0]["CTUR_CREATEDON"].ToString();
            model.Username = dr.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
            var ds = MemberDB.GetMemberByUsername(model.Username, out ok, out msg);
            model.Name = ds.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.USDAmount = float.Parse(dr.Tables[0].Rows[0]["CTUR_AMOUNT"].ToString()).ToString("n2");
            model.LocalAmount = float.Parse(dr.Tables[0].Rows[0]["CTUR_LOCAL_AMOUNT"].ToString()).ToString("n2");
            model.Country = Misc.GetCountryNameByCountryCode(ds.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString());
            model.Status = dr.Tables[0].Rows[0]["CTUR_STATUS"].ToString();
            int extensionIndex = dr.Tables[0].Rows[0]["CTUR_IMAGE"].ToString().LastIndexOf('\\');
            string imageName = dr.Tables[0].Rows[0]["CTUR_IMAGE"].ToString().Substring(extensionIndex + 1);
            model.ImagePath = "http://" + Request.Url.Authority + "/Images/Payment" + "/" + imageName;
            model.PaymentMethod = dr.Tables[0].Rows[0]["CTUR_PAYMENT_METHOD"].ToString();
            model.Remark = dr.Tables[0].Rows[0]["CTUR_REMARK"].ToString();
            model.ApproveBy = dr.Tables[0].Rows[0]["CTUR_APPROVEBY"].ToString();
            model.ApproveOn = dr.Tables[0].Rows[0]["CTUR_APPROVETIME"].ToString();


            return PartialView("GenerateTopUpDataByID", model);

        }

        public ActionResult ApproveRequestTopUp(string ID, string AdminRemark)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (string.IsNullOrEmpty(AdminRemark))
                {
                    AdminRemark = " ";
                }


                AdminStockistDB.ApproveRequestTopUp(ID, AdminRemark, Session["Admin"].ToString());

                return RequestTopUp();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RejectRequestTopUp(string ID, string AdminRemark)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if(AdminRemark =="" || AdminRemark == null)
                {
                    Response.Write("Please key in reject remark!");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                AdminStockistDB.RejectRequestTopUp(ID, AdminRemark, Session["Admin"].ToString());

                return RequestTopUp();
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RequestTopUpHistory(int selectedPage = 1)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int pages = 0;
                int ok = 0;
                string msg = string.Empty;

                PaginationMobileAgent model = new PaginationMobileAgent();


                DataSet dsWallet = AdminGeneralDB.GetAllRequest(selectedPage, out pages, out ok, out msg);

                selectedPage = ConstructPageList(selectedPage, pages, model);
                string previous = string.Empty;
                foreach (DataRow dr in dsWallet.Tables[0].Rows)
                {
                    MobileRequestList Request = new MobileRequestList();
                    Request.Date = dr["CTUR_CREATEDON"].ToString();
                    Request.ReferenceOrder = dr["CTUR_REFFERALID"].ToString();
                    Request.Status = dr["CTUR_STATUS"].ToString();
                    Request.MemberID = dr["CUSR_USERNAME"].ToString();
                    Request.FullName = dr["CTUR_FULLNAME"].ToString();
                    Request.TopUpFor = dr["CTUR_FROM"].ToString();
                    Request.Currency = dr["CTUR_CURRENCY"].ToString();
                    Request.LocalAmount = float.Parse(dr["CTUR_LOCAL_AMOUNT"].ToString()).ToString("n2");
                    Request.Amount = float.Parse(dr["CTUR_AMOUNT"].ToString()).ToString("n2");
                    Request.GeneratedBy = dr["CTUR_GENERATEBY"].ToString();
                    Request.GeneratedOn = dr["CTUR_GENERATEON"].ToString();
                    Request.AdminID = dr["CTUR_APPROVEBY"].ToString();
                    Request.ProcessedDate = dr["CTUR_APPROVETIME"].ToString();
                    Request.Remark = dr["CTUR_REMARK"].ToString();
                    Request.ImagePath = dr["CTUR_IMAGE"].ToString();




                    model.MobileTopUpList.Add(Request);
                }

                return PartialView("RequestTopUpHistory", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public void DownloadPaymentSlip(string fileName, string refOrder)
        {
            //because the filename is fullpath, we just need the file name
            string fileNameRemoved = fileName.Substring(fileName.LastIndexOf('\\') + 1);
            refOrder = string.Format("{0}.jpg", refOrder.Trim());
            Response.Buffer = false; //transmitfile self buffers
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            //Response.AddHeader("Content-Disposition", string.Format("attachment; filename=myfile.pdf"));
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", System.Web.HttpUtility.UrlEncode(refOrder, System.Text.Encoding.UTF8)));
            Response.TransmitFile(Request.MapPath("~/Images/Payment/") + fileNameRemoved); //transmitfile keeps entire file from loading into memory
            Response.End();
        }

        public ActionResult GenerateTopUp(string ID)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                AdminStockistDB.GenerateRequestTopUp(ID, Session["Admin"].ToString());

                return RequestTopUp();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion        

        #region ConstructPageList
        private int ConstructPageList(int selectedPage, int pages, PaginationMobileAgent model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        #endregion

    }
}
