﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Web.Services;
using ECFBase.Resources;
using ECFBase.Helpers;
using ECFBase.ThirdParties;
using System.Text;
using System.Collections.Specialized;
using System.Net;
using ClosedXml.Generic;
using DocumentFormat.OpenXml;
using DataGridViewAutoFilter;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace ECFBase.Controllers.Admin
{
    public class AdminMemberController : Controller
    {

        #region FunctionSetting
        public ActionResult GetFunctionSettingList(int selectedPage = 1, string Member = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int status = 0;
            int ok = 0;
            string message = string.Empty;
            string memberFunctionSetting = string.Empty;
            int pages = 0; 
            string languageCode = Session["LanguageChosen"].ToString();

            MemberFunctionSettingModel model = new MemberFunctionSettingModel();
            DataSet ds = AdminMemberDB.GetMemberFunctionSetting(Member, selectedPage, out pages, out status, out message);

           

            selectedPage = ConstructPageList(selectedPage, pages, model);

            if(ds.Tables[0].Rows.Count!=0)
            {

                model.FunctionID = ds.Tables[0].Rows[0]["rownumber"].ToString();
                model.Username = ds.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                model.Fullname = ds.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
             
                model.TradeableHash = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_TRADEABLE_HASH"]);
                model.TradeableHashPerfomance = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_TRADEABLE_HASH_MANAGEMENT"]);
                model.TradeableHashManagement = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_TRADEABLE_HASH_MANAGEMENT"]);
                model.TradeableTransfer = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_TRADEABLE_TRANSFER"]);
                model.TradeableSellTrade = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_TRADEABLE_SELLTRADE"]);
                model.TradeableBuyTrade = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_TRADEABLE_BUYTRADE"]);
                
                model.UnTradeableHash = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_UNTRADEABLE_HASH"]);
                model.UnTradeableHashPerfomance = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_UNTRADEABLE_HASH_PERFORMANCE"]);
                model.UnTradeableHashManagement = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_UNTRADEABLE_HASH_MANAGEMENT"]);
                model.UnTradeableTransfer = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_UNTRADEABLE_TRANSFER"]);
                model.UnTradeableSellTrade = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_UNTRADEABLE_SELLTRADE"]);
                model.UnTradeableBuyTrade = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_UNTRADEABLE_BUYTRADE"]);

                model.AccessRight = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMFS_ACCESSRIGHT"]);

            }

             

            return PartialView("MemberFunctionSetting", model);
        }

        [HttpPost]
        public ActionResult SaveFunctionSettingMethod(string data, string userId)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok;
                string msg;
                string[] parameters = data.Split(new string[] { "&" }, StringSplitOptions.None);

                string Username = "";
                bool UnTradeableHash = false,
                    UnTradeableHashManagement = false,
                    UnTradeableHashPerfomance = false,
                    UnTradeableTransfer = false,
                    UnTradeableSellTrade = false,
                    UnTradeableBuyTrade = false;
                bool TradeableHash = false,
                    TradeableHashManagement = false,
                    TradeableHashPerfomance = false,
                    TradeableTransfer = false,
                    TradeableSellTrade = false,
                    TradeableBuyTrade = false,
                    AccessRight = false;
                foreach (string parameter in parameters)
                {
                    string[] value = parameter.Split(new string[] { "=" }, StringSplitOptions.None);
                    if(value[0] == "username")
                    {
                        Username = value[1];
                    }
                    if(value[0] == "UnTradeableHash" && value[1] == "true")
                    {
                        UnTradeableHash = true;
                    }
                    if (value[0] == "UnTradeableHashManagement" && value[1] == "true")
                    {
                        UnTradeableHashManagement = true;
                    }
                    if (value[0] == "UnTradeableHashPerfomance" && value[1] == "true")
                    {
                        UnTradeableHashPerfomance = true;
                    }
                    if (value[0] == "UnTradeableTransfer" && value[1] == "true")
                    {
                        UnTradeableTransfer = true;
                    }
                    if (value[0] == "UnTradeableSellTrade" && value[1] == "true")
                    {
                        UnTradeableSellTrade = true;
                    }
                    if (value[0] == "UnTradeableBuyTrade" && value[1] == "true")
                    {
                        UnTradeableBuyTrade = true;
                    }
                    if (value[0] == "TradeableHash" && value[1] == "true")
                    {
                        TradeableHash = true;
                    }
                    if (value[0] == "TradeableHashManagement" && value[1] == "true")
                    {
                        TradeableHashManagement = true;
                    }
                    if (value[0] == "TradeableHashPerfomance" && value[1] == "true")
                    {
                        TradeableHashPerfomance = true;
                    }
                    if (value[0] == "TradeableTransfer" && value[1] == "true")
                    {
                        TradeableTransfer = true;
                    }
                    if (value[0] == "TradeableSellTrade" && value[1] == "true")
                    {
                        TradeableSellTrade = true;
                    }
                    if (value[0] == "TradeableBuyTrade" && value[1] == "true")
                    {
                        TradeableBuyTrade = true;
                    }
                    if (value[0] == "AccessRight" && value[1] == "true")
                    {
                        AccessRight = true;
                    }

                }


                AdminMemberDB.UpdateMemberFunctionSetting(Username, UnTradeableHash, UnTradeableHashManagement, UnTradeableHashPerfomance, UnTradeableTransfer, UnTradeableSellTrade, UnTradeableBuyTrade, 
                    TradeableHash, TradeableHashManagement, TradeableHashPerfomance, TradeableTransfer, TradeableSellTrade, TradeableBuyTrade, AccessRight, 
                    Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);

                //return GetFunctionSettingList();
                return RedirectToAction("GetFunctionSettingList", "AdminMember", new { Member = Username });
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region MembersList
        public ActionResult NextPageMemberList(string selectedPage, string walletType, string searchMember)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                return RedirectToAction("MembersList", "AdminMember", new { selectedPage = selectedPage, searchMemberList = searchMember });
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SearchMemberListByFC(string fc, string searchFC, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberModel model = new PaginationMemberModel();
            model.MemberType = "Member";
            DataSet dsMembers = MemberDB.GetAllMembersByFilteringCriteria(fc, searchFC, selectedPage, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMembers.Tables[0].Rows)
            {
                var member = new MemberModel();
                member.Number = dr["rownumber"].ToString();
                member.MemberId = dr["CMEM_ID"].ToString();
                member.Username = dr["CUSR_USERNAME"].ToString();
                member.SurName = dr["CUSR_FULLNAME"].ToString();
                //member.BonusWallet = float.Parse(dr["CMEM_BONUSWALLET"].ToString());
                member.Intro = dr["CMEM_INTRO"].ToString();
                member.Upline = dr["CMEM_INTRO"].ToString();
                member.RegisteredBy = dr["CMEM_REGISTEREDBY"].ToString();
                member.PackageName = dr["PACKAGE"].ToString();
                member.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
                member.Invest = dr["INVEST"].ToString();
                member.JoinedDate = (DateTime)dr["CMEM_CREATEDON"];
                member.UserActive = (bool)dr["CUSR_ACTIVE"];
                member.IC = dr["CUSRINFO_IC"].ToString();
                model.MemberList.Add(member);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();
            model.Months = Misc.ConstructsMonth().ToList();
            model.Years = Misc.ConstructYears(2013).ToList();
            model.SelectedYears = DateTime.Now.Year.ToString();
            model.SelectedMonth = DateTime.Now.Month.ToString();

            ViewBag.Title = "Members List";
            return PartialView("MembersList", model);
        }
        public ActionResult SearchMemberListByJoinedDate(string month, string year, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberModel model = new PaginationMemberModel();
            model.MemberType = "Member";
            DataSet dsMembers = MemberDB.GetAllMembersByJoinedDate(month, year, selectedPage, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMembers.Tables[0].Rows)
            {
                var member = new MemberModel();
                member.Number = dr["rownumber"].ToString();
                member.MemberId = dr["CMEM_ID"].ToString();
                member.Username = dr["CUSR_USERNAME"].ToString();
                member.SurName = dr["CUSR_FULLNAME"].ToString();
                //member.BonusWallet = float.Parse(dr["CMEM_BONUSWALLET"].ToString());
                member.RegisteredBy = dr["CMEM_REGISTEREDBY"].ToString();
                member.PackageName = dr["PACKAGE"].ToString();
                member.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
                member.Invest = dr["INVEST"].ToString();
                member.JoinedDate = (DateTime)dr["CMEM_CREATEDON"];
                member.UserActive = (bool)dr["CUSR_ACTIVE"];
                member.Intro = dr["CMEM_INTRO"].ToString();
                member.IC = dr["CUSRINFO_IC"].ToString();
                model.MemberList.Add(member);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();
            model.Months = Misc.ConstructsMonth().ToList();
            model.Years = Misc.ConstructYears(2013).ToList();
            model.SelectedYears = year;
            model.SelectedMonth = month;

            ViewBag.Title = "Members List";
            return PartialView("MembersList", model);
        }
        public ActionResult MembersList(int selectedPage = 1, string Option = "0", string Text = "", string Day = "0", string Month = "0", string Year = "0")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            if (Day == "Days")
            {
                Day = "0";
            }
            if (Month == "Months")
            {
                Month = "0";
            }
            if (Year == "Years")
            {
                Year = "0";
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberModel model = new PaginationMemberModel();
            model.MemberType = "Member";


            DataSet dsMembers = new DataSet();

            if (Option == "0")
            {
                dsMembers = MemberDB.GetAllMembers(Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else if (Option == "USERNAME")
            {
                dsMembers = MemberDB.GetAllMemberByUsername(Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else if (Option == "FULLNAME")
            {
                dsMembers = MemberDB.GetAllMemberByFullName(Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else if (Option == "RANKING")
            {
                dsMembers = MemberDB.GetAllMemberByRanking(Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            //else if (Option == "PACKAGE")
            //{
            //    dsMembers = MemberDB.GetAllMemberByPackage(Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            //}
            else if (Option == "SPONSOR")
            {
                dsMembers = MemberDB.GetAllMemberBySponsor(Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else if (Option == "COUNTRY")
            {
                dsMembers = MemberDB.GetAllMemberByCountry(Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }
            else
            {
                dsMembers = MemberDB.GetAllMembers(Text, Convert.ToInt32(Day), Convert.ToInt32(Month), Convert.ToInt32(Year), selectedPage, out pages, out status, out message);
            }




            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMembers.Tables[0].Rows)
            {
                var member = new MemberModel();
                member.Number = dr["rownumber"].ToString();
                member.MemberId = dr["CMEM_ID"].ToString();
                member.Username = dr["CUSR_USERNAME"].ToString();
                member.MemberCountry = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                member.SurName = dr["CUSR_FULLNAME"].ToString();
                //member.BonusWallet = float.Parse(dr["CMEM_BONUSWALLET"].ToString());
                member.Intro = dr["CMEM_INTRO"].ToString();
                member.Upline = dr["Upline"].ToString();
                member.RegisteredBy = dr["CMEM_REGISTEREDBY"].ToString();
                member.PackageName = dr["PACKAGE"].ToString();
                member.Rank = Misc.GetMemberRanking(dr["CRANK_CODE"].ToString());
                //member.Invest = dr["INVEST"].ToString();
                member.Total = (float.Parse(dr["CMEM_UNTRADEABLEWALLET"].ToString()) + float.Parse(dr["CMEM_TRADEABLEWALLET"].ToString())).ToString();
                member.JoinedDate = (DateTime)dr["CMEM_CREATEDON"];
                member.UserActive = (bool)dr["CUSR_ACTIVE"];
                member.IC = dr["CUSR_ICORPASSPORT"].ToString();
                model.MemberList.Add(member);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteria().ToList();

            model.Days = Misc.ConstructsDay().ToList();
            model.Months = Misc.ConstructsMonth().ToList();
            model.Years = Misc.ConstructYears(2018).ToList();

            model.SelectedDays = Day;
            model.SelectedYears = Year;
            model.SelectedMonth = Month;

            int isShow = 0;
            Session["isShow"] = isShow;

            ViewBag.Title = "Members List";
            return PartialView("MembersList", model);
        }
        public ActionResult MemberHome(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            try
            {
                EditMemberModel memberMC = new EditMemberModel();

                int ok;
                string msg;
                string Username = Session["Admin"].ToString();
                string languageCode = Session["LanguageChosen"].ToString();

                DataSet dsMember = AdminMemberDB.GetMemberInfoByUserName(username, out ok, out msg);

                memberMC.Username = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                memberMC.FullName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                //memberMC.Member.FirstName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                //memberMC.UserInfo.Nickname = dsMember.Tables[0].Rows[0]["CUSRINFO_NICKNAME"].ToString();

                DataSet dsCountry = AdminGeneralDB.GetCountryByID(Convert.ToInt32(dsMember.Tables[0].Rows[0]["CCOUNTRY_ID"]), out ok, out msg);
                memberMC.SelectedCountry = dsCountry.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                memberMC.ICLength = dsCountry.Tables[0].Rows[0]["CCOUNTRY_ICLENGTH"].ToString();
                memberMC.SelectedICType = dsCountry.Tables[0].Rows[0]["CCOUNTRY_ICTYPE"].ToString();

                memberMC.MemberEmail = dsMember.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
                memberMC.CellPhone = dsMember.Tables[0].Rows[0]["CUSR_CELLPHONE"].ToString();
                memberMC.IdentityNumber = dsMember.Tables[0].Rows[0]["CUSR_ICORPASSPORT"].ToString();
                
                #region Country
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                memberMC.Countries = from c in countries
                                     select new SelectListItem
                                     {
                                         Selected = false,
                                         Text = c.CountryName,
                                         Value = c.CountryCode
                                     };
                 

                foreach (CountrySetupModel temp in countries)
                {
                    if (temp.CountryCode == memberMC.SelectedCountry)
                    {
                        memberMC.MobileCountryCode = temp.MobileCode;
                        break;
                    }
                }

                if (memberMC.SelectedCountry == string.Empty && memberMC.Countries.Any())
                {
                    var firstOption = memberMC.Countries.First();
                    memberMC.SelectedCountry = firstOption != null ? firstOption.Value : string.Empty;
                }

                #endregion

                #region Province
                DataSet dsProvinces = AdminGeneralDB.GetAllProvincesByCountry(Session["LanguageChosen"].ToString(), memberMC.SelectedCountry);
                List<ProvinceSetupModel> provinceList = new List<ProvinceSetupModel>();

                foreach (DataRow dr in dsProvinces.Tables[0].Rows)
                {
                    ProvinceSetupModel proModel = new ProvinceSetupModel();
                    proModel.ProvinceCode = dr["CPROVINCE_CODE"].ToString();
                    proModel.ProvinceName = dr["CMULTILANGPROVINCE_NAME"].ToString();

                    provinceList.Add(proModel);
                }
                
                #endregion
                                

                return PartialView("ViewMemberDetails", memberMC);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ViewMemberDetails()
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                EditMemberModel memberMC = new EditMemberModel();

                int ok;
                string msg;
                string Username = Session["Admin"].ToString();
                string languageCode = Session["LanguageChosen"].ToString();

                DataSet dsMember = AdminMemberDB.GetMemberInfoByUserName(Username, out ok, out msg);

                memberMC.Username = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                memberMC.FullName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();

                DataSet dsCountry = AdminGeneralDB.GetCountryByID(Convert.ToInt32(dsMember.Tables[0].Rows[0]["CCOUNTRY_ID"]), out ok, out msg);
                memberMC.SelectedCountry = dsCountry.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                memberMC.ICLength = dsCountry.Tables[0].Rows[0]["CCOUNTRY_ICLENGTH"].ToString();
                memberMC.SelectedICType = dsCountry.Tables[0].Rows[0]["CCOUNTRY_ICTYPE"].ToString();

                memberMC.MemberEmail = dsMember.Tables[0].Rows[0]["CUSR_EMAIL"].ToString();
                memberMC.CellPhone = dsMember.Tables[0].Rows[0]["CUSR_CELLPHONE"].ToString();
                memberMC.IdentityNumber = dsMember.Tables[0].Rows[0]["CUSR_ICORPASSPORT"].ToString();



                #region Country
                List<CountrySetupModel> countries = Misc.GetAllCountryList(languageCode, ref ok, ref msg);

                memberMC.Countries = from c in countries
                                     select new SelectListItem
                                     {
                                         Selected = false,
                                         Text = c.CountryName,
                                         Value = c.CountryCode
                                     };

                memberMC.Countries = from c in countries
                                              select new SelectListItem
                                              {
                                                  Selected = false,
                                                  Text = c.CountryName,
                                                  Value = c.CountryCode
                                              };

                foreach (CountrySetupModel temp in countries)
                {
                    if (temp.CountryCode == memberMC.SelectedCountry)
                    {
                        memberMC.MobileCountryCode = temp.MobileCode;
                        break;
                    }
                }

                if (memberMC.SelectedCountry == string.Empty && memberMC.Countries.Any())
                {
                    var firstOption = memberMC.Countries.First();
                    memberMC.SelectedCountry = firstOption != null ? firstOption.Value : string.Empty;
                }

                #endregion
                

                return PartialView("ViewMemberDetails", memberMC);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateMemberDetails(EditMemberModel mem)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok = 0;
                string msg = "";


                #region Check IC
                if (string.IsNullOrEmpty(mem.IdentityNumber))
                {
                    Response.Write(Resources.Member.warningPlsKeyInIC);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex digits = new Regex("^[0-9]*$");
                Regex alphadigits = new Regex("^[a-zA-Z0-9]*$");

                if (mem.SelectedICType == "True")
                {
                    if (!alphadigits.IsMatch(mem.IdentityNumber))
                    {
                        Response.Write(Resources.Member.warningICAlpha);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (!digits.IsMatch(mem.IdentityNumber))
                    {
                        Response.Write(Resources.Member.warningICNumeric);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                //if (mem.IdentityNumber.Length != float.Parse(mem.ICLength))
                //{
                //    Response.Write(Resources.Member.warningICLength);
                //    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                //}

                #endregion
                #region Fullname
                if (string.IsNullOrEmpty(mem.FullName))
                {
                    Response.Write(Resources.Member.warningFullname);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion
                #region Phone
                if (string.IsNullOrEmpty(mem.MobileCountryCode))
                {
                    Response.Write(Resources.Member.warningPleaseSelectPhoneCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(mem.CellPhone))
                {
                    Response.Write(Resources.Member.warningPleaseKeyInPhoneNumber);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex phonereg = new Regex(@"^([a-zA-Z])$");

                if (phonereg.IsMatch(mem.CellPhone) == true)
                {
                    Response.Write(Resources.Member.warningPhoneNotAllowChar);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Country
                if (string.IsNullOrEmpty(mem.SelectedCountry))
                {
                    Response.Write(Resources.Member.warningSelectCountry);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #region Email
                if (string.IsNullOrEmpty(mem.MemberEmail))
                {
                    Response.Write(Resources.Member.msgInvalidEmail);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex reg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

                if (reg.IsMatch(mem.MemberEmail) == false)
                {
                    Response.Write(Resources.Member.msgEmailInvalid);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                #endregion
                MemberProfileDB.UpdateMemberFromAdmin(mem, out ok, out msg);


                return MembersList();

            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult CheckFileExtension(string file)
        {
            var isValid = Misc.IsFileExtensionValid(file);

            return Json(isValid.ToString(), JsonRequestBehavior.AllowGet);
        }

        public void ExportMemberList()
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=FullMemberList.csv");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintMemberList());
            sw.Close();
            Response.End();
        }

        private string PrintMemberList()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("No,Member Id,Full Name,IC, EMail ,Package Code,Ranking,Invest,Sponsor,Registered By,Join Date Time"));
            string message = string.Empty;
            DataSet dsMemberlist = new DataSet();
            dsMemberlist = MemberDB.GetAllMembersExport();

            foreach (DataRow dr in dsMemberlist.Tables[0].Rows)
            {

                sb.AppendLine(dr["rownumber"].ToString() + "," +
                              dr["CUSR_USERNAME"].ToString() + "," +
                              dr["CUSR_FULLNAME"].ToString() + "," +
                              dr["CUSRINFO_IC"].ToString() + "\t," +
                              dr["CUSR_EMAIL"].ToString() + "," +
                              dr["PACKAGE"].ToString() + "," +
                              Misc.GetMemberRanking(dr["CRANK_CODE"].ToString()) + "," +
                              float.Parse(dr["INVEST"].ToString()) + "," +
                              dr["CMEM_INTRO"].ToString() + "," +
                              dr["CMEM_REGISTEREDBY"].ToString() + "," +
                              Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));
            }

            return sb.ToString();
        }
        private string PrintEwallet()
        {
            StringBuilder sb = new StringBuilder();
            //sb.AppendLine(string.Format("No,Member Id,Full Name,IC,Package Code,Ranking,E-Wallet,Sponsor,Registered By,Join Date Time"));
            
            string message = string.Empty;
            DataSet dsMemberlist = new DataSet();
            dsMemberlist = MemberDB.GetAllMembersExport();

            foreach (DataRow dr in dsMemberlist.Tables[0].Rows)
            {

                string Name = dr["CUSR_FULLNAME"].ToString();

                sb.AppendLine(dr["rownumber"].ToString() + "," +
                              dr["CUSR_USERNAME"].ToString() + "," +
                              Name + "," +
                              dr["CUSRINFO_IC"].ToString() + "\t," +
                              dr["PACKAGE"].ToString() + "," +
                              Misc.GetMemberRanking(dr["CRANK_CODE"].ToString()) + "," +
                              float.Parse(dr["CMEM_BONUSWALLET"].ToString()) + "," +
                              dr["CMEM_INTRO"].ToString() + "," +
                              dr["CMEM_REGISTEREDBY"].ToString() + "," +
                              Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt"));



            }

            return sb.ToString();
        }
        

        #endregion
            
        #region Wallet Balance

        /// <summary>
        /// Retrieve list of members wallet balance
        /// </summary>
        /// <returns></returns>
        public ActionResult WalletsBalance(int selectedPage = 1, string searchMemberList = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberWalletModel model = new PaginationMemberWalletModel();
            DataSet dsMemberWallet = MemberDB.GetAllMemberWalletBalance(searchMemberList, selectedPage, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMemberWallet.Tables[0].Rows)
            {
                var memberWallet = new MemberWalletModel();
                memberWallet.Number = dr["rownumber"].ToString();
                memberWallet.Username = dr["CUSR_USERNAME"].ToString();
                memberWallet.Fullname = dr["CUSR_FULLNAME"].ToString();
                memberWallet.Rank = dr["CRANKSET_NAME"].ToString();
                memberWallet.UntradeableWallet = float.Parse(dr["CMEM_UNTRADEABLEWALLET"].ToString());
                memberWallet.TradeableWallet = float.Parse(dr["CMEM_TRADEABLEWALLET"].ToString());          
                memberWallet.Total = float.Parse(dr["TOTAL"].ToString());
                model.MemberWalletList.Add(memberWallet);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteriaWB().ToList();
            model.Months = Misc.ConstructsMonth().ToList();
            model.Years = Misc.ConstructYears(2018).ToList();
            model.SelectedYears = DateTime.Now.Year.ToString();
            model.SelectedMonth = DateTime.Now.Month.ToString();

            return PartialView("WalletsBalance", model);
        }

        /// <summary>
        /// Next page of member wallet balance list
        /// </summary>
        /// <param name="selectedPage"></param>
        /// <param name="searchMember"></param>
        /// <returns></returns>
        public ActionResult NextPageMemberWalletBalanceList(string selectedPage, string searchMember)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                return RedirectToAction("WalletsBalance", "AdminMember", new { selectedPage = selectedPage, searchMemberList = searchMember });
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Retrieve list of members wallet balance by joined date
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchMemberWalletBalanceListByJoinedDate(string month, string year, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;

            if(month =="Months")
            {
                month = "";
            }
            if (year == "Years")
            {
                year = "";
            }
            PaginationMemberWalletModel model = new PaginationMemberWalletModel();
            DataSet dsMemberWallet = MemberDB.GetAllMemberWalletBalanceByJoinedDate(month, year, selectedPage, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMemberWallet.Tables[0].Rows)
            {
                var memberWallet = new MemberWalletModel();
                memberWallet.Number = dr["rownumber"].ToString();
                memberWallet.Username = dr["CUSR_USERNAME"].ToString();
                memberWallet.Fullname = dr["CUSR_FULLNAME"].ToString();
                memberWallet.Rank = dr["CRANKSET_NAME"].ToString();
                memberWallet.UntradeableWallet = float.Parse(dr["CMEM_UNTRADEABLEWALLET"].ToString());
                memberWallet.TradeableWallet = float.Parse(dr["CMEM_TRADEABLEWALLET"].ToString());

                memberWallet.Total = float.Parse(dr["TOTAL"].ToString());
                model.MemberWalletList.Add(memberWallet);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteriaWB().ToList();
            model.Months = Misc.ConstructsMonth().ToList();
            model.Years = Misc.ConstructYears(2018).ToList();
            model.SelectedYears = year;
            model.SelectedMonth = month;

            return PartialView("WalletsBalance", model);
        }

        /// <summary>
        /// Retrieve list of members wallet balance by filtering criteria
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchMemberWalletBalanceListByFC(string fc, string searchFC, int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberWalletModel model = new PaginationMemberWalletModel();
            DataSet dsMemberWallet = MemberDB.GetAllMemberWalletBalanceByFilteringCriteria(fc, searchFC, selectedPage, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMemberWallet.Tables[0].Rows)
            {
                var memberWallet = new MemberWalletModel();
                memberWallet.Number = dr["rownumber"].ToString();
                memberWallet.Username = dr["CUSR_USERNAME"].ToString();
                memberWallet.Fullname = dr["FULLNAME"].ToString();
                memberWallet.Rank = dr["CRANKSET_NAME"].ToString();
                memberWallet.UntradeableWallet = float.Parse(dr["CMEM_UNTRADEABLEWALLET"].ToString());
                memberWallet.TradeableWallet = float.Parse(dr["CMEM_TRADEABLEWALLET"].ToString());
                memberWallet.Total = float.Parse(dr["TOTAL"].ToString());
                model.MemberWalletList.Add(memberWallet);
            }

            model.FilteringCriteria = Misc.ConstructsFilteringCriteriaWB().ToList();
            model.Months = Misc.ConstructsMonth().ToList();
            model.Years = Misc.ConstructYears(2018).ToList();
            model.SelectedYears = DateTime.Now.Year.ToString();
            model.SelectedMonth = DateTime.Now.Month.ToString();

            return PartialView("WalletsBalance", model);
        }

        #endregion
        
        #region Sponsor Chart
        public ActionResult SponsorChartTree(string memberUsername, int? memberLevel)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            string username;
            if (memberUsername == null)
            {
                username = Session["Admin"].ToString();
                ViewBag.MemberLevel = 1;
                ViewBag.NextLevel = 4;
                Session["PreviousView"] = null;
            }
            else
            {
                username = memberUsername;
                ViewBag.MemberLevel = memberLevel;
                ViewBag.NextLevel = memberLevel + 3;
            }

            //Store Previous username and level
            List<String> userNameList = new List<String>();
            if (Session["PreviousView"] == null)
            {
                userNameList.Add(username + ";" + ViewBag.MemberLevel);
                Session["PreviousView"] = userNameList;
            }
            else
            {
                userNameList = (List<string>)Session["PreviousView"];
                userNameList.Add(memberUsername + ";" + memberLevel);
                Session["PreviousView"] = userNameList;
            }

            var resultList = new List<MemberSponsorChartModel>();

            int ok;
            string msg;
            DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(username, out ok, out msg);
            ViewBag.MemberCount = 0;
            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                var mntm = new MemberSponsorChartModel();
                mntm.Level = dr["LEVEL"].ToString();
                //mntm.FirstLevelMemberCount = dr["FIRSTLEVEL_MEMBERCOUNT"].ToString();
                //mntm.FirstLevelMemberTotalInvest = dr["FIRSTLEVEL_TOTALINVEST"].ToString();

                //mntm.SecondLevelMember = dr["SECONDLEVEL"].ToString();
                //mntm.SecondLevelMemberCount = dr["SECONDLEVEL_MEMBERCOUNT"].ToString();
                //mntm.SecondLevelMemberTotalInvest = dr["SECONDLEVEL_TOTALINVEST"].ToString();

                //mntm.ThirdLevelMember = dr["THIRDLEVEL"].ToString();
                //mntm.ThirdLevelMemberCount = dr["THIRDLEVEL_MEMBERCOUNT"].ToString();
                //mntm.ThirdLevelMemberTotalInvest = dr["THIRDLEVEL_TOTALINVEST"].ToString();

                resultList.Add(mntm);
            }

            return PartialView("SponsorChartTree", resultList);
        }

        public ActionResult BackToPreviousTree()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            string username;
            List<String> userNameList = new List<String>();
            userNameList = (List<string>)Session["PreviousView"];

            if (userNameList.Count == 1)
            {
                string[] PreviousView = userNameList[userNameList.Count - 1].ToString().Split(';');
                username = PreviousView[0];
                ViewBag.MemberLevel = Convert.ToInt32(PreviousView[1]);
                ViewBag.NextLevel = Convert.ToInt32(ViewBag.MemberLevel) + 3;
            }
            else
            {
                string[] PreviousView = userNameList[userNameList.Count - 2].ToString().Split(';');

                username = PreviousView[0];
                ViewBag.MemberLevel = Convert.ToInt32(PreviousView[1]);
                ViewBag.NextLevel = Convert.ToInt32(ViewBag.MemberLevel) + 3;

                userNameList.RemoveAt(userNameList.Count - 1);
                Session["PreviousView"] = userNameList;
            }

            var resultList = new List<MemberSponsorChartModel>();

            int ok;
            string msg;
            DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(username, out ok, out msg);
            ViewBag.MemberCount = 0;
            foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
            {
                var mntm = new MemberSponsorChartModel();
                mntm.Level = dr["LEVEL"].ToString();
                //mntm.FirstLevelMemberCount = dr["FIRSTLEVEL_MEMBERCOUNT"].ToString();
                //mntm.FirstLevelMemberTotalInvest = dr["FIRSTLEVEL_TOTALINVEST"].ToString();

                //mntm.SecondLevelMember = dr["SECONDLEVEL"].ToString();
                //mntm.SecondLevelMemberCount = dr["SECONDLEVEL_MEMBERCOUNT"].ToString();
                //mntm.SecondLevelMemberTotalInvest = dr["SECONDLEVEL_TOTALINVEST"].ToString();

                //mntm.ThirdLevelMember = dr["THIRDLEVEL"].ToString();
                //mntm.ThirdLevelMemberCount = dr["THIRDLEVEL_MEMBERCOUNT"].ToString();
                //mntm.ThirdLevelMemberTotalInvest = dr["THIRDLEVEL_TOTALINVEST"].ToString();

                resultList.Add(mntm);
            }

            return PartialView("SponsorChartTree", resultList);
        }

        public ActionResult ModalViewMemberData(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var mm = new MemberModel();

            int ok;
            string msg;
            string languageCode = Session["LanguageChosen"].ToString();
            DataSet dsMember = MemberShopCenterDB.GetSponsorChartMemberByUsername(username, languageCode, out ok, out msg);
            if (dsMember.Tables[0].Rows.Count != 0)
            {
                mm.MemberCountry = dsMember.Tables[0].Rows[0]["CMULTILANGCOUNTRY_NAME"].ToString();
                mm.Username = dsMember.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                mm.FirstName = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                mm.JoinedDate = (DateTime)dsMember.Tables[0].Rows[0]["CMEM_CREATEDON"];
                mm.MemberPackage = dsMember.Tables[0].Rows[0]["CPKG_CODE"].ToString();
                mm.Intro = dsMember.Tables[0].Rows[0]["CMEM_INTRO"].ToString();
                mm.Rank = dsMember.Tables[0].Rows[0]["CWPOOLPRM_RANK"].ToString();
            }

            return PartialView("ModalViewMemberData", mm);
        }
        #endregion

        #region View Member Password

        public ActionResult ViewMemberPassword(int selectedPage = 1, string SearchFirstName = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var model = new PaginationMemberModel();
            int pages = 0;
            string message = string.Empty;
            int status = 0;

            if (selectedPage == 0)
            {
                selectedPage = 1;
            }

            var dsMember = AdminMemberDB.GetMemberPassword(SearchFirstName, selectedPage, out pages, out status, out message);
            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsMember.Tables[0].Rows)
            {
                var member = new MemberModel();
                member.Number = dr["RowNumber"].ToString();
                member.MemberId = dr["CUSR_USERNAME"].ToString();
                member.FirstName = dr["CUSR_FULLNAME"].ToString();
                member.Password = Authentication.Decrypt(dr["CUSR_PASSWORD"].ToString());
                member.Pin = Authentication.Decrypt(dr["CUSR_PIN"].ToString());
                model.MemberList.Add(member);
            }

            return PartialView("ViewMemberPassword", model);
        }

        #endregion

        #region Direct Sponsor

        public ActionResult CheckTotalSponsor(int selectedPage = 1, double SponsorAmount = 0, string StartDate = "01-04-2018", string EndDate = "01-04-2019", string SearchFirstName = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

           
            DateTime sdate = DateTime.ParseExact(StartDate, "dd-MM-yyyy", null);
            string SD = sdate.ToString("yyyy-MM-dd");
            DateTime edate = DateTime.ParseExact(EndDate, "dd-MM-yyyy", null);
            string ED = edate.ToString("yyyy-MM-dd");

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            PaginationMemberModel model = new PaginationMemberModel();
            model.StartDate = sdate;
            model.EndDate = edate;
            model.SponsorAmount = SponsorAmount;
            model.SearchFirstName = SearchFirstName;

            DataSet dsSponsorAmount = AdminMemberDB.GetMemberTotalSponsor(SponsorAmount, sdate, edate, SearchFirstName, selectedPage, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsSponsorAmount.Tables[0].Rows)
            {
                var member = new MemberModel();
                member.Number = dr["RowNumber"].ToString();
                member.MemberId = dr["CUSR_USERNAME"].ToString();
                member.FirstName = dr["CUSR_FULLNAME"].ToString();
                member.TotalDirectSponsor = string.Format("{0}", Convert.ToInt32(dr["CMEM_TOTAL_DIRECT_SPONSOR"]));
                model.MemberList.Add(member);
            }

            return PartialView("CheckTotalSponsor", model);
        }

        #endregion

        #region ChangePassword
        public ActionResult ChangePassword(string Username, bool noRedirectionRequired = false)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            ChangePasswordModel model = new ChangePasswordModel();

            int ok;
            string msg;
            DataSet dsUser = AdminMemberDB.GetMemberByUserName(Username, out ok, out msg);

            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.Admin.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.UserName = Username;
            model.UserFullName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.CurrentPassword = Authentication.Decrypt(dsUser.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString());
            model.NewPassword = "";
            model.ConfirmNewPassword = "";
            model.NoRedirectionRequired = noRedirectionRequired;

            //model.BonusWallet = float.Parse(dsUser.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
            model.DateJoined = (DateTime)dsUser.Tables[0].Rows[0]["CMEM_CREATEDON"];


            return PartialView("ChangePassword", model);
        }

        [HttpPost]
        public ActionResult ChangePasswordMethod(ChangePasswordModel cpm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (cpm.CurrentPassword == null)
                {
                    Response.Write(Resources.Admin.msgReqCurrPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (cpm.NewPassword == null)
                {
                    Response.Write(Resources.Admin.msgReqNewPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (cpm.ConfirmNewPassword == null)
                {
                    Response.Write(Resources.Admin.msgReqConfirmNewPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (cpm.NewPassword != cpm.ConfirmNewPassword)
                {
                    Response.Write(Resources.Admin.msgPwdNotMatch);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok;
                string msg;
                AdminMemberDB.UpdateChangePassword("SP_ChangePassword", cpm.UserName, Authentication.Encrypt(cpm.CurrentPassword), Authentication.Encrypt(cpm.NewPassword), Session["Admin"].ToString(), out ok, out msg);
                if (ok == -1)
                {
                    Response.Write(Resources.Admin.msgInvalidCurrPwd);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (ok == -2)
                {
                    Response.Write(Resources.Admin.msgPasswordAndPinMustDifferent);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (!cpm.NoRedirectionRequired)
                {
                    return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangePassword });
                }
                else
                {
                    //return ViewMemberDetails();
                    return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangePassword });
                }
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ChangePin
        public ActionResult ChangePin(string Username, bool noRedirectionRequired = false)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            ChangePasswordModel model = new ChangePasswordModel();

            int ok;
            string msg;
            DataSet dsUser = AdminMemberDB.GetMemberByUserName(Username, out ok, out msg);

            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.Admin.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.UserName = Username;
            model.UserFullName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.CurrentPassword = Authentication.Decrypt(dsUser.Tables[0].Rows[0]["CUSR_PIN"].ToString());
            model.NewPassword = "";
            model.ConfirmNewPassword = "";
            model.NoRedirectionRequired = noRedirectionRequired;

            //model.BonusWallet = float.Parse(dsUser.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
            model.DateJoined = (DateTime)dsUser.Tables[0].Rows[0]["CMEM_CREATEDON"];


            return PartialView("ChangePin", model);
        }

        [HttpPost]
        public ActionResult ChangePinMethod(ChangePasswordModel cpm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (cpm.CurrentPassword == null)
                {
                    Response.Write(Resources.Admin.warningConfirmNewPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (cpm.NewPassword == null)
                {
                    Response.Write(Resources.Admin.warningConfirmNewPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (cpm.ConfirmNewPassword == null)
                {
                    Response.Write(Resources.Admin.warningConfirmNewPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (cpm.NewPassword != cpm.ConfirmNewPassword)
                {
                    Response.Write(Resources.Admin.warningPINNotMatch);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok;
                string msg;
                AdminMemberDB.UpdateChangePassword("SP_ChangePin", cpm.UserName, Authentication.Encrypt(cpm.CurrentPassword), Authentication.Encrypt(cpm.NewPassword), Session["Admin"].ToString(), out ok, out msg);
                if (ok == -1)
                {
                    Response.Write(Resources.Admin.msgInvalidCurrPIN);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (!cpm.NoRedirectionRequired)
                {
                    return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangePin });
                }
                else
                {
                    //return ViewMemberDetails();
                    return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangePin });
                }
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ChangeMemberPermission
        public ActionResult ChangeMemberPermission(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            MemberPermissionModel model = new MemberPermissionModel();

            int ok;
            string msg;
            DataSet dsUser = AdminMemberDB.GetMemberByUserName(Username, out ok, out msg);

            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.Admin.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.UserName = Username;
            model.UserFullName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            model.IsBlocked = !bool.Parse(dsUser.Tables[0].Rows[0]["CUSR_ACTIVE"].ToString());

            //model.BonusWallet = float.Parse(dsUser.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
            model.DateJoined = (DateTime)dsUser.Tables[0].Rows[0]["CMEM_CREATEDON"];
            //if (!string.IsNullOrEmpty(dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString()))
            //{
            //    var index = dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString().LastIndexOf('\\');
            //    var filename = dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString().Substring(index + 1);
            //    model.MemberPhoto = "http://" + Request.Url.Authority + "/MemberProfile/" + Username + "/" + filename;
            //}
            //else
            //{
            //    model.MemberPhoto = "http://" + Request.Url.Authority + "/Images/NoPhoto.png";
            //}

            return PartialView("ChangeMemberPermission", model);
        }

        [HttpPost]
        public ActionResult ChangeMemberPermissionMethod(MemberPermissionModel cpm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok;
                string msg;
                AdminMemberDB.ChangeMemberPermission(cpm.UserName, !cpm.IsBlocked, Session["Admin"].ToString(), out ok, out msg);
                if (ok == -1)
                {
                    Response.Write(Resources.Admin.msgFailedOperation);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangeMemberPermission });
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ChangeOwnership
        public ActionResult ChangeOwnership(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            ChangeOwnershipModel model = new ChangeOwnershipModel();

            int ok;
            string msg;
            DataSet dsUser = AdminMemberDB.GetMemberByUserName(Username, out ok, out msg);

            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.Admin.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.Username = Username;
            model.FirstName = dsUser.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();

            //model.BonusWallet = float.Parse(dsUser.Tables[0].Rows[0]["CMEM_REGISTERWALLET"].ToString());
            model.DateJoined = (DateTime)dsUser.Tables[0].Rows[0]["CMEM_CREATEDON"];
            //if (!string.IsNullOrEmpty(dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString()))
            //{
            //    var index = dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString().LastIndexOf('\\');
            //    var filename = dsUser.Tables[0].Rows[0]["CMEM_IMAGEPATH"].ToString().Substring(index + 1);
            //    model.MemberPhoto = "http://" + Request.Url.Authority + "/MemberProfile/" + Username + "/" + filename;
            //}
            //else
            //{
            //    model.MemberPhoto = "http://" + Request.Url.Authority + "/Images/NoPhoto.png";
            //}

            return PartialView("ChangeOwnership", model);
        }

        [HttpPost]
        public ActionResult ChangeOwnershipMethod(ChangeOwnershipModel cpm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (cpm.FirstName == "" || cpm.FirstName == null)
                {
                    Response.Write("Please Key In Full Name");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok;
                string msg;
                AdminMemberDB.UpdateChangeOwnership(cpm.Username, cpm.FirstName, Session["Admin"].ToString(), out ok, out msg);

                if (ok == -1)
                {
                    Response.Write(Resources.Admin.msgUsernameNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangeOwnership });
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ChangeSponsor
        public ActionResult ChangeSponsor(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            ChangeSponsorModel model = new ChangeSponsorModel();
            int ok = 0;
            string msg = "";

            DataSet dsUser = MemberDB.GetMemberByUsername(Username, out ok, out msg);
            if (dsUser.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.Admin.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.Username = Username;
            model.CurrIntro = dsUser.Tables[0].Rows[0]["CMEM_INTRO"].ToString();
            model.NewIntro = "";

            return PartialView("ChangeSponsor", model);
        }

        [HttpPost]
        public ActionResult ChangeSponsorMethod(ChangeSponsorModel cpm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (string.IsNullOrEmpty(cpm.NewIntro))
                {
                    Response.Write("Please Key In New Sponsor");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok;
                string msg;
                AdminMemberDB.UpdateChangeSponsor(cpm.Username, cpm.CurrIntro, cpm.NewIntro, Session["Admin"].ToString(), out ok, out msg);

                if (ok == -1)
                {
                    Response.Write(string.Format(Resources.Admin.msgUserNotExist, cpm.NewIntro));
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangeSponsor });
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ChangeRanking
        public ActionResult ChangeRanking(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var model = new ChangeRankingModel();

            int ok;
            string msg;
            DataSet dsMember = AdminMemberDB.GetMemberByUserName(Username, out ok, out msg);

            if (dsMember.Tables[0].Rows.Count == 0)
            {
                Response.Write(string.Format(Resources.Admin.msgUserNotExist, Username));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            model.UserName = Username;
            model.SelectedRanking = dsMember.Tables[0].Rows[0]["CRANK_CODE"] == null ? string.Empty : dsMember.Tables[0].Rows[0]["CRANK_CODE"].ToString();

            model.RankList = Misc.GetallRankList();

            model.DateJoined = (DateTime)dsMember.Tables[0].Rows[0]["CMEM_CREATEDON"];

            return PartialView("ChangeRanking", model);
        }

        [HttpPost]
        public ActionResult ChangeRankingMethod(ChangeRankingModel crm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok;
                string msg;
                AdminMemberDB.UpdateMemberRanking(crm.UserName, crm.SelectedRanking, Session["Admin"].ToString(), out ok, out msg);
                if (ok == -1)
                {
                    Response.Write(Resources.Admin.msgInvalidRanking);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                return RedirectToAction("AdminSearchUser", "Admin", new { Type = ProjectStaticString.ChangeRanking });
            }

            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Shared Function
        #region ConstructPageList

        private int ConstructPageList(int selectedPage, int pages, MemberFunctionSettingModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            if (pages == 0)
            {
                pageList.Add(0);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };


            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationMemberModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            if (pages == 0)
            {
                pageList.Add(0);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };


            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationMemberAutoMaintainModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationMemberWalletModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        #endregion

        #endregion

        #region SearchSponsor
        public ActionResult SearchSponsor(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }


            var model = new MemberSponsorChartModel();

            if (Username == null)
            {
                int ok;
                string msg;

                DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart("Hegemons", out ok, out msg);

                var member = MemberDB.GetMemberByUsername("Hegemons", out ok, out msg);

                model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
                model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
                //model.FirstLevelJoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
                model.FirstLevelJoinedDate = DateTime.Parse(dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString()).ToString("dd-MM-yyyy");
                model.FirstLevelRanking = Misc.GetMemberRanking(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());
                model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();
                model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();
                model.FirstLevelRankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
                {
                    MemberList resultList = new MemberList();
                    var info = GetMemberSponsorInfo(dr);
                    resultList.Level = dr["LEVEL"].ToString();
                    resultList.Member = dr["USERNAME"].ToString();
                    resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
                    //resultList.JoinedDate = dr["DATE"].ToString();
                    resultList.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToString("dd-MM-yyyy");
                    resultList.FullName = dr["FULLNAME"].ToString();
                    resultList.Intro = dr["INTRO"].ToString();
                    resultList.RankIcon = dr["ICON"].ToString();

                    model.resultList.Add(resultList);

                }
            }
            else
            {
                int ok;
                string msg;

                DataSet dsNetworkTree = MemberShopCenterDB.GetSponsorChart(Username, out ok, out msg);

                if (dsNetworkTree.Tables[0].Rows.Count == 0)
                {
                    Response.Write("Invalid Username");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var member = MemberDB.GetMemberByUsername(Username, out ok, out msg);

                model.FirstLevelMember = dsNetworkTree.Tables[0].Rows[0]["USERNAME"].ToString();
                model.FirstLevelLevel = dsNetworkTree.Tables[0].Rows[0]["LEVEL"].ToString();
                //model.FirstLevelJoinedDate = dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString();
                model.FirstLevelJoinedDate = DateTime.Parse(dsNetworkTree.Tables[0].Rows[0]["DATE"].ToString()).ToString("dd-MM-yyyy");
                model.FirstLevelRanking = Misc.GetMemberRanking(dsNetworkTree.Tables[0].Rows[0]["RANK"].ToString());
                model.FirstLevelIntro = dsNetworkTree.Tables[0].Rows[0]["INTRO"].ToString();
                model.FirstLevelFullname = dsNetworkTree.Tables[0].Rows[0]["FULLNAME"].ToString();
                model.FirstLevelRankIcon = member.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();

                foreach (DataRow dr in dsNetworkTree.Tables[0].Rows)
                {
                    MemberList resultList = new MemberList();
                    var info = GetMemberSponsorInfo(dr);
                    resultList.Level = dr["LEVEL"].ToString();
                    resultList.Member = dr["USERNAME"].ToString();
                    resultList.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
                    //resultList.JoinedDate = dr["DATE"].ToString();
                    resultList.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToString("dd-MM-yyyy");
                    resultList.FullName = dr["FULLNAME"].ToString();
                    resultList.Intro = dr["INTRO"].ToString();
                    resultList.RankIcon = dr["ICON"].ToString();

                    model.resultList.Add(resultList);

                }
            }
            ViewBag.MemberCount = 0;

            return PartialView("SponsorList", model);
        }
        #endregion

        #region Sponsor List
        public ActionResult SponsorListing(int selectedPage = 1)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }


                int pages = 0;
                PaginationIntroListModel model = new PaginationIntroListModel();
                DataSet dsSponsorList = MemberShopCenterDB.GetAllSponsorListByUsername("BVA", selectedPage, out pages);

                selectedPage = SponsorConstructPageList(selectedPage, pages, model);

                foreach (DataRow dr in dsSponsorList.Tables[0].Rows)
                {
                    IntroListModel sponsorlist = new IntroListModel();
                    sponsorlist.No = dr["rownumber"].ToString();
                    sponsorlist.Username = dr["CUSR_MEMBERID"].ToString();
                    sponsorlist.Name = dr["CUSR_FULLNAME"].ToString();
                    sponsorlist.Rank = Misc.RankNumber(dr["CRANK_CODE"].ToString());
                    sponsorlist.Sponsor = dr["CUSR_INTRO"].ToString();
                    sponsorlist.Level = dr["CMTREE_LEVEL"].ToString();
                    sponsorlist.CreatedDate = Convert.ToDateTime(dr["CMEM_DATEJOINED"]).ToString("dd/MM/yyyy hh:mm:ss tt");

                    model.SponsorList.Add(sponsorlist);
                }

                return PartialView("SponsorListing", model);
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        private int SponsorConstructPageList(int selectedPage, int pages, PaginationIntroListModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private MemberSponsorChartModel GetMemberSponsorInfo(DataRow dr)
        {
            var mntm = new MemberSponsorChartModel();

            mntm.Level = dr["LEVEL"].ToString();
            mntm.Member = dr["USERNAME"].ToString();
            mntm.Ranking = Misc.GetMemberRanking(dr["RANK"].ToString());
            mntm.JoinedDate = DateTime.Parse(dr["DATE"].ToString()).ToShortDateString();
            mntm.FullName = dr["FULLNAME"].ToString();
            mntm.Intro = dr["INTRO"].ToString();


            return mntm;
        }

        #region SearchUpLine
        public ActionResult SearchUpline(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var resultList = new List<MemberSponsorChartModel>();

            if (string.IsNullOrEmpty(Username))
            {

            }
            else
            {
                int ok;
                int count;
                string msg;

                var dsMember = MemberDB.GetMemberByUsername(Username, out ok, out msg);

                if (dsMember.Tables[0].Rows.Count == 0)
                {
                    Response.Write("Invalid Username");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                DataSet dsNetworkTree = MemberShopCenterDB.SearchUpline(Username, out ok, out count, out msg);               

                for (int i = 0; count > i; i++)
                {
                    var mntm = new MemberSponsorChartModel();
                    mntm.FirstLevelMember = dsNetworkTree.Tables[i].Rows[0]["CUSR_USERNAME"].ToString();
                    mntm.FirstLevelMemberCount = dsNetworkTree.Tables[i].Rows[0]["CUSR_FULLNAME"].ToString();
                    resultList.Add(mntm);
                }
            }
            ViewBag.MemberCount = 0;

            return PartialView("UplineList", resultList);
        }
        #endregion

        #region Member Authentication

        public ActionResult RealNameAuthentication(int selectedPage = 1, string searchMember ="")
        {

            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }


            int ok = 0; int pages = 0;
            string msg = "";

            PaginationMemberModel member = new PaginationMemberModel();

            var drMember = MemberProfileDB.GetMemberVerification(searchMember, selectedPage, out pages, out ok, out msg);
            if (drMember.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dr in drMember.Tables[0].Rows)
                {

                    EditMemberModel mem = new EditMemberModel();
                    mem.Number = dr["rownumber"].ToString();
                    mem.VerificationID = Convert.ToInt32(dr["CMEMVERVER_ID"].ToString());
                    mem.CreatedDate = Convert.ToDateTime(dr["CMEMVER_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    mem.Username = dr["CUSR_USERNAME"].ToString();
                    mem.FullName = dr["CUSR_FULLNAME"].ToString();
                    mem.IdentityNumber = dr["IdentityNumber"].ToString();
                    mem.ProtraitPhotoPath = dr["CMEMVER_PROTRAIT_PHOTO"].ToString();
                    mem.ReversePhotoPath = dr["CMEMVER_REVERSE_PHOTO"].ToString();
                    mem.HandheldPhotoPath = dr["CMEMVER_HANDHELD_PHOTO"].ToString();
                    mem.Status = dr["CMEMVER_STATUS"].ToString();
                    Session["Status"] = mem.Status;
                    member.EditMember.Add(mem);
                }
            }
            selectedPage = ConstructPageList(selectedPage, pages, member);

            return PartialView("RealNameAuthentication", member);

        }

        [HttpPost]
        public ActionResult ApproveMemberAuthenticationMethod(string memberusername ="", int verificationID = 0)
        {

            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";

            if (memberusername == "")
            {
                Response.Write("Error");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            string adminusername = Session["Admin"].ToString();

            MemberProfileDB.ApproveMemberVerification(memberusername, verificationID, adminusername, out ok, out msg);

            return RealNameAuthentication();

        }

        [HttpPost]
        public ActionResult RejectMemberAuthenticationMethod(string memberusername = "", int verificationID = 0)
        {

            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";

            if (memberusername == "")
            {
                Response.Write("Error");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            string adminusername = Session["Admin"].ToString();

            MemberProfileDB.RejectMemberVerification(memberusername, verificationID, adminusername, out ok, out msg);

            return RealNameAuthentication();

        }


        public ActionResult VerifiedRealNameAuthentication(int selectedPage = 1, string searchMember = "" , string Status = "0" , int FromDay = 0, int FromMonth = 0, int FromYear = 0, int ToDay = 0, int ToMonth = 0, int ToYear = 0)
        {

            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }


            int ok = 0; int pages = 0;
            string msg = "";
            DateTime fromDate = new DateTime();
            DateTime toDate = new DateTime();
            if (FromDay != 0)
            {
                if (FromMonth != 0)
                {
                    if (FromYear != 0)
                    {
                        fromDate = new DateTime(FromYear, FromMonth, FromDay);                        
                    }
                }
            }

            if (ToDay != 0)
            {
                if (ToMonth != 0)
                {
                    if (ToYear != 0)
                    {
                        toDate = new DateTime(ToYear, ToMonth, ToDay, 23, 59, 59);
                    }
                }
            }

            PaginationMemberModel member = new PaginationMemberModel();
            DataSet drMember = new DataSet();
            if (FromDay != 0)
            {
                if (FromMonth != 0)
                {
                    if (FromYear != 0)
                    {
                        if (ToDay != 0)
                        {
                            if (ToMonth != 0)
                            {
                                if (ToYear != 0)
                                {
                                    drMember = MemberProfileDB.GetVerifiedMemberAuthenticationByDate(fromDate, toDate, selectedPage, out pages, out ok, out msg);
                                }
                                else
                                {
                                    drMember = MemberProfileDB.GetVerifiedMemberAuthentication(searchMember, Status, selectedPage, out pages, out ok, out msg);
                                }
                            }
                            else
                            {
                                drMember = MemberProfileDB.GetVerifiedMemberAuthentication(searchMember, Status, selectedPage, out pages, out ok, out msg);
                            }
                        }
                        else
                        {
                            drMember = MemberProfileDB.GetVerifiedMemberAuthentication(searchMember, Status, selectedPage, out pages, out ok, out msg);
                        }
                    }
                    else
                    {
                        drMember = MemberProfileDB.GetVerifiedMemberAuthentication(searchMember, Status, selectedPage, out pages, out ok, out msg);
                    }
                }
                else
                {
                    drMember = MemberProfileDB.GetVerifiedMemberAuthentication(searchMember, Status, selectedPage, out pages, out ok, out msg);
                }
            }
            else
            {
                drMember = MemberProfileDB.GetVerifiedMemberAuthentication(searchMember, Status, selectedPage, out pages, out ok, out msg);
            }


            


            if (drMember.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dr in drMember.Tables[0].Rows)
                {

                    EditMemberModel mem = new EditMemberModel();
                    mem.Number = dr["rownumber"].ToString();
                    mem.UpdatedDate = Convert.ToDateTime(dr["CMEMVER_UPDATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                    mem.Username = dr["CUSR_USERNAME"].ToString();
                    mem.FullName = dr["CUSR_FULLNAME"].ToString();
                    mem.IdentityNumber = dr["IdentityNumber"].ToString();
                    mem.ProtraitPhotoPath = dr["CMEMVER_PROTRAIT_PHOTO"].ToString();
                    mem.ReversePhotoPath = dr["CMEMVER_REVERSE_PHOTO"].ToString();
                    mem.HandheldPhotoPath = dr["CMEMVER_HANDHELD_PHOTO"].ToString();
                    mem.Status = dr["CMEMVER_STATUS"].ToString();
                    Session["Status"] = mem.Status;
                    member.EditMember.Add(mem);
                }
            }
            selectedPage = ConstructPageList(selectedPage, pages, member);

            member.FromDay = Misc.ConstructsDay().ToList();
            member.FromMonth = Misc.ConstructsMonth().ToList();
            member.FromYear = Misc.ConstructYears(DateTime.Now.Year).ToList();
            member.ToDay = Misc.ConstructsDay().ToList();
            member.ToMonth = Misc.ConstructsMonth().ToList();
            member.ToYear = Misc.ConstructYears(DateTime.Now.Year).ToList();

            member.SelectedFromDay = DateTime.Now.Day.ToString();
            member.SelectedToDay = DateTime.Now.Day.ToString();
            member.SelectedFromMonth = DateTime.Now.Month.ToString();
            member.SelectedToMonth = DateTime.Now.Month.ToString();
            member.SelectedFromYear = DateTime.Now.Year.ToString();
            member.SelectedToYear = DateTime.Now.Year.ToString();

            #region Position
            member.StatusList.Clear();

            SelectListItem loc = new SelectListItem();
            loc.Text = ECFBase.Resources.Member.lblAll;
            loc.Value = "All";
            member.StatusList.Add(loc);

            loc = new SelectListItem();
            loc.Text = ECFBase.Resources.Member.lblStatusApproved;
            loc.Value = "Approve";
            member.StatusList.Add(loc);

            loc = new SelectListItem();
            loc.Text = ECFBase.Resources.Member.lblStatusRejected;
            loc.Value = "Reject";
            member.StatusList.Add(loc);

            #endregion

            member.SelectedStatusList = Status;

            return PartialView("VerifiedRealNameAuthentication", member);

        }

        public void ExportVerifiedRealNameAuthentication(string searchMember = "", string Status = "0")
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=VerifiedAuthenticationList.csv");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintVerifiedRealNameAuthentication(searchMember, Status));
            sw.Close();
            Response.End();
        }

        private string PrintVerifiedRealNameAuthentication(string searchMember = "", string Status = "0")
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("No, Updated Date, Username , Full Name, Identity Number, Identity Front, Identity Back, Selfie With Identity, Status");

            var drMember = MemberProfileDB.GetVerifiedMemberAuthenticationExport(searchMember, Status);

            foreach (DataRow dr in drMember.Tables[0].Rows)
            {

                sb.AppendLine(dr["rownumber"].ToString() + "\t," +
                              Convert.ToDateTime(dr["CMEMVER_UPDATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt") + "\t," +
                              dr["CUSR_USERNAME"].ToString()+ "\t," +
                              dr["CUSR_FULLNAME"].ToString() + "\t," +
                              dr["IdentityNumber"].ToString() + "\t," +
                              dr["CMEMVER_PROTRAIT_PHOTO"].ToString() + "\t," +
                              dr["CMEMVER_REVERSE_PHOTO"].ToString() + "\t," +
                              dr["CMEMVER_HANDHELD_PHOTO"].ToString() + "\t," +
                              dr["CMEMVER_STATUS"].ToString());
            }

            return sb.ToString();
        }

        #endregion


        #region Frozen Account
        public ActionResult AddFrozenAccountList(int selectedPage = 1, string searchMemberList = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            string languageCode = Session["LanguageChosen"].ToString();

            PaginationMobileAgent model = new PaginationMobileAgent();
            DataSet dsAgents = MemberDB.GetAllFrozenAccount(searchMemberList, selectedPage, languageCode, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsAgents.Tables[0].Rows)
            {
                var mobileAgent = new MobileAgent();
                mobileAgent.Number = dr["rownumber"].ToString();
                mobileAgent.AgentID = dr["CUSR_USERNAME"].ToString();
                mobileAgent.FullName = dr["CUSR_FULLNAME"].ToString();
                mobileAgent.JoinedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToShortDateString();
                mobileAgent.Country = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                mobileAgent.AdminApproved = dr["CMEM_UPDATEDBY"].ToString();
                mobileAgent.DateApproved = Convert.ToDateTime(dr["CMEM_UPDATEDON"]).ToShortDateString();
                model.MobileList.Add(mobileAgent);
            }

            return PartialView("AddFrozenAccountList", model);
        }

        public ActionResult DisqualifyFrozenAccount(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            MemberDB.DisqualifiedFrozenAccount(username, Session["Admin"].ToString());

            return AddFrozenAccountList(1, "");
        }

        public ActionResult AppointFrozenAccount(string username, string percentage)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";

            var ds = MemberDB.GetMemberByUsername(username, out ok, out msg);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Response.Write("Invalid Username");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            MemberDB.AppointFrozenAccount(username, Session["Admin"].ToString(), "0");
            return AddFrozenAccountList();
        }

        [HttpPost]
        public JsonResult findUserName(string memberID)
        {
            int ok = 0;
            string msg = string.Empty;

            if (string.IsNullOrEmpty(memberID))
            {
                Response.Write(Resources.Admin.warningInvalidUsername2);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            var dsMember = MemberDB.GetMemberByUsername(memberID, out ok, out msg);

            if (dsMember.Tables[0].Rows.Count == 0)
            {
                Response.Write(Resources.Admin.warningInvalidUsername2);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            var result = dsMember.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        private int ConstructPageList(int selectedPage, int pages, PaginationMobileAgent model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        #endregion

        #region Block Account
        public ActionResult AddBlockAccountList(int selectedPage = 1, string searchMemberList = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int status = 0;
            string message = string.Empty;
            int pages = 0;
            string languageCode = Session["LanguageChosen"].ToString();

            PaginationMobileAgent model = new PaginationMobileAgent();
            DataSet dsAgents = MemberDB.GetAllBlockAccount(searchMemberList, selectedPage, languageCode, out pages, out status, out message);

            selectedPage = ConstructPageList(selectedPage, pages, model);

            foreach (DataRow dr in dsAgents.Tables[0].Rows)
            {
                var mobileAgent = new MobileAgent();
                mobileAgent.Number = dr["rownumber"].ToString();
                mobileAgent.AgentID = dr["CUSR_USERNAME"].ToString();
                mobileAgent.FullName = dr["CUSR_FULLNAME"].ToString();
                mobileAgent.JoinedDate = Convert.ToDateTime(dr["CMEM_CREATEDON"]).ToShortDateString();
                mobileAgent.Country = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                mobileAgent.AdminApproved = dr["CMEM_UPDATEDBY"].ToString();
                mobileAgent.DateApproved = Convert.ToDateTime(dr["CMEM_UPDATEDON"]).ToShortDateString();
                model.MobileList.Add(mobileAgent);
            }

            return PartialView("AddBlockAccountList", model);
        }

        public ActionResult DisqualifyBlockAccount(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            MemberDB.DisqualifiedBlockAccount(username, Session["Admin"].ToString());

            return AddBlockAccountList(1, "");
        }

        public ActionResult AppointBlockAccount(string username, string percentage)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";

            var ds = MemberDB.GetMemberByUsername(username, out ok, out msg);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Response.Write("Invalid Username");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            MemberDB.AppointBlockAccount(username, Session["Admin"].ToString(), "0");
            return AddBlockAccountList();
        }

        #endregion

    }
}