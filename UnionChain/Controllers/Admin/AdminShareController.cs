﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Models;
using ECFBase.Components;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Text;

namespace ECFBase.Controllers.Admin
{
    public class AdminShareController : Controller
    {

        public ActionResult TradingPriceAdjustment()
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int Page = 0;
                int ok = 0;
                string msg = string.Empty;

                ShareLogModel model = new ShareLogModel();

                DataSet dsTradingPrice = MemberShareDB.GetTradingPriceValue();
                if(dsTradingPrice.Tables[0].Rows.Count != 0 )
                {
                    model.TradingAdjustmentID = dsTradingPrice.Tables[0].Rows[0]["CPI_ID"].ToString();
                    model.Value = dsTradingPrice.Tables[0].Rows[0]["CPI_VALUE"].ToString();
                }


                return PartialView("TradingPriceAdjustment", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TradingSavePrice(string Price = "0", int ID = 0)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok = 0;
                string msg = string.Empty;

                if(Price == "0")
                {
                    Response.Write(Resources.Member.warningPlsInputTradingPrice);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
               
                MemberShareDB.SaveTradingPriceValue(Price, ID, Session["Admin"].ToString(), out ok, out msg);

                return TradingPriceAdjustment();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TradingBuyingQueue(int SelectedPage = 1, string Username = "" , string Status = "" , string Rate = "" , string StartTime = "" , string EndTime = "" )
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int Page = 0;
                int ok = 0;
                string msg = string.Empty;

                ShareLogModel model = new ShareLogModel();

                DataSet dsTradingBuyingQueue = MemberShareDB.GetAllTradingBuyingQueue(Username , Status , Rate , StartTime , EndTime ,Convert.ToInt32(SelectedPage), out Page , out ok , out msg );

                SelectedPage = ConstructPageList(SelectedPage, Page, model);

                foreach (DataRow dr in dsTradingBuyingQueue.Tables[0].Rows)
                {
                    ShareListLogModel TradingBuy = new ShareListLogModel();
                        TradingBuy.Username = dr["CUSR_USERNAME"].ToString();
                        TradingBuy.OriValue = dr["CTBQ_ORI_VALUE"].ToString();
                        TradingBuy.PendingValue = dr["CTBQ_VALUE"].ToString();
                        TradingBuy.Status = Misc.TradingStatus(dr["CTBQ_STATUS"].ToString());
                        TradingBuy.Rate = dr["CTBQ_RATE"].ToString();
                        TradingBuy.Amount = dr["CTBQ_AMOUNT"].ToString();
                        TradingBuy.LocalAmount = dr["CTBQ_LOCAL_AMOUNT"].ToString();
                        TradingBuy.Date = Convert.ToDateTime(dr["CTBQ_CREATEDON"]).ToString("dd-MM-yyyy hh:mm:ss tt");
                    
                    model.TradingBuyQueue.Add(TradingBuy);
                }

                List<Status> StatusList = Misc.DBGetAllBuyQueueStatusList("BUY");

                model.StatusList = from c in StatusList
                                         select new SelectListItem
                                         {
                                             Text = c.StatusDisplay,
                                             Value = c.StatusX
                                         };


                model.MemberID = Username;
                model.TheRate = Rate;
                model.SelectedStatus = Status;
                return PartialView("TradingBuyingQueue", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TradingSellingQueue(int SelectedPage = 1, string Username = "", string Status = "", string Rate = "", string StartTime = "", string EndTime = "")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int Page = 0;
                int ok = 0;
                string msg = string.Empty;

                ShareLogModel model = new ShareLogModel();

                DataSet dsTradingSellingQueue = MemberShareDB.GetAllTradingSellingQueue(Username, Status, Rate, StartTime, EndTime, Convert.ToInt32(SelectedPage), out Page, out ok, out msg);

                SelectedPage = ConstructPageList(SelectedPage, Page, model);

                foreach (DataRow dr in dsTradingSellingQueue.Tables[0].Rows)
                {
                    ShareListLogModel TradingSell = new ShareListLogModel();

                    TradingSell.Username = dr["CUSR_USERNAME"].ToString();
                    TradingSell.OriValue = dr["CTSQ_ORI_VALUE"].ToString();
                    TradingSell.PendingValue = dr["CTSQ_VALUE"].ToString();
                    TradingSell.Status = Misc.TradingStatus(dr["CTSQ_STATUS"].ToString());
                    TradingSell.Rate = dr["CTSQ_RATE"].ToString();
                    TradingSell.Amount = dr["CTSQ_AMOUNT"].ToString();
                    TradingSell.LocalAmount = dr["CTSQ_LOCAL_AMOUNT"].ToString();
                    TradingSell.Date = Convert.ToDateTime(dr["CTSQ_CREATEDON"]).ToString("dd-MM-yyyy hh:mm:ss tt");

                    model.TradingSellQueue.Add(TradingSell);
                }

                List<Status> StatusList = Misc.DBGetAllBuyQueueStatusList("SELL");

                model.StatusList = from c in StatusList
                                   select new SelectListItem
                                   {
                                       Text = c.StatusDisplay,
                                       Value = c.StatusX
                                   };

             
                model.MemberID = Username;
                model.TheRate = Rate;
                model.SelectedStatus = Status;

                return PartialView("TradingSellingQueue", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TradingMatchQueue(int SelectedPage = 1, string Buyer = "" , string Seller = "", string Status = "", string Rate = "", string StartTime = "", string EndTime = "")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int Page = 0;
                int ok = 0;
                string msg = string.Empty;

                ShareLogModel model = new ShareLogModel();

                DataSet dsTradingSellingQueue = MemberShareDB.GetAllTradingMatchQueue(Buyer, Seller, Status, Rate, StartTime, EndTime, Convert.ToInt32(SelectedPage), out Page, out ok, out msg);

                SelectedPage = ConstructPageList(SelectedPage, Page, model);

                foreach (DataRow dr in dsTradingSellingQueue.Tables[0].Rows)
                {
                    ShareListLogModel TradingSell = new ShareListLogModel();

                    TradingSell.Buyer = dr["CTMP_BUYER"].ToString();
                    TradingSell.Seller = dr["CTMP_SELLER"].ToString();
                    TradingSell.OriValue = dr["CTMP_SELLER_VALUE"].ToString();
                    TradingSell.Status = Misc.TradingStatus(dr["CTMP_STATUS"].ToString());
                    TradingSell.Amount = dr["CTMP_SELLER_AMOUNT"].ToString();
                    TradingSell.LocalAmount = dr["CTMP_SELLER_LOCAL_AMOUNT"].ToString();
                    TradingSell.Date = Convert.ToDateTime(dr["CTMP_CREATEDON"]).ToString("dd-MM-yyyy hh:mm:ss tt");

                    model.TradingSellQueue.Add(TradingSell);
                }

                List<Status> StatusList = Misc.DBGetAllBuyQueueStatusList("SELL");

                model.StatusList = from c in StatusList
                                   select new SelectListItem
                                   {
                                       Text = c.StatusDisplay,
                                       Value = c.StatusX
                                   };

                model.Buyer = Buyer;
                model.Seller = Seller;
                model.TheRate = Rate;
                model.SelectedStatus = Status;
   

                return PartialView("TradingMatchQueue", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TradingMatchCancel(int SelectedPage = 1, string Buyer = "", string Seller = "", string Status = "", string Rate = "", string StartTime = "", string EndTime = "")
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int Page = 0;
                int ok = 0;
                string msg = string.Empty;

                ShareLogModel model = new ShareLogModel();

                DataSet dsTradingSellingQueue = MemberShareDB.GetAllTradingMatchPendingQueue(Buyer, Seller, Status, Rate, StartTime, EndTime, Convert.ToInt32(SelectedPage), out Page, out ok, out msg);

                SelectedPage = ConstructPageList(SelectedPage, Page, model);

                foreach (DataRow dr in dsTradingSellingQueue.Tables[0].Rows)
                {
                    ShareListLogModel TradingSell = new ShareListLogModel();
                    
                        TradingSell.BuyerQueueID = dr["CTMP_BUYER_ID"].ToString();
                        TradingSell.SellerQueueID = dr["CTMP_SELLER_QUEUE_ID"].ToString();
                        TradingSell.MatchQueueID = dr["CTMP_ID"].ToString();
                        TradingSell.Buyer = dr["CTMP_BUYER"].ToString();
                        TradingSell.Seller = dr["CTMP_SELLER"].ToString();
                        TradingSell.OriValue = dr["CTMP_SELLER_VALUE"].ToString();
                        TradingSell.Status = Misc.TradingStatus(dr["CTMP_STATUS"].ToString());
                        TradingSell.Amount = dr["CTMP_SELLER_AMOUNT"].ToString();
                        TradingSell.LocalAmount = dr["CTMP_SELLER_LOCAL_AMOUNT"].ToString();
                        TradingSell.Date = Convert.ToDateTime(dr["CTMP_CREATEDON"]).ToString("dd-MM-yyyy hh:mm:ss tt");
                    
                    model.TradingSellQueue.Add(TradingSell);
                }

                List<Status> StatusList = Misc.DBGetAllBuyQueueStatusList("SELL");

                model.StatusList = from c in StatusList
                                   select new SelectListItem
                                   {
                                       Text = c.StatusDisplay,
                                       Value = c.StatusX
                                   };

                model.Buyer = Buyer;
                model.Seller = Seller;
                model.TheRate = Rate;
                model.SelectedStatus = Status;


                return PartialView("TradingMatchCancel", model);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CancelAll(string MatchTradingID)
        {

            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                ShareLogModel model = new ShareLogModel();
                MemberShareDB.CancelTradingMatch(MatchTradingID, "ALL", Session["Admin"].ToString());

                return TradingMatchCancel();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CancelTradingMatch(string MatchTradingID)
        {

            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                ShareLogModel model = new ShareLogModel();

                MemberShareDB.CancelTradingMatch(MatchTradingID, "ONLYMATCHPOINT", Session["Admin"].ToString());

                return TradingMatchCancel();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        private int ConstructPageList(int selectedPage, int pages, ShareLogModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        #region Export Function

        public void ExportBuyingQueue(int SelectedPage = 1, string Username = "", string Status = "", string Rate = "", string StartTime = "", string EndTime = "", string Export = "1")
        {

            string Title = string.Empty;
                        
            Title = "BuyingQueueList-Log.csv";
           

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + Title + "");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintBuyingQueue(Username, Status, Rate, StartTime, EndTime, Export, SelectedPage));
            sw.Close();
            Response.End();
        }
        private string PrintBuyingQueue(string username, string Status, string Rate, string startdate, string enddate, string Export, int SelectedPage)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Username,Volume,Pending Volume,Price,Status,Date "));

            int Page = 0;
            int ok = 0;
            string msg = string.Empty;

            DataSet dsTradingBuyingQueue = MemberShareDB.GetAllTradingBuyingQueue(username, Status, Rate, startdate, enddate, SelectedPage, out Page, out ok, out msg);
            foreach (DataRow dr in dsTradingBuyingQueue.Tables[0].Rows)
            {
                sb.AppendLine(
                                dr["CUSR_USERNAME"].ToString() + "," +
                                //dr["Member_Fullname"].ToString() + "," +
                                dr["CTBQ_ORI_VALUE"].ToString() + "," +
                                dr["CTBQ_VALUE"].ToString() + "," +
                                float.Parse(dr["CTBQ_RATE"].ToString()) + "," +
                                Misc.TradingStatus(dr["CTBQ_STATUS"].ToString()) + "," +
                Convert.ToDateTime(dr["CTBQ_CREATEDON"]).ToString("dd-MM-yyyy"));
            }
            return sb.ToString();
        }

        public void ExportSellingQueue(int SelectedPage = 1, string Username = "", string Status = "", string Rate = "", string StartTime = "", string EndTime = "", string Export = "1")
        {

            string Title = string.Empty;

            Title = "SellingQueueList-Log.csv";


            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + Title + "");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintSellingQueue(Username, Status, Rate, StartTime, EndTime, Export, SelectedPage));
            sw.Close();
            Response.End();
        }
        private string PrintSellingQueue(string username, string Status, string Rate, string startdate, string enddate, string Export, int SelectedPage)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Username,Volume,Pending Volume,Price,Status,Date "));

            int Page = 0;
            int ok = 0;
            string msg = string.Empty;
          
            DataSet dsTradingSellingQueue = MemberShareDB.GetAllTradingSellingQueue(username, Status, Rate, startdate, enddate, SelectedPage, out Page, out ok, out msg);
            foreach (DataRow dr in dsTradingSellingQueue.Tables[0].Rows)
            {
                sb.AppendLine(
                                dr["CUSR_USERNAME"].ToString() + "," +
                                //dr["Member_Fullname"].ToString() + "," +
                                dr["CTSQ_ORI_VALUE"].ToString() + "," +
                                dr["CTSQ_VALUE"].ToString() + "," +
                                float.Parse(dr["CTSQ_RATE"].ToString()) + "," +
                                Misc.TradingStatus(dr["CTSQ_STATUS"].ToString()) + "," +
                Convert.ToDateTime(dr["CTSQ_CREATEDON"]).ToString("dd-MM-yyyy"));
            }
            return sb.ToString();
        }

        public void ExportMatchQueue(int SelectedPage = 1, string Buyer = "", string Seller = "", string Status = "", string Rate = "", string StartTime = "", string EndTime = "", string Export = "1")
        {

            string Title = string.Empty;

            Title = "MatchQueueList-Log.csv";


            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + Title + "");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintMatchQueue(Buyer, Seller, Status, Rate, StartTime, EndTime, Export, SelectedPage));
            sw.Close();
            Response.End();
        }
        private string PrintMatchQueue(string Buyer, string Seller, string Status, string Rate, string startdate, string enddate, string Export, int SelectedPage)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Buyer,Seller,Volume,Price,Local Price,Status,Date "));

            int Page = 0;
            int ok = 0;
            string msg = string.Empty;
           
            DataSet dsTradingSellingQueue = MemberShareDB.GetAllTradingMatchQueue(Buyer, Seller, Status, Rate, startdate, enddate, Convert.ToInt32(SelectedPage), out Page, out ok, out msg);
            foreach (DataRow dr in dsTradingSellingQueue.Tables[0].Rows)
            {
                sb.AppendLine(
                                dr["CTMP_BUYER"].ToString() + "," +
                                //dr["Member_Fullname"].ToString() + "," +
                                dr["CTMP_SELLER"].ToString() + "," +
                                dr["CTMP_SELLER_VALUE"].ToString() + "," +
                                float.Parse(dr["CTMP_SELLER_AMOUNT"].ToString()) + "," +
                                float.Parse(dr["CTMP_SELLER_LOCAL_AMOUNT"].ToString()) + "," +
                                Misc.TradingStatus(dr["CTMP_STATUS"].ToString()) + "," +
                Convert.ToDateTime(dr["CTMP_CREATEDON"]).ToString("dd-MM-yyyy"));
            }
            return sb.ToString();
        }


        #endregion
    }
}
