﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Models;
using ECFBase.Components;
using ECFBase.Helpers;
using System.Data;

namespace ECFBase.Controllers.Admin
{
    public class AdminHelpDeskController : Controller
    {
        #region Inbox
        #region AllInbox
        public ActionResult AllInbox(int selectedPage = 1, string type = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            PaginationInboxModel model = new PaginationInboxModel();

            if (type == "")
                type = InboxType.General.ToString();

            DataSet dsCorpNews = AdminHelpDeskDB.GetAllInbox(type, Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsCorpNews.Tables[0].Rows)
            {
                var idm = new InboxModel();
                idm.Username = dr["CUSR_USERNAME"].ToString();
                idm.InboxID = Convert.ToInt32(dr["CMSG_ID"]);
                idm.Title = dr["CMULTILANGMSG_TITLE"].ToString();

                if (dr["CMULTILANGMSG_REPLY"].ToString() != "")
                {
                    //idm.Reply = dr["CMULTILANGMSG_REPLY"].ToString();
                    idm.Reply = ECFBase.Resources.Admin.lblReplied;
                }

                idm.PostDate = Convert.ToDateTime(dr["CMSG_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                idm.State = int.Parse(dr["CMSG_STATE"].ToString());

                if (idm.State == 1)
                {
                    idm.ReplyDate = Convert.ToDateTime(dr["CMSG_UPDATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                }

                model.InboxList.Add(idm);
            }

            return PartialView("AllInbox", model);
        }
        #endregion

        #region InboxData
        public ActionResult InboxData(int InboxID = 0)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var inboxModel = new InboxModel();
            int ok = 0;
            string msg = "";

            //combobox for question, currently only general, so hardcode here first
            List<string> questionTypeList = new List<string>();
            questionTypeList.Add(ECFBase.Resources.Admin.lblGeneral);

            inboxModel.QuestionTypes = from c in questionTypeList
                                       select new SelectListItem
                                       {
                                           Text = c.ToString(),
                                           Value = InboxType.General.ToString()
                                       };

            inboxModel.SelectedType = ECFBase.Resources.Admin.lblGeneral;

            //combobox for language
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            inboxModel.LanguageList = from c in languages
                                      select new SelectListItem
                                      {
                                          Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                          Text = c.Text,
                                          Value = c.Value
                                      };
            inboxModel.SelectedLanguage = Session["LanguageChosen"].ToString();

            if (InboxID == 0)
            {
                //Create New Corporate News
                inboxModel.Title = "";
                inboxModel.SendTo = "";
                inboxModel.InboxID = 0;

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    inboxModel.TitleLanguageList.Add(tlm);
                }

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    inboxModel.ContentLanguageList.Add(tlm);
                }
            }
            else
            {
                //View Existing Corporate News
                DataSet dsCorpNews = AdminHelpDeskDB.GetMessageByID(InboxID, out ok, out msg);
                if (dsCorpNews.Tables[0].Rows.Count != 0)
                {
                    inboxModel.SelectedType = dsCorpNews.Tables[0].Rows[0]["CMSG_TYPE"].ToString();
                    inboxModel.SendTo = dsCorpNews.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                    inboxModel.State = int.Parse(dsCorpNews.Tables[0].Rows[0]["CMSG_STATE"].ToString());
                    inboxModel.InboxID = InboxID;

                    DataSet dsMultiLanguageMessage = AdminHelpDeskDB.GetMultiLanguageMessageByID(inboxModel.InboxID, out ok, out msg);

                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguageMessage.Tables[0].Select("CMSG_ID='" + inboxModel.InboxID + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGMSG_TITLE"].ToString();
                        inboxModel.TitleLanguageList.Add(tlm);
                    }

                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguageMessage.Tables[0].Select("CMSG_ID='" + inboxModel.InboxID + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGMSG_CONTENT"].ToString();
                        inboxModel.ContentLanguageList.Add(tlm);
                    }

                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguageMessage.Tables[0].Select("CMSG_ID='" + inboxModel.InboxID + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGMSG_REPLY"].ToString();
                        inboxModel.ReplyLanguageList.Add(tlm);
                    }
                }
            }

            return PartialView("InboxData", inboxModel);

        }
        #endregion

        #region CreateNewMessageMethod
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateNewMessageMethod(InboxModel idm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (idm.InboxID == null || idm.InboxID == 0)
                {
                    if (idm.SendTo == "" || idm.SendTo == null)
                    {
                        Response.Write(Resources.Admin.warningUsername);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    //Create New Record
                    int ok;
                    string msg;
                    int messageID;
                    string[] members = idm.SendTo.Split(new string[] { ";" }, StringSplitOptions.None);

                    foreach (string member in members)
                    {
                        DataSet dsUser = LoginDB.GetUserByUserName(member.Trim());
                        if (dsUser.Tables[0].Rows.Count == 0)
                        {
                            Response.Write("Invalid username: " + member);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }

                    if (idm.SendTo == "" || idm.SendTo == null)
                    {
                        Response.Write("Please Key in Username");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    foreach (string member in members)
                    {
                        AdminHelpDeskDB.CreateNewMessage(member.Trim(), Misc.GetInboxType(idm.SelectedType), Session["Admin"].ToString(), Session["Admin"].ToString(), out messageID, out ok, out msg);
                        if (ok == 1)
                        {
                            for (int i = 0; i < idm.TitleLanguageList.Count; i++)
                            {
                                AdminHelpDeskDB.CreateNewMultiLanguageMessage(messageID, idm.TitleLanguageList[i].LanguageCode, Helper.NVL(idm.TitleLanguageList[i].Text), Helper.NVL(idm.ContentLanguageList[i].Text), "", Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                            }
                        }
                    }
                }
                else
                {

                    //Update Existing Record
                    int ok = 0;
                    string msg;
                    AdminHelpDeskDB.UpdateMessage(idm.InboxID, Misc.GetInboxType(idm.SelectedType), Session["Admin"].ToString(), out ok, out msg);
                    if (ok == 1)
                    {
                        AdminHelpDeskDB.DeleteMultiLanguageMessageByID(idm.InboxID, out ok, out msg);
                        for (int i = 0; i < idm.TitleLanguageList.Count; i++)
                        {
                            AdminHelpDeskDB.CreateNewMultiLanguageMessage(idm.InboxID, idm.TitleLanguageList[i].LanguageCode, Helper.NVL(idm.TitleLanguageList[i].Text), Helper.NVL(idm.ContentLanguageList[i].Text), Helper.NVL(idm.ReplyLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                }
                return AllInbox();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region DeleteMessage
        public ActionResult DeleteMessage(int? InboxID)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete Message
                int ok;
                string msg;
                DataSet ds = AdminHelpDeskDB.GetMessageByID(InboxID, out ok, out msg);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    AdminHelpDeskDB.DeleteMessageByID(InboxID, out ok, out msg);//this will set the multilanguage message to deletion state 1 as well
                }

                return AllInbox();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion

        #region Notice

        #region AllNotice

        public ActionResult AllNotices(int selectedPage = 1, string type = "", string searchFilter = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            var model = new PaginationInboxModel();
            var dsCorpNews = new DataSet();

            if (string.IsNullOrEmpty(searchFilter))
            {
                dsCorpNews = AdminHelpDeskDB.GetAllNotices(Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);
            }
            else
            {
                dsCorpNews = AdminHelpDeskDB.GetAllNoticesByFilteringCriteria(Session["LanguageChosen"].ToString(), type, searchFilter, selectedPage, out pages, out ok, out msg);
            }

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsCorpNews.Tables[0].Rows)
            {
                var idm = new InboxModel();
                idm.Username = dr["CUSR_USERNAME"].ToString();
                idm.InboxID = Convert.ToInt32(dr["CNOT_ID"]);
                idm.Title = dr["CMULTILANGNOT_TITLE"].ToString();
                idm.PostDate = Convert.ToDateTime(dr["CNOT_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                model.InboxList.Add(idm);
            }

            model.FilteringCriteria = Misc.ConstructsNoticeFilteringCriteria();
            model.SelectedFilteringCriteria = type;

            return PartialView("AllNotices", model);
        }

        #endregion

        #region NoticeData
        public ActionResult NoticeData(int InboxID = 0)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var inboxModel = new InboxModel();
            int ok = 0;
            string msg = "";

            //combobox for language
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            inboxModel.LanguageList = from c in languages
                                      select new SelectListItem
                                      {
                                          Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                          Text = c.Text,
                                          Value = c.Value
                                      };
            inboxModel.SelectedLanguage = Session["LanguageChosen"].ToString();

            if (InboxID == 0)
            {
                //Create New Corporate News
                inboxModel.Title = "";
                inboxModel.InboxID = 0;

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    inboxModel.TitleLanguageList.Add(tlm);
                }

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    inboxModel.ContentLanguageList.Add(tlm);
                }
            }
            else
            {
                //View Existing Notice
                DataSet dsCorpNews = AdminHelpDeskDB.GetNoticeByID(InboxID, out ok, out msg);
                if (dsCorpNews.Tables[0].Rows.Count != 0)
                {
                    inboxModel.SendTo = dsCorpNews.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();

                    var dsMultiLanguageMessage = AdminHelpDeskDB.GetMultiLanguageNoticeByID(InboxID, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguageMessage.Tables[0].Select("CNOT_ID='" + InboxID + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGNOT_TITLE"].ToString();
                        inboxModel.TitleLanguageList.Add(tlm);
                    }

                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguageMessage.Tables[0].Select("CNOT_ID='" + InboxID + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGNOT_CONTENT"].ToString();
                        inboxModel.ContentLanguageList.Add(tlm);
                    }
                }
            }

            return PartialView("NoticeData", inboxModel);
        }

        #endregion

        #region CreateNewNoticeMethod

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateNewNoticeMethod(InboxModel idm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (idm.SendTo == "" || idm.SendTo == null)
                {
                    Response.Write("Please Key in Username");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (idm.InboxID == null || idm.InboxID == 0)
                {
                    //Create New Record
                    int ok;
                    string msg;
                    int messageID;

                    var members = idm.SendTo.Split(new string[] { ";" }, StringSplitOptions.None);

                    foreach (string member in members)
                    {
                        DataSet dsUser = LoginDB.GetUserByUserName(member.Trim());
                        if (dsUser.Tables[0].Rows.Count == 0)
                        {
                            Response.Write("Invalid username: " + member);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }

                    foreach (string member in members)
                    {
                        AdminHelpDeskDB.CreateNewNotice(member.Trim(), Session["Admin"].ToString(), Session["Admin"].ToString(), out messageID, out ok, out msg);
                        if (ok == 1)
                        {
                            for (int i = 0; i < idm.TitleLanguageList.Count; i++)
                            {
                                AdminHelpDeskDB.CreateNewMultiLanguageNotice(messageID, idm.TitleLanguageList[i].LanguageCode, Helper.NVL(idm.TitleLanguageList[i].Text), Helper.NVL(idm.ContentLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                            }
                        }
                    }
                }
                else
                {
                    //Update Existing Record
                    int ok = 0;
                    string msg;

                    AdminHelpDeskDB.DeleteMultiLanguageNoticeByID(idm.InboxID, out ok, out msg);
                    for (int i = 0; i < idm.TitleLanguageList.Count; i++)
                    {
                        AdminHelpDeskDB.CreateNewMultiLanguageNotice(idm.InboxID, idm.TitleLanguageList[i].LanguageCode, Helper.NVL(idm.TitleLanguageList[i].Text), Helper.NVL(idm.ContentLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                    }
                }

                return AllNotices();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region DeleteNotice

        public ActionResult DeleteNotice(int? InboxID)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete Message
                int ok;
                string msg;
                AdminHelpDeskDB.DeleteNoticeByID(InboxID, out ok, out msg);
                return AllNotices();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

    }
}
