﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ECFBase.Components;
using ECFBase.Models;
using System.Data;
using ECFBase.Helpers;
using System.IO;


namespace ECFBase.Controllers.Admin
{
    public class AdminInfoDeskController : Controller
    {
        #region Corporate News


        public ActionResult CorporateNews(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            PaginationInfoDeskModel model = new PaginationInfoDeskModel();
            DataSet dsCorpNews = AdminInfoDeskDB.GetAllInfoDesk("C", Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsCorpNews.Tables[0].Rows)
            {
                var idm = new InfoDeskModel();
                idm.InfoDeskID = Convert.ToInt32(dr["CINF_ID"]);
                idm.InfoDeskTitle = dr["CMULTILANGINFODESK_TITLE"].ToString();
                idm.InfoDeskPostDate = Convert.ToDateTime(dr["CINF_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                model.InfoDeskList.Add(idm);
            }

            return PartialView("CorporateNews", model);
        }

        public ActionResult CorporateNewsData(int CorporateNewsID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var idm = new InfoDeskModel();

            //combobox for category
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            idm.LanguageList = from c in languages
                               select new SelectListItem
                               {
                                   Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                   Text = c.Text,
                                   Value = c.Value
                               };
            idm.SelectedLanguage = Session["LanguageChosen"].ToString();

            if (CorporateNewsID == 0)
            {
                //Create New Corporate News
                idm.InfoDeskID = null;
                idm.InfoDeskType = "C";

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    idm.ProductImageName = "";
                    idm.ProductImagePath = "";
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    idm.TitleLanguageList.Add(tlm);
                }

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    idm.ProductImageName = "";
                    idm.ProductImagePath = "";
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    idm.ContentLanguageList.Add(tlm);
                }
            }
            else
            {
                //View Existing Corporate News
                DataSet dsCorpNews = AdminInfoDeskDB.GetInfoDeskByID(CorporateNewsID, out ok, out msg);
                if (dsCorpNews.Tables[0].Rows.Count != 0)
                {
                    idm.InfoDeskID = Convert.ToInt32(dsCorpNews.Tables[0].Rows[0]["CINF_ID"]);
                    idm.InfoDeskType = dsCorpNews.Tables[0].Rows[0]["CINF_TYPE"].ToString();
                    if (dsCorpNews.Tables[0].Rows[0]["CINF_IMAGEPATH"].ToString() != "")
                    {
                        idm.ProductImagePath = dsCorpNews.Tables[0].Rows[0]["CINF_IMAGEPATH"].ToString();
                        int extensionIndex = dsCorpNews.Tables[0].Rows[0]["CINF_IMAGEPATH"].ToString().LastIndexOf('\\');
                        idm.ProductImageName = dsCorpNews.Tables[0].Rows[0]["CINF_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                    }
                    else
                    {
                        idm.ProductImageName = "";
                        idm.ProductImagePath = "";
                    }

                    DataSet dsMultiLanguageInfoDesk = AdminInfoDeskDB.GetMultiLanguageInfoDeskByID(idm.InfoDeskID, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguageInfoDesk.Tables[0].Select("CINF_ID='" + idm.InfoDeskID + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGINFODESK_TITLE"].ToString();
                        idm.TitleLanguageList.Add(tlm);
                    }

                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguageInfoDesk.Tables[0].Select("CINF_ID='" + idm.InfoDeskID + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGINFODESK_CONTENT"].ToString();
                        idm.ContentLanguageList.Add(tlm);
                    }
                }
            }

            return PartialView("CorporateNewsData", idm);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CorporateNewsMethod(InfoDeskModel idm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                //-----------------------------------------------------------------------
                
                string A = string.Empty;

                if (idm.InfoDeskID == null || idm.InfoDeskID == 0)
                {
                    MemberDB.MakeMemberID(out A);
                }
                else
                {
                    int ok = 0;
                    string msg = string.Empty;
                    DataSet dsCorpNews = AdminInfoDeskDB.GetInfoDeskByID(idm.InfoDeskID, out ok, out msg);
                    A = dsCorpNews.Tables[0].Rows[0]["CINF_CODE"].ToString();
                }
                
                //------------------------------------------------------------------------

                idm.ProductImage = Request.Files["ProductImage"];
                if (idm.ProductImage != null && idm.ProductImage.FileName != string.Empty)
                {
                    string basePath = Server.MapPath("~/Products/" + A);
                    if (Directory.Exists(basePath) == false)
                    {
                        Directory.CreateDirectory(basePath);
                    }
                    if (idm.ProductImage.FileName.Contains(" "))
                    {
                        Response.Write(Resources.Admin.warningReUploadImg);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    idm.ProductImagePath = basePath + "\\" + idm.ProductImage.FileName;
                    idm.ProductImage.SaveAs(idm.ProductImagePath);
                }

                for (int i = 0; i < idm.TitleLanguageList.Count; i++)
                {

                    if (idm.SelectedLanguage == "en-US")
                    {
                        if (idm.TitleLanguageList[0].Text == null || idm.ContentLanguageList[0].Text == null)
                        {
                            Response.Write(Resources.Admin.warningTitleContent);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                        
                        if (idm.ProductImagePath == null)
                        {
                            Response.Write(Resources.Admin.warningUploadImage);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }

                    }
                    else if (idm.SelectedLanguage == "zh-CN")
                    {
                        if (idm.TitleLanguageList[1].Text == null || idm.ContentLanguageList[1].Text == null)
                        {
                            Response.Write(Resources.Admin.warningTitleContent);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                        if (idm.ProductImagePath == null)
                        {
                            Response.Write(Resources.Admin.warningUploadImage);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (idm.SelectedLanguage == "zh-TW")
                    {
                        if (idm.TitleLanguageList[2].Text == null || idm.ContentLanguageList[2].Text == null)
                        {
                            Response.Write(Resources.Admin.warningTitleContent);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                        if (idm.ProductImagePath == null)
                        {
                            Response.Write(Resources.Admin.warningUploadImage);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }

                }

                if (idm.InfoDeskID == null || idm.InfoDeskID == 0)
                {
                   
                    //Create New Record
                    int ok;
                    string msg;
                    int corpNewsID;
                    AdminInfoDeskDB.CreateNewInfoDesk(idm.InfoDeskType, Helper.NVL(idm.ProductImagePath), Session["Admin"].ToString(), Session["Admin"].ToString(), A, out corpNewsID, out ok, out msg);
                    if (ok == 1)
                    {
                        for (int i = 0; i < idm.TitleLanguageList.Count; i++)
                        {
                            AdminInfoDeskDB.CreateNewMultiLanguageInfoDesk(corpNewsID, Helper.NVL(idm.ProductImagePath), idm.TitleLanguageList[i].LanguageCode, Helper.NVL(idm.TitleLanguageList[i].Text), Helper.NVL(idm.ContentLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                            //Response.Write(Resources.Admin.msgNewsCreatedSuccessfully);
                        }
                    }
                }
                else
                {
                   
                    //Update Existing Record
                    int ok;
                    string msg;
                    AdminInfoDeskDB.UpdateInfoDesk(idm.InfoDeskID, Helper.NVL(idm.ProductImagePath), idm.InfoDeskType, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminInfoDeskDB.DeleteMultiLanguageInfoDeskByID(idm.InfoDeskID, out ok, out msg);
                        for (int i = 0; i < idm.TitleLanguageList.Count; i++)
                        {
                            AdminInfoDeskDB.CreateNewMultiLanguageInfoDesk(idm.InfoDeskID, Helper.NVL(idm.ProductImagePath), idm.TitleLanguageList[i].LanguageCode, Helper.NVL(idm.TitleLanguageList[i].Text), Helper.NVL(idm.ContentLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);

                            //Response.Write(Resources.Admin.msgNewsUpdatedSuccessfully);
                        }
                    }
                }
                //Session["page"] = "CorporateNews";
                //return RedirectToAction("Home", "Admin");
                return CorporateNews();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteCorpNews(int? CorpNewsId)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete existing Corporate News
                int ok;
                string msg;
                DataSet dsCorpNews = AdminInfoDeskDB.GetInfoDeskByID(CorpNewsId, out ok, out msg);
                if (dsCorpNews.Tables[0].Rows.Count != 0)
                {
                    AdminInfoDeskDB.DeleteInfoDeskByID(CorpNewsId, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminInfoDeskDB.DeleteMultiLanguageInfoDeskByID(CorpNewsId, out ok, out msg);
                    }
                }

                return CorporateNews();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Upload Center

        public ActionResult UploadCenter()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            PaginationFileCenterModel model = new PaginationFileCenterModel();

            DirectoryInfo directory = null;
            FileInfo[] files = null;

            string basePath = Server.MapPath("~/FileCenter/");
            if (Directory.Exists(basePath) == false)
            {
                Directory.CreateDirectory(basePath);
            }

            string dirPath = @"/FileCenter/";
            directory = new DirectoryInfo(Server.MapPath(dirPath));
            files = directory.GetFiles();
            files = files.OrderBy(f => f.Name).ToArray();

            int ok;
            string msg;
            foreach (FileInfo file in files)
            {
                DataSet dsFile = AdminInfoDeskDB.GetFileByName(file.Name, out ok, out msg);
                if (dsFile.Tables[0].Rows.Count != 0)
                {
                    var ucm = new FileCenterModel();
                    ucm.FileName = file.Name;
                    ucm.FileSize = (file.Length / 1000) + 1 + " kb";
                    ucm.FileTitle = dsFile.Tables[0].Rows[0]["CFILE_TITLE"].ToString();

                    model.FileCenterList.Add(ucm);
                }
            }

            return PartialView("UploadCenter", model);
        }

        public ActionResult deleteUploadCenter(string fileName)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok;
                string msg;
                DirectoryInfo directory = null;
                FileInfo[] files = null;

                string dirPath = @"/FileCenter/";
                directory = new DirectoryInfo(Server.MapPath(dirPath));
                files = directory.GetFiles();
                files = files.OrderBy(f => f.Name).ToArray();

                foreach (FileInfo file in files)
                {
                    if (file.Name == fileName)
                    {
                        file.Delete();
                        AdminInfoDeskDB.DeleteFile(file.Name, out ok, out msg);
                    }
                }

                return UploadCenter();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadCenterFile(HttpPostedFileBase file)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            if (file == null)
            {
                return PartialView("UploadCenterFile");
            }
            else
            {
                string fileTitle = Request["txtFileTitle"];

                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/FileCenter/"), fileName);
                file.SaveAs(path);

                int ok;
                string msg;
                AdminInfoDeskDB.CreateNewFile(fileName, fileTitle, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);

                Session["page"] = "UploadCenter";
                return RedirectToAction("Home", "Admin");
            }
        }
        [HttpPost]
        public ActionResult UploadCenterFileMethod(HttpPostedFileBase file)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }
            string fileTitle = Request["txtFileTitle"];

            if(fileTitle == null || fileTitle=="")
            {
                Response.Write("Please input filename!");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            else if (file == null)
            {
                Response.Write("Please upload a file!");
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/FileCenter/"), fileName);
                file.SaveAs(path);

                int ok;
                string msg;
                AdminInfoDeskDB.CreateNewFile(fileName, fileTitle, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);

                Session["page"] = "UploadCenter";
                return RedirectToAction("Home", "Admin");
            }
        }
        #endregion
    }
}
