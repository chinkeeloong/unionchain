﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using ECFBase.Components;
using ECFBase.Helpers;
using ECFBase.Models;
using System.Text;
using System.IO;
using System.Globalization;

namespace ECFBase.Controllers.Admin
{
    public class AdminBonusController : Controller
    {

        #region Reports

        public ActionResult DailySalesSummary(int selectedPage = 1, int selectedYear = -1, int selectedMonth = -1, int Country = 0)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages;

            if (selectedYear == -1)
                selectedYear = DateTime.Now.Year;
            if (selectedMonth == -1)
                selectedMonth = DateTime.Now.Month;

            var model = new PaginationCurrentSalesModel();

            #region Country
            //combobox for country

            List<CountrySetupModel> countries = Misc.GetAllCountryID("en-US");

            model.Countries = from c in countries
                              select new SelectListItem
                              {
                                  Text = c.CountryName,
                                  Value = c.CountryCode
                              };

            model.SelectedCountry = Convert.ToString(Country);
            #endregion

            var dsCurrentSales = BonusSettingDB.GetCurrentSales(selectedPage, selectedYear, selectedMonth, Country, out pages, out ok, out msg);
            selectedPage = ConstructPageList(selectedPage, pages, model);

            float TotalTradeableAmount = 0;
            float TotalUntradeableAmount = 0;
            float TotalTopUp = 0;
            float TotalHashBonus = 0;
            float TotalHashPowerBonus = 0;
            float TotalHashPerformanceBonus = 0;
            float LevelBonusAmount = 0;

            //tradeable
            foreach (DataRow dr in dsCurrentSales.Tables[0].Rows)
            {
                var csm = new CurrentSalesModel();
                csm.SalesDate = dr["SalesDate"].ToString();
                csm.NewRegSalesAmount = float.Parse(dr["SalesAmount"].ToString());
                csm.TotalTradeAmount += float.Parse(dr["SalesAmount"].ToString());
                //csm.TotalSalesInBVAmount += float.Parse(dr["SalesInBV"].ToString());
                csm.TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                model.CurrentSalesModel.Add(csm);

                TotalTradeableAmount = TotalTradeableAmount + float.Parse(dr["SalesAmount"].ToString());
            }

            //untradeable
            foreach (DataRow dr in dsCurrentSales.Tables[1].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].TotalUntradeAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                    TotalUntradeableAmount = TotalUntradeableAmount + float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.TotalUntradeAmount = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);

                    TotalUntradeableAmount = TotalUntradeableAmount + float.Parse(dr["SalesAmount"].ToString());
                }
            }

            //Hash
            foreach (DataRow dr in dsCurrentSales.Tables[2].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].Hash = float.Parse(dr["SalesAmount"].ToString());
                    TotalHashBonus = TotalHashBonus + float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].TotalBonus += float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.Hash = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalBonus = csm.TotalBonus + float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);

                    TotalHashBonus = TotalHashBonus + float.Parse(dr["SalesAmount"].ToString());
                }
            }

            //Hash Power
            foreach (DataRow dr in dsCurrentSales.Tables[3].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].HashPower = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].TotalBonus += float.Parse(dr["SalesAmount"].ToString());
                    TotalHashPowerBonus = TotalHashPowerBonus + float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.HashPower = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalBonus = csm.TotalBonus + float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);

                    TotalHashPowerBonus = TotalHashPowerBonus + float.Parse(dr["SalesAmount"].ToString());
                }
            }

            //Performance Hash
            foreach (DataRow dr in dsCurrentSales.Tables[4].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].HashPerformance = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].TotalBonus += float.Parse(dr["SalesAmount"].ToString());
                    TotalHashPerformanceBonus = TotalHashPerformanceBonus + float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.HashPerformance = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalReferralAmount = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalBonus = csm.TotalBonus + float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);

                    TotalHashPerformanceBonus = TotalHashPerformanceBonus + float.Parse(dr["SalesAmount"].ToString());
                }
            }

            //TopUp
            foreach (DataRow dr in dsCurrentSales.Tables[5].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].TotalTopUP = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                    TotalTopUp = TotalTopUp + float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.TotalTopUP = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);

                    TotalTopUp = TotalTopUp + float.Parse(dr["SalesAmount"].ToString());
                }
            }

            //sort first before add total
            var sort = new sortOnSalesDate();
            model.CurrentSalesModel.Sort(sort);

            var totalCSM = new CurrentSalesModel();
            totalCSM.SalesDate = Resources.Admin.lblTotal;
            totalCSM.TotalTradeAmount = TotalTradeableAmount;
            totalCSM.TotalUntradeAmount = TotalUntradeableAmount;
            totalCSM.TotalTopUP = TotalTopUp;
            totalCSM.TotalSalesAmount = TotalTradeableAmount + TotalUntradeableAmount;
            totalCSM.Hash = TotalHashBonus;
            totalCSM.HashPower = TotalHashPowerBonus;
            totalCSM.HashPerformance = TotalHashPerformanceBonus;
            totalCSM.LevelBonusAmount = LevelBonusAmount;
            totalCSM.TotalBonus = TotalHashBonus + TotalHashPowerBonus + TotalHashPerformanceBonus;

            model.CurrentSalesModel.Add(totalCSM);

            ViewBag.selectedYear = selectedYear;
            ViewBag.selectedMonth = selectedMonth;

            return PartialView("DailySalesSummary", model);
        }

        public ActionResult HashBonusList(int selectedPage = 1, int selectedYear = -1, int selectedMonth = -1, int Country = 0, string username ="")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages;

            if (selectedYear == -1)
                selectedYear = DateTime.Now.Year;
            if (selectedMonth == -1)
                selectedMonth = DateTime.Now.Month;

            var model = new PaginationCurrentSalesModel();

            #region Country
            //combobox for country

            List<CountrySetupModel> countries = Misc.GetAllCountryID("en-US");

            model.Countries = from c in countries
                              select new SelectListItem
                              {
                                  Text = c.CountryName,
                                  Value = c.CountryCode
                              };

            model.SelectedCountry = Convert.ToString(Country);
            #endregion
            DataSet dsHashBonus = new DataSet();

            if (username != "")
            {
                dsHashBonus = BonusSettingDB.GetHashBonusListByUsername(selectedPage, selectedYear, selectedMonth, Country, username, out pages, out ok, out msg);
            }
            else
            {
                dsHashBonus = BonusSettingDB.GetHashBonusList(selectedPage, selectedYear, selectedMonth, Country, out pages, out ok, out msg);
            }
            
            if (dsHashBonus.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    var csm = new HashBonusModel();
                    csm.MemberID = dr["CUSR_USERNAME"].ToString();
                    csm.SalesAmount = float.Parse(dr["CROIWL_CASHIN"].ToString());
                    csm.TotalAmount = 0;
                    csm.TotalAmount += csm.SalesAmount;
                    ViewBag.TotalAmount = csm.TotalAmount;
                    csm.SalesDate = Convert.ToDateTime(dr["CROIWL_CREATEDON"].ToString()).ToString("dd-MM-yyyy");
                    csm.CashName = dr["CROIWL_CASHNAME"].ToString();
                    csm.AppUser = dr["CROIWL_APPUSER"].ToString();
                    csm.Remarks = dr["CROIWL_APPOTHER"].ToString();
                    model.HashBonusModel.Add(csm);
                }

            }

            ViewBag.selectedYear = selectedYear;
            ViewBag.selectedMonth = selectedMonth;


            selectedPage = ConstructPageList(selectedPage, pages, model);



            return PartialView("HashBonusSummary", model);
        }

        public ActionResult HashPowerBonusList(int selectedPage = 1, int selectedYear = -1, int selectedMonth = -1, int Country = 0, string username = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages;

            if (selectedYear == -1)
                selectedYear = DateTime.Now.Year;
            if (selectedMonth == -1)
                selectedMonth = DateTime.Now.Month;

            var model = new PaginationCurrentSalesModel();

            #region Country
            //combobox for country

            List<CountrySetupModel> countries = Misc.GetAllCountryID("en-US");

            model.Countries = from c in countries
                              select new SelectListItem
                              {
                                  Text = c.CountryName,
                                  Value = c.CountryCode
                              };

            model.SelectedCountry = Convert.ToString(Country);
            #endregion
            DataSet dsHashBonus = new DataSet();

            if (username != "")
            {
                dsHashBonus = BonusSettingDB.GetHashPowerBonusListByUsername(selectedPage, selectedYear, selectedMonth, Country, username, out pages, out ok, out msg);
            }
            else
            {
                dsHashBonus = BonusSettingDB.GetHashPowerBonusList(selectedPage, selectedYear, selectedMonth, Country, out pages, out ok, out msg);
            }
            if (dsHashBonus.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    var csm = new HashPowerModel();
                    csm.MemberID = dr["CUSR_USERNAME"].ToString();
                    csm.SalesAmount = float.Parse(dr["CPHWL_CASHIN"].ToString());
                    csm.TotalAmount = 0;
                    csm.TotalAmount += csm.SalesAmount;
                    ViewBag.TotalAmount = csm.TotalAmount;
                    csm.SalesDate = Convert.ToDateTime(dr["CPHWL_CREATEDON"].ToString()).ToString("dd-MM-yyyy");
                    csm.CashName = dr["CPHWL_CASHNAME"].ToString();
                    csm.AppUser = dr["CPHWL_APPUSER"].ToString();
                    csm.Remarks = dr["CPHWL_APPOTHER"].ToString();
                    model.HashPowerModel.Add(csm);
                }
            }

            ViewBag.selectedYear = selectedYear;
            ViewBag.selectedMonth = selectedMonth;


            selectedPage = ConstructPageList(selectedPage, pages, model);



            return PartialView("HashPowerSummary", model);
        }
        
        public ActionResult HashPerformanceBonusList(int selectedPage = 1, int selectedYear = -1, int selectedMonth = -1, int Country = 0, string username = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages;

            if (selectedYear == -1)
                selectedYear = DateTime.Now.Year;
            if (selectedMonth == -1)
                selectedMonth = DateTime.Now.Month;

            var model = new PaginationCurrentSalesModel();

            #region Country
            //combobox for country

            List<CountrySetupModel> countries = Misc.GetAllCountryID("en-US");

            model.Countries = from c in countries
                              select new SelectListItem
                              {
                                  Text = c.CountryName,
                                  Value = c.CountryCode
                              };

            model.SelectedCountry = Convert.ToString(Country);
            #endregion

            DataSet dsHashBonus = new DataSet();

            if (username != "")
            {
                dsHashBonus = BonusSettingDB.GetHashPerformanceBonusListByUsername(selectedPage, selectedYear, selectedMonth, Country, username, out pages, out ok, out msg);
            }
            else
            {
                dsHashBonus = BonusSettingDB.GetHashPerformanceBonusList(selectedPage, selectedYear, selectedMonth, Country, out pages, out ok, out msg);
            }
            if (dsHashBonus.Tables[0].Rows.Count != 0)
            {
                foreach(DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    var csm = new HashPerformanceBonusModel();
                    csm.MemberID = dr["CUSR_USERNAME"].ToString();
                    csm.SalesAmount = float.Parse(dr["CMHWL_CASHIN"].ToString());
                    csm.TotalAmount = 0;
                    csm.TotalAmount += csm.SalesAmount;
                    ViewBag.TotalAmount = csm.TotalAmount;
                    csm.SalesDate = Convert.ToDateTime(dr["CMHWL_CREATEDON"].ToString()).ToString("dd-MM-yyyy");
                    csm.CashName = dr["CMHWL_CASHNAME"].ToString();
                    csm.AppUser = dr["CMHWL_APPUSER"].ToString();
                    csm.Remarks = dr["CMHWL_APPOTHER"].ToString();
                    model.HashPerformanceBonusModel.Add(csm);
                }
            }

            ViewBag.selectedYear = selectedYear;
            ViewBag.selectedMonth = selectedMonth;


            selectedPage = ConstructPageList(selectedPage, pages, model);



            return PartialView("HashPerformanceBonusSummary", model);
        }
        
        #endregion

        #region Export Function
        public void ExportDailySales(int selectedPage = 1, int selectedYear = -1, int selectedMonth = -1, int Country = 0, string username = "", string Export = "1")
        {

            string Title = string.Empty;
            
            Title = "DailySalesLog.csv";

            if (selectedYear == -1)
                selectedYear = DateTime.Now.Year;
            if (selectedMonth == -1)
                selectedMonth = DateTime.Now.Month;

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + Title + "");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintDailySalesBonus(selectedYear, selectedMonth, Country, Export, selectedPage));
            sw.Close();
            Response.End();
        }
        private string PrintDailySalesBonus(int selectedYear = -1, int selectedMonth = -1, int Country = 0, string Export = "1", int SelectedPage = 1)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Date,Tradeable Wallet,Untradeable Wallet,Total,UNS Hash, Performance Hash Bonus, Management Hash Bonus, Grand Total"));

            int pages = 0;
            int ok = 0;
            string msg = string.Empty;

            if (selectedYear == -1)
                selectedYear = DateTime.Now.Year;
            if (selectedMonth == -1)
                selectedMonth = DateTime.Now.Month;

            var model = new PaginationCurrentSalesModel();

            DataSet dsCurrentSales = BonusSettingDB.GetCurrentSales(SelectedPage, selectedYear, selectedMonth, Country, out pages, out ok, out msg);

            float TotalTradeableAmount = 0;
            float TotalUntradeableAmount = 0;
            float TotalHashBonus = 0;
            float TotalHashPowerBonus = 0;
            float TotalHashPerformanceBonus = 0;
            float LevelBonusAmount = 0;

            //tradeable
            foreach (DataRow dr in dsCurrentSales.Tables[0].Rows)
            {
                var csm = new CurrentSalesModel();
                csm.SalesDate = dr["SalesDate"].ToString();
                csm.NewRegSalesAmount = float.Parse(dr["SalesAmount"].ToString());
                csm.TotalTradeAmount += float.Parse(dr["SalesAmount"].ToString());
                //csm.TotalSalesInBVAmount += float.Parse(dr["SalesInBV"].ToString());
                csm.TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                model.CurrentSalesModel.Add(csm);

                TotalTradeableAmount = TotalTradeableAmount + float.Parse(dr["SalesAmount"].ToString());
            }

            //untradeable
            foreach (DataRow dr in dsCurrentSales.Tables[1].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].TotalUntradeAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                    TotalUntradeableAmount = TotalUntradeableAmount + float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.TotalUntradeAmount = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalSalesAmount += float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);

                    TotalUntradeableAmount = TotalUntradeableAmount + float.Parse(dr["SalesAmount"].ToString());
                }
            }

            //Hash
            foreach (DataRow dr in dsCurrentSales.Tables[2].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].Hash = float.Parse(dr["SalesAmount"].ToString());
                    TotalHashBonus = TotalHashBonus + float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.Hash = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);

                    TotalHashBonus = TotalHashBonus + float.Parse(dr["SalesAmount"].ToString());
                }
            }

            //Hash Power
            foreach (DataRow dr in dsCurrentSales.Tables[3].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].HashPower = float.Parse(dr["SalesAmount"].ToString());
                    TotalHashPowerBonus = TotalHashPowerBonus + float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.HashPower = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);

                    TotalHashPowerBonus = TotalHashPowerBonus + float.Parse(dr["SalesAmount"].ToString());
                }
            }

            //Performance Hash
            foreach (DataRow dr in dsCurrentSales.Tables[4].Rows)
            {
                //already has that date
                int nIndex = model.CurrentSalesModel.FindIndex(f => f.SalesDate == dr["SalesDate"].ToString());

                if (nIndex >= 0)
                {
                    model.CurrentSalesModel[nIndex].HashPerformance = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel[nIndex].TotalReferralAmount += float.Parse(dr["SalesAmount"].ToString());
                    TotalHashPerformanceBonus = TotalHashPerformanceBonus + float.Parse(dr["SalesAmount"].ToString());
                }
                else
                {
                    var csm = new CurrentSalesModel();
                    csm.SalesDate = dr["SalesDate"].ToString();
                    csm.HashPerformance = float.Parse(dr["SalesAmount"].ToString());
                    csm.TotalReferralAmount = float.Parse(dr["SalesAmount"].ToString());
                    model.CurrentSalesModel.Add(csm);

                    TotalHashPerformanceBonus = TotalHashPerformanceBonus + float.Parse(dr["SalesAmount"].ToString());
                }
            }

            //sort first before add total
            var sort = new sortOnSalesDate();
            model.CurrentSalesModel.Sort(sort);

            var totalCSM = new CurrentSalesModel();
            totalCSM.SalesDate = Resources.Admin.lblTotal;
            totalCSM.TotalTradeAmount = TotalTradeableAmount;
            totalCSM.TotalUntradeAmount = TotalUntradeableAmount;
            totalCSM.TotalSalesAmount = TotalTradeableAmount + TotalUntradeableAmount;
            totalCSM.Hash = TotalHashBonus;
            totalCSM.HashPower = TotalHashPowerBonus;
            totalCSM.HashPerformance = TotalHashPerformanceBonus;
            totalCSM.LevelBonusAmount = LevelBonusAmount;
            totalCSM.TotalBonus = TotalHashBonus + TotalHashPowerBonus + TotalHashPerformanceBonus;

            foreach (DataRow dr in dsCurrentSales.Tables[0].Rows)
                {
                    sb.AppendLine(
                                   totalCSM.SalesDate + "," +
                                   TotalTradeableAmount + "," +
                                   TotalUntradeableAmount + "," +
                                   totalCSM.TotalUntradeAmount + "," +
                                   totalCSM.TotalSalesAmount + "," +
                                   totalCSM.Hash + "," +
                                   totalCSM.HashPower + "," +
                                   totalCSM.HashPerformance + "," +
                                   totalCSM.TotalBonus
                                   );
                }

           
            return sb.ToString();
        }
        public void ExportHashBonus(string type = "", int selectedPage = 1, int selectedYear = -1, int selectedMonth = -1, int Country = 0, string username = "", string Export = "1")
        {

            string Title = string.Empty;

            if(type=="Hash")
            {
                Title = "HashBonus-Log.csv";
            }
            if (type == "HashPower")
            {
                Title = "PerformanceHashBonus-Log.csv";
            }
            if (type == "HashPerformance")
            {
                Title = "ManagementHashBonus-Log.csv";
            }

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + Title + "");
            Response.ContentType = "application/octet-stream";
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            sw.Write(PrintBonus(type, username, selectedYear, selectedMonth, Country, Export, selectedPage));
            sw.Close();
            Response.End();
        }
        private string PrintBonus(string type, string username, int selectedYear, int selectedMonth, int Country, string Export, int SelectedPage)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Date,Username,Amount,Remark,Extra Info"));

            int pages = 0;
            int ok = 0;
            string msg = string.Empty;
          
            if(type == "Hash")
            {
                
                DataSet dsHashBonus = BonusSettingDB.GetHashBonusListExport(SelectedPage, selectedYear, selectedMonth, Country, username, out pages, out ok, out msg);

                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    sb.AppendLine(
                                    Convert.ToDateTime(dr["CROIWL_CREATEDON"]).ToString("dd-MM-yyyy") + "," +
                                    dr["CUSR_USERNAME"].ToString() + "," +
                                   dr["CROIWL_CASHIN"].ToString() + "," +
                                    dr["CROIWL_CASHNAME"].ToString() + "," +
                                   dr["CROIWL_APPOTHER"].ToString());
                }
             
            }
            else if (type == "HashPower")
            {
                DataSet dsHashBonus = BonusSettingDB.GetHashPowerBonusListExport(SelectedPage, selectedYear, selectedMonth, Country, username, out pages, out ok, out msg);

                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    sb.AppendLine(
                                    Convert.ToDateTime(dr["CPHWL_CREATEDON"]).ToString("dd-MM-yyyy") + "," +
                                    dr["CUSR_USERNAME"].ToString() + "," +
                                   dr["CPHWL_CASHIN"].ToString() + "," +
                                    dr["CPHWL_CASHNAME"].ToString() + "," +
                                   dr["CPHWL_APPOTHER"].ToString());
                }
            }
            else if (type == "HashPerformance")
            {
                DataSet dsHashBonus = BonusSettingDB.GetHashPerformanceBonusListExport(SelectedPage, selectedYear, selectedMonth, Country, username, out pages, out ok, out msg);

                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    sb.AppendLine(
                                    Convert.ToDateTime(dr["CMHWL_CREATEDON"]).ToString("dd-MM-yyyy") + "," +
                                    dr["CUSR_USERNAME"].ToString() + "," +
                                   dr["CMHWL_CASHIN"].ToString() + "," +
                                    dr["CMHWL_CASHNAME"].ToString() + "," +
                                   dr["CMHWL_APPOTHER"].ToString());
                }
            }
            return sb.ToString();
        }
        #endregion

        #region Total Direct Sponsor
        public ActionResult GetTotalDirectSponsor(int selectedPage = 1, string username = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }
                       
            int pages;
            var model = new DirectSponsorModel();
            DataSet dsHashBonus = new DataSet();

            dsHashBonus = BonusSettingDB.GetTotalDirectSponsorList(selectedPage, username, out pages);
          
            if (dsHashBonus.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dr in dsHashBonus.Tables[0].Rows)
                {
                    var csm = new DirectSponsorModel();
                    csm.MemberID = dr["CUSR_USERNAME"].ToString();
                    csm.Fullname = dr["CUSR_FULLNAME"].ToString();
                    csm.TotalDirectSponsor = dr["TotalDirectSponsor"].ToString();
              
                    model.TotalSponsorModel.Add(csm);
                }
            }



            selectedPage = ConstructPageList2(selectedPage, pages, model);



            return PartialView("DirectSponsorSummary", model);
        }
        
        #endregion
        
        #region Shared
        private static int ConstructPageList(int selectedPage, int pages, PaginationCurrentSalesModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }
            

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        private static int ConstructPageList2(int selectedPage, int pages, DirectSponsorModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }


            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        #endregion


    }
}
