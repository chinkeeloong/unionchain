﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ECFBase.Components;
using ECFBase.Models;
using System.Data;
using ECFBase.Helpers;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;

namespace ECFBase.Controllers.Admin
{
    public class AdminGeneralController : Controller
    {

        #region Utility
        public ActionResult UtilitySetup(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            PaginationUtilitySetupModel model = new PaginationUtilitySetupModel();
            DataSet dsUtility = AdminGeneralDB.GetAllUtility(Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            #region Country
            string selectedcountry = "CN";

            List<CountrySetupModel> countries = Misc.GetAllCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);

            model.Countries = from c in countries
                              select new SelectListItem
                              {
                                  Text = c.CountryName,
                                  Value = c.CountryCode
                              };

            model.SelectedCountry = selectedcountry;
            DataSet dsCountrySetup = AdminGeneralDB.GetCountryByCountryCode(selectedcountry, out ok, out msg);

            //model.MobileCountryCode = dsCountrySetup.Tables[0].Rows[0]["CMULTILANGCOUNTRY_NAME"].ToString() + " (" + dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_MOBILECODE"].ToString() + ") ";


            #endregion

            foreach (DataRow dr in dsUtility.Tables[0].Rows)
            {
                var csm = new UtilitySetupModel();
                csm.UtilityID = Convert.ToInt32(dr["CUTI_ID"]);
                csm.UtilityCode = dr["CUTI_CODE"].ToString();
                csm.UtilityName = dr["CMULTILANGUTILITY_NAME"].ToString();
                csm.UtilityDescription = dr["CMULTILANGUTILITY_DESCRIPTION"].ToString();
                csm.SelectedCountry = Misc.GetCountryNameByCountryCode(dr["CCOUNTRY_CODE"].ToString());
                model.UtilityList.Add(csm);
            }

            return PartialView("UtilitySetup", model);
        }

        public ActionResult UtilitySetupData(int UtilityID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var csm = new UtilitySetupModel();

            //combobox for Utility
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            csm.LanguageList = from c in languages
                               select new SelectListItem
                               {
                                   Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                   Text = c.Text,
                                   Value = c.Value
                               };
            csm.SelectedLanguage = Session["LanguageChosen"].ToString();

            if (UtilityID == 0)
            {
                //Create New Utility Setup
                csm.UtilityID = null;
                csm.UtilityCode = "";
                csm.MobileBill = false;
                csm.ProcessingTime = "";
                csm.Digit = "";


                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    csm.NameLanguageList.Add(tlm);
                }

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    csm.DescriptionLanguageList.Add(tlm);
                }

                #region Country
                string selectedcountry = "MY";

                List<CountrySetupModel> countries = Misc.GetAllCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);

                csm.Countries = from c in countries
                                select new SelectListItem
                                {
                                    Text = c.CountryName,
                                    Value = c.CountryCode
                                };

                csm.SelectedCountry = selectedcountry;
                DataSet dsCountrySetup = AdminGeneralDB.GetCountryByCountryCode(selectedcountry, out ok, out msg);

                //model.MobileCountryCode = dsCountrySetup.Tables[0].Rows[0]["CMULTILANGCOUNTRY_NAME"].ToString() + " (" + dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_MOBILECODE"].ToString() + ") ";


                #endregion

            }
            else
            {
                //View Existing Utility Setup
                DataSet dsUtilitySetup = AdminGeneralDB.GetUtilityByID(UtilityID, Session["LanguageChosen"].ToString(), "", out ok, out msg);
                if (dsUtilitySetup.Tables[0].Rows.Count != 0)
                {
                    csm.UtilityID = Convert.ToInt32(dsUtilitySetup.Tables[0].Rows[0]["CUTI_ID"]);
                    csm.UtilityCode = dsUtilitySetup.Tables[0].Rows[0]["CUTI_CODE"].ToString();
                    csm.ProcessingTime = dsUtilitySetup.Tables[0].Rows[0]["CUTI_PROCESS"].ToString();
                    csm.Digit = dsUtilitySetup.Tables[0].Rows[0]["CUTI_DIGIT"].ToString();
                    csm.MinAmount = float.Parse(dsUtilitySetup.Tables[0].Rows[0]["CUTI_MINAMOUNT"].ToString()).ToString("n2");
                    csm.MaxAmount = float.Parse(dsUtilitySetup.Tables[0].Rows[0]["CUTI_MAXAMOUNT"].ToString()).ToString("n2");
                    csm.SelectedCountry = dsUtilitySetup.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

                    if (dsUtilitySetup.Tables[0].Rows[0]["CUTI_IMAGEPATH"].ToString() != "")
                    {
                        csm.UtilityImagePath = dsUtilitySetup.Tables[0].Rows[0]["CUTI_IMAGEPATH"].ToString();
                        int extensionIndex = dsUtilitySetup.Tables[0].Rows[0]["CUTI_IMAGEPATH"].ToString().LastIndexOf('\\');
                        csm.UtilityImageName = dsUtilitySetup.Tables[0].Rows[0]["CUTI_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                    }
                    else
                    {
                        csm.UtilityImageName = "";
                        csm.UtilityImagePath = "";
                    }

                    DataSet dsMultiLanguageUtility = AdminGeneralDB.GetMultiLanguageUtilityByCode(csm.UtilityCode, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        csm.MobileBill = Convert.ToBoolean(dsUtilitySetup.Tables[0].Rows[0]["CUTI_MOBILEBILL"].ToString());
                        DataRow[] dr = dsMultiLanguageUtility.Tables[0].Select("CUTI_CODE='" + csm.UtilityCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGUTILITY_NAME"].ToString();
                        csm.NameLanguageList.Add(tlm);
                    }

                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        csm.MobileBill = Convert.ToBoolean(dsUtilitySetup.Tables[0].Rows[0]["CUTI_MOBILEBILL"].ToString());
                        DataRow[] dr = dsMultiLanguageUtility.Tables[0].Select("CUTI_CODE='" + csm.UtilityCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGUTILITY_DESCRIPTION"].ToString();
                        csm.DescriptionLanguageList.Add(tlm);
                    }


                    #region Country
                    

                    List<CountrySetupModel> countries = Misc.GetAllCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);

                    csm.Countries = from c in countries
                                    select new SelectListItem
                                    {
                                        Text = c.CountryName,
                                        Value = c.CountryCode
                                    };

                 
                    DataSet dsCountrySetup = AdminGeneralDB.GetCountryByCountryCode(csm.SelectedCountry, out ok, out msg);
                    
                    #endregion

                }
            }

            return PartialView("UtilitySetupData", csm);
        }

        [HttpPost]
        public ActionResult UtilitySetupDataMethod(UtilitySetupModel csm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                csm.UtilityImage = Request.Files["UtilityImage"];
                if (csm.UtilityImage != null && csm.UtilityImage.FileName != string.Empty)
                {
                    string basePath = Server.MapPath("~/Utility/" + csm.UtilityCode);
                    if (Directory.Exists(basePath) == false)
                    {
                        Directory.CreateDirectory(basePath);
                    }
                    csm.UtilityImagePath = basePath + "\\" + csm.UtilityImage.FileName;
                    csm.UtilityImage.SaveAs(csm.UtilityImagePath);
                }

                if(string.IsNullOrEmpty(csm.UtilityCode))
                {
                    Response.Write(Resources.Member.warningInputUtilityCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                #region Country
                if (string.IsNullOrEmpty(csm.SelectedCountry))
                {
                    Response.Write(Resources.Member.warningSelectCountry);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                #endregion
                if (string.IsNullOrEmpty(csm.MinAmount))
                {
                    Response.Write(Resources.Member.warningPlsInputMinAmt);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(csm.MaxAmount))
                {
                    Response.Write(Resources.Member.WarningPlsInputMaxAmt);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(csm.Digit))
                {
                    Response.Write(Resources.Member.WarningPlsInputNumOfDigit);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                if (string.IsNullOrEmpty(csm.ProcessingTime))
                {
                    csm.ProcessingTime = "1";
                }

                if (csm.ProcessingTime.Contains(" ") || csm.Digit.Contains(" ") || csm.MaxAmount.Contains(" ") || csm.MinAmount.Contains(" "))
                {
                    Response.Write(Resources.Member.warningNotAllowSpacingUtility);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                Regex phonereg = new Regex(@"^[0-9]*$");

                if (!phonereg.IsMatch(csm.ProcessingTime) == true || !phonereg.IsMatch(csm.Digit) == true)
                {
                    Response.Write(Resources.Member.warningNotAllowCharUtility);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                Regex phonereg2 = new Regex(@"^[A-Za-z]$");

                if (phonereg2.IsMatch(csm.MaxAmount) == true || phonereg2.IsMatch(csm.MinAmount) == true)
                {
                    Response.Write(Resources.Member.warningNotAllowCharUtility);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (csm.UtilityID == null || csm.UtilityID == 0)
                {
                    //Create New Record
                    int ok;
                    string msg;
                    AdminGeneralDB.CreateNewUtility(csm.UtilityCode, csm.SelectedCountry, Helper.NVL(csm.UtilityImagePath), csm.ProcessingTime, csm.MinAmount, csm.MaxAmount, csm.Digit, csm.MobileBill, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                    if (ok == 1)
                    {
                        for (int i = 0; i < csm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageUtility(csm.UtilityCode, csm.NameLanguageList[i].LanguageCode, Helper.NVL(csm.NameLanguageList[i].Text), Helper.NVL(csm.DescriptionLanguageList[i].Text), csm.MobileBill, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Member.msgDuplicateCatCode);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }

                }
                else
                {
                    //Update Existing Record
                    int ok;
                    string msg;
                    AdminGeneralDB.UpdateUtility(csm.UtilityID, csm.UtilityCode, csm.SelectedCountry, Helper.NVL(csm.UtilityImagePath), csm.ProcessingTime, csm.MinAmount, csm.MaxAmount, csm.Digit, csm.MobileBill, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguageUtilityByCode(csm.UtilityCode, out ok, out msg);
                        for (int i = 0; i < csm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageUtility(csm.UtilityCode, csm.NameLanguageList[i].LanguageCode, Helper.NVL(csm.NameLanguageList[i].Text), Helper.NVL(csm.DescriptionLanguageList[i].Text), csm.MobileBill, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Member.msgDuplicateCatCode);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return UtilitySetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UtilityDeleteData(int UtilityID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            try
            {


                int ok = 0;
                string msg = "";
                var csm = new UtilitySetupModel();

                //View Existing Utility Setup
                DataSet dsUtilitySetup = AdminGeneralDB.GetUtilityByID(UtilityID, Session["LanguageChosen"].ToString(),"", out ok, out msg);
                if (dsUtilitySetup.Tables[0].Rows.Count != 0)
                {
                    csm.UtilityID = Convert.ToInt32(dsUtilitySetup.Tables[0].Rows[0]["CUTI_ID"]);
                    csm.UtilityCode = dsUtilitySetup.Tables[0].Rows[0]["CUTI_CODE"].ToString();
                }
                else
                {
                    Response.Write(Resources.Member.warningUtilityNotExist);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                AdminGeneralDB.DeleteUtilityByID(UtilityID, out ok, out msg);
                if (ok == 1)
                {
                    AdminGeneralDB.DeleteMultiLanguageUtilityByCode(csm.UtilityCode, out ok, out msg);                   
                }
                if (ok == 0)
                {
                    if (msg == "-1")
                    {
                        Response.Write(Resources.Member.warningUtilityNotExist);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                 }
              

                return UtilitySetup();
            }
           catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Company Setup
        public ActionResult CompanySetupData()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var message = string.Empty;
            var csm = new CompanySetupModel();
            DataSet dsCompanySetup = AdminGeneralDB.GetCompanySetup();



            if (dsCompanySetup.Tables[0].Rows.Count == 0)
            {
                //Create New Company Setup
                csm.CompanyName = null;
                csm.CompanyRegistrationNo = "";
                csm.CompanyID = 0;
                csm.CompanyGSTNo = "";
                csm.PostCode = "";

                csm.State = "";
                csm.Country = "";

                csm.TelNo = "";
                csm.FaxNo = "";

                csm.Email = "";

                csm.CompanyImageName = "";
                csm.CompanyImagePath = "";
            }
            else
            {
                //View Existing Company Setup

                if (dsCompanySetup.Tables[0].Rows.Count != 0)
                {
                    csm.CompanyID = Convert.ToInt32(dsCompanySetup.Tables[0].Rows[0]["COM_ID"]);
                    csm.CompanyName = dsCompanySetup.Tables[0].Rows[0]["COM_COMPANY_NAME"].ToString();
                    csm.CompanyRegistrationNo = dsCompanySetup.Tables[0].Rows[0]["COM_REGISTERNO"].ToString();
                    csm.CompanyGSTNo = dsCompanySetup.Tables[0].Rows[0]["COM_COMPANY_GST_NO"].ToString();
                    csm.PostCode = dsCompanySetup.Tables[0].Rows[0]["COM_POSTCODE"].ToString();
                    csm.State = dsCompanySetup.Tables[0].Rows[0]["COM_STATE"].ToString();
                    csm.Country = dsCompanySetup.Tables[0].Rows[0]["COM_COUNTRY"].ToString();
                    csm.TelNo = dsCompanySetup.Tables[0].Rows[0]["COM_TEL"].ToString();
                    csm.FaxNo = dsCompanySetup.Tables[0].Rows[0]["COM_FAX"].ToString();
                    csm.Email = dsCompanySetup.Tables[0].Rows[0]["COM_EMAIL"].ToString();
                    csm.CompanyAddress = dsCompanySetup.Tables[0].Rows[0]["COM_ADDRESS"].ToString();
                    csm.CompanyAddress1 = dsCompanySetup.Tables[0].Rows[0]["COM_ADDRESS1"].ToString();
                    csm.CompanyAddress2 = dsCompanySetup.Tables[0].Rows[0]["COM_ADDRESS2"].ToString();
                    csm.CompanyAddress3 = dsCompanySetup.Tables[0].Rows[0]["COM_ADDRESS3"].ToString();
                    //var CompanyAddress = dsCompanySetup.Tables[0].Rows[0]["COM_ADDRESS"].ToString();
                    //if (!string.IsNullOrEmpty(CompanyAddress.Trim()))
                    //{
                    //    var address = CompanyAddress.Split(new char[] { '\r' });
                    //    for (int i = 0; i < address.Length; i++)
                    //    {
                    //        if (i == 0) csm.CompanyAddress = address[0];
                    //        else if (i == 1) csm.CompanyAddress1 = address[1];
                    //        else if (i == 2) csm.CompanyAddress2 = address[2];
                    //        else if (i == 3) csm.CompanyAddress3 = address[3];
                    //    }
                    //}

                    if (dsCompanySetup.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString() != "")
                    {
                        csm.CompanyImagePath = dsCompanySetup.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString();
                        int extensionIndex = dsCompanySetup.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString().LastIndexOf('\\');
                        csm.CompanyImageName = dsCompanySetup.Tables[0].Rows[0]["COM_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                    }
                    else
                    {
                        csm.CompanyImageName = "";
                        csm.CompanyImagePath = "";
                    }

                }
            }

            return PartialView("CompanySetupData", csm);
        }

        [HttpPost]
        public ActionResult CompanySetupDataMethod(CompanySetupModel csm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (csm.CompanyName == null || csm.CompanyName == "")
                {
                    Response.Write("Please Keyin Company Name");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                if (csm.CompanyRegistrationNo == null || csm.CompanyRegistrationNo == "0")
                {
                    csm.CompanyRegistrationNo = "";
                }
                if (csm.CompanyGSTNo == null || csm.CompanyGSTNo == "0")
                {
                    csm.CompanyGSTNo = "";
                }
                if (csm.FaxNo == null)
                {
                    csm.FaxNo = "";
                }
                if (csm.Email == null)
                {
                    csm.Email = "";
                }
                if (csm.TelNo == null)
                {
                    csm.TelNo = "";
                }
                if (csm.PostCode == null)
                {
                    csm.PostCode = "";
                }
                if (csm.State == null)
                {
                    csm.State = "";
                }
                if (csm.Country == null)
                {
                    csm.Country = "";
                }
                if (csm.CompanyAddress == null)
                {
                    csm.CompanyAddress = "";
                }
                if (csm.CompanyAddress1 == null)
                {
                    csm.CompanyAddress1 = "";
                }
                if (csm.CompanyAddress2 == null)
                {
                    csm.CompanyAddress2 = "";
                }
                if (csm.CompanyAddress3 == null)
                {
                    csm.CompanyAddress3 = "";
                }


                if (csm.CompanyID == 0)
                {
                    csm.CompanyImage = Request.Files["CompanyImage"];
                    if (csm.CompanyImage != null && csm.CompanyImage.FileName != string.Empty)
                    {
                        string basePath = Server.MapPath("~/Images/Company/");
                        if (Directory.Exists(basePath) == false)
                        {
                            Directory.CreateDirectory(basePath);
                        }
                        var filename = Path.GetFileName(csm.CompanyImage.FileName);
                        if (csm.CompanyImage.FileName.Contains(" "))
                        {
                            Response.Write("Please re - upload image without space on image name");
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }

                        csm.CompanyImagePath = basePath + "\\" + filename;
                        csm.CompanyImage.SaveAs(csm.CompanyImagePath);
                        csm.CompanyImagePath = "../../Images/Company/" + filename;
                    }




                    //Create New Record
                    int ok;
                    string msg;
                    AdminGeneralDB.CreateCompanySetup(csm, out ok, out msg);

                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    //Update Existing Record
                    int ok;
                    string msg;



                    csm.CompanyImage = Request.Files["CompanyImage"];
                    if (csm.CompanyImage != null && csm.CompanyImage.FileName != string.Empty)
                    {
                        string basePath = Server.MapPath("~/Images/Company/");
                        if (Directory.Exists(basePath) == false)
                        {
                            Directory.CreateDirectory(basePath);
                        }
                        var filename = Path.GetFileName(csm.CompanyImage.FileName);

                        if (csm.CompanyImage.FileName.Contains(" "))
                        {
                            Response.Write("Please re - upload image without space on image name");
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                        csm.CompanyImagePath = basePath + "\\" + filename;
                        csm.CompanyImage.SaveAs(csm.CompanyImagePath);
                        csm.CompanyImagePath = "../../Images/Company/" + filename;
                    }


                    AdminGeneralDB.UpdateCompanySetup(csm, out ok, out msg);
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }


                return CompanySetupData();
            }
            catch (Exception e)
            {
                Response.Write("1|" + e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Slideshow

        public ActionResult ImageSlider(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            PaginationInfoDeskModel model = new PaginationInfoDeskModel();
            DataSet dsCorpNews = AdminInfoDeskDB.GetAllImageSlide(selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsCorpNews.Tables[0].Rows)
            {
                var idm = new InfoDeskModel();
                idm.InfoDeskID = Convert.ToInt32(dr["CINF_ID"]);
                idm.InfoDeskTitle = dr["CINF_TITLE"].ToString();
                idm.InfoDeskPostDate = Convert.ToDateTime(dr["CINF_CREATEDON"]).ToString("dd/MM/yyyy hh:mm:ss tt");
                model.InfoDeskList.Add(idm);
            }

            return PartialView("ImageSlider", model);
        }

        public ActionResult ImageSliderData(int ImageID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var idm = new InfoDeskModel();



            if (ImageID == 0)
            {
                //Create New Image Slide
                idm.InfoDeskID = null;
                //idm.InfoDeskType = "C";
                idm.InfoDeskTitle = "";
                idm.ProductImageName = "";
                idm.ProductImagePath = "";


            }
            else
            {
                //View Existing Image Slide
                DataSet dsCorpNews = AdminInfoDeskDB.GetImageSlideByID(ImageID, out ok, out msg);
                if (dsCorpNews.Tables[0].Rows.Count != 0)
                {
                    idm.InfoDeskID = Convert.ToInt32(dsCorpNews.Tables[0].Rows[0]["CINF_ID"]);
                    //idm.InfoDeskType = dsCorpNews.Tables[0].Rows[0]["CINF_TYPE"].ToString();
                    idm.InfoDeskTitle = dsCorpNews.Tables[0].Rows[0]["CINF_TITLE"].ToString();
                    if (dsCorpNews.Tables[0].Rows[0]["CINF_IMAGEPATH"].ToString() != "")
                    {
                        idm.ProductImagePath = dsCorpNews.Tables[0].Rows[0]["CINF_IMAGEPATH"].ToString();
                        int extensionIndex = dsCorpNews.Tables[0].Rows[0]["CINF_IMAGEPATH"].ToString().LastIndexOf('\\');
                        idm.ProductImageName = dsCorpNews.Tables[0].Rows[0]["CINF_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                    }

                    else
                    {
                        idm.ProductImageName = "";
                        idm.ProductImagePath = "";
                    }

                }
            }

            return PartialView("ImageSliderData", idm);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ImageSliderMethod(InfoDeskModel idm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                idm.ProductImage = Request.Files["ProductImage"];
                if (idm.ProductImage != null && idm.ProductImage.FileName != string.Empty)
                {
                    string basePath = Server.MapPath("~/Slider/");
                    if (Directory.Exists(basePath) == false)
                    {
                        Directory.CreateDirectory(basePath);
                    }
                    var filename = Path.GetFileName(idm.ProductImage.FileName);
                    if (idm.ProductImage.FileName.Contains(" "))
                    {
                        Response.Write("Please re - upload image without space on image name");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    idm.ProductImagePath = basePath + "\\" + filename;

                    idm.ProductImage.SaveAs(idm.ProductImagePath);
                    //CK
                    idm.ProductImagePath = "../../Slider/" + filename;

                }


                if (idm.InfoDeskID == null || idm.InfoDeskID == 0)
                {
                    if (idm.InfoDeskTitle == null || idm.InfoDeskTitle == "")
                    {
                        Response.Write("Please fill in Slider Title!");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    if (Helper.NVL(idm.ProductImagePath) == null || Helper.NVL(idm.ProductImagePath) == "")
                    {
                        Response.Write("Please upload document!");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    //Create New Record
                    int ok;
                    string msg;
                    int ImageID;
                    AdminInfoDeskDB.CreateNewImageSlide(Helper.NVL(idm.ProductImagePath), idm.InfoDeskTitle, Session["Admin"].ToString(), Session["Admin"].ToString(), out ImageID, out ok, out msg);
                }
                else
                {
                    if (idm.InfoDeskTitle == null || idm.InfoDeskTitle == "")
                    {
                        Response.Write("Please fill in Slider Title!");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    if (Helper.NVL(idm.ProductImagePath) == null || Helper.NVL(idm.ProductImagePath) == "")
                    {
                        Response.Write("Please upload document!");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    //Update Existing Record
                    int ok;
                    string msg;
                    AdminInfoDeskDB.UpdateImageSlide(idm.InfoDeskID, Helper.NVL(idm.ProductImagePath), idm.InfoDeskTitle, Session["Admin"].ToString(), out ok, out msg);
                }
                //Session["page"] = "ImageSlider";
                //return RedirectToAction("Home", "Admin");
                return ImageSlider();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteSlide(int? InfoDeskID)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete existing Image Slider
                int ok;
                string msg;
                DataSet dsCorpNews = AdminInfoDeskDB.GetImageSlideByID(InfoDeskID, out ok, out msg);

                {
                    AdminInfoDeskDB.DeleteSlideByID(InfoDeskID, out ok, out msg);
                }

                return ImageSlider();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Category

        public ActionResult CategorySetup(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            PaginationCategorySetupModel model = new PaginationCategorySetupModel();
            DataSet dsCategory = AdminGeneralDB.GetAllCategory(Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsCategory.Tables[0].Rows)
            {
                var csm = new CategorySetupModel();
                csm.CategoryID = Convert.ToInt32(dr["CCAT_ID"]);
                csm.CategoryCode = dr["CCAT_CODE"].ToString();
                csm.CategoryName = dr["CMULTILANGCATEGORY_NAME"].ToString();
                csm.CategoryDescription = dr["CMULTILANGCATEGORY_DESCRIPTION"].ToString();
                model.CategoryList.Add(csm);
            }

            return PartialView("CategorySetup", model);
        }

        public ActionResult CategorySetupData(int CategoryID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var csm = new CategorySetupModel();

            //combobox for category
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            csm.LanguageList = from c in languages
                               select new SelectListItem
                               {
                                   Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                   Text = c.Text,
                                   Value = c.Value
                               };
            csm.SelectedLanguage = Session["LanguageChosen"].ToString();

            if (CategoryID == 0)
            {
                //Create New Category Setup
                csm.CategoryID = null;
                csm.CategoryCode = "";

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    csm.NameLanguageList.Add(tlm);
                }

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    csm.DescriptionLanguageList.Add(tlm);
                }

            }
            else
            {
                //View Existing Category Setup
                DataSet dsCategorySetup = AdminGeneralDB.GetCategoryByID(CategoryID, out ok, out msg);
                if (dsCategorySetup.Tables[0].Rows.Count != 0)
                {
                    csm.CategoryID = Convert.ToInt32(dsCategorySetup.Tables[0].Rows[0]["CCAT_ID"]);
                    csm.CategoryCode = dsCategorySetup.Tables[0].Rows[0]["CCAT_CODE"].ToString();

                    DataSet dsMultiLanguageCategory = AdminGeneralDB.GetMultiLanguageCategoryByCode(csm.CategoryCode, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguageCategory.Tables[0].Select("CCAT_CODE='" + csm.CategoryCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGCATEGORY_NAME"].ToString();
                        csm.NameLanguageList.Add(tlm);
                    }

                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguageCategory.Tables[0].Select("CCAT_CODE='" + csm.CategoryCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGCATEGORY_DESCRIPTION"].ToString();
                        csm.DescriptionLanguageList.Add(tlm);
                    }
                }
            }

            return PartialView("CategorySetupData", csm);
        }

        [HttpPost]
        public ActionResult CategorySetupDataMethod(CategorySetupModel csm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (csm.CategoryID == null || csm.CategoryID == 0)
                {
                    //Create New Record
                    int ok;
                    string msg;
                    AdminGeneralDB.CreateNewCategory(csm.CategoryCode, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                    if (ok == 1)
                    {
                        for (int i = 0; i < csm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageCategory(csm.CategoryCode, csm.NameLanguageList[i].LanguageCode, Helper.NVL(csm.NameLanguageList[i].Text), Helper.NVL(csm.DescriptionLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgDuplicateCatCode);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }

                }
                else
                {
                    //Update Existing Record
                    int ok;
                    string msg;
                    AdminGeneralDB.UpdateCategory(csm.CategoryID, csm.CategoryCode, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguageCategoryByCode(csm.CategoryCode, out ok, out msg);
                        for (int i = 0; i < csm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageCategory(csm.CategoryCode, csm.NameLanguageList[i].LanguageCode, Helper.NVL(csm.NameLanguageList[i].Text), Helper.NVL(csm.DescriptionLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgDuplicateCatCode);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return CategorySetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region PaymentMode

        public ActionResult PaymentModeSetup(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;

            PaginationPaymentModeSetupModel model = new PaginationPaymentModeSetupModel();
            DataSet dsPaymentMode = PaymentModeSetupDB.GetAllPaymentMode(out ok, out msg);

            if (ok == -1)
            {
                Response.Write(string.Format(ECFBase.Resources.Admin.msgFailToRetrievePaymode));
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }


            var pageList = new List<int>();
            for (int z = 1; z <= selectedPage; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsPaymentMode.Tables[0].Rows)
            {
                var pmsm = new PaymentModeSetupModel();
                pmsm.PayModeID = Convert.ToInt32(dr["CPAYMD_ID"]);
                pmsm.PayModeType = dr["CPAYMD_TYPE"].ToString();
                model.CategoryList.Add(pmsm);
            }

            return PartialView("PaymentModeSetup", model);
        }

        public ActionResult PaymentModeSetupData(int payModeId)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            var pmsm = new PaymentModeSetupModel();

            if (payModeId == 0)
            {
                //Create New Category Setup
                pmsm.PayModeID = null;
                pmsm.PayModeType = "";
            }
            else
            {
                //View Existing Category Setup
                string msg;
                DataSet dsPaymentMode = PaymentModeSetupDB.GetPaymentModeByID(payModeId, out ok, out msg);

                if (ok == -1)
                {
                    Response.Write(string.Format(ECFBase.Resources.Admin.msgFailToRetrievePaymode));
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                if (dsPaymentMode.Tables[0].Rows.Count != 0)
                {
                    pmsm.PayModeID = Convert.ToInt32(dsPaymentMode.Tables[0].Rows[0]["CPAYMD_ID"]);
                    pmsm.PayModeType = dsPaymentMode.Tables[0].Rows[0]["CPAYMD_TYPE"].ToString();
                }
            }

            return PartialView("PaymentModeSetupData", pmsm);
        }

        [HttpPost]
        public ActionResult PaymentModeSetupDataMethod(PaymentModeSetupModel pmsm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok;
                string msg;

                if (pmsm.PayModeID == null || pmsm.PayModeID == 0)
                {
                    //Create New Record
                    PaymentModeSetupDB.CreateNewPaymentMode(pmsm.PayModeType, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);

                    if (ok == -1)
                    {
                        Response.Write(string.Format(ECFBase.Resources.Admin.msgPaymodeDuplicated));
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //Update Existing Record
                    PaymentModeSetupDB.UpdatePaymentMode(pmsm.PayModeID, pmsm.PayModeType, Session["Admin"].ToString(), out ok, out msg);

                    if (ok == -1)
                    {
                        Response.Write(string.Format(ECFBase.Resources.Admin.msgInvalidPayModeType));
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    else if (ok == -2)
                    {
                        Response.Write(string.Format(ECFBase.Resources.Admin.msgInvalidInputPayModeParam));
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                //refresh
                return PaymentModeSetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult PaymentModeDeleteMethod(int payModeId)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok;
                string msg;
                PaymentModeSetupDB.DeletePaymentMode(payModeId, Session["Admin"].ToString(), out ok, out msg);
                if (ok == -1)
                {
                    Response.Write(string.Format(ECFBase.Resources.Admin.msgFailToDeletePaymentMode));
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                return PaymentModeSetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion //end of PaymentMode

        #region Product

        //public ActionResult ProductSetup(int selectedPage = 1)
        //{
        //    if (Session["Admin"] == null || Session["Admin"].ToString() == "")
        //    {
        //        return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
        //    }

        //    int ok = 0;
        //    string msg = "";
        //    int pages = 0;
        //    PaginationProductSetupModel model = new PaginationProductSetupModel();

        //    DataSet dsProduct = AdminGeneralDB.GetAllProduct(Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

        //    List<int> pageList = new List<int>();
        //    for (int z = 1; z <= pages; z++)
        //    {
        //        pageList.Add(z);
        //    }

        //    model.Pages = from c in pageList
        //                  select new SelectListItem
        //                  {
        //                      Selected = (c.ToString() == selectedPage.ToString()),
        //                      Text = c.ToString(),
        //                      Value = c.ToString()
        //                  };

        //    foreach (DataRow dr in dsProduct.Tables[0].Rows)
        //    {
        //        var psm = new ProductSetupModel();
        //        psm.ProductID = Convert.ToInt32(dr["CPRD_ID"]);
        //        psm.SelectedCategory = dr["CMULTILANGCATEGORY_NAME"].ToString();
        //        psm.ProductCode = dr["CPRD_CODE"].ToString();
        //        psm.ProductName = dr["PRODNAME"].ToString();
        //        psm.IsBronzePackage = Convert.ToBoolean(dr["CPRD_BRONZE_PKG"]);
        //        model.ProductList.Add(psm);
        //    }

        //    return PartialView("ProductSetup", model);
        //}

        //public ActionResult ProductSetupData(int ProductID)
        //{
        //    if (Session["Admin"] == null || Session["Admin"].ToString() == "")
        //    {
        //        return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
        //    }

        //    int ok = 0;
        //    var message = string.Empty;
        //    var psm = new ProductSetupModel();

        //    var countries = Misc.GetCountryList(Session["LanguageChosen"].ToString(), ref ok, ref message);
        //    psm.CountryList = from c in countries select new SelectListItem { Selected = false, Text = c.Text, Value = c.Value };

        //    //combobox for category
        //    List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref message);
        //    psm.LanguageList = from c in languages
        //                       select new SelectListItem
        //                       {
        //                           Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
        //                           Text = c.Text,
        //                           Value = c.Value
        //                       };
        //    psm.SelectedLanguage = Session["LanguageChosen"].ToString();



        //    List<SelectListItem> RankList = Misc.RankList();
        //    psm.Rank = from c in RankList
        //               select new SelectListItem
        //               {
        //                   Selected = c.Selected,
        //                   Text = c.Text,
        //                   Value = c.Value
        //               };


        //    if (ProductID == 0)
        //    {
        //        //Create New Product Setup
        //        psm.ProductID = null;
        //        psm.ProductCode = "";

        //        psm.ProductURL = "";
        //        psm.ProductPV = 0;

        //        psm.ProductImageName = "";
        //        psm.ProductImagePath = "";

        //        psm.OriPrice = 0;
        //        psm.SellingPrice = 0;

        //        psm.Freight = 0;
        //        psm.SubFreight = 0;

        //        psm.AddtionalRD = 0;
        //        psm.PPUNIT = 0;
        //        psm.ProductQuantity = 0;

        //        psm.IsBronzePackage = false;

        //        foreach (SelectListItem language in languages)
        //        {
        //            var tlm = new TextLanguageModel();
        //            tlm.LanguageCode = language.Value;
        //            tlm.LanguageName = language.Text;
        //            tlm.Text = "";
        //            psm.NameLanguageList.Add(tlm);
        //        }

        //        foreach (SelectListItem language in languages)
        //        {
        //            var tlm = new TextLanguageModel();
        //            tlm.LanguageCode = language.Value;
        //            tlm.LanguageName = language.Text;
        //            tlm.Text = "";
        //            psm.DescriptionLanguageList.Add(tlm);
        //        }
        //    }
        //    else
        //    {
        //        //View Existing Product Setup
        //        string msg;
        //        DataSet dsProductSetup = AdminGeneralDB.GetProductByID(ProductID, out ok, out msg);
        //        if (dsProductSetup.Tables[0].Rows.Count != 0)
        //        {
        //            psm.ProductID = Convert.ToInt32(dsProductSetup.Tables[0].Rows[0]["CPRD_ID"]);
        //            psm.ProductCode = dsProductSetup.Tables[0].Rows[0]["CPRD_CODE"].ToString();
        //            psm.SelectedCategory = dsProductSetup.Tables[0].Rows[0]["CCAT_CODE"].ToString();

        //            psm.ProductURL = dsProductSetup.Tables[0].Rows[0]["CPRD_URL"].ToString();
        //            psm.ProductPV = float.Parse(dsProductSetup.Tables[0].Rows[0]["CPRD_PV"].ToString());
        //            psm.CountryName = dsProductSetup.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

        //            if (dsProductSetup.Tables[0].Rows[0]["CPRD_IMAGEPATH"].ToString() != "")
        //            {
        //                psm.ProductImagePath = dsProductSetup.Tables[0].Rows[0]["CPRD_IMAGEPATH"].ToString();
        //                int extensionIndex = dsProductSetup.Tables[0].Rows[0]["CPRD_IMAGEPATH"].ToString().LastIndexOf('\\');
        //                psm.ProductImageName = dsProductSetup.Tables[0].Rows[0]["CPRD_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
        //            }
        //            else
        //            {
        //                psm.ProductImageName = "";
        //                psm.ProductImagePath = "";
        //            }

        //            psm.OriPrice = float.Parse(dsProductSetup.Tables[0].Rows[0]["CPRD_ORIPRICE"].ToString());
        //            psm.SellingPrice = float.Parse(dsProductSetup.Tables[0].Rows[0]["CPRD_SELLINGPRICE"].ToString());
        //            psm.SelectedRank = dsProductSetup.Tables[0].Rows[0]["CRANK_CODE"].ToString();
        //            psm.Freight = float.Parse(dsProductSetup.Tables[0].Rows[0]["CPRD_FREIGHT"].ToString());
        //            psm.SubFreight = float.Parse(dsProductSetup.Tables[0].Rows[0]["CPRD_SUBFREIGHT"].ToString());
        //            psm.ProductQuantity = Convert.ToInt32(dsProductSetup.Tables[0].Rows[0]["CPRD_QUANTITY"].ToString());
        //            psm.share = float.Parse(dsProductSetup.Tables[0].Rows[0]["CPRD_SHARE"].ToString());
        //            psm.AddtionalRD = float.Parse(dsProductSetup.Tables[0].Rows[0]["CPRD_ADDITIONALRD"].ToString());
        //            psm.PPUNIT = Convert.ToInt32(dsProductSetup.Tables[0].Rows[0]["CPRD_PPUNIT"].ToString());

        //            psm.IsBronzePackage = Convert.ToBoolean(dsProductSetup.Tables[0].Rows[0]["CPRD_BRONZE_PKG"]);

        //            foreach (SelectListItem language in languages)
        //            {
        //                DataSet dsMultiLanguageProduct = AdminGeneralDB.GetMultiLanguageProductByCode(psm.ProductCode, language.Value, out ok, out msg);
        //                string name = dsMultiLanguageProduct.Tables[0].Rows[0][2].ToString();
        //                string description = dsMultiLanguageProduct.Tables[0].Rows[0][3].ToString();

        //                var tlm2 = new TextLanguageModel();
        //                tlm2.LanguageCode = language.Value;
        //                tlm2.LanguageName = language.Text;
        //                tlm2.Text = name;
        //                psm.NameLanguageList.Add(tlm2);

        //                var tlm = new TextLanguageModel();
        //                tlm.LanguageCode = language.Value;
        //                tlm.LanguageName = language.Text;
        //                tlm.Text = description;
        //                psm.DescriptionLanguageList.Add(tlm);
        //            }
        //        }
        //    }

        //    //combobox for category
        //    List<CategoryModel> categories = Misc.GetAllCategoryList(psm.SelectedLanguage, ref ok, ref message);
        //    psm.CategoryList = from c in categories
        //                       select new SelectListItem
        //                       {
        //                           Selected = false,
        //                           Text = c.CategoryName,
        //                           Value = c.CategoryCode
        //                       };

        //    return PartialView("ProductSetupData", psm);
        //}

        //[HttpPost]
        //public ActionResult ProductSetupDataMethod(ProductSetupModel psm)
        //{
        //    try
        //    {
        //        if (Session["Admin"] == null || Session["Admin"].ToString() == "")
        //        {
        //            return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
        //        }

        //        psm.ProductImage = Request.Files["ProductImage"];
        //        if (psm.ProductImage != null && psm.ProductImage.FileName != string.Empty)
        //        {
        //            string basePath = Server.MapPath("~/Products/" + psm.ProductCode);
        //            if (Directory.Exists(basePath) == false)
        //            {
        //                Directory.CreateDirectory(basePath);
        //            }
        //            psm.ProductImagePath = basePath + "\\" + psm.ProductImage.FileName;
        //            psm.ProductImage.SaveAs(psm.ProductImagePath);
        //        }

        //        psm.OriPrice = 0;
        //        psm.SellingPrice = 0;
        //        psm.Freight = 0;
        //        psm.SubFreight = 0;
        //        psm.ProductQuantity = 0;
        //        psm.ProductPV = 0;

        //        if (psm.ProductID == null || psm.ProductID == 0)
        //        {
        //            Regex ex = new Regex(@"^[a-zA-Z0-9]{1,10}$");

        //            if (!ex.IsMatch(psm.ProductCode))
        //            {
        //                Response.Write("1|" + Resources.Admin.msgProdCodeError);
        //                return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //            }

        //            //Create New Record
        //            int ok;
        //            string msg;

        //            AdminGeneralDB.CreateNewProduct(psm.ProductCode, 
        //                                            psm.SelectedCategory, 
        //                                            Helper.NVL(psm.ProductURL), 
        //                                            psm.ProductPV, 
        //                                            Helper.NVL(psm.ProductImagePath), 
        //                                            psm.OriPrice, 
        //                                            psm.SellingPrice, 
        //                                            psm.Freight, 
        //                                            psm.SubFreight, 
        //                                            psm.ProductQuantity, 
        //                                            Session["Admin"].ToString(), 
        //                                            Session["Admin"].ToString(), 
        //                                            psm.CountryName,
        //                                            psm.IsBronzePackage,
        //                                            0,
        //                                            psm.SelectedRank,
        //                                            psm.share,
        //                                            psm.PPUNIT,
        //                                            out ok, 
        //                                            out msg);
        //            if (ok == 1)
        //            {
        //                for (int i = 0; i < psm.NameLanguageList.Count; i++)
        //                {
        //                    AdminGeneralDB.CreateNewMultiLanguageProduct(psm.ProductCode, psm.NameLanguageList[i].LanguageCode, Helper.NVL(psm.NameLanguageList[i].Text), Helper.NVL(psm.DescriptionLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
        //                }
        //            }
        //            if (ok == 0)
        //            {
        //                if (msg == "-1")
        //                {
        //                    Response.Write("1|" + Resources.Admin.msgDuplicateProdCode);
        //                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            //Update Existing Record
        //            int ok;
        //            string msg;
        //            AdminGeneralDB.UpdateProduct(psm.ProductID, 
        //                psm.ProductCode, 
        //                psm.SelectedCategory, 
        //                Helper.NVL(psm.ProductURL),
        //                psm.ProductPV, 
        //                Helper.NVL(psm.ProductImagePath), 
        //                Helper.NVLInteger(psm.OriPrice),
        //                Helper.NVLInteger(psm.SellingPrice), 
        //                Helper.NVLInteger(psm.Freight), 
        //                Helper.NVLInteger(psm.SubFreight), 
        //                Helper.NVLInteger(psm.ProductQuantity), 
        //                Helper.NVLInteger(psm.IncreaseProductQuantity), 
        //                psm.CountryName,
        //                psm.PPUNIT,
        //                psm.IsBronzePackage,
        //                0, 
        //                psm.SelectedRank,
        //                psm.share,
        //                out ok, out msg);
        //            if (ok == 1)
        //            {
        //                for (int i = 0; i < psm.NameLanguageList.Count; i++)
        //                {
        //                    AdminGeneralDB.DeleteMultiLanguageProductByCode(psm.ProductCode, psm.NameLanguageList[i].LanguageCode, out ok, out msg);
        //                }
        //                for (int i = 0; i < psm.NameLanguageList.Count; i++)
        //                {
        //                    AdminGeneralDB.CreateNewMultiLanguageProduct(psm.ProductCode, psm.NameLanguageList[i].LanguageCode, Helper.NVL(psm.NameLanguageList[i].Text), Helper.NVL(psm.DescriptionLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
        //                }
        //            }
        //            if (ok == 0)
        //            {
        //                if (msg == "-1")
        //                {
        //                    Response.Write("1|" + Resources.Admin.msgDuplicateProdCode);
        //                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //        }

        //        Session["page"] = "ProductSetup";
        //        return RedirectToAction("Home", "Admin");
        //    }
        //    catch (Exception e)
        //    {
        //        Response.Write("1|" + e.Message);
        //        return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //    }
        //}

        #endregion

        #region Registration Package

        [HttpPost]
        public JsonResult FindProduct(string CategoryCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetProductList(LanguageCode, CategoryCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetProductList(string LanguageCode, string CategoryCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<ProductModel> products = Misc.GetAllProductByCategory(LanguageCode, CategoryCode, ref ok, ref message);

            var result = from c in products
                         select new SelectListItem
                         {
                             Selected = false,
                             Text = c.ProductName,
                             Value = c.ProductCode
                         };
            return result.OrderBy(m => m.Value).ToList();
        }

        public ActionResult RegistrationPackageSetup(int selectedPage = 1, string selectedCountryCode = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            string LanguageCode = Session["LanguageChosen"].ToString();
            int ok;
            string msg;
            int pages = 0;
            PaginationPackageSetupModel model = new PaginationPackageSetupModel();
            DataSet dsPackage = AdminGeneralDB.GetAllPackage(LanguageCode, "R", selectedPage, out pages, out ok, out msg);

            var allRank = Misc.GetRankList(1, Session["LanguageChosen"].ToString(), ref ok, ref msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            if (pages == 0)
            {
                pageList.Add(0);
            }
            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsPackage.Tables[0].Rows)
            {
                var psm = new PackageSetupModel();
                psm.PackageID = Convert.ToInt32(dr["CPKG_ID"]);
                psm.PackageCode = dr["CPKG_CODE"].ToString();
                psm.PackageName = dr["CMULTILANGPACKAGE_NAME"].ToString();
                psm.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                psm.Account = dr["CPKG_ACCQTY"].ToString();
                psm.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();
                psm.ValidityStart = DateTime.Parse(dr["CPKG_VALIDITY_START"].ToString()).ToString("dd-MM-yyyy");
                psm.ValidityEnd = DateTime.Parse(dr["CPKG_VALIDITY_END"].ToString()).ToString("dd-MM-yyyy");
                psm.PackagePV = float.Parse(dr["CPKG_BV"].ToString());
                psm.TempPackageInvestment = psm.PackageInvestment = float.Parse(dr["CPKG_AMOUNT"].ToString());
                psm.TotalCharge = psm.PackageInvestment;
                psm.ROIMaxAmount = float.Parse(dr["CPKG_ROIMAXAMOUNT"].ToString()).ToString("n2");
                psm.ROIPoint = float.Parse(dr["CPKG_ROIPOINT"].ToString()).ToString("n2");
                var ranking = allRank.First(c => c.Value == Helper.NVL(dr["CRANKSET_CODE"]));
                psm.RankName = ranking == null ? Helper.NVL(dr["CRANKSET_CODE"]) : ranking.Text;
                model.PackageList.Add(psm);
            }


            DataSet dsCountry = AdminGeneralDB.GetAllCountryList(Session["LanguageChosen"].ToString(), out ok, out msg);
            var countries = new List<CountrySetupModel>();
            //var countries = Misc.GetCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);
            CountrySetupModel country = new CountrySetupModel();
            country.CountryCode = "0";
            country.CountryName = "ALL";
            country.MobileCode = "0";
            countries.Add(country);


            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                var csm = new CountrySetupModel();
                csm.CountryId = Convert.ToInt32(dr["CCOUNTRY_ID"]);
                csm.CountryCode = dr["CCOUNTRY_CODE"].ToString();
                csm.CountryName = dr["COUNTRYNAME"].ToString();
                countries.Add(csm);
            }
            //List<CountrySetupModel> countries = Misc.GetAllCountryID("en-US");

            model.CountryList = from c in countries
                                select new SelectListItem
                                {
                                    Text = c.CountryName.ToString(),
                                    Value = c.CountryCode.ToString()
                                };
            model.SelectedCountry = selectedCountryCode;

            return PartialView("RegistrationPackageSetup", model);
        }

        public ActionResult GetPackageByCountry(int selectedPage = 1, string selectedCountryCode = "")
        {
            string LanguageCode = Session["LanguageChosen"].ToString();
            int ok;
            string msg;
            int pages = 0;
            PaginationPackageSetupModel model = new PaginationPackageSetupModel();

            DataSet dsPackage = AdminGeneralDB.GetAllPackage(LanguageCode, "R", selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };


            var allRank = Misc.GetRankList(1, Session["LanguageChosen"].ToString(), ref ok, ref msg);
            foreach (DataRow dr in dsPackage.Tables[0].Rows)
            {
                var psm = new PackageSetupModel();
                if (dr["CCOUNTRY_CODE"].ToString() == selectedCountryCode)
                {
                    psm.PackageID = Convert.ToInt32(dr["CPKG_ID"]);
                    psm.PackageCode = dr["CPKG_CODE"].ToString();
                    psm.PackageName = dr["CMULTILANGPACKAGE_NAME"].ToString();
                    psm.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                    psm.Account = dr["CPKG_ACCQTY"].ToString();
                    psm.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();
                    psm.ValidityStart = DateTime.Parse(dr["CPKG_VALIDITY_START"].ToString()).ToString("dd-MM-yyyy");
                    psm.ValidityEnd = DateTime.Parse(dr["CPKG_VALIDITY_END"].ToString()).ToString("dd-MM-yyyy");
                    psm.PackagePV = float.Parse(dr["CPKG_BV"].ToString());
                    psm.TempPackageInvestment = psm.PackageInvestment = float.Parse(dr["CPKG_AMOUNT"].ToString());
                    psm.TotalCharge = psm.PackageInvestment;
                    psm.ShareSplit = float.Parse(dr["CPKG_SHARESPLIT"].ToString());
                    psm.ROIMaxAmount = float.Parse(dr["CPKG_ROIMAXAMOUNT"].ToString()).ToString("n2");
                    psm.ROIPoint = float.Parse(dr["CPKG_ROIPOINT"].ToString()).ToString("n2");
                    var ranking = allRank.First(c => c.Value == Helper.NVL(dr["CRANKSET_CODE"]));
                    psm.RankName = ranking == null ? Helper.NVL(dr["CRANKSET_CODE"]) : ranking.Text;
                    model.PackageList.Add(psm);

                }
                else if (selectedCountryCode == "0")
                { 
                    psm.PackageID = Convert.ToInt32(dr["CPKG_ID"]);
                    psm.PackageCode = dr["CPKG_CODE"].ToString();
                    psm.PackageName = dr["CMULTILANGPACKAGE_NAME"].ToString();
                    psm.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                    psm.Account = dr["CPKG_ACCQTY"].ToString();
                    psm.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();
                    psm.ValidityStart = DateTime.Parse(dr["CPKG_VALIDITY_START"].ToString()).ToString("dd-MM-yyyy");
                    psm.ValidityEnd = DateTime.Parse(dr["CPKG_VALIDITY_END"].ToString()).ToString("dd-MM-yyyy");
                    psm.PackagePV = float.Parse(dr["CPKG_BV"].ToString());
                    psm.TempPackageInvestment = psm.PackageInvestment = float.Parse(dr["CPKG_AMOUNT"].ToString());
                    psm.TotalCharge = psm.PackageInvestment;
                    psm.ROIMaxAmount = float.Parse(dr["CPKG_ROIMAXAMOUNT"].ToString()).ToString("n2");
                    psm.ROIPoint = float.Parse(dr["CPKG_ROIPOINT"].ToString()).ToString("n2");
                    var ranking = allRank.First(c => c.Value == Helper.NVL(dr["CRANKSET_CODE"]));
                    psm.RankName = ranking == null ? Helper.NVL(dr["CRANKSET_CODE"]) : ranking.Text;
                    model.PackageList.Add(psm);
                }


                DataSet dsCountry = AdminGeneralDB.GetAllCountryList(Session["LanguageChosen"].ToString(), out ok, out msg);
                var countries = new List<CountrySetupModel>();

                CountrySetupModel country = new CountrySetupModel();
                country.CountryCode = "0";
                country.CountryName = "ALL";
                country.MobileCode = "0";
                countries.Add(country);


                foreach (DataRow ds in dsCountry.Tables[0].Rows)
                {
                    var csm = new CountrySetupModel();
                    csm.CountryId = Convert.ToInt32(ds["CCOUNTRY_ID"]);
                    csm.CountryCode = ds["CCOUNTRY_CODE"].ToString();
                    csm.CountryName = ds["COUNTRYNAME"].ToString();
                    countries.Add(csm);
                }


                model.CountryList = from c in countries
                                    select new SelectListItem
                                    {
                                        Text = c.CountryName.ToString(),
                                        Value = c.CountryCode.ToString()
                                    };
                model.SelectedCountry = selectedCountryCode;
            }

            return PartialView("RegistrationPackageSetup", model);
        }

        public ActionResult RegistrationPackageSetupData(int PackageID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            var psm = new PackageSetupModel();
            string msg = "";

            //combobox for language
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            psm.LanguageList = from c in languages select new SelectListItem { Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false, Text = c.Text, Value = c.Value };
            psm.SelectedLanguage = Session["LanguageChosen"].ToString();

            var months = new List<SelectListItem>();
            for (int z = 0; z < 12; z++)
            {
                SelectListItem month = new SelectListItem();
                month.Text = z.ToString();
                month.Value = z.ToString();
                month.Selected = false;
                months.Add(month);
            }

            psm.EntitledMonthList = from c in months select new SelectListItem { Selected = false, Text = c.Text, Value = c.Value };
            var countries = Misc.GetCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);
            psm.CountryList = from c in countries select new SelectListItem { Selected = false, Text = c.Text, Value = c.Value };

            var allRank = Misc.GetRankList(1, Session["LanguageChosen"].ToString(), ref ok, ref msg);
            psm.RankList = from c in allRank select new SelectListItem { Selected = false, Text = c.Text, Value = c.Value };

            var accounts = Misc.GetMultipleAccountChoice();
            psm.MultipleAccountList = from c in accounts select new SelectListItem { Selected = false, Text = c.Text, Value = c.Value };

            var currencies = Misc.GetCurrencyList(ref ok, ref msg);
            foreach (var currency in currencies)
            {
                var newCurrency = new CurrencyModel { CurrencyName = currency.Text, CountryCode = currency.Value };
                psm.CurrencyList.Add(newCurrency);
            }

            if (PackageID == 0)
            {
                // Create New Package Setup
                psm.PackageID = null;
                psm.PackageCode = "";
                psm.CountryName = psm.CountryList.First().Value;
                psm.RankName = psm.RankList.First().Value;
                psm.PackageInvestment = 0;
                psm.PackagePV = 0;
                psm.DeliveryCharge = 0;
                psm.TotalCharge = 0;
                psm.SelectedBonusEntitledMonth = 0;
                psm.MaxPairing = 0;
                psm.ValidityStart = DateTime.Now.ToString("dd-MM-yyyy");
                psm.ValidityEnd = DateTime.Now.ToString("dd-MM-yyyy");
                psm.ProductImageName = "";
                psm.ProductImagePath = "";
                psm.SelectedAccountNo = "1";
                psm.RefEUnit = 0;
                psm.Eunit = 0;
                psm.EunitB = 0;
                psm.ShareSplit = 0;
                psm.GoldMindDays = "0";
                psm.GoldMindPercentage = "0";
                psm.ROIMaxAmount = "0";
                psm.ROIPoint = "0";

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    psm.NameLanguageList.Add(tlm);
                }

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    psm.DescriptionLanguageList.Add(tlm);
                }
            }
            else
            {
                //View Existing Package Setup
                DataSet dsPackageSetup = AdminGeneralDB.GetPackageByID("R", PackageID, out ok, out msg);
                if (dsPackageSetup.Tables[0].Rows.Count != 0)
                {
                    psm.PackageID = Convert.ToInt32(dsPackageSetup.Tables[0].Rows[0]["CPKG_ID"]);
                    psm.PackageCode = dsPackageSetup.Tables[0].Rows[0]["CPKG_CODE"].ToString();
                    psm.CountryName = dsPackageSetup.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                    psm.TempPackageInvestment = psm.PackageInvestment = float.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
                    psm.PackagePV = float.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_BV"].ToString());
                    psm.ValidityStart = DateTime.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_VALIDITY_START"].ToString()).ToString("dd-MM-yyyy");
                    psm.ValidityEnd = DateTime.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_VALIDITY_END"].ToString()).ToString("dd-MM-yyyy");
                    psm.TotalCharge = psm.PackageInvestment;
                    psm.TotalSetOfProduct = int.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_PRODUCTQTY"].ToString());
                    psm.DisplaySequence = int.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_SEQUENCE"].ToString());
                    psm.SelectedAccountNo = dsPackageSetup.Tables[0].Rows[0]["CPKG_ACCQTY"].ToString();
                    var rankCode = Helper.NVL(dsPackageSetup.Tables[0].Rows[0]["CRANKSET_CODE"]);
                    psm.RankName = rankCode;

                    if (dsPackageSetup.Tables[0].Rows[0]["CPRG_IMAGEPATH"].ToString() != "")
                    {
                        psm.ProductImagePath = dsPackageSetup.Tables[0].Rows[0]["CPRG_IMAGEPATH"].ToString();
                        int extensionIndex = dsPackageSetup.Tables[0].Rows[0]["CPRG_IMAGEPATH"].ToString().LastIndexOf('\\');
                        psm.ProductImageName = dsPackageSetup.Tables[0].Rows[0]["CPRG_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                    }
                    else
                    {
                        psm.ProductImageName = "";
                        psm.ProductImagePath = "";
                    }

                    DataSet dsMultiLanguagePackage = AdminGeneralDB.GetMultiLanguagePackageByCode(psm.PackageCode, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguagePackage.Tables[0].Select("CPKG_CODE='" + psm.PackageCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGPACKAGE_NAME"].ToString();
                        psm.NameLanguageList.Add(tlm);
                    }

                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguagePackage.Tables[0].Select("CPKG_CODE='" + psm.PackageCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGPACKAGE_DESCRIPTION"].ToString();
                        psm.DescriptionLanguageList.Add(tlm);
                    }

                }
            }

            //combobox for category
            List<CategoryModel> categories = Misc.GetAllCategoryList(psm.SelectedLanguage, ref ok, ref msg);
            psm.CategoryList = from c in categories select new SelectListItem { Selected = false, Text = c.CategoryName, Value = c.CategoryCode };

            //combobox for product
            //string categoryCode = ((CategoryModel)categories.First()).CategoryCode.ToString();
            //List<ProductModel> products = Misc.GetAllProductByCategory(psm.SelectedLanguage, categoryCode, ref ok, ref msg);
            //psm.ProdList = from c in products select new SelectListItem { Selected = false, Text = c.ProductName, Value = c.ProductCode };

            return PartialView("RegistrationPackageSetupData", psm);
        }

        [HttpPost]
        public ActionResult RegistrationPackageSetupDataMethod(PackageSetupModel psm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                psm.ProductImage = Request.Files["ProductImage"];
                if (psm.ProductImage != null && psm.ProductImage.FileName != string.Empty)
                {
                    string basePath = Server.MapPath("~/Packages/" + psm.PackageCode);
                    if (Directory.Exists(basePath) == false)
                    {
                        Directory.CreateDirectory(basePath);
                    }
                    var filename = Path.GetFileName(psm.ProductImage.FileName);
                    if (psm.ProductImage.FileName.Contains(" "))
                    {
                        Response.Write("Please re-upload image without space on image name");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    //psm.ProductImagePath = basePath + "\\" + psm.ProductImage.FileName;
                    psm.ProductImagePath = basePath + "\\" + filename;
                    psm.ProductImage.SaveAs(psm.ProductImagePath);
                }

                int ok = 0;
                string msg = "";



                DateTime dtEnd = DateTime.ParseExact(psm.ValidityEnd, "dd-MM-yyyy", null);
                dtEnd = new DateTime(dtEnd.Year, dtEnd.Month, dtEnd.Day, 23, 59, 59);

                psm.ValidityEnd = dtEnd.ToString("yyyy-MM-dd");

                DateTime dtStart = DateTime.ParseExact(psm.ValidityStart, "dd-MM-yyyy", null);
                psm.ValidityStart = dtStart.ToString("yyyy-MM-dd");


                if (psm.PackageID == null || psm.PackageID == 0)
                {

                    if (psm.PackageCode == null || psm.PackageCode == "")
                    {
                        Response.Write("Please Keyin Package Code");
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    if (psm.CountryName == null || psm.CountryName == "")
                    {
                        psm.CountryName = "";
                    }
                    if (psm.TempPackageInvestment == 0)
                    {
                        psm.TempPackageInvestment = 0;
                    }
                    if (psm.PackagePV == 0)
                    {
                        psm.PackagePV = 0;
                    }
                    if (psm.GoldMindDays == null || psm.GoldMindDays == "0")
                    {
                        psm.GoldMindDays = "0";
                    }
                    psm.ROIMaxAmount = "0";
                    psm.ROIPoint = "0";
                    psm.Eunit = 0;
                    //Create New Record
                    psm.Validity = "1";
                    AdminGeneralDB.CreateNewPackage("R", psm, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                    if (ok == 1)
                    {
                        for (int i = 0; i < psm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguagePackage(psm.PackageCode, psm.NameLanguageList[i].LanguageCode, Helper.NVL(psm.NameLanguageList[i].Text), Helper.NVL(psm.DescriptionLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    else
                    {
                        
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgDuplicatePkgCode);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    if (psm.PackageCode == null || psm.PackageCode == "")
                    {
                        psm.PackageCode = "";
                    }
                    if (psm.CountryName == null || psm.CountryName == "")
                    {
                        psm.CountryName = "";
                    }
                    if (psm.TempPackageInvestment == 0)
                    {
                        psm.TempPackageInvestment = 0;
                    }
                    if (psm.PackagePV == 0)
                    {
                        psm.PackagePV = 0;
                    }
                    if (psm.GoldMindDays == null || psm.GoldMindDays == "0")
                    {
                        psm.GoldMindDays = "0";
                    }
                    if (psm.GoldMindPercentage == null || psm.GoldMindPercentage == "0")
                    {
                        psm.GoldMindPercentage = "0";
                    }
                    if (psm.DisplaySequence == 0)
                    {
                        psm.DisplaySequence = 0;
                    }
                    if (psm.ROIMaxAmount == null || psm.ROIMaxAmount == "0")
                    {
                        psm.ROIMaxAmount = "0";
                    }
                    if (psm.ROIPoint == null || psm.ROIPoint == "0")
                    {
                        psm.ROIPoint = "0";
                    }
                    //Update Existing Record
                    psm.Validity = "1";
                    AdminGeneralDB.UpdatePackage(psm.PackageID, "R", psm, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguagePackageByCode(psm.PackageCode, out ok, out msg);
                        for (int i = 0; i < psm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguagePackage(psm.PackageCode, psm.NameLanguageList[i].LanguageCode, Helper.NVL(psm.NameLanguageList[i].Text), Helper.NVL(psm.DescriptionLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }

                    }
                    else
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgDuplicatePkgCode);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                //Session["page"] = "RegistrationPackageSetup";
                //return RedirectToAction("Home", "Admin");
                return RegistrationPackageSetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Expired Package
        public ActionResult ExpiredPackageSetup(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            string LanguageCode = Session["LanguageChosen"].ToString();
            int ok;
            string msg;
            int pages = 0;
            PaginationPackageSetupModel model = new PaginationPackageSetupModel();
            DataSet dsPackage = AdminGeneralDB.GetAllExpiredPackage(LanguageCode, "R", selectedPage, out pages, out ok, out msg);

            var allRank = Misc.GetRankList(1, Session["LanguageChosen"].ToString(), ref ok, ref msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            if (pages == 0)
            {
                pageList.Add(1);
            }
            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsPackage.Tables[0].Rows)
            {
                var psm = new PackageSetupModel();
                psm.PackageID = Convert.ToInt32(dr["CPKG_ID"]);
                psm.PackageCode = dr["CPKG_CODE"].ToString();
                psm.PackageName = dr["CMULTILANGPACKAGE_NAME"].ToString();
                psm.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                psm.Account = dr["CPKG_ACCQTY"].ToString();
                psm.PackageDescription = dr["CMULTILANGPACKAGE_DESCRIPTION"].ToString();
                psm.ValidityStart = DateTime.Parse(dr["CPKG_VALIDITY_START"].ToString()).ToString("dd-MM-yyyy");
                psm.ValidityEnd = DateTime.Parse(dr["CPKG_VALIDITY_END"].ToString()).ToString("dd-MM-yyyy");
                psm.PackagePV = float.Parse(dr["CPKG_BV"].ToString());
                psm.TempPackageInvestment = psm.PackageInvestment = float.Parse(dr["CPKG_AMOUNT"].ToString());
                psm.TotalCharge = psm.PackageInvestment;
                //psm.CompanyPoint = float.Parse(dr["CPKG_COMPANYPOINT"].ToString());
                psm.ShareSplit = float.Parse(dr["CPKG_SHARESPLIT"].ToString());
                //psm.OCTUNIT = float.Parse(dr["CPKG_OCTUNIT"].ToString());
                psm.Eunit = float.Parse(dr["CPKG_EUNIT"].ToString());
                psm.EunitB = float.Parse(dr["CPKG_EUNITB"].ToString());
                var ranking = allRank.First(c => c.Value == Helper.NVL(dr["CRANKSET_CODE"]));
                psm.RankName = ranking == null ? Helper.NVL(dr["CRANKSET_CODE"]) : ranking.Text;
                model.PackageList.Add(psm);
            }

            return PartialView("ExpiredPackageSetup", model);
        }

        public ActionResult ExpiredPackageSetupData(int PackageID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            var psm = new PackageSetupModel();
            string msg = "";

            //combobox for language
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            psm.LanguageList = from c in languages select new SelectListItem { Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false, Text = c.Text, Value = c.Value };
            psm.SelectedLanguage = Session["LanguageChosen"].ToString();

            var months = new List<SelectListItem>();
            for (int z = 0; z < 12; z++)
            {
                SelectListItem month = new SelectListItem();
                month.Text = z.ToString();
                month.Value = z.ToString();
                month.Selected = false;
                months.Add(month);
            }

            psm.EntitledMonthList = from c in months select new SelectListItem { Selected = false, Text = c.Text, Value = c.Value };
            var countries = Misc.GetCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);
            psm.CountryList = from c in countries select new SelectListItem { Selected = false, Text = c.Text, Value = c.Value };

            var allRank = Misc.GetRankList(1, Session["LanguageChosen"].ToString(), ref ok, ref msg);
            psm.RankList = from c in allRank select new SelectListItem { Selected = false, Text = c.Text, Value = c.Value };

            var accounts = Misc.GetMultipleAccountChoice();
            psm.MultipleAccountList = from c in accounts select new SelectListItem { Selected = false, Text = c.Text, Value = c.Value };

            var currencies = Misc.GetCurrencyList(ref ok, ref msg);
            foreach (var currency in currencies)
            {
                var newCurrency = new CurrencyModel { CurrencyName = currency.Text, CountryCode = currency.Value };
                psm.CurrencyList.Add(newCurrency);
            }

            if (PackageID == 0)
            {
                // Create New Package Setup
                psm.PackageID = null;
                psm.PackageCode = "";
                psm.CountryName = psm.CountryList.First().Value;
                psm.RankName = psm.RankList.First().Value;
                psm.PackageInvestment = 0;
                psm.PackagePV = 0;
                psm.DeliveryCharge = 0;
                psm.TotalCharge = 0;
                psm.SelectedBonusEntitledMonth = 0;
                psm.MaxPairing = 0;
                psm.ValidityStart = DateTime.Now.ToString("dd-MM-yyyy");
                psm.ValidityEnd = DateTime.Now.ToString("dd-MM-yyyy");
                psm.ProductImageName = "";
                psm.ProductImagePath = "";
                psm.SelectedAccountNo = "1";
                psm.RefEUnit = 0;
                //psm.Eunit = 0;
                //psm.Referral = 0;
                //psm.Pairing = 0;
                //psm.MaxWeek = 0;
                //psm.PairingEUnit = 0;
                psm.ROIMaxAmount = "0";
                psm.ROIPoint = "0";

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    psm.NameLanguageList.Add(tlm);
                }

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = "";
                    psm.DescriptionLanguageList.Add(tlm);
                }
            }
            else
            {
                //View Existing Package Setup
                DataSet dsPackageSetup = AdminGeneralDB.GetExpiredPackageByID("R", PackageID, out ok, out msg);
                if (dsPackageSetup.Tables[0].Rows.Count != 0)
                {
                    psm.PackageID = Convert.ToInt32(dsPackageSetup.Tables[0].Rows[0]["CPKG_ID"]);
                    psm.PackageCode = dsPackageSetup.Tables[0].Rows[0]["CPKG_CODE"].ToString();
                    psm.CountryName = dsPackageSetup.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                    psm.TempPackageInvestment = psm.PackageInvestment = float.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_AMOUNT"].ToString());
                    psm.PackagePV = float.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_BV"].ToString());
                    psm.ValidityStart = DateTime.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_VALIDITY_START"].ToString()).ToString("dd-MM-yyyy");
                    psm.ValidityEnd = DateTime.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_VALIDITY_END"].ToString()).ToString("dd-MM-yyyy");
                    psm.TotalCharge = psm.PackageInvestment;
                    psm.TotalSetOfProduct = int.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_PRODUCTQTY"].ToString());
                    psm.DisplaySequence = int.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_SEQUENCE"].ToString());
                    psm.SelectedAccountNo = dsPackageSetup.Tables[0].Rows[0]["CPKG_ACCQTY"].ToString();
                    var rankCode = Helper.NVL(dsPackageSetup.Tables[0].Rows[0]["CRANKSET_CODE"]);
                    //psm.ShareSplit = float.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_SHARESPLIT"].ToString());
                    //psm.GoldMindPercentage = dsPackageSetup.Tables[0].Rows[0]["CPKG_GOLDMIND_PERCENTAGE"].ToString();
                    //psm.GoldMindDays = dsPackageSetup.Tables[0].Rows[0]["CPKG_GOLDMIND_DAYS"].ToString();
                    psm.RankName = rankCode;
                    //psm.Eunit = float.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_EUNIT"].ToString());
                    //psm.EunitB = float.Parse(dsPackageSetup.Tables[0].Rows[0]["CPKG_EUNITB"].ToString());
                    //psm.ROIMaxAmount = dsPackageSetup.Tables[0].Rows[0]["CPKG_ROIMAXAMOUNT"].ToString();
                    //psm.ROIPoint = dsPackageSetup.Tables[0].Rows[0]["CPKG_ROIPOINT"].ToString();

                    if (dsPackageSetup.Tables[0].Rows[0]["CPRG_IMAGEPATH"].ToString() != "")
                    {
                        psm.ProductImagePath = dsPackageSetup.Tables[0].Rows[0]["CPRG_IMAGEPATH"].ToString();
                        int extensionIndex = dsPackageSetup.Tables[0].Rows[0]["CPRG_IMAGEPATH"].ToString().LastIndexOf('\\');
                        psm.ProductImageName = dsPackageSetup.Tables[0].Rows[0]["CPRG_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                    }
                    else
                    {
                        psm.ProductImageName = "";
                        psm.ProductImagePath = "";
                    }

                    DataSet dsMultiLanguagePackage = AdminGeneralDB.GetMultiLanguagePackageByCode(psm.PackageCode, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguagePackage.Tables[0].Select("CPKG_CODE='" + psm.PackageCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGPACKAGE_NAME"].ToString();
                        psm.NameLanguageList.Add(tlm);
                    }

                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiLanguagePackage.Tables[0].Select("CPKG_CODE='" + psm.PackageCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr[0]["CMULTILANGPACKAGE_DESCRIPTION"].ToString();
                        psm.DescriptionLanguageList.Add(tlm);
                    }


                }
            }

            //combobox for category
            List<CategoryModel> categories = Misc.GetAllCategoryList(psm.SelectedLanguage, ref ok, ref msg);
            psm.CategoryList = from c in categories select new SelectListItem { Selected = false, Text = c.CategoryName, Value = c.CategoryCode };

            //combobox for product
            string categoryCode = ((CategoryModel)categories.First()).CategoryCode.ToString();
            List<ProductModel> products = Misc.GetAllProductByCategory(psm.SelectedLanguage, categoryCode, ref ok, ref msg);
            psm.ProdList = from c in products select new SelectListItem { Selected = false, Text = c.ProductName, Value = c.ProductCode };

            return PartialView("ExpiredPackageSetupData", psm);
        }

        [HttpPost]
        public ActionResult ExpiredPackageSetupDataMethod(PackageSetupModel psm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                psm.ProductImage = Request.Files["ProductImage"];
                if (psm.ProductImage != null && psm.ProductImage.FileName != string.Empty)
                {
                    string basePath = Server.MapPath("~/Packages/" + psm.PackageCode);
                    if (Directory.Exists(basePath) == false)
                    {
                        Directory.CreateDirectory(basePath);
                    }
                    psm.ProductImagePath = basePath + "\\" + psm.ProductImage.FileName;
                    psm.ProductImage.SaveAs(psm.ProductImagePath);
                }

                int ok = 0;
                string msg = "";


                DateTime dtEnd = DateTime.ParseExact(psm.ValidityEnd, "dd-MM-yyyy", null);
                dtEnd = new DateTime(dtEnd.Year, dtEnd.Month, dtEnd.Day, 23, 59, 59);

                psm.ValidityEnd = dtEnd.ToString("yyyy-MM-dd");

                DateTime dtStart = DateTime.ParseExact(psm.ValidityStart, "dd-MM-yyyy", null);
                psm.ValidityStart = dtStart.ToString("yyyy-MM-dd");

                if (psm.PackageID == null || psm.PackageID == 0)
                {
                    //Create New Record
                    psm.Validity = "1";
                    AdminGeneralDB.CreateNewPackage("R", psm, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                    if (ok == 1)
                    {
                        for (int i = 0; i < psm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguagePackage(psm.PackageCode, psm.NameLanguageList[i].LanguageCode, Helper.NVL(psm.NameLanguageList[i].Text), Helper.NVL(psm.DescriptionLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    else
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgDuplicatePkgCode);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    //Update Existing Record
                    psm.Validity = "1";
                    AdminGeneralDB.UpdatePackage(psm.PackageID, "R", psm, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguagePackageByCode(psm.PackageCode, out ok, out msg);
                        for (int i = 0; i < psm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguagePackage(psm.PackageCode, psm.NameLanguageList[i].LanguageCode, Helper.NVL(psm.NameLanguageList[i].Text), Helper.NVL(psm.DescriptionLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }

                    }
                    else
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgDuplicatePkgCode);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                //Session["page"] = "ExpiredPackageSetup";
                //return RedirectToAction("Home", "Admin");

                return ExpiredPackageSetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Language

        public ActionResult LanguageSetup(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            var model = new PaginationLanguageSetupModel();
            var dsLanguage = AdminGeneralDB.GetAllLanguages(selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsLanguage.Tables[0].Rows)
            {
                var languageModel = new LanguageSetupModel();
                languageModel.LanguageId = Convert.ToInt32(dr["CLANG_ID"]);
                languageModel.LanguageCode = dr["CLANG_CODE"].ToString();
                languageModel.LanguageName = dr["CLANG_NAME"].ToString();
                model.LanguageList.Add(languageModel);
            }

            return PartialView("LanguageSetup", model);
        }

        public ActionResult LanguageSetupData(int languageID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var languageModel = new LanguageSetupModel();

            if (languageID == 0)
            {
                //Create New Language Setup
                languageModel.LanguageId = null;
                languageModel.LanguageCode = string.Empty;
                languageModel.LanguageName = string.Empty;
            }
            else
            {
                //View Existing Language Setup
                var dsLanguageSetup = AdminGeneralDB.GetLanguageByID(languageID, out ok, out msg);
                if (dsLanguageSetup.Tables[0].Rows.Count != 0)
                {
                    languageModel.LanguageId = Convert.ToInt32(dsLanguageSetup.Tables[0].Rows[0]["CLANG_ID"]);
                    languageModel.LanguageCode = dsLanguageSetup.Tables[0].Rows[0]["CLANG_CODE"].ToString();
                    languageModel.LanguageName = dsLanguageSetup.Tables[0].Rows[0]["CLANG_NAME"].ToString();
                }
            }

            return PartialView("LanguageSetupData", languageModel);
        }

        [HttpPost]
        public ActionResult LanguageSetupDataMethod(LanguageSetupModel languageSetupModel)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (languageSetupModel.LanguageCode == "" || languageSetupModel.LanguageCode == null)
                {
                    Response.Write("Please key in Language Code");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (languageSetupModel.LanguageName == "" || languageSetupModel.LanguageName == null)
                {
                    Response.Write("Please key in Language Name");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                if (languageSetupModel.LanguageId == null || languageSetupModel.LanguageId == 0)
                {
                    //Create New Record
                    int ok;
                    string msg;
                    AdminGeneralDB.CreateNewLanguage(languageSetupModel.LanguageCode, languageSetupModel.LanguageName, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);

                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgLanguageCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    //Update Existing Record
                    int ok;
                    string msg;
                    AdminGeneralDB.UpdateLanguage(languageSetupModel.LanguageId, languageSetupModel.LanguageName, out ok, out msg);

                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgLanguageNameAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return LanguageSetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Country

        public ActionResult CountrySetup(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            int ok;
            string msg;
            int pages = 0;
            PaginationCountrySetupModel model = new PaginationCountrySetupModel();
            DataSet dsCountry = AdminGeneralDB.GetAllCountry(Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                var csm = new CountrySetupModel();
                csm.CountryId = Convert.ToInt32(dr["CCOUNTRY_ID"]);
                csm.CountryCode = dr["CCOUNTRY_CODE"].ToString();
                csm.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                csm.SequenceNumber = Helper.NVLInteger(dr["CCOUNTRY_SEQUENCE"]);
                csm.MobileCode = Helper.NVL(dr["CCOUNTRY_MOBILECODE"]);
                csm.ICLength = Helper.NVL(dr["CCOUNTRY_ICLENGTH"]);
                csm.Status = dr["CCOUNTRY_DELETIONSTATE"].ToString();
                model.CountryList.Add(csm);
            }

            return PartialView("CountrySetup", model);
        }

        public ActionResult CountrySetupData(int CountryID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { });
            }

            int ok = 0;
            string msg = "";
            int pages = 0;
            var csm = new CountrySetupModel();

            //combobox for category
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            csm.LanguageList = from c in languages
                               select new SelectListItem
                               {
                                   Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                   Text = c.Text,
                                   Value = c.Value
                               };
            csm.SelectedLanguage = Session["LanguageChosen"].ToString();

            var ICType = new List<CountrySetupModel>();
            ICType.Add(new CountrySetupModel { ICTypeCode = "False", ICType = Resources.Admin.lblICDigits });
            ICType.Add(new CountrySetupModel { ICTypeCode = "True", ICType = Resources.Admin.lblICAlphabets });
            csm.ICTypeSelection = from c in ICType
                                  select new SelectListItem
                                  {
                                      Text = c.ICType,
                                      Value = c.ICTypeCode
                                  };

            if (CountryID == 0)
            {
                //Create New Category Setup
                csm.CountryId = null;
                csm.CountryCode = string.Empty;
                csm.CountryName = string.Empty;

                csm.FlagImageName = "";
                csm.FlagImagePath = "";

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = string.Empty;
                    csm.NameLanguageList.Add(tlm);
                }
            }
            else
            {
                //View Existing Category Setup
                DataSet dsCountrySetup = AdminGeneralDB.GetCountryByID(CountryID, out ok, out msg);
                if (dsCountrySetup.Tables[0].Rows.Count != 0)
                {
                    csm.CountryId = Convert.ToInt32(dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_ID"]);
                    csm.CountryCode = dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                    csm.SequenceNumber = Helper.NVLInteger(dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_SEQUENCE"]);
                    csm.MobileCode = Helper.NVL(dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_MOBILECODE"]);
                    csm.ICLength = Helper.NVL(dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_ICLENGTH"]);
                    csm.SelectedICType = Helper.NVL(dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_ICTYPE"]);
                   

                    if (dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_IMAGEPATH"].ToString() != "")
                    {
                        csm.FlagImagePath = dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_IMAGEPATH"].ToString();
                        int extensionIndex = dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_IMAGEPATH"].ToString().LastIndexOf('\\');
                        csm.FlagImageName = dsCountrySetup.Tables[0].Rows[0]["CCOUNTRY_IMAGEPATH"].ToString().Substring(extensionIndex + 1);
                    }
                    else
                    {
                        csm.FlagImageName = "";
                        csm.FlagImagePath = "";
                    }

                    DataSet dsMultiCountryCategory = AdminGeneralDB.GetMultiLanguageCountryByCode(csm.CountryCode, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiCountryCategory.Tables[0].Select("CCOUNTRY_CODE='" + csm.CountryCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr.Count() > 0 ? dr[0]["CMULTILANGCOUNTRY_NAME"].ToString() : string.Empty;
                        csm.NameLanguageList.Add(tlm);
                    }
                }
            }

            return PartialView("CountrySetupData", csm);
        }

        [HttpPost]
        public ActionResult CountrySetupDataMethod(CountrySetupModel csm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { });
                }

                if (string.IsNullOrEmpty(csm.NameLanguageList[0].Text))
                {
                    if (string.IsNullOrEmpty(csm.NameLanguageList[1].Text))
                    {
                        if (string.IsNullOrEmpty(csm.NameLanguageList[2].Text))
                        {
                            Response.Write("Please Key In Country Name");
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);

                        }
                    }
                }

                csm.FlagImage = Request.Files["FlagImage"];
                if (csm.FlagImage != null && csm.FlagImage.FileName != string.Empty)
                {
                    string basePath = Server.MapPath("~/Country/" + csm.CountryCode);
                    if (Directory.Exists(basePath) == false)
                    {
                        Directory.CreateDirectory(basePath);
                    }
                    csm.FlagImagePath = basePath + "\\" + csm.FlagImage.FileName;
                    csm.FlagImage.SaveAs(csm.FlagImagePath);
                }

                if (csm.CountryId == null || csm.CountryId == 0)
                {
                    //Create New Record
                    int ok;
                    string msg;
                    AdminGeneralDB.CreateNewCountry(csm.CountryCode, Helper.NVLInteger(csm.SequenceNumber), Helper.NVL(csm.MobileCode), Helper.NVL(csm.ICLength), Convert.ToBoolean(csm.SelectedICType), Session["Admin"].ToString(), Session["Admin"].ToString(), Helper.NVL(csm.FlagImagePath), out ok, out msg);
                    if (ok == 1)
                    {
                        for (int i = 0; i < csm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageCountry(csm.CountryCode, csm.NameLanguageList[i].LanguageCode, Helper.NVL(csm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgCountryCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    //Update Existing Record
                    int ok;
                    string msg;
                    AdminGeneralDB.UpdateCountry(csm.CountryId, csm.CountryCode, Helper.NVLInteger(csm.SequenceNumber), Helper.NVL(csm.MobileCode), Helper.NVL(csm.ICLength), Convert.ToBoolean(csm.SelectedICType), Helper.NVL(csm.FlagImagePath), Session["Admin"].ToString(), out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguageCountryByCode(csm.CountryCode, out ok, out msg);
                        for (int i = 0; i < csm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageCountry(csm.CountryCode, csm.NameLanguageList[i].LanguageCode, Helper.NVL(csm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgCountryCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                return CountrySetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult InAvtiveCountry(int? CountryID)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete existing currency
                int ok;
                string msg;



                AdminGeneralDB.InactiveCountryByID(CountryID, out ok, out msg);
                AdminGeneralDB.InsertAdminOperation(Session["Admin"].ToString(), "Inactive Country", Convert.ToString(CountryID), 0);

                return CountrySetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AvtiveCountry(int? CountryID)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete existing currency
                int ok;
                string msg;

                AdminGeneralDB.ActiveCountryByID(CountryID, out ok, out msg);
                AdminGeneralDB.InsertAdminOperation(Session["Admin"].ToString(), "Active Country", Convert.ToString(CountryID), 0);

                return CountrySetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CourierCompany

        //public ActionResult CourierCompanySetup(int selectedPage = 1)
        //{
        //    if (Session["Admin"] == null || Session["Admin"].ToString() == "")
        //    {
        //        return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
        //    }

        //    int ok;
        //    string msg;
        //    int pages = 0;
        //    var model = new PaginationCourierCompanySetupModel();
        //    DataSet dsCourier = AdminGeneralDB.GetAllCourier(Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

        //    List<int> pageList = new List<int>();
        //    for (int z = 1; z <= pages; z++)
        //    {
        //        pageList.Add(z);
        //    }

        //    model.Pages = from c in pageList
        //                  select new SelectListItem
        //                  {
        //                      Selected = (c.ToString() == selectedPage.ToString()),
        //                      Text = c.ToString(),
        //                      Value = c.ToString()
        //                  };

        //    foreach (DataRow dr in dsCourier.Tables[0].Rows)
        //    {
        //        var csm = new CourierCompanySetupModel();
        //        csm.CourierId = Int32.Parse(dr["CRCOM_ID"].ToString());
        //        csm.CourierCode = dr["CRCOM_CODE"].ToString();
        //        csm.CourierCompany = dr["CMULTILANGCRCOM_COMPANYNAME"].ToString();
        //        csm.ContactPerson = dr["CRCOM_CONTACTPERSON"].ToString();
        //        csm.ContactNumber = dr["CRCOM_CONTACTNO1"].ToString();
        //        csm.ContactNumber2 = dr["CRCOM_CONTACTNO2"].ToString();
        //        csm.Website = dr["CRCOM_WEBSITE"].ToString();
        //        model.CourierCompanyList.Add(csm);
        //    }

        //    return PartialView("CourierCompanySetup", model);
        //}

        //public ActionResult CourierCompanySetupData(string courierCode)
        //{
        //    if (Session["Admin"] == null || Session["Admin"].ToString() == "")
        //    {
        //        return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
        //    }

        //    int ok = 0;
        //    string msg = "";
        //    var csm = new CourierCompanySetupModel();

        //    //combobox for category
        //    var languages = Misc.GetLanguageList(ref ok, ref msg);
        //    csm.LanguageList = from c in languages
        //                       select new SelectListItem
        //                       {
        //                           Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
        //                           Text = c.Text,
        //                           Value = c.Value
        //                       };
        //    csm.SelectedLanguage = Session["LanguageChosen"].ToString();

        //    if (string.IsNullOrEmpty(courierCode))
        //    {
        //        //Create New Category Setup
        //        csm.CourierId = null;
        //        csm.CourierCode = string.Empty;
        //        csm.ContactPerson = string.Empty;
        //        csm.ContactNumber = string.Empty;
        //        csm.ContactNumber2 = string.Empty;
        //        csm.Website = string.Empty;

        //        foreach (SelectListItem language in languages)
        //        {
        //            var tlm = new TextLanguageModel();
        //            tlm.LanguageCode = language.Value;
        //            tlm.LanguageName = language.Text;
        //            tlm.Text = string.Empty;
        //            csm.NameLanguageList.Add(tlm);
        //        }
        //        ViewBag.IsExisting = false;
        //    }
        //    else
        //    {
        //        //View Existing Courier Company Setup
        //        DataSet dsCourierCompanySetup = AdminGeneralDB.GetCourierCompanyByCode(courierCode, out ok, out msg);
        //        if (dsCourierCompanySetup.Tables[0].Rows.Count != 0)
        //        {
        //            csm.CourierId = Int32.Parse(dsCourierCompanySetup.Tables[0].Rows[0]["CRCOM_ID"].ToString());
        //            csm.CourierCode = dsCourierCompanySetup.Tables[0].Rows[0]["CRCOM_CODE"].ToString(); ;
        //            csm.ContactPerson = dsCourierCompanySetup.Tables[0].Rows[0]["CRCOM_CONTACTPERSON"].ToString();
        //            csm.ContactNumber = dsCourierCompanySetup.Tables[0].Rows[0]["CRCOM_CONTACTNO1"].ToString();
        //            csm.ContactNumber2 = dsCourierCompanySetup.Tables[0].Rows[0]["CRCOM_CONTACTNO2"].ToString();
        //            csm.Website = dsCourierCompanySetup.Tables[0].Rows[0]["CRCOM_WEBSITE"].ToString();

        //            DataSet dsMultiCourierCompanyCategory = AdminGeneralDB.GetMultiLanguageCourierCompanyByCode(courierCode, out ok, out msg);
        //            foreach (SelectListItem language in languages)
        //            {
        //                var tlm = new TextLanguageModel();
        //                tlm.LanguageCode = language.Value;
        //                tlm.LanguageName = language.Text;
        //                DataRow[] dr = dsMultiCourierCompanyCategory.Tables[0].Select("CRCOM_CODE='" + courierCode + "' AND CLANG_CODE='" + language.Value + "'");
        //                tlm.Text = dr.Any() ? dr[0]["CMULTILANGCRCOM_COMPANYNAME"].ToString() : string.Empty;
        //                csm.NameLanguageList.Add(tlm);
        //            }
        //        }
        //        ViewBag.IsExisting = true;
        //    }

        //    return PartialView("CourierCompanySetupData", csm);
        //}

        //[HttpPost]
        //public ActionResult CourierCompanySetupDataMethod(CourierCompanySetupModel csm)
        //{
        //    try
        //    {
        //        if (Session["Admin"] == null || Session["Admin"].ToString() == "")
        //        {
        //            return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
        //        }

        //        if (csm.CourierId == null || csm.CourierId == 0)
        //        {
        //            //Create New Record
        //            int ok;
        //            string msg;
        //            AdminGeneralDB.CreateNewCourierCompany(csm.CourierCode, csm.ContactPerson, Helper.NVL(csm.ContactNumber), Helper.NVL(csm.ContactNumber2), Helper.NVL(csm.Website), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
        //            if (ok == 1)
        //            {
        //                for (int i = 0; i < csm.NameLanguageList.Count; i++)
        //                {
        //                    AdminGeneralDB.CreateNewMultiLanguageCourierCompany(csm.CourierCode, csm.NameLanguageList[i].LanguageCode, Helper.NVL(csm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
        //                }
        //            }
        //            if (ok == 0)
        //            {
        //                if (msg == "-1")
        //                {
        //                    Response.Write(Resources.Admin.msgCourierCompanyAlreadyExists);
        //                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            //Update Existing Record
        //            int ok;
        //            string msg;
        //            AdminGeneralDB.UpdateCourierCompany(csm.CourierId, csm.CourierCode, Helper.NVL(csm.ContactPerson), Helper.NVL(csm.ContactNumber), Helper.NVL(csm.ContactNumber2), Helper.NVL(csm.Website), out ok, out msg);
        //            if (ok == 1)
        //            {
        //                AdminGeneralDB.DeleteMultiLanguageCourierCompanyByCode(csm.CourierCode, out ok, out msg);
        //                for (int i = 0; i < csm.NameLanguageList.Count; i++)
        //                {
        //                    AdminGeneralDB.CreateNewMultiLanguageCourierCompany(csm.CourierCode, csm.NameLanguageList[i].LanguageCode, Helper.NVL(csm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
        //                }
        //            }
        //            if (ok == 0)
        //            {
        //                if (msg == "-1")
        //                {
        //                    Response.Write(Resources.Admin.msgCourierCompanyAlreadyExists);
        //                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //        }

        //        return CourierCompanySetup();
        //    }
        //    catch (Exception e)
        //    {
        //        Response.Write(e.Message);
        //        return Json(string.Empty, JsonRequestBehavior.AllowGet);
        //    }
        //}

        #endregion

        #region Province

        public ActionResult ProvinceSetup(string selectedCountryCode = "0", int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            DataSet dsCountry = AdminGeneralDB.GetAllCountry(Session["LanguageChosen"].ToString(), 1, out pages, out ok, out msg);

            var countries = new List<CountrySetupModel>();

            var csm = new CountrySetupModel();
            csm.CountryId = 0;
            csm.CountryCode = "0";
            csm.CountryName = "--- Select ---";
            countries.Add(csm);

            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                csm = new CountrySetupModel();
                csm.CountryId = Convert.ToInt32(dr["CCOUNTRY_ID"]);
                csm.CountryCode = dr["CCOUNTRY_CODE"].ToString();
                csm.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                countries.Add(csm);
            }

            var model = new PaginationProvinceSetupModel();
            string defaultCountryCode;
            if (string.IsNullOrEmpty(selectedCountryCode))
            {
                var defaultCountry = countries.Count > 0 ? countries.First() : null;
                defaultCountryCode = defaultCountry != null ? defaultCountry.CountryCode : string.Empty;
            }
            else
            {
                defaultCountryCode = selectedCountryCode;
            }

            DataSet dsProvinces = AdminGeneralDB.GetAllProvincesByCountryAndLanguageCode(Session["LanguageChosen"].ToString(), defaultCountryCode, selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.CountryList = from c in countries
                                select new SelectListItem
                                {
                                    Selected = (c.CountryCode == defaultCountryCode),
                                    Text = c.CountryName.ToString(),
                                    Value = c.CountryCode.ToString()
                                };

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsProvinces.Tables[0].Rows)
            {
                var psm = new ProvinceSetupModel();
                psm.ProvinceId = Convert.ToInt32(dr["CPROVINCE_ID"]);
                psm.ProvinceCode = dr["CPROVINCE_CODE"].ToString();
                psm.ProvinceName = dr["CMULTILANGPROVINCE_NAME"].ToString();
                model.ProvinceList.Add(psm);
            }

            return PartialView("ProvinceSetup", model);
        }

        public ActionResult ProvinceSetupData(int ProvinceId, string selectedCountryCode = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var psm = new ProvinceSetupModel();

            //combobox for language
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            psm.LanguageList = from c in languages select new SelectListItem { Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false, Text = c.Text, Value = c.Value };
            psm.SelectedLanguage = Session["LanguageChosen"].ToString();
            psm.SelectedCountry = selectedCountryCode;

            if (ProvinceId == 0)
            {
                //Create New province Setup
                psm.ProvinceId = null;
                psm.ProvinceCode = string.Empty;
                psm.ProvinceName = string.Empty;

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = string.Empty;
                    psm.NameLanguageList.Add(tlm);
                }
            }
            else
            {
                //View Existing Province Setup
                DataSet dsProvinceSetup = AdminGeneralDB.GetProvinceById(ProvinceId, out ok, out msg);
                if (dsProvinceSetup.Tables[0].Rows.Count != 0)
                {
                    psm.ProvinceId = Convert.ToInt32(dsProvinceSetup.Tables[0].Rows[0]["CPROVINCE_ID"]);
                    psm.ProvinceCode = dsProvinceSetup.Tables[0].Rows[0]["CPROVINCE_CODE"].ToString();
                    psm.SelectedCountry = dsProvinceSetup.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

                    DataSet dsMultiProvinceCategory = AdminGeneralDB.GetMultiLanguageProvinceByCountryCodeAndProvinceCode(psm.ProvinceCode, psm.SelectedCountry, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiProvinceCategory.Tables[0].Select("CPROVINCE_CODE='" + psm.ProvinceCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr.Count() == 0 ? "" : dr[0]["CMULTILANGPROVINCE_NAME"].ToString();
                        psm.NameLanguageList.Add(tlm);
                    }
                }
            }

            //combobox for category
            List<CountrySetupModel> countries = Misc.GetAllCountryList(psm.SelectedLanguage, ref ok, ref msg);
            psm.Countries = from c in countries select new SelectListItem { Selected = false, Text = c.CountryName, Value = c.CountryCode };
            return PartialView("ProvinceSetupData", psm);
        }

        [HttpPost]
        public ActionResult ProvinceSetupDataMethod(ProvinceSetupModel psm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }
                if (string.IsNullOrEmpty(psm.NameLanguageList[0].Text))
                {
                    if (string.IsNullOrEmpty(psm.NameLanguageList[1].Text))
                    {
                        if (string.IsNullOrEmpty(psm.NameLanguageList[2].Text))
                        {
                            Response.Write("Please Key In City Name");
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                //Create New Record or Update Existing Record
                int ok;
                string msg;
                if (psm.ProvinceId == null || psm.ProvinceId == 0)
                {
                    //Create New Record                    
                    AdminGeneralDB.CreateNewProvince(psm.ProvinceCode, psm.SelectedCountry, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                    if (ok == 1)
                    {
                        for (int i = 0; i < psm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageProvince(psm.SelectedCountry, psm.NameLanguageList[i].LanguageCode, psm.ProvinceCode, Helper.NVL(psm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    else if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgProvinceCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    //Update Existing Record
                    AdminGeneralDB.UpdateProvince(psm.ProvinceId, psm.ProvinceCode, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguageProvinceByProvinceCode(psm.ProvinceCode, out ok, out msg);
                        for (int i = 0; i < psm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageProvince(psm.SelectedCountry, psm.NameLanguageList[i].LanguageCode, psm.ProvinceCode, Helper.NVL(psm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgProvinceCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                return ProvinceSetup(psm.SelectedCountry);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteProvince(int? ProvinceId)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete existing province
                int ok;
                string msg;
                DataSet dsProvinceSetup = AdminGeneralDB.GetProvinceById(ProvinceId, out ok, out msg);
                if (dsProvinceSetup.Tables[0].Rows.Count != 0)
                {
                    var provinceCode = dsProvinceSetup.Tables[0].Rows[0]["CPROVINCE_CODE"].ToString();
                    AdminGeneralDB.DeleteProvinceByProvinceCode(provinceCode, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguageProvinceByProvinceCode(provinceCode, out ok, out msg);
                    }
                }

                return ProvinceSetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region City

        [HttpPost]
        public JsonResult FindProvince(string CountryCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetProvinceList(LanguageCode, CountryCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetProvinceList(string LanguageCode, string CountryCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<ProvinceModel> provinces = Misc.GetAllProvinceByCountry(LanguageCode, CountryCode, ref ok, ref message);

            var result = from c in provinces
                         select new SelectListItem
                         {
                             Selected = false,
                             Text = c.ProvinceName,
                             Value = c.ProvinceCode
                         };
            return result.OrderBy(m => m.Value).ToList();
        }

        public ActionResult CitySetup(string selectedProvinceCode = "", int selectedPage = 1, string selectedCountryCode = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;

            DataSet dsCountry = AdminGeneralDB.GetAllCountry(Session["LanguageChosen"].ToString(), 1, out pages, out ok, out msg);
            var countries = new List<CountrySetupModel>();
            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                var csm = new CountrySetupModel();
                csm.CountryId = Convert.ToInt32(dr["CCOUNTRY_ID"]);
                csm.CountryCode = dr["CCOUNTRY_CODE"].ToString();
                csm.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                countries.Add(csm);
            }

            selectedCountryCode = string.IsNullOrEmpty(selectedCountryCode) ? countries.First().CountryCode : selectedCountryCode;
            var dsProvinces = AdminGeneralDB.GetAllProvincesByCountry(Session["LanguageChosen"].ToString(), selectedCountryCode);
            var provinces = new List<ProvinceSetupModel>();
            foreach (DataRow dr in dsProvinces.Tables[0].Rows)
            {
                var csm = new ProvinceSetupModel();
                csm.ProvinceId = Convert.ToInt32(dr["CPROVINCE_ID"]);
                csm.ProvinceCode = dr["CPROVINCE_CODE"].ToString();
                csm.ProvinceName = dr["CMULTILANGPROVINCE_NAME"].ToString();
                provinces.Add(csm);
            }

            var model = new PaginationCitySetupModel();
            string defaultProvinceCode;
            if (string.IsNullOrEmpty(selectedProvinceCode))
            {
                var defaultProvince = provinces.Count > 0 ? provinces.First() : null;
                defaultProvinceCode = defaultProvince != null ? defaultProvince.ProvinceCode : string.Empty;
            }
            else
            {
                defaultProvinceCode = selectedProvinceCode;
            }

            DataSet dsCity = AdminGeneralDB.GetAllCityByProvinceAndLanguageCode(Session["LanguageChosen"].ToString(), defaultProvinceCode, selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.CountryList = from c in countries
                                select new SelectListItem
                                {
                                    Text = c.CountryName.ToString(),
                                    Value = c.CountryCode.ToString()
                                };
            model.SelectedCountry = selectedCountryCode;
            model.SelectedProvince = selectedProvinceCode;

            model.ProvinceList = from c in provinces
                                 select new SelectListItem
                                 {
                                     Text = c.ProvinceName.ToString(),
                                     Value = c.ProvinceCode.ToString()
                                 };

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsCity.Tables[0].Rows)
            {
                var csm = new CitySetupModel();
                csm.CityID = Convert.ToInt32(dr["CCITY_ID"]);
                csm.CityCode = dr["CCITY_CODE"].ToString();
                csm.CityName = dr["CMULTILANGCITY_NAME"].ToString();
                model.CityList.Add(csm);
            }

            return PartialView("CitySetup", model);
        }

        public ActionResult CitySetupData(int CityId, string SelectedCountryCode = "", string SelectedProvinceCode = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var csm = new CitySetupModel();

            //combobox for language
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            csm.LanguageList = from c in languages
                               select new SelectListItem
                               {
                                   Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                   Text = c.Text,
                                   Value = c.Value
                               };
            csm.SelectedLanguage = Session["LanguageChosen"].ToString();

            if (CityId == 0)
            {
                //Create New city Setup
                csm.CityID = null;
                csm.CityCode = string.Empty;
                csm.CityName = string.Empty;
                csm.SelectedCountry = SelectedCountryCode;
                csm.SelectedProvince = SelectedProvinceCode;

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = string.Empty;
                    csm.NameLanguageList.Add(tlm);
                }
            }
            else
            {
                //View Existing City Setup
                DataSet dsCitySetup = AdminGeneralDB.GetCityById(CityId, out ok, out msg);
                if (dsCitySetup.Tables[0].Rows.Count != 0)
                {
                    csm.CityID = Convert.ToInt32(dsCitySetup.Tables[0].Rows[0]["CCITY_ID"]);
                    csm.CityCode = dsCitySetup.Tables[0].Rows[0]["CCITY_CODE"].ToString();

                    csm.SelectedProvince = dsCitySetup.Tables[0].Rows[0]["CPROVINCE_CODE"].ToString();
                    DataSet dsProvince = AdminGeneralDB.GetProvinceByCode(csm.SelectedProvince, out ok, out msg);
                    csm.SelectedCountry = dsProvince.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

                    DataSet dsMultiCity = AdminGeneralDB.GetMultiLanguageCityByProvinceAndCityCode(csm.CityCode, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiCity.Tables[0].Select("CCITY_CODE='" + csm.CityCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr.Count() == 0 ? "" : dr[0]["CMULTILANGCITY_NAME"].ToString();
                        csm.NameLanguageList.Add(tlm);
                    }
                }
            }

            //combobox for country
            List<CountrySetupModel> countries = Misc.GetAllCountryList(csm.SelectedLanguage, ref ok, ref msg);
            csm.CountryList = from c in countries
                              select new SelectListItem
                              {
                                  Selected = csm.SelectedCountry == c.CountryCode ? true : false,
                                  Text = c.CountryName,
                                  Value = c.CountryCode
                              };

            //combobox for province
            string CountryCode = csm.SelectedCountry == "" ? csm.CountryList.First().Value : csm.SelectedCountry;
            List<ProvinceModel> provinces = Misc.GetAllProvinceByCountry(csm.SelectedLanguage, CountryCode, ref ok, ref msg);
            csm.ProvinceList = from c in provinces
                               select new SelectListItem
                               {
                                   Selected = false,
                                   Text = c.ProvinceName,
                                   Value = c.ProvinceCode
                               };

            return PartialView("CitySetupData", csm);
        }

        [HttpPost]
        public ActionResult CitySetupDataMethod(CitySetupModel csm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                //Create New Record or Update Existing Record
                int ok;
                string msg;
                if (csm.CityID == null || csm.CityID == 0)
                {
                    //Create New Record                    
                    AdminGeneralDB.CreateNewCity(csm.SelectedProvince, csm.CityCode, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                    if (ok == 1)
                    {
                        for (int i = 0; i < csm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageCity(csm.SelectedProvince, csm.NameLanguageList[i].LanguageCode, csm.CityCode, Helper.NVL(csm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    else if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgCityCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    //Update Existing Record
                    AdminGeneralDB.UpdateCity(csm.CityID, csm.CityCode, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguageCityByCityCode(csm.CityCode, out ok, out msg);
                        for (int i = 0; i < csm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageCity(csm.SelectedProvince, csm.NameLanguageList[i].LanguageCode, csm.CityCode, Helper.NVL(csm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgCityCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                return CitySetup(csm.SelectedProvince, 1, csm.SelectedCountry);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteCity(int? CityId)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete existing city
                int ok;
                string msg;
                DataSet dsCitySetup = AdminGeneralDB.GetCityById(CityId, out ok, out msg);
                if (dsCitySetup.Tables[0].Rows.Count != 0)
                {
                    var cityCode = dsCitySetup.Tables[0].Rows[0]["CCITY_CODE"].ToString();
                    AdminGeneralDB.DeleteCityByCityCode(cityCode, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguageCityByCityCode(cityCode, out ok, out msg);
                    }
                }

                return CitySetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region District

        [HttpPost]
        public JsonResult FindCity(string ProvinceCode)
        {
            string LanguageCode = Session["LanguageChosen"].ToString();

            var result = GetCityList(LanguageCode, ProvinceCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static IEnumerable<SelectListItem> GetCityList(string LanguageCode, string ProvinceCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int ok = 0;
            string message = "";
            List<CityModel> cities = Misc.GetAllCityByProvince(LanguageCode, ProvinceCode, ref ok, ref message);

            var result = from c in cities
                         select new SelectListItem
                         {
                             Selected = false,
                             Text = c.CityName,
                             Value = c.CityCode
                         };

            return result.OrderBy(m => m.Value).ToList();
        }

        public ActionResult DistrictSetup(string selectedCityCode = "", int selectedPage = 1, string selectedCountryCode = "", string selectedProvinceCode = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;

            DataSet dsCountry = AdminGeneralDB.GetAllCountry(Session["LanguageChosen"].ToString(), 1, out pages, out ok, out msg);
            var countries = new List<CountrySetupModel>();
            foreach (DataRow dr in dsCountry.Tables[0].Rows)
            {
                var csm = new CountrySetupModel();
                csm.CountryId = Convert.ToInt32(dr["CCOUNTRY_ID"]);
                csm.CountryCode = dr["CCOUNTRY_CODE"].ToString();
                csm.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                countries.Add(csm);
            }

            selectedCountryCode = string.IsNullOrEmpty(selectedCountryCode) ? countries.First().CountryCode : selectedCountryCode;
            DataSet dsProvinces = AdminGeneralDB.GetAllProvincesByCountry(Session["LanguageChosen"].ToString(), selectedCountryCode);
            var provinces = new List<ProvinceSetupModel>();
            foreach (DataRow dr in dsProvinces.Tables[0].Rows)
            {
                var csm = new ProvinceSetupModel();
                csm.ProvinceId = Convert.ToInt32(dr["CPROVINCE_ID"]);
                csm.ProvinceCode = dr["CPROVINCE_CODE"].ToString();
                csm.ProvinceName = dr["CMULTILANGPROVINCE_NAME"].ToString();
                provinces.Add(csm);
            }

            selectedProvinceCode = string.IsNullOrEmpty(selectedProvinceCode) ? provinces.First().ProvinceCode : selectedProvinceCode;
            DataSet dsCities = AdminGeneralDB.GetAllCitiesByProvince(Session["LanguageChosen"].ToString(), selectedProvinceCode);
            var cities = new List<CitySetupModel>();
            foreach (DataRow dr in dsCities.Tables[0].Rows)
            {
                var csm = new CitySetupModel();
                csm.CityID = Convert.ToInt32(dr["CCITY_ID"]);
                csm.CityCode = dr["CCITY_CODE"].ToString();
                csm.CityName = dr["CMULTILANGCITY_NAME"].ToString();
                cities.Add(csm);
            }

            var model = new PaginationDistrictSetupModel();
            string defaultCityCode;
            if (string.IsNullOrEmpty(selectedCityCode))
            {
                var defaultCity = cities.Count > 0 ? cities.First() : null;
                defaultCityCode = defaultCity != null ? defaultCity.CityCode : string.Empty;
            }
            else
            {
                defaultCityCode = selectedCityCode;
            }

            DataSet dsDistrict = AdminGeneralDB.GetAllDistrictByCityAndLanguageCode(Session["LanguageChosen"].ToString(), defaultCityCode, selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.CountryList = from c in countries
                                select new SelectListItem
                                {
                                    Text = c.CountryName.ToString(),
                                    Value = c.CountryCode.ToString()
                                };
            model.SelectedCountry = selectedCountryCode;

            model.ProvinceList = from c in provinces
                                 select new SelectListItem
                                 {
                                     Text = c.ProvinceName.ToString(),
                                     Value = c.ProvinceCode.ToString()
                                 };
            model.SelectedProvince = selectedProvinceCode;

            model.CityList = from c in cities
                             select new SelectListItem
                             {
                                 Selected = (c.CityCode == defaultCityCode),
                                 Text = c.CityName.ToString(),
                                 Value = c.CityCode.ToString()
                             };
            model.SelectedCity = selectedCityCode;

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsDistrict.Tables[0].Rows)
            {
                var dsm = new DistrictSetupModel();
                dsm.DistrictID = Convert.ToInt32(dr["CDISTRICT_ID"]);
                dsm.DistrictCode = dr["CDISTRICT_CODE"].ToString();
                dsm.DistrictName = dr["CMULTILANGDISTRICT_NAME"].ToString();
                model.DistrictList.Add(dsm);
            }

            return PartialView("DistrictSetup", model);
        }

        public ActionResult DistrictSetupData(int DistrictId, string SelectedCountryCode = "", string SelectedProvinceCode = "", string SelectedCityCode = "")
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var dsm = new DistrictSetupModel();

            //combobox for language
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            dsm.LanguageList = from c in languages
                               select new SelectListItem
                               {
                                   Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                   Text = c.Text,
                                   Value = c.Value
                               };
            dsm.SelectedLanguage = Session["LanguageChosen"].ToString();

            if (DistrictId == 0)
            {
                //Create New district Setup
                dsm.DistrictID = null;
                dsm.DistrictCode = string.Empty;
                dsm.DistrictName = string.Empty;
                dsm.SelectedCountry = SelectedCountryCode;
                dsm.SelectedProvince = SelectedProvinceCode;
                dsm.SelectedCity = SelectedCityCode;

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = string.Empty;
                    dsm.NameLanguageList.Add(tlm);
                }
            }
            else
            {
                //View Existing District Setup
                DataSet dsDistrictSetup = AdminGeneralDB.GetDistrictById(DistrictId, out ok, out msg);
                if (dsDistrictSetup.Tables[0].Rows.Count != 0)
                {
                    dsm.DistrictID = Convert.ToInt32(dsDistrictSetup.Tables[0].Rows[0]["CDISTRICT_ID"]);
                    dsm.DistrictCode = dsDistrictSetup.Tables[0].Rows[0]["CDISTRICT_CODE"].ToString();
                    dsm.SelectedCity = dsDistrictSetup.Tables[0].Rows[0]["CCITY_CODE"].ToString();

                    DataSet dsCity = AdminGeneralDB.GetCityByCode(dsm.SelectedCity, out ok, out msg);
                    dsm.SelectedProvince = dsCity.Tables[0].Rows[0]["CPROVINCE_CODE"].ToString();

                    DataSet dsProvince = AdminGeneralDB.GetProvinceByCode(dsm.SelectedProvince, out ok, out msg);
                    dsm.SelectedCountry = dsProvince.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

                    DataSet dsMultiDistrict = AdminGeneralDB.GetMultiLanguageDistrictByDistrictCode(dsm.DistrictCode, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiDistrict.Tables[0].Select("CDISTRICT_CODE='" + dsm.DistrictCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr.Count() == 0 ? "" : dr[0]["CMULTILANGDISTRICT_NAME"].ToString();
                        dsm.NameLanguageList.Add(tlm);
                    }
                }
            }

            //combobox for country
            List<CountrySetupModel> countries = Misc.GetAllCountryList(dsm.SelectedLanguage, ref ok, ref msg);
            dsm.CountryList = from c in countries
                              select new SelectListItem
                              {
                                  Selected = dsm.SelectedCountry == c.CountryCode ? true : false,
                                  Text = c.CountryName,
                                  Value = c.CountryCode
                              };

            //combobox for province
            string CountryCode = dsm.SelectedCountry == "" ? dsm.CountryList.First().Value : dsm.SelectedCountry;
            List<ProvinceModel> provinces = Misc.GetAllProvinceByCountry(dsm.SelectedLanguage, CountryCode, ref ok, ref msg);
            dsm.ProvinceList = from c in provinces
                               select new SelectListItem
                               {
                                   Selected = false,
                                   Text = c.ProvinceName,
                                   Value = c.ProvinceCode
                               };

            //combobox for city
            string ProvinceCode = dsm.SelectedProvince == "" ? dsm.ProvinceList.First().Value : dsm.SelectedProvince;
            List<CityModel> cities = Misc.GetAllCityByProvince(dsm.SelectedLanguage, ProvinceCode, ref ok, ref msg);
            dsm.CityList = from c in cities
                           select new SelectListItem
                           {
                               Selected = false,
                               Text = c.CityName,
                               Value = c.CityCode
                           };

            return PartialView("DistrictSetupData", dsm);
        }

        [HttpPost]
        public ActionResult DistrictSetupDataMethod(DistrictSetupModel dsm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                //Create New Record or Update Existing Record
                int ok;
                string msg;
                if (dsm.DistrictID == null || dsm.DistrictID == 0)
                {
                    //Create New Record                    
                    AdminGeneralDB.CreateNewDistrict(dsm.SelectedCity, dsm.DistrictCode, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                    if (ok == 1)
                    {
                        for (int i = 0; i < dsm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageDistrict(dsm.SelectedCity, dsm.NameLanguageList[i].LanguageCode, dsm.DistrictCode, Helper.NVL(dsm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    else if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgDistrictCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    //Update Existing Record
                    AdminGeneralDB.UpdateDistrict(dsm.DistrictID, dsm.DistrictCode, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguageDistrictByDistrictCode(dsm.DistrictCode, out ok, out msg);
                        for (int i = 0; i < dsm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateNewMultiLanguageDistrict(dsm.SelectedCity, dsm.NameLanguageList[i].LanguageCode, dsm.DistrictCode, Helper.NVL(dsm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgDistrictCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                return DistrictSetup(dsm.SelectedCity, 1, dsm.SelectedCountry, dsm.SelectedProvince);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteDistrict(int? DistrictId)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete existing district
                int ok;
                string msg;
                DataSet dsDistrictSetup = AdminGeneralDB.GetDistrictById(DistrictId, out ok, out msg);
                if (dsDistrictSetup.Tables[0].Rows.Count != 0)
                {
                    var districtCode = dsDistrictSetup.Tables[0].Rows[0]["CDISTRICT_CODE"].ToString();
                    AdminGeneralDB.DeleteDistrictByDistrictCode(districtCode, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguageDistrictByDistrictCode(districtCode, out ok, out msg);
                    }
                }

                return DistrictSetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Bank

        public ActionResult BankSetup(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            var model = new PaginationBankSetupModel();
            DataSet dsBank = AdminGeneralDB.GetAllBank(Session["LanguageChosen"].ToString(), selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsBank.Tables[0].Rows)
            {
                var bsm = new BankSetupModel();
                bsm.BankID = Convert.ToInt32(dr["CBANK_ID"]);
                bsm.BankCode = dr["CBANK_CODE"].ToString();
                bsm.BankName = dr["CMULTILANGBANK_NAME"].ToString();
                bsm.CountryName = dr["CMULTILANGCOUNTRY_NAME"].ToString();
                model.BankList.Add(bsm);
            }

            return PartialView("BankSetup", model);
        }

        public ActionResult BankSetupData(int BankId, string CountryName)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var bsm = new BankSetupModel();
            bsm.SelectedLanguage = Session["LanguageChosen"].ToString();

            //combobox for language
            List<SelectListItem> languages = Misc.GetLanguageList(ref ok, ref msg);
            bsm.LanguageList = from c in languages
                               select new SelectListItem
                               {
                                   Selected = c.Value == Session["LanguageChosen"].ToString() ? true : false,
                                   Text = c.Text,
                                   Value = c.Value
                               };

            if (BankId == 0)
            {
                //Create New province Setup
                bsm.BankID = null;
                bsm.BankCode = string.Empty;
                bsm.BankName = string.Empty;
                bsm.SelectedCountryCode = string.Empty;
                bsm.BankCharges = 0;
                bsm.MaximumBankCharges = 0;
                var countryList = Misc.GetCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);
                var defaultCountry = countryList.Count > 0 ? countryList.First().Text : string.Empty;
                bsm.CountryList = from c in countryList
                                  select new SelectListItem
                                  {
                                      Selected = c.Value == defaultCountry,
                                      Text = c.Text,
                                      Value = c.Value
                                  };

                foreach (SelectListItem language in languages)
                {
                    var tlm = new TextLanguageModel();
                    tlm.LanguageCode = language.Value;
                    tlm.LanguageName = language.Text;
                    tlm.Text = string.Empty;
                    bsm.NameLanguageList.Add(tlm);
                }
            }
            else
            {
                //View Existing Bank Setup
                DataSet dsBankSetup = AdminGeneralDB.GetBankById(BankId, out ok, out msg);
                if (dsBankSetup.Tables[0].Rows.Count != 0)
                {
                    bsm.BankID = Convert.ToInt32(dsBankSetup.Tables[0].Rows[0]["CBANK_ID"]);
                    bsm.BankCode = dsBankSetup.Tables[0].Rows[0]["CBANK_CODE"].ToString();

                    bsm.SelectedCountryCode = dsBankSetup.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();
                    bsm.BankCharges = float.Parse(dsBankSetup.Tables[0].Rows[0]["CBANK_BANKCHARGES"].ToString());
                    bsm.MaximumBankCharges = float.Parse(dsBankSetup.Tables[0].Rows[0]["CBANK_MAXBANKCHARGES"].ToString());

                    var countryList = Misc.GetCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);
                    bsm.CountryList = from c in countryList
                                      select new SelectListItem
                                      {
                                          Selected = c.Value == bsm.SelectedCountryCode,
                                          Text = c.Text,
                                          Value = c.Value
                                      };

                    DataSet dsMultiBankCategory = AdminGeneralDB.GetMultiLanguageBankByCode(bsm.BankCode, out ok, out msg);
                    foreach (SelectListItem language in languages)
                    {
                        var tlm = new TextLanguageModel();
                        tlm.LanguageCode = language.Value;
                        tlm.LanguageName = language.Text;
                        DataRow[] dr = dsMultiBankCategory.Tables[0].Select("CBANK_CODE='" + bsm.BankCode + "' AND CLANG_CODE='" + language.Value + "'");
                        tlm.Text = dr.Count() == 0 ? "" : dr[0]["CMULTILANGBANK_NAME"].ToString();
                        bsm.NameLanguageList.Add(tlm);
                    }
                }
            }

            return PartialView("BankSetupData", bsm);
        }

        [HttpPost]
        public ActionResult BankSetupDataMethod(BankSetupModel bsm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (bsm.BankCode == "" || bsm.BankCode == null)
                {
                    Response.Write("Please key in Bank Code");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (Convert.ToString(bsm.BankCharges) == "" || Convert.ToString(bsm.BankCharges) == null)
                {
                    Response.Write("Please key in Bank Charges");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (Convert.ToString(bsm.MaximumBankCharges) == "" || Convert.ToString(bsm.MaximumBankCharges) == null)
                {
                    Response.Write("Please key in Maximum Amount");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //Create New Record or Update Existing Record
                int ok;
                string msg;
                if (bsm.BankID == null || bsm.BankID == 0)
                {
                    //Create New Record                    
                    AdminGeneralDB.CreateNewBank(bsm.SelectedCountryCode, bsm.BankCode, bsm.BankCharges, bsm.MaximumBankCharges, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                    if (ok == 1)
                    {
                        for (int i = 0; i < bsm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateMultiLanguageBank(bsm.BankCode, bsm.NameLanguageList[i].LanguageCode, Helper.NVL(bsm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    else if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgBankCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    //Update Existing Record
                    AdminGeneralDB.UpdateBank(bsm.BankID, bsm.BankCode, bsm.BankCharges, bsm.MaximumBankCharges, bsm.SelectedCountryCode, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguageBankByCode(bsm.BankCode, out ok, out msg);
                        for (int i = 0; i < bsm.NameLanguageList.Count; i++)
                        {
                            AdminGeneralDB.CreateMultiLanguageBank(bsm.BankCode, bsm.NameLanguageList[i].LanguageCode, Helper.NVL(bsm.NameLanguageList[i].Text), Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                        }
                    }
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgBankCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                return BankSetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteBank(int? BankId)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete existing bank
                int ok;
                string msg;
                DataSet dsBankSetup = AdminGeneralDB.GetBankById(BankId, out ok, out msg);
                if (dsBankSetup.Tables[0].Rows.Count != 0)
                {
                    var bankCode = dsBankSetup.Tables[0].Rows[0]["CBANK_CODE"].ToString();
                    AdminGeneralDB.DeleteBankByCode(bankCode, out ok, out msg);
                    if (ok == 1)
                    {
                        AdminGeneralDB.DeleteMultiLanguageBankByCode(bankCode, out ok, out msg);
                    }
                }

                return BankSetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Currency

        public ActionResult CurrencySetup(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            var model = new PaginationCurrencySetupModel();
            DataSet dsCurrency = AdminGeneralDB.GetAllCurrency(selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsCurrency.Tables[0].Rows)
            {
                var csm = new CurrencySetupModel();
                csm.CurrencyId = Convert.ToInt32(dr["CCURRENCY_ID"]);
                csm.CurrencyCode = dr["CCURRENCY_CODE"].ToString();
                csm.CurrencyName = dr["CCURRENCY_NAME"].ToString();
                csm.CurrencySell = Convert.ToDouble(dr["CCURRENCY_SELL"]);
                csm.CurrencyBuy = Convert.ToDouble(dr["CCURRENCY_BUY"]);
                csm.Status = dr["CCURRENCY_DELETIONSTATE"].ToString();
                model.CurrencyList.Add(csm);
            }

            return PartialView("CurrencySetup", model);
        }

        public ActionResult CurrencySetupData(int CurrencyId, string CountryName)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var csm = new CurrencySetupModel();

            if (CurrencyId == 0)
            {
                //Create New province Setup
                csm.CurrencyId = null;
                csm.CurrencyCode = string.Empty;
                csm.CurrencyName = string.Empty;
                csm.CurrencyBuy = 0;
                csm.CurrencySell = 0;

                var countryList = Misc.GetCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);
                var defaultCountry = countryList.Count > 0 ? countryList.First().Text : string.Empty;
                var defaultCountryCode = countryList.Count > 0 ? countryList.First().Value : string.Empty;
                csm.SelectedCountryCode = defaultCountryCode;

                csm.CountryList = from c in countryList
                                  select new SelectListItem
                                  {
                                      Selected = c.Value == defaultCountry,
                                      Text = c.Text,
                                      Value = c.Value
                                  };
            }
            else
            {
                //View Existing Bank Setup
                DataSet dsCurrencySetup = AdminGeneralDB.GetCurrencyById(CurrencyId, out ok, out msg);
                if (dsCurrencySetup.Tables[0].Rows.Count != 0)
                {
                    csm.CurrencyId = Convert.ToInt32(dsCurrencySetup.Tables[0].Rows[0]["CCURRENCY_ID"]);
                    csm.CurrencyCode = dsCurrencySetup.Tables[0].Rows[0]["CCURRENCY_CODE"].ToString();
                    csm.CurrencyName = dsCurrencySetup.Tables[0].Rows[0]["CCURRENCY_NAME"].ToString();
                    csm.CurrencyBuy = Convert.ToDouble(dsCurrencySetup.Tables[0].Rows[0]["CCURRENCY_BUY"]);
                    csm.CurrencySell = Convert.ToDouble(dsCurrencySetup.Tables[0].Rows[0]["CCURRENCY_SELL"]);

                    csm.SelectedCountryCode = dsCurrencySetup.Tables[0].Rows[0]["CCOUNTRY_CODE"].ToString();

                    var countryList = Misc.GetCountryList(Session["LanguageChosen"].ToString(), ref ok, ref msg);
                    csm.CountryList = from c in countryList
                                      select new SelectListItem
                                      {
                                          Selected = c.Value == csm.SelectedCountryCode,
                                          Text = c.Text,
                                          Value = c.Value
                                      };
                }
            }

            return PartialView("CurrencySetupData", csm);
        }

        [HttpPost]
        public ActionResult CurrencySetupDataMethod(CurrencySetupModel csm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (csm.CurrencyCode == "" || csm.CurrencyCode == null)
                {
                    Response.Write(Resources.Admin.warningPleaseKeyInCurrencyCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (csm.CurrencyName == "" || csm.CurrencyName == null)
                {
                    Response.Write(Resources.Admin.warningPleaseKeyInCurrencyName);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (Convert.ToString(csm.CurrencyBuy) == "" || Convert.ToString(csm.CurrencyBuy) == null)
                {
                    Response.Write(Resources.Admin.warningPleaseKeyInCurrencyBuy);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (Convert.ToString(csm.CurrencySell) == "" || Convert.ToString(csm.CurrencySell) == null)
                {
                    Response.Write(Resources.Admin.warningPleaseKeyInCurrencySell);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                //Create New Record or Update Existing Record
                int ok;
                string msg;
                if (csm.CurrencyId == null || csm.CurrencyId == 0)
                {
                    //Create New Record                    
                    AdminGeneralDB.CreateNewCurrency(csm.CurrencyCode, csm.CurrencyName, csm.CurrencySell, csm.CurrencyBuy, csm.SelectedCountryCode, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgCurrencyCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    //Update Existing Record
                    AdminGeneralDB.UpdateCurrency(csm.CurrencyId, csm.CurrencyCode, csm.CurrencyName, csm.CurrencySell, csm.CurrencyBuy, csm.SelectedCountryCode, out ok, out msg);
                    if (ok == 0)
                    {
                        if (msg == "-1")
                        {
                            Response.Write(Resources.Admin.msgCurrencyCodeAlreadyExists);
                            return Json(string.Empty, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                return CurrencySetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult InactiveCurrency(int? CurrencyId)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete existing currency
                int ok;
                string msg;
                AdminGeneralDB.InActiveCurrencyByID(CurrencyId, out ok, out msg);
                AdminGeneralDB.InsertAdminOperation(Session["Admin"].ToString(), "Inactive Currency", Convert.ToString(CurrencyId), 0);

                return CurrencySetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ActiveCurrency(int? CurrencyId)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                // Delete existing currency
                int ok;
                string msg;
                AdminGeneralDB.ActiveCurrencyByID(CurrencyId, out ok, out msg);
                AdminGeneralDB.InsertAdminOperation(Session["Admin"].ToString(), "Active Currency", Convert.ToString(CurrencyId), 0);

                return CurrencySetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Reset Admin Password

        public ActionResult ResetAdminPassword()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            AdminPasswordModel model = new AdminPasswordModel();
            model.Username = Session["Admin"].ToString();
            return PartialView("ResetAdminPassword", model);
        }

        [HttpPost]
        public ActionResult ResetAdminPasswordMethod(AdminPasswordModel apm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok;
                string msg;
                AdminGeneralDB.UpdateAdminPassword(apm.Username, Authentication.Encrypt(apm.NewPassword), out ok, out msg);
                if (ok == 0)
                {
                    if (msg == "-1")
                    {
                        Response.Write(ECFBase.Resources.Admin.lblUserNotExist);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }
                return ResetAdminPassword();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region General Settings

        public ActionResult GeneralSettings()
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            ParameterModel pm = new ParameterModel();

            int ok;
            string msg;
            DataSet dsParameter = ParameterDB.GetAllParameter(out ok, out msg);
            foreach (DataRow dr in dsParameter.Tables[0].Rows)
            {
                var gs = new GeneralSettingModel();
                gs.ParameterName = dr["CPARA_NAME"].ToString();
                gs.ParameterDescription = Misc.GetTranslatedGeneralSetting(dr["CPARA_NAME"].ToString());
                gs.ParameterUOM = Misc.GetTranslatedGeneralSetting(dr["CPARA_UOM"].ToString());
                gs.ParameterStringVal = dr["CPARA_STRINGVALUE"].ToString();
                gs.ParameterFloatVal = Convert.ToSingle(dr["CPARA_FLOATVALUE"]);
                pm.SettingList.Add(gs);
            }

            return PartialView("GeneralSettings", pm);

        }

        [HttpPost]
        public ActionResult GeneralSettingsMethod(ParameterModel pm)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok;
                string msg;
                for (int i = 0; i < pm.SettingList.Count; i++)
                {
                    if (pm.SettingList[i].ParameterName == "paraNameTableRows" && pm.SettingList[i].ParameterFloatVal == 0)

                    {
                        Response.Write(Resources.Admin.WarningTableRow);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    ParameterDB.UpdateParameter(pm.SettingList[i].ParameterName, "", pm.SettingList[i].ParameterFloatVal, Session["Admin"].ToString(), out ok, out msg);
                }
                return GeneralSettings();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region SalesDeliveryMode
        public ActionResult SalesDeliveryMode(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int pages = 0;
            PaginationSalesDeliveryModel model = new PaginationSalesDeliveryModel();
            DataSet dataset = AdminGeneralDB.GetAllSalesDeliveryMode(selectedPage, out pages);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dataset.Tables[0].Rows)
            {
                var salesModel = new SalesDeliveryModel();
                salesModel.SalesDeliveryID = Convert.ToInt32(dr["CDELIMD_ID"]);
                salesModel.DeliveryMethod = dr["CDELIMD_METHOD"].ToString();
                salesModel.Number = dr["rownumber"].ToString();
                model.SalesDeliveryList.Add(salesModel);
            }

            return PartialView("SalesDeliveryMode", model);
        }

        public ActionResult SalesDeliveryModeData(int DeliveryID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var model = new SalesDeliveryModel();

            if (DeliveryID == 0)
            {
                //Create New Category Setup
                model.DeliveryMethod = "";
            }
            else
            {
                //View Existing Category Setup
                DataSet dataset = AdminGeneralDB.GetSalesDeliveryModeByID(DeliveryID, out ok, out msg);
                if (dataset.Tables[0].Rows.Count != 0)
                {
                    model.SalesDeliveryID = Convert.ToInt32(dataset.Tables[0].Rows[0]["CDELIMD_ID"]);
                    model.DeliveryMethod = dataset.Tables[0].Rows[0]["CDELIMD_METHOD"].ToString();
                }
            }

            return PartialView("SalesDeliveryModeData", model);
        }

        [HttpPost]
        public ActionResult SalesDeliveryModeDataMethod(SalesDeliveryModel model)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (model.SalesDeliveryID == 0)
                {
                    //Create New Record
                    AdminGeneralDB.CreateSalesDeliveryMode(model.DeliveryMethod, Session["Admin"].ToString());
                }
                else
                {
                    //Update Existing Record
                    AdminGeneralDB.UpdateSalesDeliveryMode(model.SalesDeliveryID, model.DeliveryMethod, Session["Admin"].ToString());
                }
                //refresh
                return SalesDeliveryMode();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteSalesDeliveryMode(int Mode)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                AdminGeneralDB.DeleteSalesDeliveryMode(Mode, Session["Admin"].ToString());

                return SalesDeliveryMode();
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region AdminList

        public ActionResult AdminList(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var model = RetrieveAdminList(selectedPage);
            return PartialView("AdminList", model);
        }

        [HttpPost]
        public ActionResult DeleteAdminList(string Username = null)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok;
                string msg;
                AdminGeneralDB.DeleteAdmin(Username, Session["Admin"].ToString(), out ok, out msg);

                return AdminList();
            }
            catch (Exception e)
            {

                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ModalEditAdminListData(string Username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var am = new AdminModel();
            int ok;
            string msg;

            if (Username == null || Username == "")
            {
                //Create New Admin
                am.UserID = 0;
                am.AdminUserName = "";
                am.AdminPassword = "";
                am.AdminPIN = "";
                am.AdminFirstName = "";
            }
            else
            {
                //View Existing Admin
                DataSet dsAdmin = AdminGeneralDB.GetAdminByUsername(Username, out ok, out msg);
                if (dsAdmin.Tables[0].Rows.Count != 0)
                {
                    am.UserID = Convert.ToInt32(dsAdmin.Tables[0].Rows[0]["CUSR_ID"]);
                    am.AdminUserName = dsAdmin.Tables[0].Rows[0]["CUSR_USERNAME"].ToString();
                    am.AdminChangePassword = false;
                    am.AdminPassword = Authentication.Decrypt(dsAdmin.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString());
                    am.AdminNewPassword = "";
                    am.AdminConfirmNewPassword = "";
                    am.AdminPIN = Authentication.Decrypt(dsAdmin.Tables[0].Rows[0]["CUSR_PIN"].ToString());
                    am.AdminNewPIN = "";
                    am.AdminConfirmNewPIN = "";
                    am.AdminFirstName = dsAdmin.Tables[0].Rows[0]["CUSR_FULLNAME"].ToString();
                }


            }

            return PartialView("ModalEditAdminListData", am);
        }

        [HttpPost]
        public ActionResult EditAdminListFromDialog(AdminModel am)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (am.AdminUserName == "" || am.AdminUserName == null)
                {
                    Response.Write("Please Key in Username");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                int ok;
                string msg;

                if (am.UserID == 0)
                {

                    //Create New Record
                    AdminGeneralDB.CreateNewAdmin(am.AdminUserName, Authentication.Encrypt(am.AdminPassword), Authentication.Encrypt(am.AdminPIN), am.AdminFirstName, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);
                }
                else
                {
                    //Update Existing Record
                    if (am.AdminNewPassword != am.AdminConfirmNewPassword)
                    {
                        Response.Write(Resources.Admin.msgPwdNotMatch);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }

                    if (am.AdminNewPIN != am.AdminConfirmNewPIN)
                    {
                        Response.Write(Resources.Admin.msgPINNotMatch);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }



                    AdminGeneralDB.UpdateAdmin(am.AdminUserName, am.AdminChangePassword, am.AdminCurrentPassword, am.AdminNewPassword, am.AdminChangePIN, am.AdminCurrentPIN, am.AdminNewPIN, am.AdminFirstName, Session["Admin"].ToString(), out ok, out msg);
                    if (ok == -1)
                    {
                        Response.Write(Resources.Admin.msgInvalidCurrPwd);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                    else if (ok == -2)
                    {
                        Response.Write(Resources.Admin.msgInvalidCurrPIN);
                        return Json(string.Empty, JsonRequestBehavior.AllowGet);
                    }
                }

                return AdminList();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region AdminPermission

        public ActionResult AdminPermissionList(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            var model = RetrieveAdminList(selectedPage);
            return PartialView("AdminPermissionList", model);
        }

        public ActionResult AdminPermissionData(string username)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            //Access Right
            var adminModel = new AdminModel();
            adminModel.AdminUserName = username;

            int ok;
            int userRightOk;
            string msg;
            string userAccessRight = "";

            DataSet dsUAR = AdminGeneralDB.GetAdminAccessRight(username, out userRightOk, out msg);
            userAccessRight = dsUAR.Tables[0].Rows[0][0].ToString();

            var resultList = new List<AdminAccessRightModel>();
            DataSet dsAccessRight = AdminGeneralDB.GetModulesAccessRight(out ok, out msg);
            foreach (DataRow dr in dsAccessRight.Tables[0].Rows)
            {
                var acrm = new AdminAccessRightModel();
                acrm.MainModule = Modules.GetModuleName(dr["CMDL_MAIN_MODULE"].ToString());
                acrm.Module = Modules.GetSubModuleName(dr["CMDL_NAME"].ToString());
                acrm.FunctionCode = dr["CAR_CODE"].ToString();
                acrm.FunctionName = Modules.GetSubModuleMethod(dr["CAR_CODE"].ToString());
                acrm.Function = userRightOk == -1 ? true : userAccessRight.Contains(acrm.FunctionCode);

                resultList.Add(acrm);
            }

            adminModel.AdminAccessRight = resultList;
            return PartialView("AdminPermissionData", adminModel);
        }

        [HttpPost]
        public ActionResult EditAdminPermissionMethod(string data, string userId)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                int ok;
                string msg;
                string[] parameters = data.Split(new string[] { "&" }, StringSplitOptions.None);

                string AccessRight = "";
                foreach (string parameter in parameters)
                {
                    string[] value = parameter.Split(new string[] { "=" }, StringSplitOptions.None);
                    if (value[0] != "username" && value[0].Substring(0, 5) != "Admin" && value[0] != "item.Function")
                    {
                        AccessRight = AccessRight + value[0] + ":1;";
                    }
                }

                AccessRight = AccessRight.Replace("%26", "&");
                AccessRight = AccessRight.Substring(0, AccessRight.Length - 1);
                AdminGeneralDB.UpdateAdminAccessRight(userId, AccessRight, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);

                return AdminPermissionList();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region private

        private PaginationAdminModel RetrieveAdminList(int selectedPage = 1)
        {
            int ok;
            string msg;
            int pages = 0;
            PaginationAdminModel model = new PaginationAdminModel();
            DataSet dsAdmin = AdminGeneralDB.GetAllAdmin(selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            //if the selected page is -1, then set the last selected page
            if (model.Pages.Count() != 0 && selectedPage == -1)
            {
                model.Pages.Last().Selected = true;
                selectedPage = int.Parse(model.Pages.Last().Value);
            }

            foreach (DataRow dr in dsAdmin.Tables[0].Rows)
            {
                var am = new AdminModel();
                am.AdminUserName = dr["CUSR_USERNAME"].ToString();
                am.AdminFirstName = dr["CUSR_FULLNAME"].ToString();
                model.AdminList.Add(am);
            }

            return model;
        }

        #region ConstructPageList
        private int ConstructPageList(int selectedPage, int pages, PaginationInvoiceListingModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }

        private int ConstructPageList(int selectedPage, int pages, PaginationStockAdjustmentLogModel model)
        {
            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            return selectedPage;
        }
        #endregion

        #endregion

        #region RankSetup
        public ActionResult RankSetup(int selectedPage = 1)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok;
            string msg;
            int pages = 0;
            PaginationRankSetupModel model = new PaginationRankSetupModel();
            DataSet dsRank = AdminGeneralDB.GetAllRankSetup(selectedPage, out pages, out ok, out msg);

            List<int> pageList = new List<int>();
            for (int z = 1; z <= pages; z++)
            {
                pageList.Add(z);
            }

            model.Pages = from c in pageList
                          select new SelectListItem
                          {
                              Selected = (c.ToString() == selectedPage.ToString()),
                              Text = c.ToString(),
                              Value = c.ToString()
                          };

            foreach (DataRow dr in dsRank.Tables[0].Rows)
            {
                var csm = new RankModel();
                csm.RankID = int.Parse(dr["CRANKSET_ID"].ToString());
                csm.RankCode = dr["CRANKSET_CODE"].ToString();
                csm.RankName = dr["CRANKSET_NAME"].ToString();
                csm.Hash = float.Parse(dr["CRANKSET_HASH"].ToString()).ToString("n2");
                csm.PerformanceHash = float.Parse(dr["CRANKSET_PERFORMANCE_HASH"].ToString()).ToString("n2");
                csm.ManagementHash = float.Parse(dr["CRANKSET_MANAGEMENT_HASH"].ToString()).ToString("n2");
                if (dr["CRANKSET_ICON"].ToString() != "")
                {
                    csm.RankIconPath = dr["CRANKSET_ICON"].ToString();
                    int extensionIndex = dr["CRANKSET_ICON"].ToString().LastIndexOf('\\');
                    csm.RankIconName = dr["CRANKSET_ICON"].ToString().Substring(extensionIndex + 1);
                    ViewBag.ImageIcon = dr["CRANKSET_ICON"].ToString();
                }
                else
                {
                    csm.RankIconName = "";
                    csm.RankIconPath = "";
                }
                model.CategoryList.Add(csm);
            }

            return PartialView("RankSetup", model);
        }

        public ActionResult RankSetupData(int rankID)
        {
            if (Session["Admin"] == null || Session["Admin"].ToString() == "")
            {
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
            }

            int ok = 0;
            string msg = "";
            var csm = new RankModel();

            if (rankID == 0)
            {

                //Create New Category Setup
                csm.RankCode = "";
                ViewBag.Title = Resources.Admin.lblCreateNewRank;
            }
            else
            {
                //View Existing Category Setup
                DataSet dsCategorySetup = AdminGeneralDB.GetRankSettingpByID(rankID, out ok, out msg);
                if (dsCategorySetup.Tables[0].Rows.Count != 0)
                {
                    csm.RankID = int.Parse(dsCategorySetup.Tables[0].Rows[0]["CRANKSET_ID"].ToString());
                    csm.RankCode = dsCategorySetup.Tables[0].Rows[0]["CRANKSET_CODE"].ToString();
                    csm.RankName = dsCategorySetup.Tables[0].Rows[0]["CRANKSET_NAME"].ToString();
                    csm.Hash = float.Parse(dsCategorySetup.Tables[0].Rows[0]["CRANKSET_HASH"].ToString()).ToString("n2");
                    csm.PerformanceHash = float.Parse(dsCategorySetup.Tables[0].Rows[0]["CRANKSET_PERFORMANCE_HASH"].ToString()).ToString("n2");
                    csm.ManagementHash = float.Parse(dsCategorySetup.Tables[0].Rows[0]["CRANKSET_MANAGEMENT_HASH"].ToString()).ToString("n2");

                    if (dsCategorySetup.Tables[0].Rows[0]["CRANKSET_ICON"].ToString() != "")
                    {
                        csm.RankIconPath = dsCategorySetup.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
                        int extensionIndex = dsCategorySetup.Tables[0].Rows[0]["CRANKSET_ICON"].ToString().LastIndexOf('\\');
                        csm.RankIconName = dsCategorySetup.Tables[0].Rows[0]["CRANKSET_ICON"].ToString().Substring(extensionIndex + 1);
                        ViewBag.ImageIcon = dsCategorySetup.Tables[0].Rows[0]["CRANKSET_ICON"].ToString();
                    }
                    else
                    {
                        csm.RankIconName = "";
                        csm.RankIconPath = "";
                    }

                }
                ViewBag.Title = Resources.Admin.lblUpdateRank;
            }

            return PartialView("RankSetupData", csm);
        }

        [HttpPost]
        public ActionResult RankSetupDataMethod(RankModel rankMod)
        {
            try
            {
                if (Session["Admin"] == null || Session["Admin"].ToString() == "")
                {
                    return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });
                }

                if (string.IsNullOrEmpty(rankMod.RankCode))
                {
                    Response.Write(Resources.Admin.warningPleaseKeyInRankCode);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(rankMod.RankName))
                {
                    Response.Write(Resources.Admin.warningPleaseKeyInRankName);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(rankMod.Hash))
                {
                    Response.Write("Please Key In Hash Value");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(rankMod.PerformanceHash))
                {
                    Response.Write("Please Key In Performance Hash Value");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(rankMod.ManagementHash))
                {
                    Response.Write("Please Key In Management Hash Value");
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }


                if (rankMod.RankCode != null || rankMod.RankCode != "")
                {
                    if (rankMod.RankID == null || rankMod.RankID == 0)
                    {
                        //Create New Record
                        int ok;
                        string msg;


                        rankMod.RankIcon = Request.Files["RankIcon"];
                        if (rankMod.RankIcon != null && rankMod.RankIcon.FileName != string.Empty)
                        {
                            string basePath = Server.MapPath("~/Images/Rank/");
                            if (Directory.Exists(basePath) == false)
                            {
                                Directory.CreateDirectory(basePath);
                            }
                            var filename = Path.GetFileName(rankMod.RankIcon.FileName);

                            if (rankMod.RankIcon.FileName.Contains(" "))
                            {
                                Response.Write(Resources.Admin.warningImgNameWOSpace);
                                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                            }
                            rankMod.RankIconPath = basePath + "\\" + filename;
                            rankMod.RankIcon.SaveAs(rankMod.RankIconPath);
                            rankMod.RankIconPath = "../../Images/Rank/" + filename;
                        }
                        else
                        {

                            rankMod.RankIconPath = "NA";

                        }


                        if (rankMod.RankCode == null || rankMod.RankCode == "")
                        {
                            rankMod.RankCode = "";
                        }
                        if (rankMod.RankName == null || rankMod.RankName == "")
                        {
                            rankMod.RankName = "";
                        }


                        AdminGeneralDB.CreateNewRank(rankMod, Session["Admin"].ToString(), Session["Admin"].ToString(), out ok, out msg);

                        if (ok == 0)
                        {
                            if (msg == "-1")
                            {
                                Response.Write(Resources.Admin.msgDuplicateRankCode);
                                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                            }
                        }

                    }
                    else
                    {
                        //Update Existing Record
                        int ok;
                        string msg;

                        rankMod.RankIcon = Request.Files["RankIcon"];
                        if (rankMod.RankIcon != null && rankMod.RankIcon.FileName != string.Empty)
                        {
                            string basePath = Server.MapPath("~/Images/Rank/");
                            if (Directory.Exists(basePath) == false)
                            {
                                Directory.CreateDirectory(basePath);
                            }
                            var filename = Path.GetFileName(rankMod.RankIcon.FileName);
                            rankMod.RankIconPath = basePath + "\\" + filename;
                            rankMod.RankIcon.SaveAs(rankMod.RankIconPath);
                            rankMod.RankIconPath = "../../Images/Rank/" + filename;
                        }

                        AdminGeneralDB.UpdateRank(rankMod, out ok, out msg);

                        if (ok == 0)
                        {
                            if (msg == "-1")
                            {
                                Response.Write(Resources.Admin.msgRankNotExist);
                                return Json(string.Empty, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return RankSetup();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}
