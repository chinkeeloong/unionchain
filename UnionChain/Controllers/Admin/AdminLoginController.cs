﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECFBase.Helpers;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.SessionState;
using System.Web.UI.HtmlControls;
using System.IO;
using ECFBase.Components;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.Web.Security;

namespace ECFBase.Controllers.Admin
{
    public class AdminLoginController : Controller
    {
        public ActionResult CaptchaImage(string prefix, bool noisy = true)
        {
            Session["CaptchaImageText"] = LoginDB.GenerateRandomCode();
            CaptchaImage ci = new CaptchaImage(Session["CaptchaImageText"].ToString(), 200, 50, "Century Schoolbook");

            FileContentResult img = null;
            var mem = new MemoryStream();

            ci.Image.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
            img = this.File(mem.GetBuffer(), "image/Jpeg");

            return img;
        }

        [HttpPost]
        public ActionResult ValidateLogin(string userName, string password, string captcha)
        {
            try
            {
                if (captcha != Session["CaptchaImageText"].ToString())
                {
                    Response.Write(Resources.Admin.msgInvalidCaptcha);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                DataSet dsUserAdmin = LoginDB.GetAdminInfoByUserName(userName);

                if (dsUserAdmin.Tables[0].Rows.Count == 0)
                {
                    Response.Write(Resources.Admin.msgInvalidLogin);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string userPassword = dsUserAdmin.Tables[0].Rows[0]["CUSR_PASSWORD"].ToString();
                if (Authentication.Encrypt(password) != userPassword)
                {

                    Response.Write(Resources.Admin.msgInvalidLogin);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                if (dsUserAdmin.Tables[0].Rows[0]["CUSR_ACTIVE"].ToString() == "False")
                {

                    Response.Write(Resources.Admin.msgAccountInactive);
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                string Language = Session["LanguageChosen"].ToString();

                //Login Successful. Create all the Session value.
                Session.RemoveAll();

                //TODO: When open any module, to check if this session is null, if null, redirect to login page.
                Session["Admin"] = userName;

                //TODO: Temporary set default language to English.
                Session["LanguageChosen"] = Language;
                int ok;
                string msg;
                string userAccessRight = "";
                DataSet dsUAR = AdminGeneralDB.GetAdminAccessRight(userName, out ok, out msg);
                userAccessRight = dsUAR.Tables[0].Rows[0][0].ToString();
                string[] uar = userAccessRight.Split(new string[] { ", " }, StringSplitOptions.None);
                foreach (string accessRight in uar)
                {
                    Session[accessRight] = "true";
                }

                if (userName.ToLower() == "admin" || userName.ToLower() == "adm001")
                {
                    Session["GrantViewAccessRight"] = "true";
                }
                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", e.Message);
                return Json(new { success = false });
            }
        }

        [HttpPost]
        public ActionResult ChangeLanguage(string selectedLanguage)
        {
            try
            {
                //TODO: incorrect. unable to change language at runtime. currently set at Global.asax.cs
                if (selectedLanguage == "EN")
                {
                    Session["LanguageChosen"] = ProjectStaticString.DefaultLanguage;
                }
                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", e.Message);
                return Json(new { success = false });
            }
        }

        public ActionResult LogOff()
        {
            try
            {
                Session.RemoveAll();
                return RedirectToAction("AdminLogin", "Admin", new { reloadPage = true });

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", e.Message);
                return Json(new { success = false });
            }
        }

    }
}
