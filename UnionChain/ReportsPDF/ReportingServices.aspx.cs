﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.Reporting.WebForms;
using ECFBase.Components;

namespace ECFBase.Reports
{
    public partial class ReportingServices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["ReportID"] != null)
                {
                    int ok;
                    string msg;
                    int type;
                    string selectedMonth = "";
                    string selectedYear = "";
                    string reportID = Request.QueryString["ReportID"].ToString();
                    var pages = 0;
                    switch (reportID)
                    {
                        case "InvoiceReport":
                            
                            int InvoiceID = Convert.ToInt32(Request.QueryString["InvoiceID"].ToString());
                            ReportViewer1.LocalReport.DataSources.Clear();      
                   

                            DataSet dsInvoice = AdminSalesOrderDB.GetInvoiceByID(InvoiceID, out ok, out msg, out type);
                            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsInvoice", dsInvoice.Tables[0]));                            

                            DataSet dsInvProd = AdminSalesOrderDB.GetInvoiceProdByID(InvoiceID, out ok, out msg);
                            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsInvProd", dsInvProd.Tables[0]));

                            DataSet dsCompany = AdminGeneralDB.GetCompanySetup();
                            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsCompany", dsCompany.Tables[0]));

                            if (type == 0)
                            {
                                //int REPORT = 0;
     

                                    ReportViewer1.LocalReport.ReportPath = "ReportsPDF/Invoice.rdlc";
                                    
                                

                                


                                
                            }
                            else
                            {
                                DataSet dsShoppingInv = AdminSalesOrderDB.GetShoppingInvoiceById(InvoiceID, out ok, out msg);
                                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsShopProd", dsShoppingInv.Tables[0]));                       
                                ReportViewer1.LocalReport.ReportPath = "Reports/ShoppingInvoice.rdlc";
                            }

                            #region
                            //string invoicenumber = dsInvoice.Tables[0].Rows[0]["CINVNO_NO"].ToString();

                            //DataSet dsdoinvno = AdminSalesOrderDB.GetDObyIN(invoicenumber);
                            //if (dsdoinvno.Tables[0].Rows.Count == 0)
                            //{
                            //}
                            //else
                            //{
                            //    string DON = dsdoinvno.Tables[0].Rows[0]["CDLVORDER_NO"].ToString();
                            //    #region Testing
                            //    DataSet dsDOn = AdminSalesOrderDB.GetDOByDONO(DON, out ok, out msg, out type);
                            //    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsDO", dsDOn.Tables[0]));

                            //    DataSet dsDOProds = AdminSalesOrderDB.GetDOProdByDONO(DON, out ok, out msg);
                            //    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsDOProd", dsDOProds.Tables[0]));

                            //    string shipAddress = dsDOn.Tables[0].Rows[0]["CINVNO_ADDRESS"].ToString();

                            //    var reportParams = new ReportParameter[5];

                            //    if (!string.IsNullOrEmpty(shipAddress.Trim()))
                            //    {
                            //        var address = shipAddress.Split(new[] { '\r' });
                            //        for (int idx = 0; idx < address.Length; idx++)
                            //        {
                            //            if (idx < 4)
                            //            {
                            //                reportParams[idx] = new ReportParameter(string.Format("ParamAddress{0}", idx), address[idx], false);
                            //            }
                            //        }
                            //    }

                            //    //get total promo price
                            //    DataSet dsPackages = AdminGeneralDB.GetAllPackage("en-US", "O", 1, out pages, out ok, out msg);
                            //    float totals = 0;
                            //    foreach (DataRow dr in dsPackages.Tables[0].Rows)
                            //    {
                            //        foreach (DataRow pr in dsDOProds.Tables[0].Rows)
                            //        {
                            //            if (String.Compare(pr["CODE"].ToString(), dr["CPKG_CODE"].ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                            //            {
                            //                totals += (float.Parse(dr["CPKG_AMOUNT"].ToString()) * int.Parse(pr["CINVPRO_QUANTITY"].ToString()));
                            //            }
                            //        }
                            //    }

                            //    reportParams[4] = new ReportParameter("ParamPromotionTotal", totals.ToString(), false);

                            //    //Ensure all array of parameters element is not null, otherwise it will crash. So becareful here :)
                            //    for (int idx = 0; idx < reportParams.Length; idx++)
                            //    {
                            //        if (reportParams[idx] == null)
                            //        {
                            //            reportParams[idx] = new ReportParameter(string.Format("ParamAddress{0}", idx),
                            //                                                   string.Empty, false);
                            //        }
                            //    }

                            //    if (type == 0)
                            //    {
                            //        ReportViewer1.LocalReport.ReportPath = "Reports/DO.rdlc";
                            //    }
                            //    else
                            //    {
                            //        DataSet dsShoppingInv = AdminSalesOrderDB.GetShoppingInvoiceById(Convert.ToInt32(DON), out ok, out msg);
                            //        ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsShopProd", dsShoppingInv.Tables[0]));
                            //        ReportViewer1.LocalReport.ReportPath = "Reports/ShoppingDO.rdlc";
                            //    }
                            //    ReportViewer1.LocalReport.SetParameters(reportParams);
                            //    #endregion
                            //}
#endregion

                            //Shit, Print button only visible in IE. And its a ActiveX control. required to install.
                            ReportViewer1.ShowPrintButton = true;
                            ReportViewer1.DataBind();
                            ReportViewer1.LocalReport.Refresh();

                            byte[] bytes = ReportViewer1.LocalReport.Render("PDF");
                            //display pdf in browser
                            Response.AddHeader("Content-Disposition", "inline; filename=Invoice.pdf");
                            Response.ContentType = "application/pdf";
                            Response.BinaryWrite(bytes);
                            Response.End();

                            break;

                        case "DOReport":

                            string DONO = Request.QueryString["DONO"].ToString();                            
                            ReportViewer1.LocalReport.DataSources.Clear();

                            DataSet dsDO = AdminSalesOrderDB.GetDOByDONO(DONO, out ok, out msg, out type);
                            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsDO", dsDO.Tables[0]));

                            DataSet dsDOProd = AdminSalesOrderDB.GetDOProdByDONO(DONO, out ok, out msg);
                            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsDOProd", dsDOProd.Tables[0]));

                            string shippingAddress = dsDO.Tables[0].Rows[0]["CINVNO_ADDRESS"].ToString();

                            var reportParam = new ReportParameter[5];

                            if (!string.IsNullOrEmpty(shippingAddress.Trim()))
                            {
                                var address = shippingAddress.Split(new[] {'\r'});
                                for (int idx = 0; idx < address.Length; idx++)
                                {
                                    if (idx < 4)
                                    {
                                        reportParam[idx] = new ReportParameter(string.Format("ParamAddress{0}", idx), address[idx], false);
                                    }
                                }
                            }

                            //get total promo price
                            DataSet dsPackage = AdminGeneralDB.GetAllPackage("en-US", "O", 1, out pages, out ok, out msg);
                            float total = 0;
                            foreach (DataRow dr in dsPackage.Tables[0].Rows)
                            {
                                foreach (DataRow pr in dsDOProd.Tables[0].Rows)
                                {
                                    if (String.Compare(pr["CODE"].ToString(), dr["CPKG_CODE"].ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                                    {
                                        total += (float.Parse(dr["CPKG_AMOUNT"].ToString()) * int.Parse(pr["CINVPRO_QUANTITY"].ToString()));
                                    }
                                }
                            }

                            reportParam[4] = new ReportParameter("ParamPromotionTotal", total.ToString(), false);

                            //Ensure all array of parameters element is not null, otherwise it will crash. So becareful here :)
                            for (int idx = 0; idx < reportParam.Length; idx++)
                            {
                                if (reportParam[idx] == null)
                                {
                                    reportParam[idx] = new ReportParameter(string.Format("ParamAddress{0}", idx),
                                                                           string.Empty, false);
                                }
                            }

                            if (type == 0)
                            {
                                ReportViewer1.LocalReport.ReportPath = "Reports/DO.rdlc";
                            }
                            else
                            {
                                DataSet dsShoppingInv = AdminSalesOrderDB.GetShoppingInvoiceById(Convert.ToInt32(DONO), out ok, out msg);
                                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsShopProd", dsShoppingInv.Tables[0]));
                                ReportViewer1.LocalReport.ReportPath = "Reports/ShoppingDO.rdlc";
                            }
                            ReportViewer1.LocalReport.SetParameters(reportParam);

                            //Shit, Print button only visible in IE. And its a ActiveX control. required to install.'

                            ReportViewer1.LocalReport.DisplayName = dsDO.Tables[0].Rows[0]["CDLVORDER_NO"].ToString(); //this displayname will reflect to export filename
                            ReportViewer1.ShowPrintButton = true;
                            ReportViewer1.DataBind();
                            ReportViewer1.LocalReport.Refresh();



                            break;
                            
                        case "SalesAnalysis":

                            if (Request.QueryString["month"] == null || Request.QueryString["month"].ToString() == Resources.Member.lblAll)
                                selectedMonth = "%";
                            else
                                selectedMonth = Request.QueryString["month"].ToString();

                            if (Request.QueryString["year"] == null || Request.QueryString["year"].ToString() == Resources.Member.lblAll)
                                selectedYear = "%";
                            else
                                selectedYear = Request.QueryString["year"].ToString();

                            DataSet dsSA = AdminSalesOrderDB.GetSalesAnalysis(selectedMonth, selectedYear, out ok, out msg);
                            ReportViewer1.LocalReport.ReportPath = "Reports/SalesAnalysis.rdlc";
                            ReportViewer1.LocalReport.DataSources.Clear();
                            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsSalesAnalysis", dsSA.Tables[0]));
                            //Shit, Print button only visible in IE. And its a ActiveX control. required to install.
                            ReportViewer1.ShowPrintButton = true;
                            ReportViewer1.DataBind();
                            ReportViewer1.LocalReport.Refresh();
                            break;

                        case "MembershipAnalysis":

                            if (Request.QueryString["month"] == null || Request.QueryString["month"].ToString() == Resources.Member.lblAll)
                                selectedMonth = "%";
                            else
                                selectedMonth = Request.QueryString["month"].ToString();

                            if (Request.QueryString["year"] == null || Request.QueryString["year"].ToString() == Resources.Member.lblAll)
                                selectedYear = "%";
                            else
                                selectedYear = Request.QueryString["year"].ToString();

                            DataSet dsMA = AdminSalesOrderDB.GetMembershipAnalysis(selectedMonth, selectedYear, out ok, out msg);
                            ReportViewer1.LocalReport.ReportPath = "Reports/MembershipAnalysis.rdlc";
                            ReportViewer1.LocalReport.DataSources.Clear();
                            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsMembershipAnalysis", dsMA.Tables[0]));
                            //Shit, Print button only visible in IE. And its a ActiveX control. required to install.
                            ReportViewer1.ShowPrintButton = true;
                            ReportViewer1.DataBind();
                            ReportViewer1.LocalReport.Refresh();
                            break;

                        default:
                            break;

                    }
                }
            }
        }

    }
}