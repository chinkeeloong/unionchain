﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportingServices.aspx.cs" Inherits="ECFBase.Reports.ReportingServices" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" style="width:100%; height:850px; text-align:center" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="width:740px; margin:auto; border-style:double">
            <rsweb:ReportViewer ID="ReportViewer1" ShowPrintButton="true" Width="740px" Height="850px" runat="server"></rsweb:ReportViewer>
        </div>
    </form>
</body>
</html>
